var menus = {
    open_firstmenu: false,
    datas: null,
    sources: null,
    menuid: 0,
    menuname: null,
    menupossition: null,
    button_next: "next",
    button_prev: "previous",
    button_complete: "finish",
    wizard: null,
    postaction: "save",
    item_id: null,
    plugin_id: null,
    init: function(properties){
        menus.open_firstmenu = properties.open_firstmenu;
        menus.button_next = properties.button_next;
        menus.button_prev = properties.button_prev;
        menus.button_complete = properties.button_complete;
        menus.complete_wizard_msg = properties.complete_wizard_msg;

        //bootstrap plugins
        $('#modal').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });

        $('#modal-edit').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });     

        $('#dialog-delete-confirm').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });   

                        
        $('#dialog-delete-confirm-item').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });

        menus.setToolTips();
        //clone the wizard and store it in a var
        menus.wizard = $('#wizard').clone(true);
        //by default wizzard is hidden
        $("#wizzard_div").hide();
                
    },
    setMenu: function(menuid , menuname){
        $("#wizzard_div").hide();
        menus.menuid = menuid;
        menus.menuname = menuname;
        
        $('#menu_container_box').html('');
        var menu_str = '<div class="span4">'+
                            '<div class="todo">'+
                                '<h4>'+menuname+' <a class="icon tip" href="#" id="add-new-content" oldtitle="Add menu" title=""><span class="icon16 icomoon-icon-plus-2"></span></a></h4>'+
                                '<div id="jqxWidget"></div>'+
                            '</div>'+    
                        '</div>';

        $('#menu_container_box').html(menu_str);  

        if(menuid > 0){
            menus.setData(function(status) { 
                menus.prepearData();
                menus.createMenu();
            });
        }
    },
    setData: function(callback){
        menus.ajax_req('menuid='+menus.menuid , 'menus/getmenubyid' , function(res) {  
            if(res.success == 'success'){
                menus.datas = res.responsedata;
                callback(true);
            }else{
                callback(false);
            }    
        });        
    },
    prepearData: function(){
        menus.sources =
        {
            datatype: "json",
            datafields: [
                { name: 'id' },
                { name: 'parrent_id' },
                { name: 'title' }
            ],
            id: 'id',
            localdata: menus.datas
        };
    },
    createMenu: function(){
        var theme = getTheme();
        var dataAdapter = new $.jqx.dataAdapter(menus.sources);
        dataAdapter.dataBind();
        var records = dataAdapter.getRecordsHierarchy('id', 'parrent_id', 'items', [{ name: 'title', map: 'label'}]);
        $('#jqxWidget').jqxTree({ source: records, width: '365px', theme: theme });
    },
    addmenu: function(){
        $('#modal').dialog('open');
    },
    editmenu: function(id){
        menus.menuid = id;
        $('#modal-edit .menu_name').val('');
        $('#modal-edit .possition_name').val('');

        menus.ajax_req('menu_id='+menus.menuid, 'menus/getmenudata' , function(res) {  
            if(res.success == 'success'){
                $('#modal-edit .menu_name').val(res.responsedata[0].menu_name);
                $('#modal-edit .possition_name').val(res.responsedata[0].menu_position_name);
                $('#modal-edit').dialog('open');
            }else{
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
            }
        });        
    },
    updatemenu: function(){
        var curr_menu_name = $('#modal-edit .menu_name').val();
        var curr_possition_name = $('#modal-edit .possition_name').val();
        menus.ajax_req('menu_id='+menus.menuid+'&menu_name='+curr_menu_name+'&possition_name='+curr_possition_name, 'menus/updatemenu' , function(res) {  
            if(res.success == 'success'){
                $.pnotify({
                    type: 'success',
                    title: 'Success',
                    text: res.msg,
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                $("#menu_list li#"+menus.menuid+" span.getMenuData").html(curr_menu_name);
            }else{
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
            }
            $('#modal-edit').dialog('close');
        });        
    },
    deleteconfirm: function(id){
        $('#dialog-delete-confirm').dialog('open');
        menus.menuid = id;
    },
    deleteItemConfirm: function(){
        $('#dialog-delete-confirm-item').dialog('open');
    },    
    deleteMenu: function(){
        menus.ajax_req('menu_id='+menus.menuid, 'menus/deletemenu' , function(res) {  
            if(res.success == 'success'){
                $.pnotify({
                    type: 'success',
                    title: 'Success',
                    text: res.msg,
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                $("#menu_list #"+menus.menuid).replaceWith('');
                $('#menu_container_box').html(''); 
                $('#dialog-delete-confirm').dialog('close');
            }else{
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                $('#dialog-delete-confirm').dialog('close');
            }
        });        
    },
    deleteItem: function(){
        console.log(menus.item_id);
        menus.ajax_req('item_id='+menus.item_id, 'menus/deleteitem' , function(res) {  
            if(res.success == 'success'){
                $.pnotify({
                    type: 'success',
                    title: 'Success',
                    text: res.msg,
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                
                $("#wizzard_div").hide();
                menus.setMenu(menus.menuid , menus.menuname); 
                $('#dialog-delete-confirm-item').dialog('close');
            }else{
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                $('#dialog-delete-confirm-item').dialog('close');
            }
        });         
    },
    savemenu: function(data){
        menus.ajax_req(data, 'menus/addmenu' , function(res) {  
            if(res.success == 'success'){
                $('#modal').dialog('close');
                var menu_row_str = '<li class="clearfix" id="'+res.responsedata.lastid+'">'+
                                        '<div class="txt">'+
                                           '<span class="getMenuData" style="cursor:pointer;" rel="'+res.responsedata.lastid+'"> '+res.responsedata.menu_name+' </span>'+
                                        '</div>'+
                                        '<div class="controls">'+
                                            '<a class="tip edit" href="#" oldtitle="Edit Menu" title="" id="'+res.responsedata.lastid+'"><span class="icon12 icomoon-icon-pencil"></span></a>'+
                                            '<a class="tip delete" href="#" oldtitle="Remove Menu" title="" id="'+res.responsedata.lastid+'"><span class="icon12 icomoon-icon-remove"></span></a>'+
                                        '</div>'+
                                    '</li>';
                $("#menu_list").append(menu_row_str);
                menus.setToolTips();
                
                $.pnotify({
                    type: 'success',
                    title: 'Success',
                    text: res.msg,
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                
            }else{
                //error notification display
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });

                $('#modal').dialog('close');
            }    
        });   
    },
    setToolTips: function(){
        $('.tip').qtip({
            content: {
                attr: "oldtitle"
            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window)
            },
            style: {
                classes: 'ui-tooltip-tipsy'
            }
        });
    },
    init_wizzard: function(){
        $("#wizard").html(menus.wizard.html());
        $("#wizzard_div").show();     
        $('#wizard').smartWizard({
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/
            onLeaveStep:menus.leaveAStepCallback,
            onFinish:menus.onFinishCallback,
            labelNext: menus.button_next,
            labelPrevious: menus.button_prev,
            labelFinish: menus.button_complete
        });
    },
    editAction: function(){
        //get info for the itemid menus.item_id
        menus.ajax_req('itemid='+menus.item_id+'&menuid='+menus.menuid , 'menus/getmenuplugindata' , function(res) {
            var plugin_id = res.responsedata.menu_results[0]['type'];
            var item_data = res.responsedata;
                dynaforms.ajax_req("plugin="+plugin_id , "plugin" , true , function(res) {
                    var form = res['form_properties'];

                    var fieldset = { fieldset : form.fieldset };

                    multiclone.tabcreation("#plugin_form" , 'tab2', function() {
                        dynaforms.setForm(form.formspan , form.formid , 1);
                        dynaforms.createForm(fieldset , '#tab2 #'+multiclone.available_languages[0]);
                    });


                    menus.plugin_id = plugin_id;


                    var counter = item_data.menu_results.length;
                    
                    if(counter > 1){
                        $.each(item_data.menu_results, function(key, value){
                            if(multiclone.available_languages[0] != value['la_shortcut']){
                                multiclone.cloneTabContent(value['la_shortcut'] , 'tab1');
                                multiclone.cloneTabContent(value['la_shortcut'] , 'tab2');
                            }
                        });
                    }
                 
                    //Set basic Elements of menu item
                    dynaforms.setValueToField("#item_settings", "parrent_id" , item_data.menu_results[0]['parrent_id']);
                    dynaforms.setValueToField("#item_settings", "template_view_id" , item_data.menu_results[0]['template_view_id']);
                    dynaforms.setValueToField("#item_settings","order" , item_data.menu_results[0]['order']);
                    dynaforms.setValueToField("#item_settings","active" , item_data.menu_results[0]['active']);
                    dynaforms.setValueToField("#item_settings","register_users_only" , item_data.menu_results[0]['register_users_only']);
                    
                  

                    $.each(item_data.menu_results, function(seo_key, seo_value){
                        if(seo_key > 0){
                            var la = "_"+seo_value.la_shortcut;
                        }else{
                            var la = "";
                        }    
                        $.each(seo_value, function(key, value){  
                          dynaforms.setValueToField("#tab1", key+la , value);
                        });
                    });    

                    $.each(item_data.plugin_results, function(seo_key, seo_value){
                        if(seo_key > 0){
                            var la = "_"+seo_value.la_shortcut;
                        }else{
                            var la = "";
                        }    
                        $.each(seo_value, function(key, value){  
                          dynaforms.setValueToField("#tab2", key+la , value);
                        });
                    });

                    //add delete button to component
                    var del_button = '<button class="btn btn-danger" onclick="menus.deleteItemConfirm();" style="margin-top:4px; margin-left:4px;">delete</button>';
                    $(".actionBar").append(del_button);

                });

        });    
    },
    leaveAStepCallback: function(){
        var return_result = false;
        dynaforms.checkValid(function(result) {
            if(result){
                return_result = true;
            }
        });  
        return return_result;
    },
    onFinishCallback: function(){
        dynaforms.getFormPostData(function(res) { 
            if(menus.postaction == "save"){
                var extraparams = "&menuid="+menus.menuid;
                menus.ajax_req(res+extraparams , 'plugin/saveplugindata' , function(res) {
                    if(res.success != "failed"){
                        $.pnotify({
                            type: 'success',
                            title: 'Done',
                            text: "No errors found",
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });

                        $("#wizzard_div").hide();
                        menus.setMenu(menus.menuid , menus.menuname);
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: res.error_msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });
            }else{
                var extraparams = "&menuid="+menus.menuid+"&menu_item_id="+menus.item_id+'&plugin_id='+menus.plugin_id;
                menus.ajax_req(res+extraparams , 'plugin/updateplugindata' , function(res) {
                    if(res.success != "failed"){
                        $.pnotify({
                            type: 'success',
                            title: 'Done',
                            text: "No errors found",
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });

                        $("#wizzard_div").hide();
                        menus.setMenu(menus.menuid , menus.menuname);
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: res.error_msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });

                //case of edit
                console.log(res);
                console.log(extraparams);
                console.log(menus.plugin_id);
                console.log('update item');
            }
        });    

    },
    creatBasicForm: function(){
        //set form for basic options
        var fieldset = {
                    fieldset:{
                        title: "Γενικές ρυθμίσεις εμφάνισης",
                        fields:[
                                {
                                    name: "category",
                                    label: "Επιλογή επιπέδου",
                                    fieldspan: "8",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "parrent_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "menus/getdropdownmenudata",
                                    datasource_params: {
                                            menuid: menus.menuid
                                        }
                                },
                                {
                                    name: "template_view_id",
                                    label: "Eικαστικό",
                                    fieldspan: "8",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "false",
                                    dbfield: "template_view_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "dropdown/getviews"
                                },
                                {
                                    name: "order",
                                    label: "Στοίχηση",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "order"
                                },
                                {
                                    name: "Active",
                                    label: "Ενεργό",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "active"
                                },
                                {
                                    name: "Registered",
                                    label: "Σε Σύνδεση",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "register_users_only"
                                }
                               ]
                        }
                    };

        dynaforms.setForm(12 , 'item_settings' , 0);
        dynaforms.createForm(fieldset , '#basic_section'); 
    },
    createTab1Form: function(){
        multiclone.tabcreation("div#multilanguage_div" , 'tab1', function() {
            //set title fields
            var fieldset = {
                            fieldset : {
                                fields:[
                                        {
                                            name: "Title",
                                            label: "Τίτλος",
                                            fieldspan: "8",
                                            labelspan: "4",
                                            type: "text",
                                            required: "true",
                                            dbfield: "title"
                                        },
                                        {
                                            name: "Seo",
                                            label: "friendly URL",
                                            fieldspan: "8",
                                            labelspan: "4",
                                            type: "text",
                                            required: true,
                                            validation : {
                                                rules: {
                                                    minlength: 5,
                                                    maxlenght: 15 
                                                }
                                            },
                                            dbfield: "url"
                                        }
                                       ]
                                }  
                            };              

            dynaforms.setForm(12 , 'item_seo' , 1);
            dynaforms.createForm(fieldset , '#'+multiclone.available_languages[0]);                             
        });
    },
    createPluginForm: function(){
        //set form for plugin options
        var fieldset = {
                    fieldset:{
                        fields:{
                                    name: "plugin",
                                    label: "Επιλογή plugin",
                                    fieldspan: "8",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "plugin_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "plugin/getplugins"
                                }
                        }
                    };

        dynaforms.setForm(12 , 'item_plugin' , 0);
        dynaforms.createForm(fieldset , '#basic_section2');
    },
    ajax_req: function(data , action  , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};

$(document).ready(function () { 

    $('body').delegate('.getMenuData', 'click', function() {
        var menu_id = $(this).attr('rel');
        var menu_name = $(this).html();
        menus.setMenu(menu_id , menu_name);
    });

    $('body').delegate('.delete', 'click', function() {
        var menu_id = $(this).attr('id');
        menus.deleteconfirm(menu_id); 
    });

    $('body').delegate('.edit', 'click', function() {
        var menu_id = $(this).attr('id');
        menus.editmenu(menu_id); 
    });

    $('body').delegate('#plugin_id', 'change', function() {
        var plugin_id = $(this).val();

        if(plugin_id != 0){
            dynaforms.ajax_req("plugin="+plugin_id , "plugin" , true , function(res) { 
                var form = res['form_properties'];

                var fieldset = { fieldset : form.fieldset };
                multiclone.tabcreation("#plugin_form" , 'tab2', function() {
                    dynaforms.setForm(form.formspan , form.formid , 1);
                    dynaforms.createForm(fieldset , '#tab2 #'+multiclone.available_languages[0]); 
                });
                $(".tags").select2(); 
            }); 
        }
         
    });

    $('body').delegate('.draggable', 'click', function() {
        var child_id = $(this).parent('li').attr('id');

        menus.postaction = "edit";
        menus.item_id = child_id;
        
        menus.init_wizzard();
        menus.creatBasicForm();
        menus.createTab1Form();

        menus.editAction();

        //console.log(menus);

        //dublicate tab in order to get languages for edit
        //multiclone.cloneTabContent('en' , 'tab1');  

    });

    $("#addmenu").click(function() {
        menus.addmenu();
    }); 

    $("#delete-menu").click(function() {
        menus.deleteMenu();
    }); 

    $("#delete-item").click(function() {
        menus.deleteItem();
    });          

    $("#savemenu").click(function() {
        var data = $("#addmenuform").serialize();
        menus.savemenu(data);
    }); 


    $('body').delegate('a#add-new-content', 'click', function() {   
        menus.postaction = "save";    
        menus.init_wizzard();
        menus.creatBasicForm();
        menus.createTab1Form();
        menus.createPluginForm();
    });

});
