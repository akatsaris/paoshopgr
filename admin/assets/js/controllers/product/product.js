var product = {
    button_next: "next",
    button_prev: "previous",
    button_complete: "finish",
    wizard: null,
    postaction: "save",
    item_id: null,
    attr_row: null,
    gall_row: null,
    acc_row: null,
    counter: 1,
    gall_counter: 1,
    acc_row_counter: 1,
    product_id: null,
    init: function(properties){
        product.open_firstmenu = properties.open_firstmenu;
        product.button_next = properties.button_next;
        product.button_prev = properties.button_prev;
        product.button_complete = properties.button_complete;
        product.complete_wizard_msg = properties.complete_wizard_msg;
        product.product_id = properties.product_id;

        product.setToolTips();
        //clone the wizard and store it in a var
        product.wizard = $('#wizard').clone(true);
                
    },
    setToolTips: function(){
        $('.tip').qtip({
            content: {
                attr: "oldtitle"
            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window)
            },
            style: {
                classes: 'ui-tooltip-tipsy'
            }
        });
    },
    init_wizzard: function(){
        $("#wizard").html(product.wizard.html());
        $("#wizzard_div").show();     
        $('#wizard').smartWizard({
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/
            onLeaveStep:product.leaveAStepCallback,
            onFinish:product.onFinishCallback,
            labelNext: product.button_next,
            labelPrevious: product.button_prev,
            labelFinish: product.button_complete
        });
    },
    leaveAStepCallback: function(){
        //var return_result = false;
        //dynaforms.checkValid(function(result) {
        //    if(result){
        //        return_result = true;
        //    }
        //});  
        //return return_result;
        return true;
    },
    onFinishCallback: function(){
        dynaforms.getFormPostData(function(res) {    
            if(product.postaction == "save"){
                product.ajax_req(res , 'product/save' , function(response) {
                    if(response.success == 'success'){
                        $.pnotify({
                            type: 'success',
                            title: 'Success',
                            text: response.msg,
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        window.location = "/admin/product";
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: response.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });    
            }else{
                var extraparams = "&product_id="+product.product_id;
                product.ajax_req(res+extraparams , 'product/update' , function(response) {
                    if(response.success == 'success'){
                        $.pnotify({
                            type: 'success',
                            title: 'Success',
                            text: response.msg,
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        window.location = "/admin/product";
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: response.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });
            }
        });    

    },
    creatBasicForm: function(){
        //basic_product_fields
        var fieldset = {
                    fieldset:{
                        fields:[{
                                    name: "home_section",
                                    label: "Εμφάνιση",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "home_section_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "dropdown/gethomesections"
                                },
                                {
                                            name: "Product Code",
                                            label: "Κωδικός προίον",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "text",
                                            required: "true",
                                            dbfield: "product_code"
                                },
                                {
                                    name: "template_view_id",
                                    label: "Eικαστικό",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "false",
                                    dbfield: "template_view_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "dropdown/getviews"
                                },
                                {
                                    name: "product_view",
                                    label: "Template προϊόντος",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "product_view_template",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "dropdown/getproductviews"
                                },
                                {
                                    name: "tags",
                                    label: "Επιλογή tags",
                                    fieldspan: "12",
                                    labelspan: "4",
                                    type: "tagsfield_ajax",
                                    required: "true",
                                    dbfield: "tag_id",
                                    datasource_action: "tagcloud/gettags"
                                },
                                {
                                    name: "Active",
                                    label: "Ενεργό",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "active"
                                },
                                {                                
                                    name: "Registered",
                                    label: "Εγγεγραμένοι χρήστες",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "registered_users_only"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'product_basic' , 0);
        dynaforms.createForm(fieldset , '#basic_product_fields');


        multiclone.tabcreation("div#multilanguage_div" , 'tab1', function() {
            //set title fields
            var fieldset = {
                            fieldset : {
                                fields:[
                                        {
                                            name: "Name",
                                            label: "Ονομασία",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "text",
                                            required: "true",
                                            dbfield: "name"
                                        },
                                        {
                                            name: "Description",
                                            label: "Περιγραφή",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "texteditor",
                                            required: "false",
                                            dbfield: "prd_description"
                                        },
                                        {
                                            name: "Content",
                                            label: "Πλήρης Περιγραφή",
                                            fieldspan: "6",
                                            labelspan: "4",
                                            type: "texteditor",
                                            required: "false",
                                            dbfield: "prd_content"
                                        },
                                        {
                                            name: "Keywords",
                                            label: "Λέξεις κλειδιά",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "textarea",
                                            required: "false",
                                            dbfield: "keywords"
                                        },
                                        {
                                            name: "photo",
                                            label: "Φωτογραφία",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "browse_file",
                                            required: "false",
                                            dbfield: "main_photo"
                                        },
                                        {
                                            name: "Seo",
                                            label: "friendly URL",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "text",
                                            required: "true",
                                            validation : {
                                                rules: {
                                                    minlength: 5,
                                                    maxlenght: 15 
                                                }
                                            },
                                            dbfield: "friendly_url"
                                        }
                                       ]
                                }  
                            };              

            dynaforms.setForm(12 , 'product_multilang' , 1);
            dynaforms.createForm(fieldset , '#'+multiclone.available_languages[0]);                             
        });
    },
    createAttributes: function(){
       //basic_product_fields
        var fieldset = {
                    fieldset:{
                    	title: "Τιμές και διαστάσεις",
                        fields:[{
                                    name: "start_price",
                                    label: "Αρχική τιμή",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "start_price"
                                },
                                {
                                    name: "end_price",
                                    label: "Τελική τιμή",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "end_price"
                                },
                                {
                                    name: "weight",
                                    label: "Βάρος",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "weight"
                                },
                                {
                                    name: "Quantity",
                                    label: "Τεμ.",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "quantity"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'attributes_prod_fields' , 0);
        dynaforms.createForm(fieldset , '#attributes_product_fields');        

        product.ajax_req('', 'dropdown/getattributesbygroup' , function(res) { 
            if(res.success == 'success'){
            	var options_str ="<option value='0'>---</option>";

            	 $.each(res.responsedata, function(key, value){
                    options_str +="<optgroup label='"+key+"'>";
                    $.each(value, function(opt_value, opt_name){
                        options_str += "<option value='"+opt_name.attr_id+"'>"+opt_name.attr_value+"</option>";
                    });
                });

                var field_drp_str = '<div class="form-row row-fluid">'+
                                            '<div class="span10" id="row_attribute_sel">'+
                                                '<div class="row-fluid" id="frmobj">'+
                                                    '<label for="normal" class="form-label span0"></label>'+
                                                    '<div class="grid-inputs span12">'+
                                                        '<select class="span4 tags" multiple="" tabindex="-1" name="attribute_sel[counter_id][][attrid]" id="attribute_sel" >'+
                                                            options_str+
                                                        '</select>&nbsp;'+
                                                    '<input class="span2 text price" id="attribute_sel" name="attribute_sel[counter_id][price]" type="text" placeholder="έξτρα κόστος">&nbsp;'+
                                                    '<input class="span2 text quantity" id="attribute_sel" name="attribute_sel[counter_id][quantity]" type="text" placeholder="Ποσότητα">'+
                                                	'<span style="cursor:pointer;" class="minia-icon-trashcan" aria-hidden="true" id="row_attribute_sel" onclick="product.removeattribute(this);"></span></div>'+
                                                '</div>'+
                                            '</div> '+
                                        '</div>'; 

                product.attr_row = $(field_drp_str).clone(true); 
            }else{

            }
        }); 
    },
    addattribute: function(){
    	var counter = product.counter++;
            
        var prd_clone = $(product.attr_row).clone(true);

        prd_clone.find('select')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end()

        prd_clone.find('div#row_attribute_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end()                

        prd_clone.find('span#row_attribute_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end() 

        $(prd_clone).html( $(prd_clone).html().replace(/counter_id/gi,counter) );
               
    	$(prd_clone.html()).appendTo("#attr_row");
    	$("#attribute_sel_"+counter).select2();
    },
    addattributeWithValues: function(selected_ids , extra_cost , quantity , callback){
        var counter = product.counter++;
            
        var prd_clone = $(product.attr_row).clone(true);

        prd_clone.find('select')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end()

        prd_clone.find('div#row_attribute_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end()                

        prd_clone.find('span#row_attribute_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+counter);
        })
        .end() 

        $(prd_clone).html( $(prd_clone).html().replace(/counter_id/gi,counter) );
               
        $(prd_clone.html()).appendTo("#attr_row");
        $("#attribute_sel_"+counter).select2();
        $("#attribute_sel_"+counter).val(selected_ids).trigger("change");
        $("#row_attribute_sel_"+counter+" .price").val(extra_cost);
        $("#row_attribute_sel_"+counter+" .quantity").val(quantity);
        callback(true);
    },
    createAccompanyingSet: function(callback){
        product.ajax_req('', 'dropdown/gettags' , function(res) { 
            //console.log(res);
            if(res.success == 'success'){
                var options_str ="<option value='0'>---</option>";

                 $.each(res.responsedata, function(key, value){
                    $.each(value, function(opt_value, opt_name){
                        //console.log(opt_name);
                        options_str += "<option value='"+opt_value+"'>"+opt_name+"</option>";
                    });
                });

                var field_drp_str = '<div class="form-row row-fluid">'+
                                            '<div class="span10" id="row_acc_sel">'+
                                                '<div class="row-fluid" id="frmobj">'+
                                                    '<label for="normal" class="form-label span0"></label>'+
                                                    '<div class="grid-inputs span12">'+
                                                        '<select class="span4 tags" multiple="" tabindex="-1" name="acc_sel[counter_id][][tagid]" id="acc_sel" >'+
                                                            options_str+
                                                        '</select>&nbsp;'+
                                                    '<input class="span2 text title" id="acc_sel" name="acc_sel[counter_id][title]" type="text" placeholder="Τιτλος συνοδευτικού">&nbsp;'+
                                                    '<span style="cursor:pointer;" class="minia-icon-trashcan" aria-hidden="true" id="row_acc_sel" onclick="product.remove_accompanying_product(this);"></span></div>'+
                                                '</div>'+
                                            '</div> '+
                                        '</div>'; 

                product.acc_row = $(field_drp_str).clone(true); 
            }else{

            }
            callback(true);
        });
        
    },
    addaccompanying_product: function(){
        var acc_row_counter = product.acc_row_counter++;

        var prd_clone = $(product.acc_row).clone(true);

        prd_clone.find('select')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end()

        prd_clone.find('div#row_acc_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end()                

        prd_clone.find('span#row_acc_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end() 

        $(prd_clone).html( $(prd_clone).html().replace(/counter_id/gi,acc_row_counter) );
               
        $(prd_clone.html()).appendTo("#acc_row");
        $("#acc_sel_"+acc_row_counter).select2();
    },
    addaccompanying_productWithValues: function(selected_ids, acc_title , callback){
        var acc_row_counter = product.acc_row_counter++;

        var prd_clone = $(product.acc_row).clone(true);

        prd_clone.find('select')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end()

        prd_clone.find('div#row_acc_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end()                

        prd_clone.find('span#row_acc_sel')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+acc_row_counter);
        })
        .end() 

        $(prd_clone).html( $(prd_clone).html().replace(/counter_id/gi,acc_row_counter) );
               
        $(prd_clone.html()).appendTo("#acc_row");
        $("#acc_sel_"+acc_row_counter).select2();
        $("#acc_sel_"+acc_row_counter).val(selected_ids).trigger("change");
        $("#row_acc_sel_"+acc_row_counter+" .title").val(acc_title);
        callback(true);
    },    
    remove_accompanying_product: function(obj){
        $("#acc_row #"+obj.id).replaceWith('');
    },
    removeattribute: function(obj){
    	$("#attr_row #"+obj.id).replaceWith('');
    },
    createGalleries: function(){
        var fieldset = {
                    fieldset:{
                    	title: "Επιλέξτε τον τύπο gallery του προιόντος",
                        fields:[{
                                    name: "gallery_type",
                                    label: "Επιλογή τύπου gallery",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "gallery_id",
                                    datasource_action: "dropdown/getgalleries"
                                }]
                        }
                    };

        dynaforms.setForm(12 , 'photogalleries_frm' , 0);
        dynaforms.createForm(fieldset , '#albums_product_fields');


        var gall_row_str = '<div class="form-row row-fluid">'+
								'<div class="span8" id="row_gallery_photo">'+
									'<div class="row-fluid" id="frmobj">'+
										'<label class="form-label span2" for="normal">Φωτογραφία</label>'+
										'<input class="span4  text g_photo" id="gallery_photo" onclick="dynaforms.browse(this);" name="gallery_photo[]" type="text" ><span class="icon16 icomoon-icon-folder-4 browse_filemanager" id="gallery_photo" style="cursor:pointer;" onclick="dynaforms.browse(this);"></span>'+
										'<span style="cursor:pointer;" class="minia-icon-trashcan" aria-hidden="true" id="row_gallery_photo" onclick="product.removephoto(this);"></span></div>'+
									'</div>'+
								'</div>'+
							'</div>';
		product.gall_row = $(gall_row_str).clone(true);						
    },
    addphotorow: function(){
    	var gall_counter = product.gall_counter++;
            
        var gall_clone = $(product.gall_row).clone(true);

        gall_clone.find('input#gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end()         

        gall_clone.find('span#row_gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 

        gall_clone.find('.browse_filemanager')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 


        gall_clone.find('div#row_gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 

               
    	$(gall_clone.html()).appendTo("#photo_row");

    },
    addphotorowWithValue: function(photo_path , callback){
        var gall_counter = product.gall_counter++;
            
        var gall_clone = $(product.gall_row).clone(true);

        gall_clone.find('input#gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end()         

        gall_clone.find('span#row_gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 

        gall_clone.find('.browse_filemanager')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 


        gall_clone.find('div#row_gallery_photo')
        .each(function(){
            this.id = this.id.replace(this.id,this.id+'_'+gall_counter);
        })
        .end() 

               
        $(gall_clone.html()).appendTo("#photo_row");
        $("#row_gallery_photo_"+gall_counter+" .g_photo").val(photo_path);
        callback(true);
    },    
    removephoto: function(obj){
    	$("#photo_row div#"+obj.id).replaceWith('');
    },
    loadvalues: function(product_id){
        product.postaction = "edit";
        product.product_id = product_id;
        
        product.ajax_req('product_id='+product_id, 'product/loadvalues' , function(res) { 

            //default product values
            dynaforms.setValueToField("#basic_product_fields", "home_section_id" , res.responsedata.prd_default['home_section_id']);
            dynaforms.setValueToField("#basic_product_fields", "template_view_id" , res.responsedata.prd_default['template_view_id']);
            dynaforms.setValueToField("#basic_product_fields", "product_view_template" , res.responsedata.prd_default['product_view_template']);
            
            dynaforms.setValueToField("#basic_product_fields", "product_code" , res.responsedata.prd_default['product_code']);
            dynaforms.setValueToField("#basic_product_fields", "active" , res.responsedata.prd_default['active']);
            dynaforms.setValueToField("#basic_product_fields", "registered_users_only" , res.responsedata.prd_default['registered_users_only']);

            //attributes_product_fields
            dynaforms.setValueToField("#attributes_product_fields", "start_price" , res.responsedata.prd_default['start_price']);
            dynaforms.setValueToField("#attributes_product_fields", "end_price" , res.responsedata.prd_default['end_price']);
            dynaforms.setValueToField("#attributes_product_fields", "weight" , res.responsedata.prd_default['weight']);
            dynaforms.setValueToField("#attributes_product_fields", "quantity" , res.responsedata.prd_default['quantity']);

            //multilang
            var counter = res.responsedata.multilang.length;

            if(counter > 1){
                $.each(res.responsedata.multilang, function(key, value){
                    if(multiclone.available_languages[0] != value['shortcuts']){
                        multiclone.cloneTabContent(value['shortcuts'] , 'tab1');
                    }
                });
            }
                    
            $.each(res.responsedata.multilang, function(m_key, m_value){
                if(m_key > 0){
                    var la = "_"+m_value.shortcuts;
                }else{
                    var la = "";
                }    
                $.each(m_value, function(key, value){  
                  dynaforms.setValueToField("#tab1", key+la , value);
                });
            }); 

            //Tags Loader
            $("#tag_id").val(res.responsedata.prd_tags).trigger("change");

            //Attributes, Stock Controll
            $.each(res.responsedata.prd_stock, function(key, value){
                product.addattributeWithValues(value.attribs , value.extra_cost , value.quantity, function(res1){

                });    
            });

            //Related accompying products   
            $.each(res.responsedata.acc_products, function(key, value){
                product.addaccompanying_productWithValues(value.tagsids , value.acc_title, function(res2){

                });
            });

            //Gallery files 
            dynaforms.setValueToField("#photogalleries_frm", "gallery_id" , res.responsedata.prd_gallery[0]['gallery_id']);
            $.each(res.responsedata.prd_gallery, function(key, value){
                product.addphotorowWithValue(value.photo_path, function(res3){

                });  
            });

        });    
    },
    ajax_req: function(data , action  , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};

$(document).ready(function () {
        product.init_wizzard();
        product.creatBasicForm();
        product.createAttributes();
        product.createGalleries();
        product.createAccompanyingSet(function(result){
        var prd_id = parseInt(product.product_id);
            if(prd_id === 0){
                product.postaction = "save";
            }else{
                product.postaction = "edit";
                product.loadvalues(product.product_id);
            }
        });



});
