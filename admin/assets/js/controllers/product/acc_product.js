var accproduct = {
    button_next: "next",
    button_prev: "previous",
    button_complete: "finish",
    wizard: null,
    postaction: "save",
    item_id: null,
    attr_row: null,
    gall_row: null,
    counter: 1,
    gall_counter: 1,
    accproduct_id: null,
    init: function(properties){
        accproduct.open_firstmenu = properties.open_firstmenu;
        accproduct.button_next = properties.button_next;
        accproduct.button_prev = properties.button_prev;
        accproduct.button_complete = properties.button_complete;
        accproduct.complete_wizard_msg = properties.complete_wizard_msg;
        accproduct.accproduct_id = properties.accproduct_id;

        accproduct.setToolTips();
        //clone the wizard and store it in a var
        accproduct.wizard = $('#wizard').clone(true);
                
    },
    setToolTips: function(){
        $('.tip').qtip({
            content: {
                attr: "oldtitle"
            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window)
            },
            style: {
                classes: 'ui-tooltip-tipsy'
            }
        });
    },
    init_wizzard: function(){
        $("#wizard").html(accproduct.wizard.html());
        $("#wizzard_div").show();     
        $('#wizard').smartWizard({
            transitionEffect: 'fade', // Effect on navigation, none/fade/slide/
            onLeaveStep:accproduct.leaveAStepCallback,
            onFinish:accproduct.onFinishCallback,
            labelNext: accproduct.button_next,
            labelPrevious: accproduct.button_prev,
            labelFinish: accproduct.button_complete
        });
    },
    leaveAStepCallback: function(){
        //var return_result = false;
        //dynaforms.checkValid(function(result) {
        //    if(result){
        //        return_result = true;
        //    }
        //});  
        //return return_result;
        return true;
    },
    onFinishCallback: function(){
        dynaforms.getFormPostData(function(res) {    
            if(accproduct.postaction == "save"){
                accproduct.ajax_req(res+"&accomp=1" , 'product/save' , function(response) {
                    if(response.success == 'success'){
                        $.pnotify({
                            type: 'success',
                            title: 'Success',
                            text: response.msg,
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        window.location = "/admin/accompaying_products";
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: response.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });    
            }else{
                var extraparams = "&product_id="+accproduct.accproduct_id;
                accproduct.ajax_req(res+extraparams , 'product/update' , function(response) {
                    if(response.success == 'success'){
                        $.pnotify({
                            type: 'success',
                            title: 'Success',
                            text: response.msg,
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        window.location = "/admin/accompaying_products";
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: response.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });
            }
        });    

    },
    creatBasicForm: function(){
        //basic_product_fields
        var fieldset = {
                    fieldset:{
                        fields:[
                                {
                                    name: "Product Code",
                                    label: "Κωδικός προίον",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "product_code"
                                },
                                {
                                    name: "tags",
                                    label: "Επιλογή tags",
                                    fieldspan: "12",
                                    labelspan: "4",
                                    type: "tagsfield_ajax",
                                    required: "true",
                                    dbfield: "tag_id",
                                    datasource_action: "tagcloud/gettags"
                                },
                                {
                                    name: "Active",
                                    label: "Ενεργό",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "active"
                                },
                                {                                
                                    name: "Registered",
                                    label: "Εγγεγραμένοι χρήστες",
                                    labelspan: "4",
                                    value: "1",
                                    type: "checkbox",
                                    dbfield: "registered_users_only"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'product_basic' , 0);
        dynaforms.createForm(fieldset , '#basic_product_fields');


        multiclone.tabcreation("div#multilanguage_div" , 'tab1', function() {
            //set title fields
            var fieldset = {
                            fieldset : {
                                fields:[
                                        {
                                            name: "Name",
                                            label: "Ονομασία",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "text",
                                            required: "true",
                                            dbfield: "name"
                                        },
                                        {
                                            name: "Description",
                                            label: "Περιγραφή",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "texteditor",
                                            required: "false",
                                            dbfield: "prd_description"
                                        },
                                        {
                                            name: "photo",
                                            label: "Φωτογραφία",
                                            fieldspan: "4",
                                            labelspan: "4",
                                            type: "browse_file",
                                            required: "false",
                                            dbfield: "main_photo"
                                        }
                                       ]
                                }  
                            };              

            dynaforms.setForm(12 , 'product_multilang' , 1);
            dynaforms.createForm(fieldset , '#'+multiclone.available_languages[0]);                             
        });
    },
    createAttributes: function(){
       //basic_product_fields
        var fieldset = {
                    fieldset:{
                    	title: "Τιμές και διαστάσεις",
                        fields:[{
                                    name: "end_price",
                                    label: "Τελική τιμή",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "end_price"
                                },
                                {
                                    name: "weight",
                                    label: "Βάρος",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "weight"
                                },
                                {
                                    name: "Quantity",
                                    label: "Τεμ.",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "true",
                                    dbfield: "quantity"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'attributes_prod_fields' , 0);
        dynaforms.createForm(fieldset , '#attributes_product_fields');         
    },
    loadvalues: function(accproduct_id){
        accproduct.postaction = "edit";
        accproduct.accproduct_id = accproduct_id;
        
        accproduct.ajax_req('product_id='+accproduct_id, 'product/loadvalues' , function(res) { 
            console.log(res);
            //default product values          
            dynaforms.setValueToField("#basic_product_fields", "product_code" , res.responsedata.prd_default['product_code']);
            dynaforms.setValueToField("#basic_product_fields", "active" , res.responsedata.prd_default['active']);
            dynaforms.setValueToField("#basic_product_fields", "registered_users_only" , res.responsedata.prd_default['registered_users_only']);

            //attributes_product_fields
            dynaforms.setValueToField("#attributes_product_fields", "end_price" , res.responsedata.prd_default['end_price']);
            dynaforms.setValueToField("#attributes_product_fields", "weight" , res.responsedata.prd_default['weight']);
            dynaforms.setValueToField("#attributes_product_fields", "quantity" , res.responsedata.prd_default['quantity']);

            //multilang
            var counter = res.responsedata.multilang.length;

            if(counter > 1){
                $.each(res.responsedata.multilang, function(key, value){
                    if(multiclone.available_languages[0] != value['shortcuts']){
                        multiclone.cloneTabContent(value['shortcuts'] , 'tab1');
                    }
                });
            }
                    
            $.each(res.responsedata.multilang, function(m_key, m_value){
                if(m_key > 0){
                    var la = "_"+m_value.shortcuts;
                }else{
                    var la = "";
                }    
                $.each(m_value, function(key, value){  
                  dynaforms.setValueToField("#tab1", key+la , value);
                });
            }); 

            //Tags Loader
            $("#tag_id").val(res.responsedata.prd_tags).trigger("change");

        });    
    },
    ajax_req: function(data , action  , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};

$(document).ready(function () {
        accproduct.init_wizzard();
        accproduct.creatBasicForm();
        accproduct.createAttributes();

        var prd_id = parseInt(accproduct.accproduct_id);
        if(prd_id === 0){
            accproduct.postaction = "save";
        }else{
            accproduct.postaction = "edit";
            accproduct.loadvalues(accproduct.accproduct_id);
        }

});
