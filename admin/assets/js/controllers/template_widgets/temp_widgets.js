var widgets = {
	template_id : 0,
    section_id : 0,
    data: [],
    init: function(properties){
       widgets.template_id = properties.template_id;
    },
    saveTemplateChanges: function(id , callback){
        if(id > 0){
            //console.log(widgets.data);
            var section_id = widgets.data[0][0];
            var widgets_ids = widgets.data[0][1];
            widgets.ajax_req("section_id="+section_id+"&widgets_ids="+widgets_ids+"&template_id="+widgets.template_id , 'templatewidgets/changetemplate' , true , function(res) { 
                console.log(res);
            });    
        }
        widgets.data = [];
        callback();
    },
    ajax_req: function(data , action , async , callback){
        $("#ajax_loader").show();
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: async, 
            success: function (result) {
                callback(result);
            },
            beforeSend: function () {
                $('#ajax_loader').show();
            },
            complete: function (result) {
                $('#ajax_loader').hide();
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }    
};
   