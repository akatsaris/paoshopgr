var orders = {
    postaction: "edit",
    order_id: null,
    init: function(properties){
        orders.order_id = properties.order_id;
        orders.setToolTips();                
    },
    setToolTips: function(){
        $('.tip').qtip({
            content: {
                attr: "oldtitle"
            },
            position: {
                my: 'bottom center',
                at: 'top center',
                viewport: $(window)
            },
            style: {
                classes: 'ui-tooltip-tipsy'
            }
        });
    },
    UpdateOrder: function(){
        dynaforms.getFormPostData(function(res) {    
                var extraparams = "&order_id="+orders.order_id;
                orders.ajax_req(res+extraparams , 'orders/update' , function(response) {
                    if(response.success == 'success'){
                        $.pnotify({
                            type: 'success',
                            title: 'Success',
                            text: response.msg,
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        window.location = "/admin/orders";
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: response.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });
        });    

    },
    creatBasicForm: function(){
        //basic_product_fields
        var fieldset = {
                    fieldset:{
                        fields:[
                                {
                                    name: "firstname",
                                    label: "Όνομα",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "firstname"
                                },
                                {
                                    name: "lastname",
                                    label: "Επώνυμο",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "lastname"
                                },
                                {
                                    name: "address",
                                    label: "Διεύθυνση",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "address"
                                },
                                {
                                    name: "address_more",
                                    label: "Διεύθυνση 2",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "address_more"
                                },
                                {
                                    name: "city_name",
                                    label: "Πόλη",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "city_name"
                                },
                                {
                                    name: "postal",
                                    label: "Τ.Κ.",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "postal"
                                },
                                {
                                    name: "phone",
                                    label: "Τηλέφωνο",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "phone"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'order_basic' , 0);
        dynaforms.createForm(fieldset , '#basic_order_fields');
    },
    createInvoiceForm: function(){
        var fieldset = {
                    fieldset:{
                        fields:[
                                {
                                    name: "invoice_company_name",
                                    label: "Όνομα εταιρίας",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "invoice_company_name"
                                },
                                {
                                    name: "invoice_profession",
                                    label: "Επάγγελμα",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "invoice_profession"
                                },
                                {
                                    name: "invoice_address",
                                    label: "Διεύθυνση",
                                    fieldspan: "4",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "invoice_address"
                                },
                                {
                                    name: "doy",
                                    label: "ΔΟΥ",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "doy"
                                },
                                {
                                    name: "vat",
                                    label: "Α.Φ.Μ.",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "vat"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'invoice_data' , 0);
        dynaforms.createForm(fieldset , '#invoice_data_fields');
    },
    createShippingForm: function(){
        var fieldset = {
                    fieldset:{
                        fields:[
                                {
                                    name: "order_shipping",
                                    label: "Μεταφορικά &euro;",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "order_shipping"
                                },
                                {
                                    name: "order_cod",
                                    label: "Αντικαταβολή &euro;",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "order_cod"
                                },
                                {
                                    name: "order_price",
                                    label: "Παραγγελία &euro;",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "order_price"
                                },
                                {
                                    name: "wrap_packaging_cost",
                                    label: "Συσκευασία &euro;",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "wrap_packaging_cost"
                                },
                                {
                                    name: "order_tracking",
                                    label: "Tracking",
                                    fieldspan: "2",
                                    labelspan: "4",
                                    type: "text",
                                    required: "false",
                                    dbfield: "order_tracking"
                                }
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'shipping_data' , 0);
        dynaforms.createForm(fieldset , '#shipping_fields');
    },
    createOrderStatus: function(){
        var fieldset = {
                    fieldset:{
                        fields:[{
                                    name: "order_status_id",
                                    label: "Στάδιο",
                                    fieldspan: "2",
                                    labelspan: "1",
                                    type: "dropdown_ajax",
                                    required: "true",
                                    dbfield: "order_status_id",
                                    default_option : {
                                            option_value: 0,
                                            option_name : '---'
                                    },
                                    datasource_action: "dropdown/getorderstatus"
                                },
                                ]
                        }
                    };

        dynaforms.setForm(12 , 'shipping_data' , 0);
        dynaforms.createForm(fieldset , '#order_status_fields');
    },
    loadvalues: function(order_id){
        orders.postaction = "edit";
        orders.order_id = order_id;
        
        orders.ajax_req('order_id='+order_id, 'orders/loadvalues' , function(res) { 
 
            var html_order = res.responsedata[0].order_html;
            var comment = res.responsedata[0].comment;
            var order_number = res.responsedata[0].order_number_id;
            
            $('#order_html').html(html_order);
            $('#customer_comment').html(comment);
            $('#order_num').html(order_number);

            //$('#order_status_id').html(order_number);
            $('#order_status_id').find('option[value="'+res.responsedata[0].order_status_id+'"]').attr("selected",true);
            

            console.log(res);
            $.each(res.responsedata, function(m_key, m_value){
                $.each(m_value, function(key, value){  
                  dynaforms.setValueToField("#alltabs", key , value);
                });
            }); 

        });    
    },
    ajax_req: function(data , action  , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};

$(document).ready(function () {
        orders.creatBasicForm();
        orders.createInvoiceForm();
        orders.createShippingForm();
        orders.createOrderStatus();

        var order_id = parseInt(orders.order_id);
        orders.loadvalues(orders.order_id);

        $('body').delegate('#ord-submit', 'click', function() {   
            orders.UpdateOrder();    
        }); 

});
