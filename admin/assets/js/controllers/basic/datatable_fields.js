var tags = {
        fieldset : {
            fields:[
                    {
                        name: "Tag",
                        label: "Tag",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "tag"
                    }
                   ]
            }  
        };
var currencies = {
        fieldset : {
            fields:[
                    {
                        name: "Currency Name",
                        label: "Currency Name",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "currency_name"
                    },
                    {
                        name: "Currency Symbol",
                        label: "Currency Symbol",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "currency_symbol"
                    }
                   ]
            }  
        };
var languages = {
        fieldset : {
            fields:[
                    {
                        name: "Digits",
                        label: "Digits",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "digits"
                    },
                    {
                        name: "Name",
                        label: "Name",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "name"
                    },
                    {
                        name: "Shortcut",
                        label: "Shortcut",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shortcuts"
                    },
                    {
                        name: "Default",
                        label: "Default",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "checkbox",
                        value: "1",
                        dbfield: "default"
                    }
                   ]
            }  
        };
var attr_groups = {
        fieldset : {
            fields:[
                    {
                        name: "Group",
                        label: "Group",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "group_name"
                    }
                   ]
            }  
        };
var attributes = {
        fieldset : {
            fields:[
                    {
                        name: "Attribute",
                        label: "Value",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "attr_value"
                    },
                    {
                        name: "Ομάδα",
                        label: "Επιλογή Ομάδας",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "attr_group_id",
                        datasource_action: "dropdown/getattributesgroups"
                    }
                   ]
            }  
        };      
var home_sections = {
        fieldset : {
            fields:[
                    {
                        name: "Tag",
                        label: "Tag",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "home_tag"
                    },
                    {
                    name: "Name",
                        label: "Name",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "name"
                    }
                   ]
            }  
        };
var gallery_types = {
        fieldset : {
            fields:[
                    {
                        name: "Name",
                        label: "Name",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "gall_name"
                    },
                    {
                        name: "gallery_template",
                        label: "Template Gallery",
                        fieldspan: "4",
                        labelspan: "4",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "gallery_template",
                        default_option : {
                                option_value: 0,
                                option_name : '---'
                        },
                        datasource_action: "dropdown/getgalleryviews"
                    },
                    {
                    name: "Width",
                        label: "Width",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "gall_width"
                    },
                    {
                    name: "Height",
                        label: "Height",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "gall_height"
                    },
                    {
                    name: "items",
                        label: "Default Items",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "how_many_items"
                    }
                   ]
            }  
        };
var templates = {
        fieldset : {
            fields:[
                    {
                        name: "template_name",
                        label: "Όνομα",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "template_name"
                    },
                    {
                        name: "template_file",
                        label: "Όνομα αρχείου",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "template_file"
                    }
                   ]
            }  
        };
   
var template_view = {
        fieldset : {
            fields:[
                    {
                        name: "template_name",
                        label: "Όνομα",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "template_name"
                    },
                    {
                        name: "choose_template",
                        label: "Επιλογή Template",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "template_id",
                        datasource_action: "dropdown/gettemplates"
                    },
                    {
                        name: "default",
                        label: "Default",
                        labelspan: "2",
                        value: "1",
                        type: "checkbox",
                        dbfield: "default"
                    }
                   ]
            }  
        };
var template_sections = {
        fieldset : {
            fields:[
                    {
                        name: "section_name",
                        label: "template section",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "section_name"
                    },
                    {
                        name: "section_file",
                        label: "template file",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "section_file"
                    },
                    {
                        name: "section_color",
                        label: "Color",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "section_color"
                    },
                    {
                        name: "choose_template",
                        label: "Επιλογή Template",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "template_id",
                        datasource_action: "dropdown/gettemplates"
                    }
                   ]
            }  
        };  

var widget = {
        fieldset : {
            fields:[
                    {
                        name: "widget_name",
                        label: "Όνομα",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "widget_name"
                    },
                    {
                        name: "choose_widget",
                        label: "Επιλογή Widget",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "widget",
                        datasource_action: "dropdown/getwidgets"
                    },
                    {
                        name: "widget_html",
                        label: "HTML",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "texteditor",
                        required: "true",
                        dbfield: "html"
                    },
                    {
                        name: "params",
                        label: "params",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "params"
                    },
                   ]
            }  
        };  
var html_content = {
        fieldset : {
            fields:[
                    {
                        name: "Name",
                        label: "name",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "name"
                    },
                    {
                        name: "html_content",
                        label: "HTML",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "texteditor",
                        required: "true",
                        dbfield: "html_content"
                    }
                   ]
            }  
        };
var simple_content = {
        fieldset : [{
            title: "Τίτλοι",
            fields:[
                    {
                        name: "Title",
                        label: "Τίτλος",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "text",
                        required: "true",
                        dbfield: "cnt_title"
                    }
                   ]
            },
            {
            title: "Περιεχόμενα",  
            fields:[
                    {
                        name: "cnt_desc",
                        label: "Περιγραφή",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "texteditor",
                        required: "true",
                        dbfield: "cnt_desc"
                    },
                    {
                        name: "cnt_fullcontent",
                        label: "Περιεχόμενο",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "texteditor",
                        required: "true",
                        dbfield: "cnt_full_content"
                    }
                    ]         
            },
            {
            title: "SEO",  
            fields:[{
                        name: "friendly_url",
                        label: "Friendly URL",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "text",
                        required: "true",
                        dbfield: "friendly_url"
                    },
                    {
                        name: "keywords",
                        label: "keywords",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "textarea",
                        required: "true",
                        dbfield: "keywords"
                    },
                    {
                        name: "google_desc",
                        label: "Περιγραφή Google",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "textarea",
                        required: "true",
                        dbfield: "google_desc"
                    }
                    ]         
            },
            {
            title: "Photo",  
            fields:[
                    {
                        name: "photo",
                        label: "photo",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "browse_file",
                        required: "false",
                        dbfield: "photo"
                    }
                    ]         
            },
            {
            title: "Τομέας Εμφάνισης",  
            fields:[
                    {
                        name: "section",
                        label: "Εμφάνιση",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "section",
                        default_option : {
                                option_value: 0,
                                option_name : '---'
                        },
                        datasource_action: "dropdown/gethomesections"
                    },
                    {
                        name: "template_view_id",
                        label: "Eικαστικό",
                        fieldspan: "6",
                        labelspan: "3",
                        type: "dropdown_ajax",
                        required: "false",
                        dbfield: "template_view_id",
                        default_option : {
                                option_value: 0,
                                option_name : '---'
                        },
                        datasource_action: "dropdown/getviews"
                    },
                    {
                        name: "Active",
                        label: "Ενεργό",
                        labelspan: "3",
                        value: "1",
                        type: "checkbox",
                        dbfield: "active"
                    },
                    {
                        name: "Registered",
                        label: "Σε Σύνδεση",
                        labelspan: "3",
                        value: "1",
                        type: "checkbox",
                        dbfield: "register_users_only"
                    }
                    ]         
            },
            {
            title: "Tags",  
            fields:[
                    {
                        name: "tags",
                        label: "Επιλογή tags",
                        fieldspan: "12",
                        labelspan: "3",
                        type: "tagsfield_ajax",
                        required: "true",
                        dbfield: "tags",
                        datasource_action: "tagcloud/gettags"
                    }
                    ]         
            }]
        };

var customers = {
        fieldset : {
            fields:[
                    {
                        name: "Name",
                        label: "Όνομα",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "firstname"
                    },
                    {
                        name: "lastname",
                        label: "Επώνυμο",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "lastname"
                    },
                    {
                        name: "email",
                        label: "email",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "username"
                    },
                    {
                        name: "password",
                        label: "Κωδικός",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "password"
                    },
                    {
                        name: "birth",
                        label: "Ημε. Γέννησης",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "birth"
                    },
                    {
                        name: "member",
                        label: "Μέλος",
                        labelspan: "3",
                        value: "1",
                        type: "checkbox",
                        dbfield: "member"
                    },
                    {
                        name: "newsletter",
                        label: "Newsletter",
                        labelspan: "3",
                        value: "1",
                        type: "checkbox",
                        dbfield: "newsletter"
                    },
                    {
                        name: "terms",
                        label: "Terms",
                        labelspan: "3",
                        value: "1",
                        type: "checkbox",
                        dbfield: "terms"
                    }
                   ]
            }  
        };    

var shipping_providers = {
        fieldset : {
            fields:[
                    {
                        name: "provider_name",
                        label: "Όνομα Courier",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_provider_name"
                    }
                   ]
            }  
        }; 

var shipping_packs = {
        fieldset : {
            fields:[
                    {
                        name: "pack_name",
                        label: "Όνομα πακέτου",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_name"
                    },
                    {
                        name: "shipping_pack_description",
                        label: "Περιγραφή",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "texteditor",
                        required: "true",
                        dbfield: "shipping_pack_description"
                    },
                    {
                        name: "shipping_provider_id",
                        label: "Courier",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "shipping_provider_id",
                        datasource_action: "dropdown/getshippingproviders"
                    },
                    {
                        name: "shipping_pack_cod_val",
                        label: "COD",
                        fieldspan: "2",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_cod_val"
                    },
                    {
                        name: "shipping_pack_maxkg",
                        label: "Max KG",
                        fieldspan: "2",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_maxkg"
                    },
                    {
                        name: "shipping_pack_extraweight",
                        label: "Αλλαγή κλίμακας / Βάρος",
                        fieldspan: "2",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_extraweight"
                    },
                    {
                        name: "shipping_pack_extraweight_price",
                        label: "Αλλαγή κόστους / Βάρος",
                        fieldspan: "2",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_extraweight_price"
                    },
                    {
                        name: "shipping_pack_maxweight_price",
                        label: "Χρέωση max kg",
                        fieldspan: "2",
                        labelspan: "4",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_pack_maxweight_price"
                    }
                   ]
            }  
        }; 

var shipping_pack_kg = {
        fieldset : {
            fields:[{
                        name: "shipping_pack_id",
                        label: "Πακέτο Αποστολής",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "shipping_pack_id",
                        datasource_action: "dropdown/getshippingpacks"
                    },
                    {
                        name: "min_kg",
                        label: "Min KG",
                        fieldspan: "2",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "min_kg"
                    },
                    {
                        name: "max_kg",
                        label: "Max KG",
                        fieldspan: "2",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "max_kg"
                    },
                    {
                        name: "shipping_price",
                        label: "Κόστος",
                        fieldspan: "2",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "shipping_price"
                    }
                   ]
            }  
        };

var shipping_areas = {
        fieldset : {
            fields:[{
                        name: "shipping_pack_id",
                        label: "Πακέτο Αποστολής",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "shipping_pack_id",
                        datasource_action: "dropdown/getshippingpacks"
                    },
                    {
                        name: "area_id",
                        label: "Περιοχή",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "area_id",
                        datasource_action: "dropdown/getshippingareas"
                    }
                   ]
            }  
        };

var payment_methods = {
        fieldset : {
            fields:[{
                        name: "pay_name",
                        label: "Ονομασία",
                        fieldspan: "4",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "pay_name"
                    },
                    {
                        name: "pay_description",
                        label: "Περιγραφή",
                        fieldspan: "4",
                        labelspan: "2",
                        type: "texteditor",
                        required: "true",
                        dbfield: "pay_description"
                    },
                    {
                        name: "pay_notification",
                        label: "Ειδοποίηση",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "false",
                        dbfield: "pay_notification"
                    },
                    {
                        name: "pay_receipt_comment",
                        label: "Σχόλιο",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "false",
                        dbfield: "pay_receipt_comment"
                    },
                    {
                        name: "merchant_plugin",
                        label: "Merchant plugin",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "merchant_plugin",
                        datasource_action: "dropdown/getmerchants"
                    },
                    {
                        name: "cod_extracost_active",
                        label: "COD active",
                        labelspan: "2",
                        value: "1",
                        type: "checkbox",
                        dbfield: "cod_extracost_active"
                    }
                   ]
            }  
        }; 

var shipping_payment_rel =  {
        fieldset : {
            fields:[{
                        name: "shipping_packet_id",
                        label: "Πακέτο Αποστολής",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "shipping_packet_id",
                        datasource_action: "dropdown/getshippingpacks"
                    },
                    {
                        name: "pay_method_id",
                        label: "Τρόπος Πληρωμής",
                        fieldspan: "6",
                        labelspan: "4",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "pay_method_id",
                        datasource_action: "dropdown/getpaymethods"
                    }
                   ]
            }  
        };  

var order_status = {
        fieldset : {
            fields:[{
                        name: "ord_status",
                        label: "Κατάσταση",
                        fieldspan: "2",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "ord_status"
                    },
                    {
                        name: "ord_email_template",
                        label: "Template",
                        fieldspan: "4",
                        labelspan: "2",
                        type: "dropdown_ajax",
                        required: "true",
                        dbfield: "ord_email_template",
                                default_option : {
                                option_value: '---',
                                option_name : '---'
                        },
                        datasource_action: "dropdown/getemailtemplates"
                    },
                    {
                        name: "send_mail",
                        label: "mail enabled",
                        labelspan: "2",
                        value: "1",
                        type: "checkbox",
                        dbfield: "send_mail"
                    }
                   ]
            }  
        };  
var vouchers = {
        fieldset : {
            fields:[
                    {
                        name: "v_code",
                        label: "Κωδικός",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "true",
                        dbfield: "v_code"
                    },
                    {
                        name: "price",
                        label: "Τιμή έκπτωσης",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "false",
                        dbfield: "price"
                    },
                    {
                        name: "percent",
                        label: "Ποσοστό",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "false",
                        dbfield: "percent"
                    },
                    {
                        name: "expiration",
                        label: "Ημερ. Λήξης",
                        fieldspan: "6",
                        labelspan: "2",
                        type: "text",
                        required: "false",
                        dbfield: "expiration"
                    }
                   ]
            }  
        };

        