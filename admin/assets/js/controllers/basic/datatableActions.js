var datatableActions = {
    datas: null,
    item_id: null,
    datatable_name: null,
    post_action: "save",
    multilang: false,
    fieldsets: null,
    init: function(properties){
        //bootstrap plugins
        $('#modal').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });

        $('#modal_edit').dialog({
            autoOpen: false,
            modal: true,
            dialogClass: 'dialog'
        });
                
    },
    createForm: function(fieldset_obj , path , formname){
        //set form fields
        dynaforms.setForm(10 , formname , 0);
        dynaforms.createForm(fieldset_obj , path); 
    },
    createFormMultilang: function(fieldset_obj , path , formname){
        multiclone.tabcreation(path , 'tab1', function() {       
            dynaforms.setForm(10 , formname , 1);
            dynaforms.createForm(fieldset_obj , '#'+path+' #'+multiclone.available_languages[0]);                             
        });
    },
    resetDynamicForms: function(callback){
       $('#form_fieldset_area').html(''); 
       $('#form_fieldset_area_edit').html('');
       callback(true);
    },
    openAddModal: function(){
       datatableActions.post_action = "save";
       datatableActions.resetDynamicForms(function(res){
           if(datatableActions.multilang =="true"){
                datatableActions.createFormMultilang(datatableActions.fieldsets , "#form_fieldset_area" , 'dataTableForm');            
            }else{
                datatableActions.createForm(datatableActions.fieldsets , "#form_fieldset_area", 'dataTableForm'); 
            }
    		$('#modal').dialog('open');
       }); 
    },
    save: function(datatable_name , prev_location){
    	dynaforms.getFormPostData(function(res) { 
    		if(datatableActions.post_action == "save"){
    			var extraparams = "&table="+datatable_name;
                datatableActions.ajax_req(res+extraparams , 'datatables/savedata' , function(res) {
                    if(res.success != "failed"){
                        $.pnotify({
                            type: 'success',
                            title: 'Done',
                            text: "No errors found",
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        //redirect here
                        setTimeout(function() {
                          window.location = prev_location;
                        }, 2000);
                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: res.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });
    		}else{
    			console.log("update");
                console.log(datatableActions.post_action);
                var extraparams = "&table="+datatable_name+"&item_id="+datatableActions.item_id;
                datatableActions.ajax_req(res+extraparams , 'datatables/updatedata' , function(res) {
                    if(res.success != "failed"){
                        $.pnotify({
                            type: 'success',
                            title: 'Done',
                            text: "No errors found",
                            icon: 'picon icon16 iconic-icon-check-alt white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                        //redirect here
                        setTimeout(function() {
                          window.location = prev_location;
                        }, 2000);

                    }else{
                        $.pnotify({
                            type: 'error',
                            title: 'Error',
                            text: res.msg,
                            icon: 'picon icon24 typ-icon-cancel white',
                            opacity: 0.95,
                            history: false,
                            sticker: false
                        });
                    }
                });                
    		}
    	});	
    },
    delete_item: function(datatable_name , prev_location , id){
        var params = "&table="+datatable_name+"&id="+id;
        datatableActions.ajax_req(params , 'datatables/delete' , function(res) {
                if(res.success != "failed"){
                    $.pnotify({
                        type: 'success',
                        title: 'Done',
                        text: "No errors found",
                        icon: 'picon icon16 iconic-icon-check-alt white',
                        opacity: 0.95,
                        history: false,
                        sticker: false
                    });
                    //redirect here
                        setTimeout(function() {
                          window.location = prev_location;
                        }, 2000);
                }else{
                    $.pnotify({
                        type: 'error',
                        title: 'Error',
                        text: res.msg,
                        icon: 'picon icon24 typ-icon-cancel white',
                        opacity: 0.95,
                        history: false,
                        sticker: false
                    });
                }
            });
    },
    delete_items: function(datatable_name , prev_location , ids){
        var params = "&table="+datatable_name+"&"+ids;
        datatableActions.ajax_req(params , 'datatables/massdelete' , function(res) {
                if(res.success != "failed"){
                    $.pnotify({
                        type: 'success',
                        title: 'Done',
                        text: "No errors found",
                        icon: 'picon icon16 iconic-icon-check-alt white',
                        opacity: 0.95,
                        history: false,
                        sticker: false
                    });
                    //redirect here
                    window.location = prev_location;
                }else{
                    $.pnotify({
                        type: 'error',
                        title: 'Error',
                        text: res.msg,
                        icon: 'picon icon24 typ-icon-cancel white',
                        opacity: 0.95,
                        history: false,
                        sticker: false
                    });
                }
            });
    },
    edit_item: function(datatable_name , prev_location , id){
        datatableActions.item_id = id;
        datatableActions.datatable_name = datatable_name;
        datatableActions.post_action = 'edit';

        var params = "&table="+datatable_name+"&id="+id;

        datatableActions.ajax_req(params , 'datatables/getdatatabledata' , function(res) {
            if(res.success != "failed"){
                var item_data = res.data;
                datatableActions.resetDynamicForms(function(result) {
                    if(datatableActions.multilang =="true"){
                        datatableActions.createFormMultilang(datatableActions.fieldsets , "#form_fieldset_area_edit" , 'dataTableFormEdit');            
                    }else{
                        datatableActions.createForm(datatableActions.fieldsets , "#form_fieldset_area_edit", 'dataTableFormEdit'); 
                    }
                    
                    //Create language tabs 
                    var counter = item_data.length;
                    if(counter > 1){
                        $.each(item_data, function(key, value){
                            if(multiclone.available_languages[0] != value['la_shortcut']){
                                multiclone.cloneTabContent(value['la_shortcut'] , 'tab1');
                            }
                        });
                    }

                    //set field values
                    $.each(item_data, function(seo_key, seo_value){
                        if(seo_key > 0){
                            var la = "_"+seo_value.la_shortcut;
                        }else{
                            var la = "";
                        }    
                        if(datatableActions.multilang =="true"){
                            $.each(seo_value, function(key, value){  
                              dynaforms.setValueToField("#tab1", key+la , value);
                            });
                        }else{
                            $.each(seo_value, function(key, value){  
                              dynaforms.setValueToField("#form_fieldset_area_edit", key+la , value);
                            });
                        }
                    });

                    $('#modal_edit').dialog('open');

                });
            }else{
                $.pnotify({
                    type: 'error',
                    title: 'Error',
                    text: res.msg,
                    icon: 'picon icon24 typ-icon-cancel white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });                
            }    
        });    
    },
    ajax_req: function(data , action  , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};
