
var multiclone = {
	available_languages : [],
    texteditors_ids: [],
    init: function(properties){
    	multiclone.available_languages = properties.available_languages;
        $("#multitabs").hide();       
    },
    tabcreation: function(path , attr , callback){
    	var multitab = $('#multitabs').clone(true); 
    	multitab.find('ul').attr('id' , attr).end();
    	multitab.find('.multitabs').attr('id' , attr).end();
    	
    	var tab_str_li = "";
    	var content_tab_str = "";

    	$.each(multiclone.available_languages, function(key, val){
    		 tab_str_li += '<li class=""><a data-toggle="tab" href="#'+attr+'_'+val+'">'+val+'</a></li>\n\n';
    		 content_tab_str += '<div id="'+attr+'_'+val+'" class="tab-pane fade">\n<div id="'+val+'">'+val+'</div>\n</div>\n';
        });      

        multitab.find('ul#'+attr+'.dropdown-menu').append(tab_str_li).end();
        multitab.find('div#'+attr+' .tab-content').append(content_tab_str).end();

    	$(path).html(multitab.html());
        
        //make active 1st language tab
        $('.tab-content #'+attr+'_'+multiclone.available_languages[0]).removeClass("fade").addClass("active");
    	callback(true);
    },
    cloneTabContent: function(la , tabid){
        multiclone.texteditors_ids = [];
        //make a clone action at this tab from default language and change name to an array
        var parent_tab_id = tabid;
        var def_la = multiclone.available_languages[0];

        var removelang_str = "<div id='rem_lang' style='cursor:pointer;' rel='"+parent_tab_id+"' def_la='"+def_la+"' la='"+la+"'>[ x ] delete</div>";

        if(la != def_la){
            var newtab = $('#'+parent_tab_id+' #'+def_la).clone(true);

            newtab.find('input')
            .each(function(){
                this.name = this.name.replace(def_la,la);
                this.id = this.id.replace(this.id,this.id+'_'+la);
            })
            .end()

            newtab.find('textarea')
            .each(function(){
                this.name = this.name.replace(def_la,la);
                if($(this).hasClass("tinymce") == false){
                    this.id = this.id.replace(this.id,this.id+'_'+la);
                }
            })
            .end()   

            newtab.find('select')
            .each(function(){
                this.name = this.name.replace(def_la,la);
                this.id = this.id.replace(this.id,this.id+'_'+la);
            })
            .end()                      

            newtab.find('form')
            .each(function(){
                this.id = this.id.replace(this.id,this.id+'_'+la);
            })
            .end()

            //browse_filemanager
            newtab.find('.browse_filemanager')
            .each(function(){
                this.id = this.id.replace(this.id,this.id+'_'+la);
            })
            .end()


            //tags
            newtab.find('div.tags')
            .each(function(){
                $(this).replaceWith('');
            })
            .end() 

            //tags
            newtab.find('select.tags')
            .each(function(){
                $(this).attr('style' , '');
            })
            .end()                       

            //fix tinymce instance
            newtab.find('textarea')
            .each(function(){

                //var temp = $(this).next('.mceEditor').is(':hidden');
                //console.log(temp);   

                if($(this).hasClass("tinymce")){
                    this.id = this.id.replace(this.id,this.id+'_'+la);
                    $(this).next('.mceEditor').replaceWith('');
                    $(this).removeAttr("aria-hidden");
                    $(this).removeClass("uniform");
                    $(this).show();
                    multiclone.texteditors_ids.push(this.id);
                }
            })
            .end() 
           //end of fix

         
            newtab.append(removelang_str).end();
            var form_id = newtab.find("form").attr("id");

            if($('#'+parent_tab_id+' #'+la).html() == la){
                $('#'+parent_tab_id+' #'+la).html(newtab.html());

                //load validations
                dynaforms.addValidationScript(form_id);
                
                //load text editors
                $.each(multiclone.texteditors_ids, function(key, val){ 
                console.log(val);         
                     dynaforms.loadtinyMCEbyPath("#"+val);
                });

                //uniform update
                $("input, textarea").not('.nostyle').uniform(); 
                //set tags
                $('select.tags').select2();
            }
        }        
    },
    ajax_req: function(data , action , callback){
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }   
};

$(document).ready(function () { 
    $('body').delegate('.dropdown-menu li a', 'click', function() {
        var la = $(this).html();
        var parent_tab_id = $(this).parent().parent().attr("id");

        multiclone.cloneTabContent(la , parent_tab_id);
    });

    $('body').delegate('#rem_lang', 'click', function() {
        var tabid = $(this).attr('rel');
        var def_la = $(this).attr('def_la');
        var la = $(this).attr('la');
        $('#'+tabid+' #'+la).html(la);
        $('.tab-content #'+tabid+'_'+la).removeClass("active");
        $('.tab-content #'+tabid+'_'+def_la).removeClass("fade").addClass("active");
        $('ul#'+tabid+'.dropdown-menu li').removeClass("active");
    });    
});

