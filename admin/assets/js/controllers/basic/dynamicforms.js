var dynaforms = {
	available_languages : [],
    box_header: null,
    box_footer: null,
    span: null,
    formids: [],
    formid: 0,
    formtitle: null,
    multilanguage: 0,
    validator: {},
    havetexteditor: 0,
    init: function(properties){
       dynaforms.available_languages = properties.available_languages;
       dynaforms.havetexteditor = false;
    },
    setForm : function(span , formid , multilanguage){
       dynaforms.span = span;
       dynaforms.formids.push(formid);
       dynaforms.formid = formid;
       dynaforms.multilanguage = multilanguage;

       dynaforms.box_header =  '<div>'+
                                '<form class="form-horizontal dynaforms '+formid+'" id="'+formid+'">';

       dynaforms.box_footer =  '</form>'+
                                '</div>';
    },
    createForm: function(jsonobj , replace){
        var formstr = dynaforms.box_header;

            if(jsonobj['fieldset'] instanceof Array){
                json_data = jsonobj['fieldset'];
            }else{
                json_data = [jsonobj['fieldset']];
            }
            
            $.each(json_data, function(key, value){
                if(value['title']){
                    formstr += dynaforms.setFieldsetTitle(value['title']);
                }


                if(value['fields'] instanceof Array){
                    json_fields = value['fields'];
                }else{
                    json_fields = [value['fields']];
                }

                $.each(json_fields, function(key, val){
                    dynaforms.createFormObject(val , function(res){
                        if(res){
                            formstr += res;
                        }    
                    });
                 }); 
            }); 
            formstr += dynaforms.box_footer;

        $(replace).html(formstr);
        $("input, textarea").not('.nostyle').uniform();
        dynaforms.addValidationScript(dynaforms.formid);

        if(dynaforms.havetexteditor){
            dynaforms.loadtinyMCE();
        }

        $("select.tags").select2();
    },
    createFormObject: function(val , callback){
        var obj_type = val['type'];
        var obj_str = dynaforms.setField(dynaforms.span);
        var field_id = val['dbfield'];
        
        //validation checks
        if(val['required'] === "true"){
            var required = val['required'];
            if(val['validation_type']){
                var validation_type = 'required '+val['validation_type'];
            }else{
                var validation_type = 'required ';
            }    
            if(val['validation']){
                var obj_val_rules = val['validation']['rules'];
            }else{
                var obj_val_rules = null;
            }
        }else{
            var validation_type = '';
            var obj_val_rules = null;
        }

        if(val['datasource_params']){
            var params = val['datasource_params'];
        }else{
            var params = null;
        }

        if(val['rows']){
            var rows = val['rows'];
        }else{
            var rows = null;
        }        

        if(val['datasource_action']){
            var datasource_action = val['datasource_action'];
        }else{
            var datasource_action = null;
        }  

        var temp = $(obj_str).clone(true);
        switch(obj_type)
        {
        case 'dropdown_ajax':
              dynaforms.dropdown_ajax(field_id , field_id , val['fieldspan'] , val['default_option'] , validation_type , datasource_action , params , function(res) {
                    if(res){
                        temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
                        temp.find("#frmobj").append(res).end();
                        callback(temp.html());
                    }
              }); 
          break;       
        case 'tagsfield_ajax':
              dynaforms.tagsfield_ajax(field_id , field_id , val['fieldspan'] , val['default_option'] , validation_type , datasource_action , params , function(res) {
                    if(res){
                        temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
                        temp.find("#frmobj").append(res).end();
                        callback(temp.html());
                    }
              }); 
          break;          
        case 'checkbox':
              temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
              temp.find("#frmobj").append(dynaforms.checkbox(field_id , field_id , val['labelspan'] , val['value'])).end();
              callback(temp.html());
          break;
        case 'text':
          var validation_rules = dynaforms.setValidationRules(required , obj_val_rules);
              temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
              temp.find("#frmobj").append(dynaforms.textfield(field_id , field_id , val['fieldspan'] , validation_type , validation_rules)).end();
              callback(temp.html());
          break; 
        case 'texteditor':
          var validation_rules = dynaforms.setValidationRules(required , obj_val_rules);
              temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
              temp.find("#frmobj").append(dynaforms.texteditor(field_id , field_id)).end();
              callback(temp.html());
              dynaforms.havetexteditor = true;
          break;  
        case 'textarea':
          var validation_rules = dynaforms.setValidationRules(required , obj_val_rules);
              temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
              temp.find("#frmobj").append(dynaforms.textarea(field_id , field_id , val['fieldspan'] , validation_type , rows)).end();
              callback(temp.html());
              dynaforms.havetexteditor = true;
          break; 
        case 'browse_file':
          var validation_rules = dynaforms.setValidationRules(required , obj_val_rules);
              temp.find("#frmobj").append(dynaforms.setlabel(val['label'] , val['labelspan'])).end();
              temp.find("#frmobj").append(dynaforms.browse_file(field_id , field_id , val['fieldspan'] , validation_type , validation_rules)).end();
              callback(temp.html());
          break;                                                                
        }
    },
    textfield: function(name , id , span , validation_type , validation_rules){
        if(dynaforms.multilanguage == 1){
            var multilang = '['+dynaforms.available_languages[0]+']';
        }else{
            var multilang = '';
        }
        var textfield_str = '<input class="span'+span+' '+validation_type+'" id="'+id+'" name="'+dynaforms.formid+multilang+'['+name+']" type="text" '+validation_rules+' />';
        return textfield_str;
    },
    textarea: function(name , id , span , validation_type , rows){
        if(dynaforms.multilanguage == 1){
            var multilang = '['+dynaforms.available_languages[0]+']';
        }else{
            var multilang = '';
        }
        var textareas_str = '<textarea class="span'+span+' '+validation_type+'" id="'+id+'" rows="'+rows+'" name="'+dynaforms.formid+multilang+'['+name+']"></textarea>';
        return textareas_str;
    },
    dropdown_ajax: function(name , id , span , def_option , validation_type , datasource_action , params , callback){
        
        if(params){
            var data = jQuery.param(params);
        }else{
            var data = ""; 
        }    
        dynaforms.ajax_req(data , datasource_action , false , function(res) {  
            if(res.success == 'success'){
                if(dynaforms.multilanguage == 1){
                    var multilang = '['+dynaforms.available_languages[0]+']';
                }else{
                    var multilang = '';
                }

                var options_str = "";

                if(def_option){
                    options_str += "<option value='"+def_option.option_value+"'>"+def_option.option_name+"</option>";
                }

                $.each(res.responsedata, function(key, value){
                    $.each(value, function(opt_value, opt_name){
                        options_str += "<option value='"+opt_value+"'>"+opt_name+"</option>";
                    });
                });

                var field_drp_str = '<div class="span'+span+' controls">\n'+   
                                        '<select class="'+validation_type+'" name="'+dynaforms.formid+multilang+'['+name+']" id="'+id+'">\n'+ 
                                            options_str+ 
                                        '</select>\n'+ 
                                    '</div>\n'; 
                callback(field_drp_str);                                    
            }
        });
    }, 
    dropdown: function(){
                       
    },
    checkbox: function(name , id , span ,  value){
        if(dynaforms.multilanguage == 1){
            var multilang = '['+dynaforms.available_languages[0]+']';
        }else{
            var multilang = '';
        }        
        var field_str = '<div class="span'+span+' controls">'+
                            '<div class="left marginT5">'+
                                '<input type="checkbox" id="'+id+'" value="'+value+'" name="'+dynaforms.formid+multilang+'['+name+']"/>'+
                            '</div>'+
                        '</div>';
        return field_str;            
    },
    texteditor: function(name , id){
        if(dynaforms.multilanguage == 1){
            var multilang = '['+dynaforms.available_languages[0]+']';
        }else{
            var multilang = '';
        } 
        var texteditor_str = '<textarea class="tinymce" name="'+dynaforms.formid+multilang+'['+name+']" id="'+id+'" style="width:43%;"></textarea>';
        return texteditor_str;
    },
    radiobutton: function(){

    },
    browse_file: function(name , id , span , validation_type , validation_rules){
        if(dynaforms.multilanguage == 1){
            var multilang = '['+dynaforms.available_languages[0]+']';
        }else{
            var multilang = '';
        } 
        var browse_str = '<div id="photo_'+id+'"></div>'+
                         '<input class="span'+span+' '+validation_type+'" id="'+id+'" onclick="dynaforms.browse(this);" name="'+dynaforms.formid+multilang+'['+name+']" type="text" '+validation_rules+' />'+
                         '<span class="icon16 icomoon-icon-folder-4 browse_filemanager" id="'+id+'" style="cursor:pointer;" onclick="dynaforms.browse(this);"></span>';
        return browse_str;
    },
    browse: function(obj){
       FileManager=window.open('/admin/wfilemanager?targetelem='+obj.id,'filemanager','width=750,height=500,resizable=1,scrollbars=1');
    },
    multiselect: function(){

    },
    tagsfield_ajax: function(name , id , span , def_option , validation_type , datasource_action , params , callback){
        if(params){
            var data = jQuery.param(params);
        }else{
            var data = ""; 
        }    
        dynaforms.ajax_req(data , datasource_action , false , function(res) {  
            if(res.success == 'success'){
                if(dynaforms.multilanguage == 1){
                    var multilang = '['+dynaforms.available_languages[0]+']';
                }else{
                    var multilang = '';
                }

                var options_str = "";

                
                $.each(res.responsedata, function(key, value){
                    $.each(value, function(opt_value, opt_name){
                        options_str += "<option value='"+opt_value+"'>"+opt_name+"</option>";
                    });
                });
                var tags_str =  '<div class="span8 controls">'+
                                '<select id="'+id+'" class="span'+span+' tags" name="'+dynaforms.formid+multilang+'['+name+'][]" id="'+id+'" multiple="" tabindex="-1">'+
                                    options_str+ 
                                '</select>\n'+
                                '</div>'; 

                callback(tags_str); 
                                 
            }
        });
    },
    tagsfield: function(){

    },
    setValidation: function(){

    },
    setlabel: function(label , labelspan){
        var label_str ='<label class="form-label span'+labelspan+'" for="normal">'+label+'</label>\n';
        return label_str;
    },
    setField: function(fieldspan){
        var field_str = '<div id="clone_wraper"><div class="form-row row-fluid">\n'+
                            '<div class="span'+fieldspan+'">\n'+
                                '<div class="row-fluid" id="frmobj">\n'+
                                '</div>\n'+
                            '</div>\n'+
                        '</div></div>\n';
        return field_str;                
    },
    setFieldsetTitle: function(title){
       var fieldsettitle_str =  '<div class="title" style="margin-top:30px;">'+
                                    '<h4>'+
                                        '<span>'+title+'</span>'+
                                    '</h4>'+
                                '</div>';
        return fieldsettitle_str;                        
    },
    getFormPostData: function(callback){
        var return_result = false;
        dynaforms.checkValid(function(result) {
            if(result){
                return_result = true;
            }
        });  

        if(return_result){
            var data = $(".dynaforms").serialize();
            /* Because serializeArray() ignores unset checkboxes and radio buttons: */
            data +=  jQuery('.dynaforms input[type=checkbox]:not(:checked)').map(
                            function() {
                                return "&"+this.name+"=0";
                            }).get();
            callback(data);   
        }
    },
    checkValid: function(callback){
        var error = "";
        $('.dynaforms').each(function () {
            var valid = $("#"+this.id).valid();
            if(valid){
                error = "false";
            }else{
                $.pnotify({
                    type: 'Error',
                    title: 'Error',
                    text: "found errors at form",
                    icon: 'picon icon16 iconic-icon-check-alt white',
                    opacity: 0.95,
                    history: false,
                    sticker: false
                });
                error = "true";
                return false;
            }
        });

        if(error == "true"){
            callback(false);
        }else{
            callback(true);
        }
    },
    addValidationScript: function(formid){           
            dynaforms.validator = $("#"+formid).validate({ ignore: [] });
            dynaforms.validator.resetForm();
    },
    setValidationRules: function(required , rules){
        if(required && rules){
            var rules_str = "";
            if(rules){
                $.each(rules, function(key, value){
                    rules_str += key+" = '"+value+"'";
                });    
            }
        }
        return rules_str;
    },
    loadtinyMCE: function(){
            $('textarea.tinymce').tinymce({
                // Location of TinyMCE script
                script_url : '/admin/assets/plugins/forms/tiny_mce/tiny_mce.js',

                // General options
                theme : "advanced",
                plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect",
                theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|media,advhr,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : false,
                entity_encoding : "raw",
                extended_valid_elements : "span[!class]",
                forced_root_block : "",

                // Example content CSS (should be your site CSS)
                content_css : "/public/assets/stylesheets/modular.css",
                file_browser_callback : 'dynaforms.elFinderBrowser',
                //template_external_list_url : "/admin/tiny_mce_listtemplates.php",
                document_base_url : "/",
                convert_urls:true,
                relative_urls:false,
                remove_script_host:true,
            });            
            dynaforms.havetexteditor = false;
    },
    loadtinyMCEbyPath: function(path){
            $(path).tinymce({
                // Location of TinyMCE script
                script_url : '/admin/assets/plugins/forms/tiny_mce/tiny_mce.js',

                // General options
                theme : "advanced",
                plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,inlinepopups,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,advlist",

                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect",
                theme_advanced_buttons2 : "bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,preview,|,forecolor,backcolor",
                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|media,advhr,|,ltr,rtl,|,fullscreen",
                theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,visualchars,nonbreaking,template,pagebreak",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : false,
                entity_encoding : "raw",
                extended_valid_elements : "span[!class]",
                forced_root_block : "",

                // Example content CSS (should be your site CSS)
                content_css : "/public/assets/stylesheets/modular.css",
                //template_external_list_url : "/admin/tiny_mce_listtemplates.php",
                file_browser_callback : 'dynaforms.elFinderBrowser',
                document_base_url : "/",
                convert_urls:true,
                relative_urls:false,
                remove_script_host:true,

            }); 
    },
    elFinderBrowser: function(field_name, url, type, win){
      var elfinder_url = '/admin/wfilemanager';    // use an absolute path!
      tinyMCE.activeEditor.windowManager.open({
        file: elfinder_url,
        title: 'elFinder 2.0',
        width: 900,  
        height: 450,
        resizable: 'yes',
        inline: 'yes',    // This parameter only has an effect if you use the inlinepopups plugin!
        popup_css: false, // Disable TinyMCE's default popup CSS
        close_previous: 'no'
      }, {
        window: win,
        input: field_name
      });
      return false;
    },
    setValueToField: function(path, field_name , field_value){
        if($(path+" #"+field_name).length > 0){

            if($(path+" #"+field_name).attr('type')){
                var elementType = $(path+" #"+field_name).attr('type').toLowerCase();
            }else{
                var elementType = $(path+" #"+field_name).get(0).tagName.toLowerCase();
            }

            switch(elementType)
            {
            case 'checkbox':
                if(field_value == "1"){
                    $(path+" input#"+field_name).prop('checked', true);
                    //uniform update
                    $("input, textarea").not('.nostyle').uniform(); 
                }
              break;
            case 'text':
              $(path+" #"+field_name).val(field_value);  
              break; 
            case 'select':
                
                if($(path+" #"+field_name).hasClass('tags')){
                    if(field_value){
                        var array = field_value.split(',');
                        $(path+" #"+field_name).select2();
                        $(path+" #"+field_name).val(array).trigger("change");
                    }
                }else{
                    $(path+" #"+field_name).find('option[value="'+field_value+'"]').attr("selected",true);
                }
              break;  
            case 'textarea':
                if(field_value){
                    $(path+" #"+field_name).val(field_value); 
                }
              break;                                                      
            }        

        }
        
    },
    ajax_req: function(data , action , async , callback){
        $("#ajax_loader").show();
        $.ajax({
            type: "POST",
            url: '/admin/'+action+'/',
            cache: false,
            data: data,  
            async: async, 
            success: function (result) {
                callback(result);
            },
            beforeSend: function () {
                $('#ajax_loader').show();
            },
            complete: function (result) {
                $('#ajax_loader').hide();
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }    
};

$(document).ready(function () { 
    
});    