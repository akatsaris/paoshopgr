SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `eshop` DEFAULT CHARACTER SET utf8 ;
USE `eshop` ;

-- -----------------------------------------------------
-- Table `eshop`.`dpge_sessions`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_sessions` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_sessions` (
  `id` CHAR(32) NOT NULL ,
  `modified` INT NOT NULL ,
  `lifetime` INT NOT NULL ,
  `data` TEXT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_useraccounts`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_useraccounts` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_useraccounts` (
  `user_id` INT NOT NULL AUTO_INCREMENT ,
  `username` VARCHAR(145) NOT NULL ,
  `password` VARCHAR(245) NOT NULL ,
  `role` VARCHAR(45) NOT NULL ,
  `firstname` VARCHAR(145) NULL ,
  `lastname` VARCHAR(145) NULL ,
  PRIMARY KEY (`user_id`) ,
  UNIQUE INDEX `user_id_UNIQUE` (`user_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_menus`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_menus` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_menus` (
  `menu_id` INT NOT NULL AUTO_INCREMENT ,
  `menu_name` VARCHAR(145) NOT NULL ,
  `menu_position_name` VARCHAR(145) NULL ,
  PRIMARY KEY (`menu_id`) ,
  UNIQUE INDEX `menu_id_UNIQUE` (`menu_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_menu_data`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_menu_data` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_menu_data` (
  `menu_id` INT NOT NULL ,
  `menu_data_id` INT NOT NULL AUTO_INCREMENT ,
  `parrent_id` INT NOT NULL DEFAULT 0 ,
  `type` VARCHAR(145) NOT NULL ,
  `order` INT NULL ,
  `author` INT NULL ,
  PRIMARY KEY (`menu_data_id`) ,
  UNIQUE INDEX `menu_data_id_UNIQUE` (`menu_data_id` ASC) ,
  INDEX `fk_menu_id_idx` (`menu_id` ASC) ,
  CONSTRAINT `fk_menu_id`
    FOREIGN KEY (`menu_id` )
    REFERENCES `eshop`.`dpge_menus` (`menu_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_menu_data_multilang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_menu_data_multilang` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_menu_data_multilang` (
  `menu_data_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `menu_data_id` INT NOT NULL ,
  `author` INT NULL ,
  `la` INT NULL ,
  `title` VARCHAR(255) NULL ,
  `url` VARCHAR(245) NULL ,
  `active` TINYINT(1) NULL ,
  PRIMARY KEY (`menu_data_multilang_id`) ,
  UNIQUE INDEX `menu_data_id_UNIQUE` (`menu_data_multilang_id` ASC) ,
  INDEX `fk_menu_data_id_idx` (`menu_data_id` ASC) ,
  CONSTRAINT `fk_menu_data_id`
    FOREIGN KEY (`menu_data_id` )
    REFERENCES `eshop`.`dpge_menu_data` (`menu_data_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_languages`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_languages` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_languages` (
  `lang_id` INT NOT NULL AUTO_INCREMENT ,
  `digits` VARCHAR(45) NOT NULL ,
  `name` VARCHAR(45) NOT NULL ,
  `default` TINYINT(1) NULL ,
  `shortcuts` CHAR(4) NULL ,
  PRIMARY KEY (`lang_id`) ,
  UNIQUE INDEX `lang_id_UNIQUE` (`lang_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_currencies`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_currencies` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_currencies` (
  `currency_id` INT NOT NULL AUTO_INCREMENT ,
  `currency_name` VARCHAR(45) NULL ,
  `currency_symbol` VARCHAR(45) NULL ,
  PRIMARY KEY (`currency_id`) ,
  UNIQUE INDEX `currency_id_UNIQUE` (`currency_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_clients`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_clients` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_clients` (
  `client_id` INT NOT NULL AUTO_INCREMENT ,
  `firtname` VARCHAR(75) NULL ,
  `lastname` VARCHAR(75) NULL ,
  `username` VARCHAR(75) NULL ,
  `password` VARCHAR(75) NULL ,
  `birthdate` DATE NULL ,
  `gender` TINYINT(1) NULL ,
  `country_id` INT NULL ,
  `city_id` INT NULL ,
  `currency_id` INT NULL ,
  PRIMARY KEY (`client_id`) ,
  UNIQUE INDEX `client_id_UNIQUE` (`client_id` ASC) ,
  INDEX `fk_currency_id_idx` (`currency_id` ASC) ,
  CONSTRAINT `fk_currency_id`
    FOREIGN KEY (`currency_id` )
    REFERENCES `eshop`.`dpge_currencies` (`currency_id` )
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_areas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_areas` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_areas` (
  `area_id` INT NOT NULL AUTO_INCREMENT ,
  `area_parent_id` INT NULL DEFAULT 0 ,
  `area_code` VARCHAR(45) NULL ,
  `area_name` VARCHAR(45) NULL ,
  `area_type` TINYINT NULL ,
  PRIMARY KEY (`area_id`) ,
  UNIQUE INDEX `area_id_UNIQUE` (`area_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_links_plugin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_links_plugin` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_links_plugin` (
  `menu_data_multilang_id` INT NOT NULL ,
  `link` VARCHAR(245) NULL ,
  `target` VARCHAR(45) NULL ,
  PRIMARY KEY (`menu_data_multilang_id`) ,
  INDEX `fk1_menu_data_multilang_id_idx` (`menu_data_multilang_id` ASC) ,
  UNIQUE INDEX `menu_data_multilang_id_UNIQUE` (`menu_data_multilang_id` ASC) ,
  CONSTRAINT `fk1_menu_data_multilang_id`
    FOREIGN KEY (`menu_data_multilang_id` )
    REFERENCES `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_content_plugin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_content_plugin` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_content_plugin` (
  `menu_data_multilang_id` INT NOT NULL ,
  `title` VARCHAR(245) NULL ,
  `subtitle` VARCHAR(245) NULL ,
  `seo` VARCHAR(245) NULL ,
  `s_desc` TEXT NULL ,
  `f_desc` TEXT NULL ,
  `google_keywords` VARCHAR(245) NULL ,
  `google_desc` VARCHAR(245) NULL ,
  `active` TINYINT(1) NULL DEFAULT 0 ,
  PRIMARY KEY (`menu_data_multilang_id`) ,
  INDEX `fk2_menu_data_multilang_id_idx` (`menu_data_multilang_id` ASC) ,
  UNIQUE INDEX `menu_data_multilang_id_UNIQUE` (`menu_data_multilang_id` ASC) ,
  CONSTRAINT `fk2_menu_data_multilang_id`
    FOREIGN KEY (`menu_data_multilang_id` )
    REFERENCES `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_tags` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_tags` (
  `tag_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `tag_id` INT NOT NULL DEFAULT 0 ,
  `la` INT NOT NULL ,
  `tag` VARCHAR(245) NOT NULL ,
  PRIMARY KEY (`tag_multilang_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_attributes_groups`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_attributes_groups` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_attributes_groups` (
  `attr_group_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `group_id` INT NOT NULL ,
  `la` INT NULL ,
  `group_name` VARCHAR(245) NULL ,
  PRIMARY KEY (`attr_group_multilang_id`) ,
  UNIQUE INDEX `attr_group_multilang_id_UNIQUE` (`attr_group_multilang_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_attributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_attributes` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_attributes` (
  `attr_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `attr_id` INT NOT NULL ,
  `la` INT NULL ,
  `attr_group_id` INT NULL ,
  `attr_value` VARCHAR(245) NULL ,
  PRIMARY KEY (`attr_multilang_id`) ,
  UNIQUE INDEX `attr_multilang_id_UNIQUE` (`attr_multilang_id` ASC) ,
  INDEX `fk_group_id_idx` (`attr_group_id` ASC) ,
  CONSTRAINT `fk_group_id`
    FOREIGN KEY (`attr_group_id` )
    REFERENCES `eshop`.`dpge_attributes_groups` (`attr_group_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_home_sections`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_home_sections` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_home_sections` (
  `home_section_id` INT NOT NULL AUTO_INCREMENT ,
  `home_tag` VARCHAR(245) NULL ,
  `name` VARCHAR(245) NULL ,
  PRIMARY KEY (`home_section_id`) ,
  UNIQUE INDEX `home_section_id_UNIQUE` (`home_section_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_products`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_products` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_products` (
  `product_id` INT NOT NULL AUTO_INCREMENT ,
  `home_section_id` INT NULL COMMENT '	' ,
  `product_code` VARCHAR(80) NULL ,
  `registered_users_only` TINYINT(1) NULL DEFAULT 0 ,
  `active` TINYINT(1) NULL DEFAULT 0 ,
  `start_price` INT NULL ,
  `end_price` INT NULL ,
  `weight` INT NULL ,
  PRIMARY KEY (`product_id`) ,
  UNIQUE INDEX `product_id_UNIQUE` (`product_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_products_multilang`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_products_multilang` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_products_multilang` (
  `product_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `product_id` INT NOT NULL ,
  `la` INT NOT NULL ,
  `name` VARCHAR(245) NULL ,
  `prd_description` TEXT NULL ,
  `prd_content` TEXT NULL ,
  `keywords` TEXT NULL ,
  `main_photo` VARCHAR(245) NULL ,
  `friendly_url` VARCHAR(245) NULL ,
  PRIMARY KEY (`product_multilang_id`) ,
  UNIQUE INDEX `product_multilang_id_UNIQUE` (`product_multilang_id` ASC) ,
  INDEX `fk_product_id_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_product_id`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_product_tags`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_product_tags` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_product_tags` (
  `prd_tag_id` INT NOT NULL AUTO_INCREMENT ,
  `product_id` INT NOT NULL ,
  `tag_id` INT NOT NULL ,
  PRIMARY KEY (`prd_tag_id`) ,
  UNIQUE INDEX `prd_tag_id_UNIQUE` (`prd_tag_id` ASC) ,
  INDEX `fk_product_tags_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_product_tags`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_product_attributes`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_product_attributes` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_product_attributes` (
  `prd_attr_id` INT NOT NULL AUTO_INCREMENT ,
  `product_id` INT NOT NULL ,
  `attr_id` INT NOT NULL ,
  PRIMARY KEY (`prd_attr_id`) ,
  UNIQUE INDEX `prd_attr_id_UNIQUE` (`prd_attr_id` ASC) ,
  INDEX `fk_prd_attr_id_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_prd_attr_id`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_gallery_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_gallery_types` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_gallery_types` (
  `gallery_type_id` INT NOT NULL AUTO_INCREMENT ,
  `gall_name` VARCHAR(245) NULL ,
  `gall_width` INT NULL ,
  `gall_height` INT NULL ,
  `how_many_items` INT NULL DEFAULT 3 ,
  PRIMARY KEY (`gallery_type_id`) ,
  UNIQUE INDEX `gallery_type_id_UNIQUE` (`gallery_type_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_product_gallery_files`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_product_gallery_files` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_product_gallery_files` (
  `photo_id` INT NOT NULL AUTO_INCREMENT ,
  `gallery_id` INT NULL ,
  `product_id` INT NULL ,
  `photo_path` VARCHAR(245) NULL ,
  `order` INT NULL ,
  PRIMARY KEY (`photo_id`) ,
  UNIQUE INDEX `photo_id_UNIQUE` (`photo_id` ASC) ,
  INDEX `fk_product_id_gallery_idx` (`product_id` ASC) ,
  INDEX `fk_gallery_id_idx` (`gallery_id` ASC) ,
  CONSTRAINT `fk_product_id_gallery`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_gallery_id`
    FOREIGN KEY (`gallery_id` )
    REFERENCES `eshop`.`dpge_gallery_types` (`gallery_type_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_stock_control`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_stock_control` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_stock_control` (
  `stock_id` INT NOT NULL AUTO_INCREMENT ,
  `product_id` INT NULL ,
  `code` VARCHAR(145) NULL ,
  `extra_cost` INT NULL ,
  `quantity` INT NULL ,
  PRIMARY KEY (`stock_id`) ,
  UNIQUE INDEX `stock_id_UNIQUE` (`stock_id` ASC) ,
  INDEX `fk_product_id_stock_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_product_id_stock`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_tags_plugin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_tags_plugin` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_tags_plugin` (
  `menu_data_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `tags` VARCHAR(245) NULL ,
  PRIMARY KEY (`menu_data_multilang_id`) ,
  UNIQUE INDEX `menu_data_multilang_id_UNIQUE` (`menu_data_multilang_id` ASC) ,
  INDEX `fk3_menu_data_multilang_id_idx` (`menu_data_multilang_id` ASC) ,
  CONSTRAINT `fk3_menu_data_multilang_id`
    FOREIGN KEY (`menu_data_multilang_id` )
    REFERENCES `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_templates`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_templates` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_templates` (
  `template_id` INT NOT NULL AUTO_INCREMENT ,
  `template_name` VARCHAR(45) NULL ,
  `template_file` VARCHAR(245) NULL ,
  PRIMARY KEY (`template_id`) ,
  UNIQUE INDEX `template_id_UNIQUE` (`template_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_template_sections`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_template_sections` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_template_sections` (
  `template_section_id` INT NOT NULL AUTO_INCREMENT ,
  `template_id` INT NULL ,
  `section_name` VARCHAR(245) NULL ,
  `section_file` VARCHAR(245) NULL ,
  `section_color` VARCHAR(45) NULL ,
  PRIMARY KEY (`template_section_id`) ,
  UNIQUE INDEX `template_section_id_UNIQUE` (`template_section_id` ASC) ,
  INDEX `fk1_template_id_idx` (`template_id` ASC) ,
  CONSTRAINT `fk1_template_id`
    FOREIGN KEY (`template_id` )
    REFERENCES `eshop`.`dpge_templates` (`template_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_template_view`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_template_view` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_template_view` (
  `template_view_id` INT NOT NULL AUTO_INCREMENT ,
  `template_id` INT NULL ,
  `template_name` VARCHAR(145) NULL ,
  PRIMARY KEY (`template_view_id`) ,
  UNIQUE INDEX `template_view_id_UNIQUE` (`template_view_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_widgets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_widgets` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_widgets` (
  `widget_id` INT NOT NULL AUTO_INCREMENT ,
  `widget_name` VARCHAR(145) NULL ,
  `widget` VARCHAR(145) NULL ,
  `params` VARCHAR(245) NULL ,
  `html` TEXT NULL ,
  PRIMARY KEY (`widget_id`) ,
  UNIQUE INDEX `widget_id_UNIQUE` (`widget_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_template_widgets`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_template_widgets` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_template_widgets` (
  `template_widget_id` INT NOT NULL AUTO_INCREMENT ,
  `template_view_id` INT NULL ,
  `template_section_id` INT NULL ,
  `widget_id` INT NULL ,
  PRIMARY KEY (`template_widget_id`) ,
  UNIQUE INDEX `widget_id_UNIQUE` (`template_widget_id` ASC) ,
  INDEX `fk_template_view_id_idx` (`template_view_id` ASC) ,
  INDEX `fk_template_section_id_idx` (`template_section_id` ASC) ,
  INDEX `fk_widget_id_idx` (`widget_id` ASC) ,
  CONSTRAINT `fk_template_view_id`
    FOREIGN KEY (`template_view_id` )
    REFERENCES `eshop`.`dpge_template_view` (`template_view_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_section_id`
    FOREIGN KEY (`template_section_id` )
    REFERENCES `eshop`.`dpge_template_sections` (`template_section_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_widget_id`
    FOREIGN KEY (`widget_id` )
    REFERENCES `eshop`.`dpge_widgets` (`widget_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_multilanguage_html_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_multilanguage_html_content` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_multilanguage_html_content` (
  `multilanguage_html_content_id` INT NOT NULL AUTO_INCREMENT ,
  `content_id` INT NULL ,
  `name` VARCHAR(245) NULL ,
  `html_content` TEXT NULL ,
  PRIMARY KEY (`multilanguage_html_content_id`) ,
  UNIQUE INDEX `multilanguage_html_content_id_UNIQUE` (`multilanguage_html_content_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_accordeon_plugin`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_accordeon_plugin` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_accordeon_plugin` (
  `menu_data_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `acc_title` VARCHAR(245) NULL ,
  `acc_content` TEXT NULL ,
  `acc_accordeon_content` TEXT NULL ,
  PRIMARY KEY (`menu_data_multilang_id`) ,
  UNIQUE INDEX `menu_data_multilang_id_UNIQUE` (`menu_data_multilang_id` ASC) ,
  INDEX `fk4_menu_data_multilang_id_idx` (`menu_data_multilang_id` ASC) ,
  CONSTRAINT `fk4_menu_data_multilang_id`
    FOREIGN KEY (`menu_data_multilang_id` )
    REFERENCES `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_simple_content`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_simple_content` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_simple_content` (
  `cnt_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `cnt_id` INT NULL ,
  `cnt_title` VARCHAR(245) NULL ,
  `cnt_desc` TEXT NULL ,
  `cnt_full_content` TEXT NULL ,
  `keywords` VARCHAR(245) NULL ,
  `google_desc` VARCHAR(245) NULL ,
  `tags` VARCHAR(245) NULL ,
  `photo` VARCHAR(245) NULL ,
  `active` TINYINT(1) NULL ,
  `section` INT NULL ,
  PRIMARY KEY (`cnt_multilang_id`) ,
  UNIQUE INDEX `cnt_multilang_id_UNIQUE` (`cnt_multilang_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_cart`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_cart` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_cart` (
  `cart_id` INT NOT NULL AUTO_INCREMENT ,
  `session_id` VARCHAR(45) NULL ,
  `product_id` INT NULL ,
  `quantity` INT NULL ,
  `product_attributes` VARCHAR(45) NULL ,
  `currency_id` INT NULL ,
  `datetime` DATETIME NULL ,
  PRIMARY KEY (`cart_id`) ,
  UNIQUE INDEX `cart_id_UNIQUE` (`cart_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_cart_shipping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_cart_shipping` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_cart_shipping` (
  `cs_id` INT NOT NULL AUTO_INCREMENT ,
  `session_id` VARCHAR(45) NULL ,
  `user_id` INT NULL ,
  `gift` INT NULL ,
  `firstname` VARCHAR(145) NULL ,
  `lastname` VARCHAR(145) NULL ,
  `email` VARCHAR(145) NULL ,
  `address` VARCHAR(145) NULL ,
  `country_id` INT NULL ,
  `city_id` INT NULL ,
  `gift_wrapping` TINYINT(1) NULL ,
  `gift_wrapping_cost` INT NULL ,
  `vat` INT NULL ,
  `doy` VARCHAR(45) NULL ,
  `company_name` VARCHAR(145) NULL ,
  PRIMARY KEY (`cs_id`) ,
  UNIQUE INDEX `cs_id_UNIQUE` (`cs_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_product_accompaying_relation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_product_accompaying_relation` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_product_accompaying_relation` (
  `acc_id` INT NOT NULL AUTO_INCREMENT ,
  `product_id` INT NULL ,
  `tags` VARCHAR(245) NULL ,
  `title` VARCHAR(45) NULL ,
  PRIMARY KEY (`acc_id`) ,
  UNIQUE INDEX `acc_id_UNIQUE` (`acc_id` ASC) ,
  INDEX `fk_acc_product_id_idx` (`product_id` ASC) ,
  CONSTRAINT `fk_acc_product_id`
    FOREIGN KEY (`product_id` )
    REFERENCES `eshop`.`dpge_products` (`product_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_shipping_areas`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_shipping_areas` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_shipping_areas` (
  `shipping_area_id` INT NOT NULL AUTO_INCREMENT ,
  `shipping_pack_id` INT(11) NULL ,
  `area_id` INT(11) NULL ,
  PRIMARY KEY (`shipping_area_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_shipping_providers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_shipping_providers` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_shipping_providers` (
  `shipping_provider_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `shipping_provider_id` INT NULL ,
  `la` INT(11) NULL ,
  `shipping_provider_name` VARCHAR(100) NULL ,
  PRIMARY KEY (`shipping_provider_multilang_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_shipping_packs`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_shipping_packs` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_shipping_packs` (
  `shipping_packet_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `shipping_packet_id` VARCHAR(45) NULL ,
  `shipping_provider_id` INT(11) NULL ,
  `shipping_pack_id` INT(11) NULL ,
  `la` INT(11) NULL ,
  `shipping_pack_name` VARCHAR(50) NULL ,
  `shipping_pack_description` VARCHAR(255) NULL ,
  `shipping_pack_paymethod` VARCHAR(30) NULL ,
  `shipping_pack_cod_val` DECIMAL(11,2) NULL ,
  `shipping_pack_maxkg` DECIMAL(11,2) NULL ,
  `shipping_pack_extraweight` DECIMAL(11,2) NULL ,
  `shipping_pack_extraweight_price` DECIMAL(11,2) NULL ,
  PRIMARY KEY (`shipping_packet_multilang_id`) ,
  INDEX `fk1_provider_id` (`shipping_provider_id` ASC) ,
  CONSTRAINT `fk1_provider_id`
    FOREIGN KEY (`shipping_provider_id` )
    REFERENCES `eshop`.`dpge_shipping_providers` (`shipping_provider_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_shipping_pack_kg`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_shipping_pack_kg` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_shipping_pack_kg` (
  `shipping_pack_kg_id` INT NOT NULL AUTO_INCREMENT ,
  `shipping_pack_id` INT NULL ,
  `min_kg` DECIMAL(11,3) NULL ,
  `max_kg` DECIMAL(11,3) NULL ,
  `shipping_price` DECIMAL(11,2) NULL ,
  PRIMARY KEY (`shipping_pack_kg_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_pay_methods`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_pay_methods` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_pay_methods` (
  `pay_method_multilang_id_id` INT NOT NULL AUTO_INCREMENT ,
  `pay_method_id` INT NULL ,
  `la` INT NULL ,
  `pay_name` VARCHAR(45) NULL ,
  `pay_description` VARCHAR(255) NULL ,
  `pay_notification` VARCHAR(255) NULL ,
  `pay_receipt_comment` VARCHAR(255) NULL ,
  PRIMARY KEY (`pay_method_multilang_id_id`) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_area_types`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_area_types` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_area_types` (
  `area_type_multilang_id` INT NOT NULL AUTO_INCREMENT ,
  `area_type_id` INT NULL ,
  `area_type_name` VARCHAR(45) NULL ,
  `la` INT NULL ,
  PRIMARY KEY (`area_type_multilang_id`) ,
  UNIQUE INDEX `area_type_multilang_id_UNIQUE` (`area_type_multilang_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_shipping_payment_rel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_shipping_payment_rel` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_shipping_payment_rel` (
  `sp_rel_id` INT NOT NULL AUTO_INCREMENT ,
  `shipping_packet_id` INT NULL ,
  `pay_method_id` INT NULL ,
  PRIMARY KEY (`sp_rel_id`) ,
  UNIQUE INDEX `sp_rel_id_UNIQUE` (`sp_rel_id` ASC) ,
  INDEX `fk2_shipping_packet_id` (`shipping_packet_id` ASC) ,
  INDEX `fk1_pay_method_id` (`pay_method_id` ASC) ,
  CONSTRAINT `fk2_shipping_packet_id`
    FOREIGN KEY (`shipping_packet_id` )
    REFERENCES `eshop`.`dpge_shipping_packs` (`shipping_packet_multilang_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk1_pay_method_id`
    FOREIGN KEY (`pay_method_id` )
    REFERENCES `eshop`.`dpge_pay_methods` (`pay_method_multilang_id_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_orders`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_orders` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_orders` (
  `order_id` INT NOT NULL AUTO_INCREMENT ,
  `order_number_id` INT NULL ,
  `order_date` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ,
  `session_id` VARCHAR(45) NULL ,
  `user_id` INT NULL ,
  `order_status_id` INT NULL ,
  `order_shipping` DECIMAL(15,2) NULL ,
  `order_price` DECIMAL(15,2) NULL ,
  `order_voucher_id` INT NULL ,
  `order_voucher_value` DECIMAL(15,2) NULL ,
  `invoice` TINYINT NULL ,
  `order_paid` TINYINT NULL ,
  `order_comment` TEXT NULL ,
  PRIMARY KEY (`order_id`) ,
  UNIQUE INDEX `order_id_UNIQUE` (`order_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_order_items`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_order_items` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_order_items` (
  `order_item_id` INT NOT NULL AUTO_INCREMENT ,
  `order_id` INT NULL ,
  `product_id` INT NULL ,
  `parent_product_id` INT NULL DEFAULT 0 ,
  `quantity` INT NULL ,
  `price` DECIMAL(15,2) NULL ,
  `accompaying` TINYINT NULL ,
  PRIMARY KEY (`order_item_id`) ,
  UNIQUE INDEX `order_item_id_UNIQUE` (`order_item_id` ASC) ,
  INDEX `fk_ord_id` (`order_id` ASC) ,
  CONSTRAINT `fk_ord_id`
    FOREIGN KEY (`order_id` )
    REFERENCES `eshop`.`dpge_orders` (`order_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_order_shipping`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_order_shipping` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_order_shipping` (
  `os_id` INT NOT NULL AUTO_INCREMENT ,
  `order_id` INT NULL ,
  `user_id` INT NULL ,
  `gift` TINYINT NULL ,
  `gift_wrapping_cost` DECIMAL(15,2) NULL ,
  `firstname` VARCHAR(245) NULL ,
  `lastname` VARCHAR(245) NULL ,
  `email` VARCHAR(245) NULL ,
  `address` VARCHAR(245) NULL ,
  `address_more` VARCHAR(245) NULL ,
  `city_name` VARCHAR(245) NULL ,
  `country_code` VARCHAR(5) NULL ,
  `region_id` INT NULL ,
  `postal` INT NULL ,
  `doy` VARCHAR(45) NULL ,
  `vat` INT NULL ,
  `company_name` VARCHAR(245) NULL ,
  `phone` VARCHAR(45) NULL ,
  `invoice_address` VARCHAR(245) NULL ,
  `invoice_company_name` VARCHAR(245) NULL ,
  `invoice_profession` VARCHAR(245) NULL ,
  `invoice_type` TINYINT NULL ,
  `timestamp` TIMESTAMP NULL ,
  PRIMARY KEY (`os_id`) ,
  UNIQUE INDEX `os_id_UNIQUE` (`os_id` ASC) ,
  INDEX `fk2_ord_id` (`order_id` ASC) ,
  CONSTRAINT `fk2_ord_id`
    FOREIGN KEY (`order_id` )
    REFERENCES `eshop`.`dpge_orders` (`order_id` )
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_order_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_order_status` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_order_status` (
  `ord_status_id` INT NOT NULL AUTO_INCREMENT ,
  `ord_status` VARCHAR(145) NULL ,
  `ord_email_template` VARCHAR(145) NULL ,
  PRIMARY KEY (`ord_status_id`) ,
  UNIQUE INDEX `ord_status_id_UNIQUE` (`ord_status_id` ASC) )
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `eshop`.`dpge_vouchers`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `eshop`.`dpge_vouchers` ;

CREATE  TABLE IF NOT EXISTS `eshop`.`dpge_vouchers` (
  `v_id` INT NOT NULL AUTO_INCREMENT ,
  `v_code` VARCHAR(45) NULL ,
  `user_id` INT NULL COMMENT '	' ,
  `price` DECIMAL(15,2) NULL ,
  `percent` VARCHAR(45) NULL ,
  `expiration` DATETIME NULL ,
  `used` TINYINT NULL ,
  PRIMARY KEY (`v_id`) ,
  UNIQUE INDEX `v_id_UNIQUE` (`v_id` ASC) )
ENGINE = InnoDB;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_useraccounts`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_useraccounts` (`user_id`, `username`, `password`, `role`, `firstname`, `lastname`) VALUES (1, 'akatsaris@gmail.com', 'a87b2aaf923eeb056925220118a8e690', 'administrator', 'Angelos', 'Katsaris');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_menus`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_menus` (`menu_id`, `menu_name`, `menu_position_name`) VALUES (1, 'top menu', 'top_menu');
INSERT INTO `eshop`.`dpge_menus` (`menu_id`, `menu_name`, `menu_position_name`) VALUES (2, 'footer menu', 'footer_menu');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_menu_data`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 1, 0, 'link', 1, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 2, 0, 'link', 2, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 3, 2, 'link', 1, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 4, 2, 'link', 2, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 5, 0, 'link', 3, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (1, 6, 0, 'emailform', 4, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (2, 7, 0, 'link', 1, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (2, 8, 0, 'link', 2, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (2, 9, 0, 'link', 3, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (2, 10, 9, 'link', 1, 1);
INSERT INTO `eshop`.`dpge_menu_data` (`menu_id`, `menu_data_id`, `parrent_id`, `type`, `order`, `author`) VALUES (2, 11, 9, 'emailform', 2, 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_menu_data_multilang`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (1, 1, 1, 1, 'Αρχική σελίδα', 'home', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (2, 2, 1, 1, 'Προιοντα', 'products', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (3, 3, 1, 1, 'Προσφορές', 'offers', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (4, 4, 1, 1, 'Νεα προιοντα', 'new-products', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (5, 5, 1, 1, 'Καλάθι', 'at-cart', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (6, 6, 1, 1, 'Επικοινωνια', 'contact-form', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (7, 7, 1, 1, 'Η εταιρία', 'the-company', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (8, 8, 1, 1, 'Σχετικα με εμας', 'about-us', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (9, 9, 1, 1, 'Υποστήριξη', 'support', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (10, 10, 1, 1, 'On line υποστήριξη', 'online-support', 1);
INSERT INTO `eshop`.`dpge_menu_data_multilang` (`menu_data_multilang_id`, `menu_data_id`, `author`, `la`, `title`, `url`, `active`) VALUES (11, 11, 1, 1, 'Φόρμα επικοινωνίας', 'contact-form', 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_languages`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_languages` (`lang_id`, `digits`, `name`, `default`, `shortcuts`) VALUES (1, 'el_GR', 'Greek', 1, 'el');
INSERT INTO `eshop`.`dpge_languages` (`lang_id`, `digits`, `name`, `default`, `shortcuts`) VALUES (2, 'en_US', 'English', 0, 'en');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_currencies`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_currencies` (`currency_id`, `currency_name`, `currency_symbol`) VALUES (1, 'euro', '&euro;');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_tags`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_tags` (`tag_multilang_id`, `tag_id`, `la`, `tag`) VALUES (1, 1, 1, 'Αλβέρτης');
INSERT INTO `eshop`.`dpge_tags` (`tag_multilang_id`, `tag_id`, `la`, `tag`) VALUES (2, 1, 2, 'Alvertis');
INSERT INTO `eshop`.`dpge_tags` (`tag_multilang_id`, `tag_id`, `la`, `tag`) VALUES (3, 3, 1, 'Παναθηναικος');
INSERT INTO `eshop`.`dpge_tags` (`tag_multilang_id`, `tag_id`, `la`, `tag`) VALUES (4, 4, 1, 'ΡΕΑΛ');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_gallery_types`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_gallery_types` (`gallery_type_id`, `gall_name`, `gall_width`, `gall_height`, `how_many_items`) VALUES (1, 'default', 600, 400, 3);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_product_gallery_files`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_product_gallery_files` (`photo_id`, `gallery_id`, `product_id`, `photo_path`, `order`) VALUES (1, 1, 1, '/temp/image.jpg', 1);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_templates`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_templates` (`template_id`, `template_name`, `template_file`) VALUES (1, 'homepage', 'homepage.phtml');

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_template_sections`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_template_sections` (`template_section_id`, `template_id`, `section_name`, `section_file`, `section_color`) VALUES (1, 1, 'sidebar', 'sidebar.phtml', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_widgets`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_widgets` (`widget_id`, `widget_name`, `widget`, `params`, `html`) VALUES (1, 'left menu', 'menu', 'id:1', NULL);

COMMIT;

-- -----------------------------------------------------
-- Data for table `eshop`.`dpge_template_widgets`
-- -----------------------------------------------------
START TRANSACTION;
USE `eshop`;
INSERT INTO `eshop`.`dpge_template_widgets` (`template_widget_id`, `template_view_id`, `template_section_id`, `widget_id`) VALUES (1, 1, 1, 1);

COMMIT;
