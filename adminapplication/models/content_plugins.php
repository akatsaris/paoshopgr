<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class content_plugins 
{
    /**
     * the database connection object
     * @var $db object 
     */
	protected $db;
	/**
	* Plugins list
	* @var $pluginslist array
	*/
	public $pluginslist;

	/**
	* Constructor
	*/
	public function __construct(){
		//Get db connection
        $this->db = Zend_Db_Table::getDefaultAdapter();
        //set plugins list
        $this->readPluginsXML();
	}
	/**
	* Read plugins xml
	*/
	protected function readPluginsXML(){
		$xmlUrl = APPLICATION_PATH."/content_plugins/plugins.xml"; 
		$this->pluginslist = $this->readxml($xmlUrl);
	}
	/**
	* Read plugin xml file
	*/
	public function readPluginXML(){

	}
	/**
	* Read xml procedure
	*/
	protected function readxml($file_url){
        $xmlStr = file_get_contents($file_url);
        $xmlObj = simplexml_load_string($xmlStr);
        $arrXml = Globals::objectsIntoArray($xmlObj);
        return $arrXml;
	}


}