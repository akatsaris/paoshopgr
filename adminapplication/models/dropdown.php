<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class dropdown 
{

	public function getAttrGroups(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_attributes_groups' , array('group_id','group_name'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_attributes_groups.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['group_id']] = $value['group_name'];
		}
		return $dropdown;		
	}

	public function getTags(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_tags' , array('tag_id','tag'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_tags.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['tag_id']] = $value['tag'];
		}
		return $dropdown;		
	}	

	public function getHomeSections(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_home_sections' , array('home_section_id','name'));

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['home_section_id']] = $value['name'];
		}
		return $dropdown;		
	}	

	public function getGalleriesTypes(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_gallery_types' , array('gallery_type_id','gall_name'));

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['gallery_type_id']] = $value['gall_name'];
		}
		return $dropdown;		
	}

	public function getFileTemplates(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_templates' , array('template_id','template_name'));

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['template_id']] = $value['template_name'];
		}
		return $dropdown;		
	}

	public function getGroupAttributes(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_attributes_groups');
		$select->join(dbprefix.'_attributes' , dbprefix.'_attributes_groups.group_id = '.dbprefix.'_attributes.attr_group_id' , array('attr_value' , 'attr_id'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_attributes.la' , array('shortcuts'));
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);
		$select->group(dbprefix.'_attributes.attr_value');
		$select->order(dbprefix.'_attributes.attr_group_id');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[$value['group_name']][] = array('group_id' => $value['group_id'] , 'group_name' => $value['group_name'] , 'attr_value' => $value['attr_value'], 'attr_id' => $value['attr_id']);

		}
		return $dropdown;		
	}

	public function getViews(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_template_view' , array('template_view_id','template_name'));

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['template_view_id']] = $value['template_name'];
		}
		return $dropdown;		
	}

	public function getWidgets(){
	    $dirs = array_filter(glob('../application/models/widgets/*'), 'is_dir');
	    foreach ($dirs as $key => $value) {
	    	$widget_dir = explode("/", $value);
	    	$dropdown[][$widget_dir[4]] = $widget_dir[4];
	    }
	    
	    return $dropdown;
	}

	public function getMerchants(){
	    $dirs = array_filter(glob('../application/models/merchants/*'), 'is_dir');
	    foreach ($dirs as $key => $value) {
	    	$widget_dir = explode("/", $value);
	    	$dropdown[][$widget_dir[4]] = $widget_dir[4];
	    }
	    
	    return $dropdown;
	}	

	public function getProductViews(){
	    $dirs = array_filter(glob('../application/views/scripts/partial_products/*'), 'is_file');
	    foreach ($dirs as $key => $value) {
	    	$products_dir = explode("/", $value);
	    	$dropdown[][$products_dir[5]] = $products_dir[5];
	    }
	    
	    return $dropdown;
	}

	public function getGalleryViews(){
	    $dirs = array_filter(glob('../application/views/scripts/partial_galleries/*'), 'is_file');
	    foreach ($dirs as $key => $value) {
	    	$galleries_dir = explode("/", $value);
	    	$dropdown[][$galleries_dir[5]] = $galleries_dir[5];
	    }
	    
	    return $dropdown;
	}

	public function getShippingPacks(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_shipping_packs' , array('shipping_packet_id','shipping_pack_name'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_shipping_packs.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['shipping_packet_id']] = $value['shipping_pack_name'];
		}
		return $dropdown;
	}

	public function getShipingProviders(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_shipping_providers' , array('shipping_provider_id','shipping_provider_name'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_shipping_providers.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['shipping_provider_id']] = $value['shipping_provider_name'];
		}
		return $dropdown;
	}	

	public function getShippingAreas(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_areas' , array('area_id','area_name'));

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['area_id']] = $value['area_name'];
		}
		return $dropdown;
	}

	public function getPayMethods(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_pay_methods' , array('pay_method_id','pay_name'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_pay_methods.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['pay_method_id']] = $value['pay_name'];
		}
		return $dropdown;		
	}

	public function getEmailTemplates(){
	    $dirs = array_filter(glob('../application/views/scripts/email_templates/*'), 'is_file');
	    foreach ($dirs as $key => $value) {
	    	$galleries_dir = explode("/", $value);
	    	$dropdown[][$galleries_dir[5]] = $galleries_dir[5];
	    }
	    
	    return $dropdown;		
	}

	public function getOrderStatus(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_order_status' , array('ord_status_id','ord_status'));
		$select->order('ord_status');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$dropdown[][$value['ord_status_id']] = $value['ord_status'];
		}
		return $dropdown;		
	}

}	