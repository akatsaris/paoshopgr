<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class dpgeshop_acl extends Zend_Acl {
  /**
   * Roles configuration array (/config/acl_config.php)
   * @var array 
   */ 
  private $_acl = array();
  /**
   * Constructor
   * Adding Roles to Coursevault application 
   */  
  public function __construct() {  
    $this->_acl = Zend_Registry::get('acl');
    $this->_addRoles();
    $this->_addResources();
    $this->_addPermitions();
  }
  
  /**
   * Add Roles to Zend_ACL 
   */
  private function _addRoles(){
      foreach ($this->_acl[1] as $role){
        $this->addRole(new Zend_Acl_Role($role));
      }
  }
  
  /**
   * Add Recources to Zend_ACL 
   */
  private function _addResources(){
      foreach (array_keys($this->_acl[0]) as $resource){
        $this->add(new Zend_Acl_Resource($resource));
      }      
  }
  
  /**
   * Add Permitions to Zend_ACL 
   */
  private function _addPermitions(){
      foreach ($this->_acl[0] as $resource => $roles){
          foreach ($roles as $user => $role){
              foreach ($role as $permition){
                $this->allow($user , $resource, $permition); 
                //echo "user->".$user." :: resource->".$resource." :: role->".$permition."<br>";                  
              }
          }
      }
  }
}
?>
