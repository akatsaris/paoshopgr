<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
abstract class plugins 
{
	//abstract protected function getDbData($item_id);
	abstract protected function saveData($data);

	public $error;
	public $error_msg;	
	public $post_data;

	static public function readXML($plugin){
     	$xmlStr = file_get_contents(APPLICATION_PATH.'/content_plugins/'.$plugin.'/plugin.xml');
        $xmlObj = simplexml_load_string($xmlStr);
        $arrXml = Globals::objectsIntoArray($xmlObj);
        return $arrXml;
	}

	static public function getErrorMessage(){
		if($this->error){
			return $this->error_msg;
		}
	}

	public function savePostData($postData){
		$plugin_formid = $this->plugin_options['formid'];
		$plugin_db_table = $this->plugin_options['dbtable'];
		$menu_id = $postData['menuid'];
		$parrent_id = $postData['item_settings']['parrent_id'];
		$template_id = $postData['item_settings']['template_view_id'];
		$active = $postData['item_settings']['active'];
		$order = $postData['item_settings']['order'];
		$register_users_only = $postData['item_settings']['register_users_only'];
		$plugin_id = $postData['item_plugin']['plugin_id'];
		$author = $_SESSION['adminpanel']['author'];

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
		
		//begin transaction for basic settings
		$db->beginTransaction();
		try {
    		//add item to menu data
    		$data = array('menu_id' => $menu_id, 'parrent_id' => $parrent_id , 'template_view_id' => $template_id ,'type' => $plugin_id , 'author' => $author , 'register_users_only' => $register_users_only , 'order' => $order);
    		$db->insert(dbprefix.'_menu_data', $data);
    		$lastInsertId = $db->lastInsertId();

    		unset($data);
			foreach ($postData['item_seo'] as $key => $value) {
				$la_id = Globals::getLangIDbyShortcut($key);
        		$data = array('menu_data_id' => $lastInsertId, 'title' => $value['title'] , 'active' => $active ,'url' => $value['url'] , 'la' => $la_id , 'author' => $author);
        		$db->insert(dbprefix.'_menu_data_multilang', $data);

        		$last_insert_multilang_id = $db->lastInsertId();
        		
        		$pl_data = $postData[$plugin_formid][$key];
        		$pl_data['menu_data_multilang_id'] = $last_insert_multilang_id;
        		$db->insert($plugin_db_table , $pl_data);
			}
		   	 
		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}
	}

	public function updatePostData($postData , $menu_item_id){
		$plugin_formid = $this->plugin_options['formid'];
		$plugin_db_table = $this->plugin_options['dbtable'];
		$menu_id = $postData['menuid'];
		$parrent_id = $postData['item_settings']['parrent_id'];
		$template_id = $postData['item_settings']['template_view_id'];
		$active = $postData['item_settings']['active'];
		$order = $postData['item_settings']['order'];
		$register_users_only = $postData['item_settings']['register_users_only'];
		$author = $_SESSION['adminpanel']['author'];	
		$plugin_id = $postData['plugin_id'];
		
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        //Get Item Data
        $select = $db->select();
        $select->from(dbprefix.'_menu_data_multilang');
        $select->where(dbprefix.'_menu_data_multilang.menu_data_id = (?)', $menu_item_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $item_results = $stmt->fetchAll();        


		//begin transaction for basic settings
		$db->beginTransaction();
		try {
    		//Update Item at menu data
    		$data = array('parrent_id' => $parrent_id , 'template_view_id' => $template_id , 'register_users_only' => $register_users_only,  'author' => $author , 'order' => $order);
    		$db->update(dbprefix.'_menu_data', $data , 'menu_data_id = '.$menu_item_id);

    		unset($data);
			foreach ($postData['item_seo'] as $key => $value) {
				$la_id = Globals::getLangIDbyShortcut($key);
				$lang_exist = Globals::search_array_return_key($item_results, 'la', $la_id);
				
				if(is_int($lang_exist)){
					$item_id = $item_results[$lang_exist]['menu_data_multilang_id'];
					//update
					$data = array('title' => $value['title'] , 'active' => $active ,'url' => $value['url'] , 'la' => $la_id , 'author' => $author);
					$db->update(dbprefix.'_menu_data_multilang', $data , 'menu_data_multilang_id = '.$item_id);

					$pl_data = $postData[$plugin_formid][$key];
        			$db->update($plugin_db_table , $pl_data , 'menu_data_multilang_id = '.$item_id);
				}else{
					//insert
	        		$data = array('menu_data_id' => $menu_item_id, 'title' => $value['title'] , 'active' => $active ,'url' => $value['url'] , 'la' => $la_id , 'author' => $author);
	        		$db->insert(dbprefix.'_menu_data_multilang', $data);

	        		$last_insert_multilang_id = $db->lastInsertId();
	        		
	        		$pl_data = $postData[$plugin_formid][$key];
	        		$pl_data['menu_data_multilang_id'] = $last_insert_multilang_id;
	        		$db->insert($plugin_db_table , $pl_data);	
	        						
				}
				$avail_languages[] = $la_id;
        		
			}
		   	 
		   	//remove deleted languages
		   	$where = $db->quoteInto('la NOT IN (?)', $avail_languages);
		   	$db->delete(dbprefix.'_menu_data_multilang', $where." AND menu_data_id = ".$menu_item_id);

		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}        			
	}

	public function checkMultilangPostData($postData){
		$this->post_data = $postData;
		$plugin_formid = $this->plugin_options['formid'];
		
		$count_plugin_lang = array_keys($postData[$plugin_formid]); 
		$count_seo_lang = array_keys($postData['item_seo']);
		
		if(count($count_plugin_lang) == count($count_seo_lang)){
			$this->error = false;
		}else{
			if(count($count_plugin_lang) < count($count_seo_lang)){
				$this->error_msg = Globals::trl('At the plugin form is missing a language tab');
			}else{
				$this->error_msg = Globals::trl('At the basic form is missing a language tab');
			}
			$this->error = true;			
		}
		return $postData;
	}

}	