<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class Globals
{
    /**
    * @var $_db [db instance]
    */
    private static $_db           = null;
    /**
    * @var $_config
    */    
    private static $_config       = null;
    /**
    * @var $_errorMessage
    */       
    private static $_errorMessage = null;
    /**
    * @var $_langname
    */     
    private static $_langname     = null;
    /**
    * @var $_metatags
    */     
    private static $_metatags     = null;

    private static $_logger       = null;
    
    public function __construct(){
        throw new Zend_Exception('You cannot instantiate Globals.php! It is static only.');
    }

    /**
     * Initialization of database
     * @return $_db instance 
     */
    static public function getDBConnection(){
        if (self::$_db != null) {
            return self::$_db;
        } // if (self::$_db != null)


        $pdoParams = array(PDO::MYSQL_ATTR_DIRECT_QUERY => true);
        self::$_db = Zend_Db::factory('Pdo_Mysql',
                               array ('host'     => self::getConfig()->resources->db->params->host,
                                      'username' => self::getConfig()->resources->db->params->username,
                                      'password' => self::getConfig()->resources->db->params->password,
                                      'dbname'   => self::getConfig()->resources->db->params->dbname,
                                      'driver_options' => $pdoParams));

        return self::$_db;
    }

    /**
     * It Reads the basic configuration file and return config values
     * @return $_config
     */
    public static function getConfig(){
        if (self::$_config != null) {
            return self::$_config;
        }

        //Get Configuration
        $configuration = Zend_Registry::get('configuration');


        self::$_config = new Zend_Config_Ini(
            '..'.DIRECTORY_SEPARATOR . appdir . DIRECTORY_SEPARATOR . 'configs/application.ini',
            $configuration['ini_status'],
            true);


        return self::$_config;
    }


    /**
     * Search an array Recursive
     * @param type $Needle
     * @param type $Haystack
     * @param type $NeedleKey
     * @param type $Strict
     * @param type $Path
     * @return type 
     */
    public static function ArraySearchRecursive($Needle,$Haystack,$NeedleKey="",$Strict=false,$Path=array()){
      if(!is_array($Haystack))
        return false;
      foreach($Haystack as $Key => $Val) {
        if(is_array($Val)&&
           $SubPath=self::ArraySearchRecursive($Needle,$Val,$NeedleKey,$Strict,$Path)) {
          $Path=array_merge($Path,Array($Key),$SubPath);
          return $Path;
        }
        elseif((!$Strict&&$Val==$Needle&&
                $Key==(strlen($NeedleKey)>0?$NeedleKey:$Key))||
                ($Strict&&$Val===$Needle&&
                 $Key==(strlen($NeedleKey)>0?$NeedleKey:$Key))) {
          $Path[]=$Key;
          return $Path;
        }
      }
      return false;
    }

    /**
     * It replace quotes
     * @param type $str
     * @return string
     */
    public static function htmlQuotes($str){
        $str = str_replace("'", "&#39;", $str);
        $str = str_replace("\"", "&quot;", $str);
        $str = str_replace("\r", "", $str);
        return $str;
    }

    /**
     * It replace quotes
     * @param type $str
     * @return string 
     */
    public static function htmlQuotes_revert($str){
        $str = str_replace("&#39;","'" , $str);
        $str = str_replace("&quot;", "\"" , $str);
        $str = str_replace("", "\r", $str);
        return $str;
    }

    /**
     * Set Language as $_SESSION
     */
    public static function setActiveLang(){
        if(!isset($_SESSION['language'])){
            Globals::setPHPLogger('Setting up default language');
            
            $ipaddress = $_SERVER["REMOTE_ADDR"];
            //$ipaddress = '79.129.102.126';

            if(Globals::getConfig()->autolocale){
                $yql = new yql_class('en');
                $result = $yql->getGeoCodeByIP($ipaddress);
                $country_code = $result['country_code'];
                $locale = new Zend_Locale();
                $new_locale = $locale->getLocaleToTerritory($country_code);
            }else{
                $new_locale = false;
            }
            
            if(!empty($new_locale)){
                $locale = new Zend_Locale($new_locale);
            }else{
                $locale = new Zend_Locale(Globals::getConfig()->language);
            }
            
            //Set Zend Local at registry
            Zend_Registry::set('Zend_Locale', $locale);
            $region = $locale->getRegion();
            $territory = $locale->getLocaleToTerritory($region);
            $language = $locale->getLanguage();
        }

        if (!isset($_SESSION['language']['lang_region'])){
            //Set Session Language
            $languageSession = new Zend_Session_Namespace("language");
            $languageSession->territory = $territory;
            $languageSession->lang_region = $language;
            $languageSession->lang_shortcut_name = $language;
            $languageSession->def_lang_id = Globals::getConfig()->lang_id;

            
        }
        define('deflang' , $_SESSION['language']['lang_region']);
        $translate = new Zend_Translate('csv' , '..'.DIRECTORY_SEPARATOR . appdir . DIRECTORY_SEPARATOR .'languages/' . deflang . '/lang_'. deflang . '.csv' , $language);
        Zend_Registry::set('Zend_Translate' , $translate);
    }//getActiveLang

    public static function change_language($lang_id){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from(dbprefix.'_languages');
        $select->where(dbprefix.'_languages.lang_id = ?' , $lang_id);
        $select->order('default DESC');

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        //set new session values
        $_SESSION['language']['territory'] = $results[0]['digits'];
        $_SESSION['language']['lang_region'] = $results[0]['shortcuts'];
        $_SESSION['language']['lang_shortcut_name'] = $results[0]['shortcuts'];
        $_SESSION['language']['def_lang_id'] = $lang_id;

        return true;
    }

    public static function set_product_categories_properties($prd_per_page , $sorting){
        if(!isset($_SESSION['products_cat_properties'])){
            $prd_properties = new Zend_Session_Namespace("products_cat_properties");
            $prd_properties->prd_per_pages = $prd_per_page;
            $prd_properties->prd_sorting   = $sorting;
        }
    }

    public static function avail_langs(){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from(dbprefix.'_languages');
        $select->order('default DESC');

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        foreach ($results as $key => $value) {
            $langs[] = "'".$value['shortcuts']."'";
        }

        $comma_separated = implode(",", $langs);
        return $comma_separated;        
    }

    /**
    * Return language id using a shortcut eg "el"
    * @return int 
    */
    public static function getLangIDbyShortcut($shortcut){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from(dbprefix.'_languages');
        $select->where('shortcuts = ?', $shortcut);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();
        return $results[0]['lang_id'];
    }
 
    /**
     * Reads a message from session
     * @return string 
     */
    public static function getInstanceMessage(){
        isset($_SESSION['alert']) == true ? $messages = $_SESSION['alert'] : $messages = null;
        return $messages;
    }
    
    /**
     * Set a message at Session
     * @param type $message 
     */
    public static function setInstanceMessage($message , $type=null){
        $_SESSION['alert']['message'] = $message;
        if(isset($type)){
            $_SESSION['alert']['type'] = $type;
        }
    }
    
    /**
     * Reset the session message
     */
    public static function resetInstanceMessage(){
       if (isset($_SESSION['alert'])) {
            unset($_SESSION['alert']);
       } 
    }
    
    /**
     * It returns the URL before the Action
     * @return string [url]
     */
    public static function getPreviousLocation(){
        isset($_SESSION['History']['LastVisit']) == true ? $previouslocation = $_SESSION['History']['LastVisit'] : $previouslocation = null;
        return $previouslocation;
    }
    
    /**
     * For lucene search 
     * @param type $str
     * @return string 
     */
    public static function clearSearchWildcards($str){
        $str = str_replace("-"," " , $str);
        $str = str_replace("_"," " , $str);
        $str = str_replace(","," " , $str);
        $str = str_replace("{"," " , $str);
        $str = str_replace(":"," " , $str);
        $str = str_replace(";"," " , $str);
        $str = str_replace("?"," " , $str);
        $str = str_replace("!"," " , $str);
        $str = str_replace("@"," " , $str);
        $str = str_replace(">"," " , $str);
        $str = str_replace("<"," " , $str);
        $str = str_replace("|"," " , $str);
        $str = str_replace("="," " , $str);
        $str = str_replace("}"," " , $str);
        $str = str_replace("+"," " , $str);
        $str = str_replace("/"," " , $str);
        $str = str_replace("["," " , $str);
        $str = str_replace("]"," " , $str);
        $str = str_replace("#"," " , $str);
        $str = str_replace("%"," " , $str);
        $str = str_replace("&"," " , $str);
        $str = str_replace("."," " , $str);
        $str = str_replace("'"," " , $str);
        $str = str_replace('"'," " , $str);
        $str = str_replace('('," " , $str);
        $str = str_replace(')'," " , $str);
        $str = str_replace("&"," " , $str);
        $str = str_replace("$"," " , $str);
        $str = str_replace("~"," " , $str);
        $str = str_replace("`"," " , $str);
        $str = str_replace("*"," " , $str);
        $str = str_replace("^"," " , $str);
        return $str;
    }

    /**
     * Set at Session the Previous location (URL)
     * @param type $ctrl_action
     * @return string 
     */
    public static function setLastVisit($ctrl_action){

        if (!isset($_SESSION['History']['LastVisit'])) {
            //Set Session History
            $HistorySession = new Zend_Session_Namespace("History");
            $server = Globals::getConfig()->sitedomain;
            $uri = $_SERVER["REQUEST_URI"];
            if($HistorySession->isLocked()){
                $HistorySession->unLock();
                $HistorySession->LastVisit = $server.$uri;
            }else{
                $HistorySession->LastVisit = $server.$uri;
            }
        }

        $server = Globals::getConfig()->sitedomain;
        $uri = $_SERVER["REQUEST_URI"];

        $ajax = stripos($ctrl_action, "ajax");
        
        //if its ajax then do not keep history
        if ($ajax === false){
            $HistorySession = new Zend_Session_Namespace("History");
            if($HistorySession->isLocked()){
                $HistorySession->unLock();
                $HistorySession->LastVisit = $server.$uri;
            }else{
                $HistorySession->LastVisit = $server.$uri;
            }    
        }
        return $_SESSION['History']['LastVisit'];
    }//setLastVisit()

    /**
     * It returns an randon number based on time
     * @return string 
     */
    public static function getRandomNum(){
        $rand = mt_rand(0, 32);
        $code = $rand.time();
        return $code;
    }

    public static function add_date($mth,$day,$year){
      $orgDate = getDate();
      $orgDate = date('Ymd');
      $cd = strtotime($orgDate);
      $retDAY = date('Ymd', mktime(0,0,0,date('m',$cd)+$mth,date('d',$cd)+$day,date('Y',$cd)+$year));
      return $retDAY;
    }

    /**
     * returns date in format (d-m-Y)
     * @param type $DateStr
     * @return string [date] 
     */
    public static function formatdate($DateStr){
        $DateStr = date("d-m-Y",strtotime($DateStr));
        return $DateStr;
    }

    /**
     * Captcha creator
     * @return array 
     */
    public static function getCaptcha(){
        $captcha_font = Globals::getConfig()->captcha->font;
        $captcha = new Zend_Captcha_Image(array(
                'font' => $captcha_font
        ));
        
        $captcha->setImgDir('./tmp/captcha/');
        $captcha->setHeight('70');
        $captcha->setWidth('250');
        $captcha->setFontSize(40);
        $captcha->setWordlen(5);

        $id = $captcha->generate();
        $captcha->render(new Zend_View());
        $word = $captcha->getWord();

        $captcha_image = "/tmp/captcha/".$id.".png";
        $result_array = array('id' => $id , 'image' => $captcha_image , 'word' => $word);

        return $result_array;
    }

    /**
     * Trim a text to specific chars
     * @param type $input
     * @param type $length
     * @param type $ellipses
     * @param type $strip_html
     * @return string 
     */
    public static function trim_text($input, $length, $ellipses = true, $strip_html = true) {
        //strip tags, if desired
        if ($strip_html) {
                $input = strip_tags($input);
        }

        //no need to trim, already shorter than trim length
        if (strlen($input) <= $length) {
                return $input;
        }

        //find last space within length
        $last_space = strrpos(substr($input, 0, $length), ' ');
        $trimmed_text = substr($input, 0, $last_space);

        //add ellipses (...)
        if ($ellipses) {
                $trimmed_text .= '...';
        }

        return $trimmed_text;
    }

    /**
     * Timer in order to get the runtime of a script
     * @return type 
     */
    public static function script_timer (){
        list ($msec, $sec) = explode(' ', microtime());
        $microtime = (float)$msec + (float)$sec;
        return $microtime;
    }

    /**
     * Search an array for a key and returns TRUE if found
     * @param type $array_name
     * @param type $return_key
     * @param type $value
     * @return type 
     */
    public static function search_array($array_name, $return_key, $value){
        while(isset($array_name[key($array_name)])){
            if($array_name[key($array_name)][$return_key] == $value){
                return true;
            }
            next($array_name);
        }
        return false;
    }

    /**
     * Search an array and returns the KEY of the array
     * @param type $array_name
     * @param type $return_key
     * @param type $value
     * @return type 
     */
    public static function search_array_return_key($array_name, $return_key, $value){
        while(isset($array_name[key($array_name)])){
            if($array_name[key($array_name)][$return_key] == $value){
                return key($array_name);
            }
            next($array_name);
        }
        return false;
    }    

    
    /**
     * Search In array for a value (returns true or false)
     * @param type $array
     * @param type $value
     * @return type 
     */
    public static function search_in_array($array , $value){
        $validator = new Zend_Validate_InArray(
            array('haystack' => $array)
        );
        $validator->setRecursive(true);

        if ($validator->isValid($value)) {
            return true;
        }else{
            return false;
        }
    }

    /**
     * Returns Months based on language (example gr,en)
     * @param type $la
     * @return array 
     */
    public static function getMonths($la){
        $months = Zend_Locale::getTranslationList('months', $la);
        return $months['format']['wide'];
    }

    /**
     * Returns Only Months based on language (example gr,en)
     * @param type $la
     * @return type 
     */
    public static function getMonths_standAlone($la){
        $months = Zend_Locale::getTranslationList('months', $la);
        if($months['stand-alone']['wide']['1'] == 1){
            return $months['format']['wide'];
        }else{
            return $months['stand-alone']['wide'];
        }
    }

    /**
     * Prepare a Date for mysql Query
     * @param type $date_array
     * @return string 
     */
    public static function setMysqlDatetime($date_array){
        $day = $date_array['day'];
        $month = $date_array['month'];
        $year = $date_array['year'];

        $date = $day."/".$month."/".$year;
        $mysqldate = new Zend_Date($date, 'yyyy-MM-dd HH:mm:ss');
        $datetime = $mysqldate->toString('yyyy-MM-dd HH:mm:ss');
        return $datetime;
    }

    /**
     * Return current date time for Mysql use
     * @return string 
     */
    public static function setMysqlDatetimeNow(){
        $datenow = Zend_Date::now();
        $mysqldate = new Zend_Date($datenow, 'yyyy-MM-dd HH:mm:ss');
        $datetime = $mysqldate->toString('yyyy-MM-dd HH:mm:ss');
        return $datetime;
    }

    /**
     * Return a date format (EEEE d MMMM yyyy)
     * @param Zend_Date $date
     * @return type 
     */
    public static function nice_dates($date){
        $date = new Zend_Date($date, 'EEEE d MMMM yyyy' , $_SESSION['language']['lang_region']);
        $newdate =  $date->toString('EEEE d MMMM yyyy');
        return $newdate;
    }

    /**
     * Get month from a date
     * @param Zend_Date $date
     * @return string 
     */
    public static function getMonth($date){
        $date = new Zend_Date($date, 'EEEE d MMM yyyy' , $_SESSION['language']['lang_region']);
        $newdate =  $date->toString('M');
        return $newdate;
    }

    /**
     * Get year from a date
     * @param Zend_Date $date
     * @return string 
     */    
    public static function getYear($date){
        $date = new Zend_Date($date, 'EEEE d MMM yyyy' , $_SESSION['language']['lang_region']);
        $newdate =  $date->toString('yyyy');
        return $newdate;
    }

    /**
     * Remove non braking spaces
     * @param type $str
     * @return string 
     */
    public static function nl2br_reverse($str){
        $str = str_replace("<br />","" , $str);
        $str = str_replace("<br>","" , $str);
        return $str;
    }

    /**
     * Sort an array by Key ASC OR DESC
     * @param type $array
     * @param type $index
     * @param type $order
     * @param type $natsort
     * @param type $case_sensitive
     * @return array 
     */
    public static function sort2d($array, $index, $order='asc', $natsort=FALSE, $case_sensitive=FALSE){
        if(is_array($array) && count($array)>0)
        {
           foreach(array_keys($array) as $key)
               $temp[$key]=$array[$key][$index];
               if(!$natsort)
                   ($order=='asc')? asort($temp) : arsort($temp);
              else
              {
                 ($case_sensitive)? natsort($temp) : natcasesort($temp);
                 if($order!='asc')
                     $temp=array_reverse($temp,TRUE);
           }
           foreach(array_keys($temp) as $key)
               (is_numeric($key))? $sorted[]=$array[$key] : $sorted[$key]=$array[$key];
           return $sorted;
      }
      return $array;
    }


    /**
     * Return a string starting with Capital 
     * @param string $str
     * @param type $encoding
     * @param type $lower_str_end
     * @return string 
     */
    public static function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
            if ($lower_str_end) {
                $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
            } else {
                $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
            }
        $str = $first_letter . $str_end;
        return $str;
    }

    /**
     * Farhenait to Celcious
     * @param type $temp
     * @return type 
     */
    public static function f2c($temp){
        $result = (($temp - 32) * (5/9));
        return round($result);
    }

    /**
     * Return an Array with unique values only
     * @param type $array
     * @return array 
     */
    public static function super_unique($array){
      $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

      foreach ($result as $key => $value)
      {
        if ( is_array($value) )
        {
          $result[$key] = self::super_unique($value);
        }
      }
      return $result;
    }
    
    /**
     * Return the translation using Zend_Translate
     * @return string [translation]
     */
    public static function trl($trl_str){
            $translate = Zend_Registry::get('Zend_Translate');
            return $translate->_($trl_str);
    }   
    
    public static function trl_reg($region , $trl_str){
        $translate = new Zend_Translate('csv' , '..'.DIRECTORY_SEPARATOR . appdir . DIRECTORY_SEPARATOR .'languages/' . $region . '/lang_'. $region . '.csv' , $language);
        return $translate->_($trl_str);
    }

    /**
     * Stores all history data of a visitor MongoDB
     */
    public static function setHistory($c_location , $p_location , $action , $message=''){       
        if(self::getConfig()->history){
            $visitor = new Visitor_history();
            $visitor->sessionid  = session_id();
            $visitor->location = $c_location;
            $visitor->prev_location = $p_location;
            $visitor->action = $action;
            $visitor->messages = $message;
            $visitor->ip = $_SERVER["REMOTE_ADDR"];
            $visitor->session = $_SESSION;
            $visitor->save();        
        }
    }
    

    /**
     * Return History by SessionID
     * @param type $sessionid
     * @return type 
     */
    public static function getHistory($sessionid){
        $visitor = new Visitor_history();
        $history = $visitor->all(array('sessionid' => $sessionid));
        return $history;
    }
    
    /**
     * Logger Object initialization
     * @return type 
     */
    public static function phpLogger(){
        if (self::$_logger != null) {
            return self::$_logger;
        }

        $writer = new Zend_Log_Writer_Firebug();
        self::$_logger = new Zend_Log($writer);
        
        return self::$_logger;        
    }
    
    /**
     * Available Status ALERT, INFO , DEBUG , EMERG , CRIT , ERR , WARN , NOTICE
     * Set a message for firebug Php Logger
     * @param string/array $message 
     */
    public static function setPHPLogger($message , $type='DEBUG'){
        if(self::getConfig()->debug){
            switch ($type) {
                case 'ALERT':
                    self::phpLogger()->log($message, Zend_Log::ALERT);
                    break;
                case 'INFO':
                    self::phpLogger()->log($message, Zend_Log::INFO);
                    break;
                case 'DEBUG':
                    self::phpLogger()->log($message, Zend_Log::DEBUG);
                    break;
                case 'EMERG':
                    self::phpLogger()->log($message, Zend_Log::EMERG);
                    break;
                case 'CRIT':
                    self::phpLogger()->log($message, Zend_Log::CRIT);
                    break;
                case 'ERR':
                    self::phpLogger()->log($message, Zend_Log::ERR);
                    break;    
                case 'WARN':
                    self::phpLogger()->log($message, Zend_Log::WARN);
                    break;   
                case 'NOTICE':
                    self::phpLogger()->log($message, Zend_Log::NOTICE);
                    break;                
            }            
        }
    }

    /**
    * Redirect to specific url
    * @param string $path
    */
    public static function setRedirect($path){
        $front      = Zend_Controller_Front::getInstance();  
        $response  = $front->getResponse();
        $response->setRedirect($path);
        $response->sendHeaders();
        exit;
    }

    /**
    * Recursive convertion from object to array
    * @param object $arrObjData
    */
    public static function objectsIntoArray($arrObjData, $arrSkipIndices = array()){
        $arrData = array();
       
        // if input is object, convert into array
        if (is_object($arrObjData)) {
            $arrObjData = get_object_vars($arrObjData);
        }
       
        if (is_array($arrObjData)) {
            foreach ($arrObjData as $index => $value) {
                if (is_object($value) || is_array($value)) {
                    $value = self::objectsIntoArray($value, $arrSkipIndices); // recursive call
                }
                if (in_array($index, $arrSkipIndices)) {
                    continue;
                }
                $arrData[$index] = $value;
            }
        }
        return $arrData;
    }    

    /**
    * Check if the user is logged in
    * @return true/false
    */
    public static function isLoggedin(){
        if(!isset($_SESSION['adminpanel']['web_login_status']) || $_SESSION['adminpanel']['web_login_status'] == false){
            return false;
        }else{
            return true;
        }
    }

    public static function getPrice($price , $offer_price=0){
      $currency_symbol = "€";
      if(!empty($offer_price) && $price > 0){
        $diff = $price - $offer_price;
      }else{
        $diff = 0;
      }
      return array('price' => number_format($price, 2, '.', '') , 'offer_price' => number_format($offer_price, 2, '.', ''), 'diff' => $diff , 'curr' => $currency_symbol);
    }

    public static function getthePrice($price){
      $currency_symbol = "€";
      return number_format($price, 2, '.', '').$currency_symbol;
    }    

    public static function getCountries(){
      $code = $_SESSION['language']['territory'];
      return Zend_Locale::getTranslationList('territory', $code, 2); 
    }

    public static function returnParams($params){
        $arr = array();
            foreach (explode(',', $params) as $line) {
                $l = explode(':', trim($line));
                if (isset($l[1]))
                    $arr[$l[0]] = $l[1];
            }
        return $arr;    
    }

    public static function greekToEnglishChars($filestr){
        //function greekToGreeklish($filestr
        $filestr = str_replace("ς", "s", $filestr); 
        $filestr = str_replace("ε", "e", $filestr); 
        $filestr = str_replace("Ε", "E", $filestr); 
        $filestr = str_replace("έ", "e", $filestr); 
        $filestr = str_replace("E", "E", $filestr); 
        $filestr = str_replace("ρ", "r", $filestr); 
        $filestr = str_replace("Ρ", "R", $filestr); 
        $filestr = str_replace("τ", "t", $filestr); 
        $filestr = str_replace("Τ", "T", $filestr); 
        $filestr = str_replace("υ", "i", $filestr); 
        $filestr = str_replace("Y", "I", $filestr); 
        $filestr = str_replace("ύ", "i", $filestr); 
        $filestr = str_replace("Ύ", "I", $filestr); 
        $filestr = str_replace("θ", "th", $filestr);  
        $filestr = str_replace("Θ", "TH", $filestr);  
        $filestr = str_replace("ι", "i", $filestr); 
        $filestr = str_replace("É", "Ι", $filestr); 
        $filestr = str_replace("Ί", "I", $filestr); 
        $filestr = str_replace("ί", "i", $filestr); 
        $filestr = str_replace("ο", "o", $filestr); 
        $filestr = str_replace("ό", "o", $filestr); 
        $filestr = str_replace("Ο", "O", $filestr); 
        $filestr = str_replace("Ο", "O", $filestr); 
        $filestr = str_replace("π", "p", $filestr); 
        $filestr = str_replace("Π", "P", $filestr); 
        $filestr = str_replace("α", "a", $filestr); 
        $filestr = str_replace("ά", "a", $filestr); 
        $filestr = str_replace("Α", "A", $filestr); 
        $filestr = str_replace("Ά", "A", $filestr); 
        $filestr = str_replace("σ", "s", $filestr); 
        $filestr = str_replace("Σ", "S", $filestr); 
        $filestr = str_replace("δ", "d", $filestr); 
        $filestr = str_replace("Δ", "D", $filestr); 
        $filestr = str_replace("Φ", "F", $filestr); 
        $filestr = str_replace("φ", "f", $filestr); 
        $filestr = str_replace("γ", "I", $filestr); 
        $filestr = str_replace("Γ", "G", $filestr); 
        $filestr = str_replace("η", "i", $filestr); 
        $filestr = str_replace("ή", "i", $filestr); 
        $filestr = str_replace("H", "I", $filestr); 
        $filestr = str_replace("Ή", "I", $filestr); 
        $filestr = str_replace("ξ", "ks", $filestr);  
        $filestr = str_replace("Ξ", "KS", $filestr);  
        $filestr = str_replace("κ", "k", $filestr); 
        $filestr = str_replace("Κ", "K", $filestr); 
        $filestr = str_replace("λ", "l", $filestr); 
        $filestr = str_replace("Λ", "L", $filestr); 
        $filestr = str_replace("ζ", "z", $filestr); 
        $filestr = str_replace("Ζ", "Z", $filestr); 
        $filestr = str_replace("χ", "ch", $filestr);  
        $filestr = str_replace("Χ", "CH", $filestr);  
        $filestr = str_replace("Ψ", "PS", $filestr);  
        $filestr = str_replace("ψ", "PS", $filestr);  
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("ώ", "o", $filestr); 
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("β", "v", $filestr); 
        $filestr = str_replace("β", "b", $filestr); 
        $filestr = str_replace("ν", "n", $filestr); 
        $filestr = str_replace("N", "N", $filestr);
        $filestr = str_replace("μ", "m", $filestr); 
        $filestr = str_replace("Μ", "M", $filestr);
        $filestr = str_replace("Ί", "I", $filestr);
        $filestr = str_replace("  ", " ", $filestr);
        $filestr = str_replace(" ", "-", $filestr);
        $filestr = str_replace("?", "-", $filestr);
        $filestr = str_replace("'", "-", $filestr);
        $filestr = str_replace('"', "-", $filestr);
        $filestr = str_replace("-", "-", $filestr);
        $filestr = str_replace("--", "-", $filestr);
        $filestr = str_replace(".", "", $filestr);
        $filestr = str_replace("(", "", $filestr);
        $filestr = str_replace(")", "", $filestr);
        $filestr = str_replace("*", "", $filestr);
        $filestr = str_replace("#", "", $filestr);
        $filestr = str_replace("!", "", $filestr);
        $filestr = str_replace("@", "", $filestr);
        $filestr = str_replace("$", "", $filestr);
        $filestr = str_replace("%", "", $filestr);
        $filestr = str_replace("^", "", $filestr);
        $filestr = str_replace("-&-", "", $filestr);
        
        return $filestr;
        //end function  
      }

    public static function greek2english($filestr){
        $filestr = str_replace("ου", "ou", $filestr);
        $filestr = str_replace("ού", "ou", $filestr);
        $filestr = str_replace("ς", "s", $filestr); 
        $filestr = str_replace("ε", "e", $filestr); 
        $filestr = str_replace("Ε", "E", $filestr); 
        $filestr = str_replace("έ", "e", $filestr); 
        $filestr = str_replace("E", "E", $filestr); 
        $filestr = str_replace("ρ", "r", $filestr); 
        $filestr = str_replace("Ρ", "R", $filestr); 
        $filestr = str_replace("τ", "t", $filestr); 
        $filestr = str_replace("Τ", "T", $filestr); 
        $filestr = str_replace("υ", "i", $filestr); 
        $filestr = str_replace("Y", "I", $filestr); 
        $filestr = str_replace("ύ", "i", $filestr); 
        $filestr = str_replace("Ύ", "I", $filestr); 
        $filestr = str_replace("θ", "th", $filestr);  
        $filestr = str_replace("Θ", "Th", $filestr); 
        $filestr = str_replace("ΐ", "i", $filestr);  
        $filestr = str_replace("ι", "i", $filestr); 
        $filestr = str_replace("É", "Ι", $filestr); 
        $filestr = str_replace("Ί", "I", $filestr); 
        $filestr = str_replace("ί", "i", $filestr); 
        $filestr = str_replace("ο", "o", $filestr); 
        $filestr = str_replace("ό", "o", $filestr); 
        $filestr = str_replace("Ο", "O", $filestr); 
        $filestr = str_replace("Ο", "O", $filestr); 
        $filestr = str_replace("π", "p", $filestr); 
        $filestr = str_replace("Π", "P", $filestr); 
        $filestr = str_replace("α", "a", $filestr); 
        $filestr = str_replace("ά", "a", $filestr); 
        $filestr = str_replace("Α", "A", $filestr); 
        $filestr = str_replace("Ά", "A", $filestr); 
        $filestr = str_replace("σ", "s", $filestr); 
        $filestr = str_replace("Σ", "S", $filestr); 
        $filestr = str_replace("δ", "d", $filestr); 
        $filestr = str_replace("Δ", "D", $filestr); 
        $filestr = str_replace("Φ", "F", $filestr); 
        $filestr = str_replace("φ", "f", $filestr); 
        $filestr = str_replace("γ", "I", $filestr); 
        $filestr = str_replace("Γ", "G", $filestr); 
        $filestr = str_replace("η", "i", $filestr); 
        $filestr = str_replace("ή", "i", $filestr); 
        $filestr = str_replace("H", "I", $filestr); 
        $filestr = str_replace("Ή", "I", $filestr); 
        $filestr = str_replace("ξ", "ks", $filestr);  
        $filestr = str_replace("Ξ", "Ks", $filestr);  
        $filestr = str_replace("κ", "k", $filestr); 
        $filestr = str_replace("Κ", "K", $filestr); 
        $filestr = str_replace("λ", "l", $filestr); 
        $filestr = str_replace("Λ", "L", $filestr); 
        $filestr = str_replace("ζ", "z", $filestr); 
        $filestr = str_replace("Ζ", "Z", $filestr); 
        $filestr = str_replace("χ", "ch", $filestr);  
        $filestr = str_replace("Χ", "Ch", $filestr);  
        $filestr = str_replace("Ψ", "Ps", $filestr);  
        $filestr = str_replace("ψ", "Ps", $filestr);  
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("ώ", "o", $filestr); 
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("ω", "o", $filestr); 
        $filestr = str_replace("β", "v", $filestr); 
        $filestr = str_replace("β", "b", $filestr); 
        $filestr = str_replace("ν", "n", $filestr); 
        $filestr = str_replace("N", "N", $filestr);
        $filestr = str_replace("μ", "m", $filestr); 
        $filestr = str_replace("Μ", "M", $filestr);
        $filestr = str_replace("Ί", "I", $filestr);       
        return $filestr;
      }

      public static function updateFriendlyURLS(){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $db->select();
        $select->from(dbprefix.'_products_multilang');


        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        foreach ($results as $key => $value) {
          $db->update(dbprefix.'_products_multilang', array('friendly_url' => Globals::greekToEnglishChars($value['name'])) , 'product_multilang_id = '.$value['product_multilang_id']);
        }
      }

} // class Globals

?>