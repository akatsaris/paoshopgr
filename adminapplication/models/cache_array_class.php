<?php
/**
 * CourseVault Platform
 * Caching Method for Arrays
 * @copyright Kuku
 * @link http://www.kukuapps.com
 * @author Angelos Katsaris
 */
class array_cache {

    /**
     * Keep the error message
     * @var string $error_msg
     */
    public  $error_msg;
    /**
     * Error True/false
     * @var boolean
     */
    public  $error;
    /**
     *
     * @var array
     */
    public  $results;
    /**
     * Unique cache ID
     * @var string $cacheid
     */
    public  $cacheid;
    /**
     * True / False
     * @var boolean $cache_status
     */    
    public  $cache_status;
    /**
     * @var $_frontendOptions 
     */
    private $_frontendOptions;
    /**
     * @var $_backendOptions 
     */
    private $_backendOptions;
    /**
     * @var $_cache 
     */
    private $_cache;


    /**
     * Set Cache Directory & life time
     * @param type $lifetime
     * @param type $cache_dir 
     */
    public function __construct($lifetime=null, $cache_dir='cache/'){
        //cache enabled or not
        $this->cache_status = Globals::getConfig()->cache;

        if($this->cache_status){
            //setting cache
            $this->_frontendOptions = array(
               'lifetime' => $lifetime,
               'automatic_serialization' => true
            );

            $this->_backendOptions = array(
                'cache_dir' => $cache_dir
            );

            $this->_cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
        }

    }

    /**
     * Set Cache ID
     * @param type $id 
     */
    public function setcacheid($id){
        if($this->cache_status){
            $this->cacheid = $id;
        }
    }

    /**
     * Set the Data To cache
     * @param type $array 
     */
    public function setData($array){
            $this->results = $array;
    }

    /**
     * Load from cache
     * @return false/true 
     */
    public function loadcache(){
        if($this->cache_status){
            if(($data = $this->_cache->load($this->cacheid))){
                $this->results = $data;
                return true;
            }else{
               return false;
            }
        }
    }

    /**
     * Cache Save
     */
    public function savecache(){
        if($this->cache_status){
            $this->_cache->save($this->results , $this->cacheid);
        }
    }
    
    /**
     * Remove cache
     */
    public function removecache(){
        if($this->cache_status){
            $this->_cache->remove($this->cacheid);
        }
    }
	
    /**
     * Remove outdated cache
     */
    public function remove_outdate_cache(){
        if($this->cache_status){
            $this->_cache->clean(Zend_Cache::CLEANING_MODE_OLD);
        }
    }

}
?>