<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class orders {

	public $old_status;

	public function getOrder($order_id){
		$lang_id = Globals::getLangIDbyShortcut(deflang);
        $db = Zend_Db_Table::getDefaultAdapter();

        //multilang product values
        $select = $db->select();
		$select->from(dbprefix.'_orders' , array('order_id','order_number_id','order_date','session_id','user_id','order_cod','order_price','order_voucher_id','order_tracking','order_voucher_value','invoice','order_paid','comment','merchant','wrap_packaging_cost','order_html','order_status_id','order_shipping','order_shipping_id','order_discount'));
		
		$select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','timestamp'));
		$select->joinLeft(dbprefix.'_areas' , dbprefix.'_order_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));
		$select->joinLeft(dbprefix.'_order_status' , dbprefix.'_order_status.ord_status_id = '.dbprefix.'_orders.order_status_id' , array('ord_status','ord_email_template','send_mail'));
		$select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_orders.order_shipping_id' , array('shipping_pack_name','shipping_pack_description'));

		$select->where(dbprefix.'_orders.order_id = ?', $order_id);
		$select->where(dbprefix.'_shipping_packs.la = ?', $lang_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		//echo $select->__toString();

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;	
	}

	public function updateOrder($order_id , $shipping_data_db , $orders_db){
		$this->_setOldStatus($order_id);

		$db = Zend_Db_Table::getDefaultAdapter();
		$db->beginTransaction();
		try {
			$db->update(dbprefix.'_orders', $orders_db , 'order_id ='.$order_id);
			$db->update(dbprefix.'_order_shipping', $shipping_data_db , 'order_id ='.$order_id);
			
			$db->commit();
			$this->sendQEmail($orders_db['order_status_id'] , $order_id);
	    } catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		} 
	}

	public function sendQEmail($curr_order_status , $order_id){
		if($curr_order_status != $this->old_status){	
			$queue = new queue('order_status_queue');
        	@EmailSender::setQueue($queue->queue);
			@EmailSender::sendEmail(array('type' => 'order_status' , 'status_id' => $curr_order_status , 'order_id' => $order_id));
		}
	}

	protected function _setOldStatus($order_id){
		$lang_id = Globals::getLangIDbyShortcut(deflang);
        $db = Zend_Db_Table::getDefaultAdapter();

        //multilang product values
        $select = $db->select();
		$select->from(dbprefix.'_orders' , array('order_id','order_number_id','order_date','session_id','user_id','order_cod','order_price','order_voucher_id','order_voucher_value','invoice','order_paid','comment','merchant','wrap_packaging_cost','order_html','order_status_id','order_shipping','order_shipping_id'));
		
		$select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','timestamp'));
		$select->join(dbprefix.'_areas' , dbprefix.'_order_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));
		$select->joinLeft(dbprefix.'_order_status' , dbprefix.'_order_status.ord_status_id = '.dbprefix.'_orders.order_status_id' , array('ord_status','ord_email_template','send_mail'));
		$select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_orders.order_shipping_id' , array('shipping_pack_name','shipping_pack_description'));

		$select->where(dbprefix.'_orders.order_id = ?', $order_id);
		$select->where(dbprefix.'_shipping_packs.la = ?', $lang_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		$this->old_status = $results[0]['order_status_id'];	
	}

	protected function _getStatusData($status_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_order_status');
		$select->where(dbprefix.'_order_status.ord_status_id = ?', $status_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0];
	}

	public function getPrintableOrder($order_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $la = 1;
        $la_region = "el";

        //multilang product values
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id', 'order_discount' ,'order_date','session_id','user_id','order_cod','order_price','order_voucher_id','order_voucher_value','invoice','order_paid','comment','merchant','wrap_packaging_cost','order_html','order_status_id','order_shipping','order_shipping_id'));
        
        $select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','timestamp'));
        $select->joinLeft(dbprefix.'_areas' , dbprefix.'_order_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));
        $select->joinLeft(dbprefix.'_order_status' , dbprefix.'_order_status.ord_status_id = '.dbprefix.'_orders.order_status_id' , array('ord_status','ord_email_template','send_mail'));
        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_orders.order_shipping_id' , array('shipping_pack_name','shipping_pack_description'));
        $select->join(dbprefix.'_pay_methods' , dbprefix.'_orders.merchant = '.dbprefix.'_pay_methods.merchant_plugin' , array('pay_name','pay_description'));


        $select->where(dbprefix.'_orders.order_id = ?', $order_id);
        $select->where(dbprefix.'_shipping_packs.la = ?', $la);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        $data[0]['email_cost'] = ($data[0]['order_price'] + $data[0]['order_cod'] + $data[0]['wrap_packaging_cost'] + $data[0]['order_shipping']) - $data[0]['order_discount'];
        $data[0]['la_region']  = $la_region;

        //ITEMS
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id'));
        
        $select->join(dbprefix.'_order_items' , dbprefix.'_orders.order_id = '.dbprefix.'_order_items.order_id' , array('price','quantity'));
        $select->join(dbprefix.'_products_multilang' , dbprefix.'_order_items.product_id = '.dbprefix.'_products_multilang.product_id' , array('name','prd_description'));
        
        $select->joinLeft(dbprefix.'_attributes' , dbprefix.'_order_items.product_attributes = '.dbprefix.'_attributes.attr_id' , array('attr_value'));
        $select->join(dbprefix.'_products' , dbprefix.'_order_items.product_id = '.dbprefix.'_products.product_id' , array('product_code','accompaying'));
        

        $select->where(dbprefix.'_products_multilang.la = ?', $la);
        $select->where(dbprefix.'_attributes.la = '.$la.' OR '.dbprefix.'_attributes.la IS NULL');
        $select->where(dbprefix.'_orders.order_id = ?', $order_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $items = $stmt->fetchAll();        

        $data[0]['items'] = $items;
        return $data;
	}


}
