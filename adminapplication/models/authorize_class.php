<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class authorize {

    public  $status_message;
    public  $login_status;

    public function __construct(){
        if (isset($_SESSION['adminpanel']['web_login_status'])) {
            $this->login_status = true;
        }else{
            $this->login_status = false;
        }
    }

    public function checkAccess($email_login , $password , $keepforever){
        $password = md5($password.Globals::getConfig()->salt);
        if($email_login == "" || $password == ""){
                $this->status_message = Globals::trl('login credentials missing');
                $this->login_status = false;
                return;
        }

        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();


        //Configure auth adapter
        $authAdapter = new Zend_Auth_Adapter_DbTable($db, dbprefix.'_useraccounts', 'username', 'password');
        $authAdapter->setIdentity($email_login)
                    ->setCredential($password);

        // Perform the authentication query, saving the result
        $result = $authAdapter->authenticate();
        $return_msg = $result->getmessages();

         if($return_msg[0] == "Authentication successful.") {
            // User Informations
            $user_info = $authAdapter->getResultRowObject();

            if($keepforever == '1'){
                 Zend_Session::rememberMe();
            }

            $select = $db->select();
            $select->from(dbprefix.'_useraccounts', array('username' , 'firstname' , 'lastname' , 'role' , 'user_id'));
            $select->where('username = (?)', $email_login);
            
            $stmt = $db->query($select);
            $results = $stmt->fetchAll();

            // Add values to session
            $userSession = new Zend_Session_Namespace("adminpanel");
            $userSession->web_login_status = true;
            $userSession->author    = $results[0]['user_id'];
            $userSession->firstname = $results[0]['firstname'];
            $userSession->lastname  = $results[0]['lastname'];
            $userSession->username  = $results[0]['username'];
            $userSession->role      = $results[0]['role'];


            //Notice Message
            $this->status_message = Globals::trl('login success');

          }else{
            $userSession->web_login_status = false;
            //Notice Message
            $this->status_message = Globals::trl('login failed');
          }
        return;
    }

	public function logout(){
            //Unset all of the session variables.
            $_SESSION = array();

            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!
            if (isset($_COOKIE[session_name()])) {
                setcookie(session_name(), '', time()-42000, '/');
            }

            // Finally, destroy the session.
            session_destroy();
            return;
	}
}
?>