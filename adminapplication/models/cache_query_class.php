<?php
/**
 * CourseVault Platform
 * Caching Method for Mysql Queries
 * @copyright Kuku
 * @link http://www.kukuapps.com
 * @author Angelos Katsaris
 */
class query_cache {

    /**
     * Keep the error message
     * @var string $error_msg
     */
    public  $error_msg;
    /**
     * Error True/false
     * @var boolean
     */
    public  $error;
    /**
     *
     * @var array
     */
    public  $results;
    /**
     * Unique cache ID
     * @var string $cacheid
     */
    public  $cacheid;
    /**
     * True / False
     * @var boolean $cache_status
     */    
    public  $cache_status;
    /**
     * db instance
     * @var $db
     */
    private $_db;
    /**
     * @var $_frontendOptions 
     */
    private $_frontendOptions;
    /**
     * @var $_backendOptions 
     */
    private $_backendOptions;
    /**
     * @var $_cache 
     */
    private $_cache;


    /**
     * Set Cache Directory & life time
     * @param type $lifetime
     * @param type $cache_dir 
     */
    public function __construct($lifetime=null, $cache_dir='cache/'){
        //cache enabled or not
        $this->cache_status = Globals::getConfig()->cache;

        if($this->cache_status){
            //setting cache
            $this->_frontendOptions = array(
               'lifetime' => $lifetime,
               'automatic_serialization' => true
            );

            $this->_backendOptions = array(
                'cache_dir' => $cache_dir
            );

            $this->_cache = Zend_Cache::factory('Core', 'File', $this->_frontendOptions, $this->_backendOptions);
        }
    }

    /**
     * Setup db connection
     */
    private function _dbconnect(){
        //database connection
        $this->_db = Globals::getDBConnection();
        $this->_db->getConnection();
        $this->_db->query("SET CHARACTER SET 'utf8'");
        $this->_db->query("SET NAMES 'utf8'");
    }

    /**
     * Close db connection
     */
    private function _dbclose(){
        //Close db connection
        $this->_db->closeConnection();
    }

    /**
     * Set Cache ID
     * @param type $id 
     */
    public function setcacheid($id){
        if($this->cache_status){
            $this->cacheid = $id;
        }
    }

    /**
     * Run a Query in order to cache results
     * @param type $sql 
     */
    public function runquery($sql){
        $this->_dbconnect();
        
        $this->_db->beginTransaction();

        try {
            $data = $this->_db->query($sql);
            $results = $data->fetchAll();
        }catch (Exception $e) {
            $this->_db->rollBack();
            $this->error = true;
            $this->error_msg = "Query Error";
        }

        $this->_dbclose();
        $this->results = $results;
    }

    /**
     * Loading Cache
     * @return boolean
     */
    public function loadcache(){
        if($this->cache_status){
            if(($data = $this->_cache->load($this->cacheid))){
                $this->results = $data;
                return true;
            }else{
               return false;
            }
        }
    }

    /**
     * Save Cache
     */
    public function savecache(){
        if($this->cache_status){
            $this->_cache->save($this->results , $this->cacheid);
        }
    }
   
    /**
     * remove cache
     */
    public function removecache(){
        if($this->cache_status){
            $this->_cache->remove($this->cacheid);
        }
    }

}
?>