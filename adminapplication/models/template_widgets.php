<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class template_widgets 
{

	public function getTemplateSections($template_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_templates' , array('template_id'));
		$select->join(dbprefix.'_template_sections' , dbprefix.'_templates.template_id = '.dbprefix.'_template_sections.template_id' , array('section_name' , 'section_file' , 'section_color' , 'template_section_id'));
		$select->join(dbprefix.'_template_view' , dbprefix.'_template_view.template_id = '.dbprefix.'_template_sections.template_id' , array('template_name'));
		$select->where(dbprefix.'_template_view.template_view_id = ?', $template_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		return $results;		
	}

	public function getActiveWidgets($template_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_template_view' , array('template_name'));
		$select->join(dbprefix.'_template_sections' , dbprefix.'_template_view.template_id = '.dbprefix.'_template_sections.template_id' , array('section_name' , 'section_file' , 'section_color' , 'template_section_id'));
		$select->join(dbprefix.'_template_widgets' , dbprefix.'_template_widgets.template_view_id = '.dbprefix.'_template_view.template_view_id');
		$select->join(dbprefix.'_widgets' , dbprefix.'_template_widgets.widget_id = '.dbprefix.'_widgets.widget_id' , array('widget_name' , 'widget' , 'widget_id'));
		$select->where(dbprefix.'_template_view.template_view_id = ?', $template_id);
		$select->group(dbprefix.'_template_widgets.template_widget_id');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		return $results;		
	}

	public function getInactiveWidgets($template_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_template_view' , array('template_name'));
		$select->join(dbprefix.'_template_sections' , dbprefix.'_template_view.template_id = '.dbprefix.'_template_sections.template_id' , array('section_name' , 'section_file' , 'section_color' , 'template_section_id'));
		$select->join(dbprefix.'_template_widgets' , dbprefix.'_template_widgets.template_view_id = '.dbprefix.'_template_view.template_view_id');
		$select->join(dbprefix.'_widgets' , dbprefix.'_template_widgets.widget_id = '.dbprefix.'_widgets.widget_id' , array('widget_name' , 'widget' , 'widget_id'));
		$select->where(dbprefix.'_template_view.template_view_id = ?', $template_id);
		$select->group(dbprefix.'_template_widgets.template_widget_id');
		

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();


		foreach ($results as $key => $widgets_ids) {
			$widgets_ids_arr[] = $widgets_ids['widget_id'];
		}
		
		$select = $db->select();
		$select->from(dbprefix.'_widgets');
		
		if(!empty($results)){
			$select->where(dbprefix.'_widgets.widget_id NOT IN (?)', $widgets_ids_arr);
		}

		
		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		return $results;		
	}	

	public function saveData($template_id , $section_id , $widgets_ids){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
		
		$widgets_ids_arr = explode(",", $widgets_ids);
		$db->beginTransaction();
		try {
	        //first delete section on current template
	        $db->delete(dbprefix.'_template_widgets', "template_view_id = ".$template_id.' AND template_section_id = '.$section_id);

	        //Then insert new Section Data
	        if(!empty($widgets_ids)){
	        	foreach ($widgets_ids_arr as $key => $widget_id) {
	        		$db->insert(dbprefix.'_template_widgets', array('template_section_id' => $section_id , 'template_view_id' => $template_id , 'widget_id' => $widget_id));
	        	}
	        }

	        $db->commit();
	    } catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		} 		
	}

	public static function readTemplateHeaders($file, $default_headers, $context = ''){
	        // We don't need to write to the file, so just open for reading.
	        $fp = fopen( $file, 'r' );
	
	        // Pull only the first 8kiB of the file in.
	        $file_data = fread( $fp, 8192 );
	
	        // PHP will close file handle, but we are good citizens.
	        fclose( $fp );
	
	        // Make sure we catch CR-only line endings.
	        $file_data = str_replace( "\r", "\n", $file_data );
	
	        if ( $context && $extra_headers = apply_filters( "extra_{$context}_headers", array() ) ) {
	                $extra_headers = array_combine( $extra_headers, $extra_headers ); // keys equal values
	                $all_headers = array_merge( $extra_headers, (array) $default_headers );
	        } else {
	                $all_headers = $default_headers;
	        }
	
	        foreach ( $all_headers as $field => $regex ) {
	                if ( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $file_data, $match ) && $match[1] )
	                        $all_headers[ $field ] = _cleanup_header_comment( $match[1] );
	                else
	                        $all_headers[ $field ] = '';
	        }
	
	        return $all_headers;
	}

}	