<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class datatable 
{

	public function save($table , $data , $datatable_config){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $multilang_id = $datatable_config[$table]['db']['multilang_id'];
        $id = $datatable_config[$table]['db']['id'];
        $multilang = $datatable_config[$table]['multilang'];

		//begin transaction for basic settings
		$db->beginTransaction();
		try {

			if(isset($data[deflang])) {

				foreach ($data as $key => $ins_row) {
					$la_id = Globals::getLangIDbyShortcut($key);
	        		
	        		if(deflang == $key){
	        			//insert & update
	        			$ins_row['la'] = $la_id;
	        			$ins_row[$id] = 0;
	        			$db->insert(dbprefix.'_'.$table, $this->prepareData($ins_row));
	        			$last_insert_multilang_id = $db->lastInsertId();

	        			$db->update(dbprefix.'_'.$table, array($id => $last_insert_multilang_id) , ' '.$multilang_id.' = '.$last_insert_multilang_id);
	        		}else{
	        			//insert with last inserted id
	        			$ins_row['la'] = $la_id;
	        			$ins_row[$id] = $last_insert_multilang_id;
	        			$db->insert(dbprefix.'_'.$table, $this->prepareData($ins_row));
	        		}
				}

			}else{
		        if($multilang == "true"){
			        $data['la'] = Globals::getLangIDbyShortcut(deflang);
					$data[$id] = 0;
					$db->insert(dbprefix.'_'.$table, $this->prepareData($data));
					$last_insert_multilang_id = $db->lastInsertId();
					$db->update(dbprefix.'_'.$table, array($id => $last_insert_multilang_id) , ' '.$multilang_id.' = '.$last_insert_multilang_id);
				}else{
					$db->insert(dbprefix.'_'.$table, $this->prepareData($data));
				}
			}
		   	 
		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}
	}

	public function update($table , $data , $datatable_config , $item_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $multilang_id = $datatable_config[$table]['db']['multilang_id'];
        $id = $datatable_config[$table]['db']['id'];
        $multilang = $datatable_config[$table]['multilang'];

        //Get item data
        $select = $db->select();
        $select->from(dbprefix.'_'.$table);
        $select->where(dbprefix.'_'.$table.'.'.$id.' = (?)', $item_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $item_results = $stmt->fetchAll();   

		//begin transaction for basic settings
		$db->beginTransaction();
		try {

			if(isset($data[deflang])) {
				foreach ($data as $key => $row) {
					$la_id = Globals::getLangIDbyShortcut($key); 
					$lang_exist = Globals::search_array_return_key($item_results, 'la', $la_id);
					if(is_int($lang_exist)){      		
        				$db->update(dbprefix.'_'.$table, $this->prepareData($row) , ' '.$id.' = '.$item_id.' AND la = '.$la_id);
        			}else{
        				$row['la'] = $la_id;
						$row[$id] = $item_id;
						$db->insert(dbprefix.'_'.$table, $this->prepareData($row));	
        			}
        			$avail_languages[] = $la_id;	
				}
				//remove deleted languages
		   		$where = $db->quoteInto('la NOT IN (?)', $avail_languages);
		   		$db->delete(dbprefix.'_'.$table, $where." AND ".$id." = ".$item_id);

			}else{
					if($multilang == "true"){
						$la_id = Globals::getLangIDbyShortcut(deflang);
     					$db->update(dbprefix.'_'.$table, $this->prepareData($data) , ' '.$id.' = '.$item_id.' AND la = '.$la_id);
     				}else{
     					$db->update(dbprefix.'_'.$table, $this->prepareData($data) , ' '.$id.' = '.$item_id);
     				}

			}
		   	 
		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}
	}

	public function delete($table , $item_id , $datatable_config){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $datatable_config[$table]['db']['id'];

		//begin transaction for basic settings
		$db->beginTransaction();
		try {
	   	 	$db->delete(dbprefix.'_'.$table, $id." = ".$item_id);
		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}		
	}

	public function massdelete($table , $item_ids , $datatable_config){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        $id = $datatable_config[$table]['db']['id'];

		//begin transaction for basic settings
		$db->beginTransaction();
		try {
	   	 	foreach ($item_ids as  $item_id) {
	   	 		$db->delete(dbprefix.'_'.$table, $id." = ".$item_id);
	   	 	}
	   	 	
		    $db->commit();
		 
		} catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}		
	}

	public function getData($table , $item_id , $datatable_config){
		$id = $datatable_config[$table]['db']['id'];
		$multilang = $datatable_config[$table]['multilang'];

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_'.$table);
		if($multilang == "true"){
			$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_'.$table.'.la' , array('la_shortcut' => 'shortcuts'));
		}
		$select->where($id.' = ?', $item_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;		
	}

	public function prepareData($data){
		foreach ($data as $key => $value) {
			if(is_array($data[$key])){
				$new_data[$key] = implode(",", $value);
			}else{
				$new_data[$key] = $value;
			}

		}
		return $new_data;
	}

}	