<?php
/**
 * CourseVault Platform
 * Queue Object
 * @copyright Kuku
 * @link http://www.kukuapps.com
 * @author Angelos Katsaris
 */
class queue {

    /**
     * @var $queue object
     */
    public $queue = null;
    /**
     * db instance
     * @var $db 
     */
    public $db;

    /**
     * Queue Preperation
     * @param type $queue_name 
     */
    public function __construct($queue_name){
        $this->_dbconnect();
        $this->queue = new Zend_Queue('Db', array(
                                      'dbAdapter' => $this->db,
                                      'options' => array(Zend_Db_Select::FOR_UPDATE => true),
                                      'name' => $queue_name
                                     )
         );
        
    }

    /**
     * Database Connection
     */
    private function _dbconnect(){
        //database connection
        $this->db = Globals::getDBConnection();
        $this->db->getConnection();
        $this->db->query("SET CHARACTER SET 'utf8'");
        $this->db->query("SET NAMES 'utf8'");
    }

    /**
     * Close database connection
     */
    private function _dbclose(){
        //Close db connection
        $this->db->closeConnection();
    }

    public function  __destruct() {
        $this->_dbclose();
    }

}
?>
