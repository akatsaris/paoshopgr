<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class menu 
{

	public function getMenuData($menuid){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menu_data', array('id' => 'menu_data_id' , 'parrent_id' , 'template_view_id' , 'register_users_only'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('title'));
		$select->where('menu_id = ?', $menuid);
		$select->where('la = ?', $_SESSION['language']['def_lang_id']);
				$select->order('order');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;
	}

	public function getMenus(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menus', array('menu_id' , 'menu_name' , 'menu_position_name'));

		//Query logger
		Globals::setPHPLogger('Query :: '. $select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;
	}

	public function addmenu($menu_name , $menu_position_name){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $data = array('menu_name' => $menu_name,'menu_position_name' => $menu_position_name);
        $db->insert(dbprefix.'_menus', $data);

        $lastInsertId = $db->lastInsertId();
        return array('lastid' => $lastInsertId , 'menu_name' => $menu_name , 'menu_position_name' => $menu_position_name);
	}

	public function deletemenu($menu_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
		$db->delete(dbprefix.'_menus','menu_id = '.$menu_id);
	}

	public function deleteitem($item_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
		$db->delete(dbprefix.'_menu_data','menu_data_id = '.$item_id);		
	}

	public function getmenudata_byID($menu_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menus', array('menu_id' , 'menu_name' , 'menu_position_name'));
		$select->where('menu_id = (?)', $menu_id);
		
		//Query logger
		Globals::setPHPLogger('Query :: '. $select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;	
	}

	public function updatemenu($menu_id , $menu_name , $menu_position_name){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $data = array('menu_name' => $menu_name,'menu_position_name' => $menu_position_name);
        $db->update(dbprefix.'_menus', $data , 'menu_id ='.$menu_id);		
	}

	public function getDropdownMenuData($menuid){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menu_data', array('menu_data_id' , 'parrent_id'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('title'));
		$select->where('menu_id = ?', $menuid);
		$select->where('la = ?', $_SESSION['language']['def_lang_id']);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		
		$this->data = array();
		$this->index = array();	

		
		foreach ($results as $row) {
		    $id = $row["menu_data_id"];
		    $parent_id = $row["parrent_id"] === 0 ? 0 : $row["parrent_id"];
		    $this->data[$id] = $row;
		    $this->index[$parent_id][] = $id;
		}	


		ksort($this->index);

		$menutree = $this->display_child_nodes(0, 0);	
		if(!isset($menutree)){
			$menutree = array();
		}

		return $menutree;	
	}

	public function getMenuPluginData($menu_id , $menu_item_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menu_data', array('id' => 'menu_data_id' , 'parrent_id' , 'type' , 'template_view_id' , 'register_users_only' , 'order'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('title' , 'la' , 'url' , 'active' , 'menu_data_multilang_id'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_menu_data_multilang.la' , array('la_shortcut' => 'shortcuts'));
        $select->where('menu_id = ?', $menu_id);
		$select->where(dbprefix.'_menu_data_multilang.menu_data_id = ?', $menu_item_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$plugin_type = $results[0]['type'];

        require_once "../adminapplication/content_plugins/".$plugin_type."/".$plugin_type.".php"; 
        $plugin_obj = new $plugin_type();

        $fieldsets = $plugin_obj->plugin_options['fieldset'];

        /*
        foreach ($fieldsets as $key => $fieldset) {
        	if(isset($fieldset['fields'][0])){
	        	foreach ($fieldset['fields'] as $key => $field) {
	        		$dbfields[] = $field['dbfield'];
	        	}
        	}else{
                 if(is_array($fieldsets['fields'][0])){
                    foreach ($fieldset as $key => $field) {
                        $dbfields[] = $field['dbfield'];
                    }
                }else{
                    $dbfields[] = $fieldset['fields']['dbfield'];
                }
        	}
        }
        */

        $db_table = $plugin_obj->plugin_options['dbtable'];

        //get multilang ids for plugins
        foreach($results as $values){
            $multilang_ids[] = $values['menu_data_multilang_id'];
        }

        $select = $db->select();
        $select->from(dbprefix.'_menu_data_multilang' , array('la'));
        $select->join($db_table , dbprefix.'_menu_data_multilang.menu_data_multilang_id = '.$db_table.'.menu_data_multilang_id');
        $select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_menu_data_multilang.la' , array('la_shortcut' => 'shortcuts'));
        $select->where($db_table.'.menu_data_multilang_id IN (?)', $multilang_ids);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $plugin_results = $stmt->fetchAll();

        return array('menu_results' => $results , 'plugin_results' => $plugin_results);
	}


	protected function display_child_nodes($parent_id, $level)
	{
	    $parent_id = $parent_id === 0 ? 0 : $parent_id;
	    if (isset($this->index[$parent_id])) {
	        foreach ($this->index[$parent_id] as $id) {
	            $this->menutree[][$id] = str_repeat("-", $level) . $this->data[$id]["title"];
	            $this->display_child_nodes($id, $level + 1 );
	        }
	    }
	    return $this->menutree;
	}

}