<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class product {

	public function saveProduct($product , $product_multilang , $tags , $product_attributes , $photogallery_type , $gallery_photo, $accompaying_products){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
		
		$db->beginTransaction();
		try {
	        //add data to main table
	        $db->insert(dbprefix.'_products', $product);
	        $prd_lastinserted_id = $db->lastInsertId();

	        //insert data to multilang table
	        foreach ($product_multilang as $key => $ins_row) {
				$la_id = Globals::getLangIDbyShortcut($key);

				$multilang_data = $ins_row;
				$multilang_data['product_id'] = $prd_lastinserted_id;
				$multilang_data['la'] = $la_id;
				$db->insert(dbprefix.'_products_multilang', $multilang_data);
			}

			//insert tags
			if(!empty($tags)){
				foreach ($tags as $key => $tag) {
					$tag_data['product_id'] = $prd_lastinserted_id;
					$tag_data['tag_id'] = $tag;
					
					$db->insert(dbprefix.'_product_tags', $tag_data);
				}
			}

			//insert attributes
			if(!empty($product_attributes)){
				$attributes_array = array();
				foreach ($product_attributes as $key => $attribute) {
					foreach ($attribute as $key => $attr) {
						if(is_numeric($key)){
							$attributes_array[] = $attr['attrid'];
						}
					}	
				}
	 			
	 			$attributes_array = array_unique($attributes_array);
	 			foreach ($attributes_array as $key => $attr_value) {
	 				$attr_data['attr_id'] = $attr_value;
					$attr_data['product_id'] = $prd_lastinserted_id;

					$db->insert(dbprefix.'_product_attributes', $attr_data);
	 			}
 			}

 			//insert stock data
 			if(!empty($product_attributes)){
	 			unset($attributes_array);
				foreach ($product_attributes as $key => $attribute) {
					foreach ($attribute as $key => $attr) {
						if(is_numeric($key)){
							$attributes_array[] = $attr['attrid'];
							asort($attributes_array);
						}
						//create product attribute code
						$product_code_str = "";
						foreach ($attributes_array as $key => $value) {
							$product_code_str .= $value."_"; 
						}
						$product_code_str = substr($product_code_str, 0, -1);


						
					}
					$stock_data = array();
					$stock_data['code'] = $product_code_str;
					$stock_data['product_id'] = $prd_lastinserted_id;
					$stock_data['extra_cost'] = $attribute['price'];
					$stock_data['quantity'] = $attribute['quantity'];

					unset($attributes_array);
					$product_code_str = "";
					$db->insert(dbprefix.'_stock_control', $stock_data);		
				}
			}

			//Insert Product accompaying relation
 			if(!empty($accompaying_products)){
	 			unset($accompaying_array);
				foreach ($accompaying_products as $key => $acc_product) {
					foreach ($acc_product as $key => $prd) {
						if(is_numeric($key)){
							$accompaying_array[] = $prd['tagid'];
						}
						//create comma separated tags
						$accompaying_tag_str = "";
						foreach ($accompaying_array as $key => $value) {
							$accompaying_tag_str .= $value.","; 
						}
						$accompaying_tag_str = substr($accompaying_tag_str, 0, -1);


						
					}
					$accompaying_data = array();
					$accompaying_data['tags'] = $accompaying_tag_str;
					$accompaying_data['product_id'] = $prd_lastinserted_id;
					$accompaying_data['title'] = $acc_product['title'];

					unset($accompaying_array);
					$accompaying_tag_str = "";
					$db->insert(dbprefix.'_product_accompaying_relation', $accompaying_data);		
				}
			}

			//Insert Gallery Data
			if(!empty($gallery_photo)){
				$gallery_array = array();
				foreach ($gallery_photo as $key => $photo) {
					$gallery_array['photo_path'] = $photo;
					$gallery_array['product_id'] = $prd_lastinserted_id;
					$gallery_array['gallery_id'] = $photogallery_type['gallery_id'];

					$db->insert(dbprefix.'_product_gallery_files', $gallery_array);
				}
			}

	        $db->commit();
	    } catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		}    
	}

	public function updateProduct($product_id , $product , $product_multilang , $tags , $product_attributes , $photogallery_type , $gallery_photo , $accompaying_products){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();
        //Get Item Data
        $select = $db->select();
        $select->from(dbprefix.'_products_multilang');
        $select->where(dbprefix.'_products_multilang.product_id = (?)', $product_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $product_results = $stmt->fetchAll();  

		$db->beginTransaction();
		try {
	        $db->update(dbprefix.'_products', $product , 'product_id ='.$product_id);

	        //Product Multilang Update Procedure
			foreach ($product_multilang as $key => $value) {
				$la_id = Globals::getLangIDbyShortcut($key);
				$lang_exist = Globals::search_array_return_key($product_results, 'la', $la_id);
				
				if(is_int($lang_exist)){
					$prd_multilang_id = $product_results[$lang_exist]['product_multilang_id'];
					//update
					$data = $product_multilang[$key];
					$db->update(dbprefix.'_products_multilang', $data , 'product_multilang_id = '.$prd_multilang_id);
				}else{
					//insert
	        		$data = $product_multilang[$key];
	        		$data['product_id'] = $product_id;
	        		$data['la'] = $la_id;
	        		$db->insert(dbprefix.'_products_multilang', $data);
	        						
				}
				$avail_languages[] = $la_id;
        		
			}
		   	 
		   	//remove deleted languages
		   	$where = $db->quoteInto('la NOT IN (?)', $avail_languages);
		   	$db->delete(dbprefix.'_products_multilang', $where." AND product_id = ".$product_id);	        

		   	//remove and add Tags
		   	$db->delete(dbprefix.'_product_tags', "product_id = ".$product_id);
			//insert tags
			if(!empty($tags)){
				foreach ($tags as $key => $tag) {
					$tag_data['product_id'] = $product_id;
					$tag_data['tag_id'] = $tag;
					
					$db->insert(dbprefix.'_product_tags', $tag_data);
				}
			}

			//remove and add attributes
			$db->delete(dbprefix.'_product_attributes', "product_id = ".$product_id);
			//insert attributes
			if(!empty($product_attributes)){
				$attributes_array = array();
				foreach ($product_attributes as $key => $attribute) {
					foreach ($attribute as $key => $attr) {
						if(is_numeric($key)){
							$attributes_array[] = $attr['attrid'];
						}
					}	
				}
	 			
	 			$attributes_array = array_unique($attributes_array);
	 			foreach ($attributes_array as $key => $attr_value) {
	 				$attr_data['attr_id'] = $attr_value;
					$attr_data['product_id'] = $product_id;

					$db->insert(dbprefix.'_product_attributes', $attr_data);
	 			}
 			}

 			//remove and add stock
 			$db->delete(dbprefix.'_stock_control', "product_id = ".$product_id);
 			//insert stock data
 			if(!empty($product_attributes)){
	 			unset($attributes_array);
				foreach ($product_attributes as $key => $attribute) {
					foreach ($attribute as $key => $attr) {
						if(is_numeric($key)){
							$attributes_array[] = $attr['attrid'];
							asort($attributes_array);
						}
						//create product attribute code
						$product_code_str = "";
						foreach ($attributes_array as $key => $value) {
							$product_code_str .= $value."_"; 
						}
						$product_code_str = substr($product_code_str, 0, -1);
						
					}
					$stock_data = array();
					$stock_data['code'] = $product_code_str;
					$stock_data['product_id'] = $product_id;
					$stock_data['extra_cost'] = $attribute['price'];
					$stock_data['quantity'] = $attribute['quantity'];

					unset($attributes_array);
					$product_code_str = "";
					$db->insert(dbprefix.'_stock_control', $stock_data);		
				}
			}

 			//remove and add accomp products
 			$db->delete(dbprefix.'_product_accompaying_relation', "product_id = ".$product_id);
 			//insert stock data
 			if(!empty($accompaying_products)){
	 			unset($accompaying_array);
				foreach ($accompaying_products as $key => $acc_product) {
					foreach ($acc_product as $key => $prd) {
						if(is_numeric($key)){
							$accompaying_array[] = $prd['tagid'];
						}
						//create product acc_product code
						$accompaying_tag_str = "";
						foreach ($accompaying_array as $key => $value) {
							$accompaying_tag_str .= $value.","; 
						}
						$accompaying_tag_str = substr($accompaying_tag_str, 0, -1);
						
					}
					$accompaying_data = array();
					$accompaying_data['tags'] = $accompaying_tag_str;
					$accompaying_data['product_id'] = $product_id;
					$accompaying_data['title'] = $acc_product['title'];

					unset($accompaying_array);
					$accompaying_tag_str = "";
					$db->insert(dbprefix.'_product_accompaying_relation', $accompaying_data);		
				}
			}			

			//remove and add galleries
			$db->delete(dbprefix.'_product_gallery_files', "product_id = ".$product_id);
			//Insert Gallery Data
			if(!empty($gallery_photo)){
				$gallery_array = array();
				foreach ($gallery_photo as $key => $photo) {
					$gallery_array['photo_path'] = $photo;
					$gallery_array['product_id'] = $product_id;
					$gallery_array['gallery_id'] = $photogallery_type['gallery_id'];

					$db->insert(dbprefix.'_product_gallery_files', $gallery_array);
				}
			}

	        $db->commit();
	    } catch (Exception $e) {
		    $db->rollBack();
		    echo $e->getMessage();
		} 

	}

	public function getProduct($product_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        //product default values
        $select = $db->select();
		$select->from(dbprefix.'_products');
		$select->where(dbprefix.'_products.product_id = ?', $product_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$product_default = $stmt->fetchAll();	
		$product['prd_default'] = $product_default[0];

        //multilang product values
        $select = $db->select();
		$select->from(dbprefix.'_products_multilang');
		$select->join(dbprefix.'_languages' , dbprefix.'_products_multilang.la = '.dbprefix.'_languages.lang_id' , array('shortcuts'));
		$select->where(dbprefix.'_products_multilang.product_id = ?', $product_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$product_multilang = $stmt->fetchAll();	
		$multilang['multilang'] = $product_multilang;

        //product Tags
        $select = $db->select();
		$select->from(dbprefix.'_product_tags' , array('tag_id'));
		$select->where(dbprefix.'_product_tags.product_id = ?', $product_id);


		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$product_tags = $stmt->fetchAll();	
		
		foreach ($product_tags as $key => $value) {
			$prd_tags[] = $value['tag_id'];
		}
		$tags['prd_tags'] = $prd_tags;

		//Product Attributes
		$select = $db->select();
		$select->from(dbprefix.'_stock_control');
		$select->where(dbprefix.'_stock_control.product_id = ?', $product_id);


		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$stock_data = $stmt->fetchAll();
		$stock_attributes = array();
		foreach ($stock_data as $key => $stock_value) {
			$stock_attributes[$key]['attribs'] = explode("_", $stock_value['code']);
			$stock_attributes[$key]['extra_cost'] = $stock_value['extra_cost'];
			$stock_attributes[$key]['quantity'] = $stock_value['quantity'];
		}

		$stock['prd_stock']	= $stock_attributes;

		//Accompanying products
		$select = $db->select();
		$select->from(dbprefix.'_product_accompaying_relation');
		$select->where(dbprefix.'_product_accompaying_relation.product_id = ?', $product_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$acc_data = $stmt->fetchAll();
		$product_tags = array();
		foreach ($acc_data as $key => $acc_value) {
			$product_tags[$key]['tagsids'] = explode(",", $acc_value['tags']);
			$product_tags[$key]['acc_title'] = $acc_value['title'];

		}

		$acc_products_rel['acc_products'] = $product_tags;	

		//Gallerys
		$select = $db->select();
		$select->from(dbprefix.'_product_gallery_files');
		$select->where(dbprefix.'_product_gallery_files.product_id = ?', $product_id);


		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$gallery_data = $stmt->fetchAll();		
		$gallery['prd_gallery'] = $gallery_data;

		$results = array_merge($product , $multilang , $tags , $stock , $gallery , $acc_products_rel);

		return $results;
	}

}	