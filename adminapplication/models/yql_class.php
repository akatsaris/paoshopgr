<?php
/**
 * CourseVault Platform
 * YQL Webservice implementation
 * @copyright Kuku
 * @link http://www.kukuapps.com
 * @author Angelos Katsaris
 */
class yql_class{

    /**
     * the client
     * @var object 
     */
    private $_client;
    /**
     * @var boolean $error (true/false)
     */
    public  $error;
    /**
     * Keep the error message
     * @var string $error_message 
     */
    public  $error_message;

    /**
     * Setting up the client
     */
    private function _setClient(){
        $this->_client = new Zend_Http_Client();
        $this->_client->setConfig(array(
            'adapter'      => 'Zend_Http_Client_Adapter_Socket',
            'maxredirects' => 0,
            'timeout'      => 60
        ));
        $this->_client->setUri('http://query.yahooapis.com/v1/public/yql');
    }

    /**
     * Query preperation
     * @param type $query 
     */
    private function _prepare($query){
            // Reset parameters
            $this->_client->resetParameters();

            // Add authentication data
            $this->_client->setMethod('POST');
            $this->_client->setParameterPost(array(
                    'q'         => $query,
                    'env'       => 'http://datatables.org/alltables.env',
                    'format'    => 'json'
            ));
    }

    /**
     * Returns Response in JSON format
     * @return json data 
     */
    private function _process(){
        $httpResponse = $this->_client->request();

        if (! $httpResponse->isSuccessful()) {
            $this->error = true;
            $this->error_message = "Service initialization failed ";
        }
        $result = $httpResponse->getBody();
        return json_decode($result);
    }


   /**
    * Google Translation
    * @param type $word
    * @param type $lang
    * @return type 
    */
   public function translate($word , $lang){
        try {
          $this->_setClient();
          $this->_prepare("select * from google.translate where q='".strtolower($word)."' and target='".$lang."'");
          $result = $this->_process();
        } catch (Zend_Rest_Client_Exception $e) {
            $this->error = true;
            $this->error_message = $e->getResponse();
        } catch (Exception $e) {
            $this->error = true;
            $this->error_message = $e->getMessage();
        }
        return $result->query->results->translatedText;
   }

   /**
    * Get Weather using latitude and longitude
    * @param type $latitude
    * @param type $longitude
    * @return type 
    */
   public function getWeather($latitude , $longitude){
        try {
          $this->_setClient();
          $this->_prepare("select * from weather.woeid where w in (select place.woeid from flickr.places where lat='".$latitude."' and lon='".$longitude."')");
          $result = $this->_process();
        } catch (Zend_Rest_Client_Exception $e) {
            $this->error = true;
            $this->error_message = $e->getResponse();
        } catch (Exception $e) {
            $this->error = true;
            $this->error_message = $e->getMessage();
        }
        return $result;
   }

  /**
   * Get Geo info by IP address (needs an api key)
   * @param type $ip
   * @return type 
   */
  public function getGeoCodeByIP($ip){
        try {
          $this->_setClient();
          $this->_prepare("select * from ip.location where ip='".$ip."' and key='21a78e81f8af2666d57951fa96d4006527129d0c8ed86fbe57339696aa02b684'");
          $result = $this->_process();
        } catch (Zend_Rest_Client_Exception $e) {
            $this->error = true;
            $this->error_message = $e->getResponse();
        } catch (Exception $e) {
            $this->error = true;
            $this->error_message = $e->getMessage();
        }

        $country_code = $result->query->results->Response->CountryCode;
        $timezone = $result->query->results->Response->TimezoneName;
        $result = array('country_code' => $country_code , 'timezone' => $timezone);
        return $result;
   }


}
?>
