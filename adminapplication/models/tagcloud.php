<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class tagcloud 
{

	public function getTags(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_tags' , array('tag_id','tag'));
		$select->join(dbprefix.'_languages' , dbprefix.'_languages.lang_id = '.dbprefix.'_tags.la' , array());
		$select->where(dbprefix.'_languages.shortcuts = ?', deflang);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $value) {
			$tags[][$value['tag_id']] = $value['tag'];
		}
		return $tags;		
	}

}	