<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */

        $content = array(
            'product_manager' => array('read'),
            'order_manager'   => array(),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );
        
        $files = array(
            'product_manager' => array('read', 'modify' , 'delete'),
            'order_manager'   => array('read'),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );
        
        $orders = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read', 'modify' , 'delete'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read', 'modify')
        );
        
        $users = array(
            'product_manager' => array(),
            'order_manager'   => array(),
            'content_manager' => array(),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array(),
        );  

        $shipping = array(
            'product_manager' => array('read', 'modify' , 'delete'),
            'order_manager'   => array('read', 'modify' , 'delete'),
            'content_manager' => array(),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        ); 

        $customers = array(
            'product_manager' => array(),
            'order_manager'   => array('read', 'modify' , 'delete'),
            'content_manager' => array(),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read', 'modify'),
        ); 

        $tags = array(
            'product_manager' => array('read', 'modify' , 'delete'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read', 'modify'),
        );  

        $currencies = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        );    

        $languages = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        );                      


        $attributes_groups = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        ); 

        $attributes = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        ); 

        $home_sections = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read'),
        ); 

        $gallery_types = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );           

        $products = array(
            'product_manager' => array('read', 'modify' , 'delete'),
            'order_manager'   => array('read'),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $templates = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $template_view = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );   

        $template_sections = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );  

        $widgets = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $templatewidgets = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        ); 

        $multilanguage_html_content = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $simple_content = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );       

        $internet_users = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read', 'modify' , 'delete'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read', 'modify' , 'delete')
        );     

        $accompaying_products = array(
            'product_manager' => array('read', 'modify' , 'delete'),
            'order_manager'   => array('read'),
            'content_manager' => array('read', 'modify' , 'delete'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $shipping_providers = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $shipping_packs = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $shipping_pack_kg = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $shipping_areas = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $pay_methods = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $shipping_payment_rel = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );

        $orders = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );   

        $order_status = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read')
        );  

        $vouchers = array(
            'product_manager' => array('read'),
            'order_manager'   => array('read', 'modify' , 'delete'),
            'content_manager' => array('read'),
            'administrator'   => array('read', 'modify' , 'delete'),
            'support'         => array('read', 'modify' , 'delete')
        );    

        $permitions = array('content' => $content , 
                            'files' => $files , 
                            'orders' => $orders, 
                            'users' => $users , 
                            'shipping' => $shipping , 
                            'customers' => $customers,
                            'tags' => $tags,
                            'currencies' => $currencies,
                            'languages' => $languages,
                            'attributes_groups' => $attributes_groups,
                            'attributes' => $attributes,
                            'home_sections' => $home_sections,
                            'products' => $products,
                            'gallery_types' => $gallery_types,
                            'templates' => $templates,
                            'template_view' => $template_view,
                            'template_sections' => $template_sections,
                            'widgets' => $widgets,
                            'templatewidgets' => $templatewidgets,
                            'multilanguage_html_content' => $multilanguage_html_content,
                            'simple_content' => $simple_content,
                            'internet_users' => $internet_users,
                            'accompaying_products' => $accompaying_products,
                            'shipping_providers' => $shipping_providers,
                            'shipping_packs' => $shipping_packs,
                            'shipping_pack_kg' => $shipping_pack_kg,
                            'shipping_areas' => $shipping_areas,
                            'pay_methods' => $pay_methods,
                            'shipping_payment_rel' => $shipping_payment_rel,
                            'orders' => $orders,
                            'order_status' => $order_status,
                            'vouchers' => $vouchers
                             );

        
        //Roles
        $roles = array('product_manager','order_manager','content_manager','administrator','support');

        //acl
        $acl = array ($permitions , $roles);
        
        //$registry = new Zend_Registry($acl);
        Zend_Registry::set('acl' , $acl);
?>