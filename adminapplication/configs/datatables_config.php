<?php
//datatables config
$dpg_config_tables = array("tags" => 
							array(
								  'db' => array('multilang_id' => 'tag_multilang_id' , 'id' => 'tag_id'),
								  'header_titles' => array('id','Tag Name'),
								  'rows' => array('tag_id','tag'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Tags",
								  'addbutton' => 'add tag',
								  'add_form_title' => 'add tag form',
								  'edit_button' => 'edit tag',
								  'edit_form_title' => 'edit tag form',
								  'fieldset_var' => 'tags',
								  'multilang' => 'true' 
							),
							"currencies" => 
							array(
								  'db' => array('id' => 'currency_id'),
								  'header_titles' => array('id','Currency Name' , 'Currency Symbol'),
								  'rows' => array('currency_id' ,'currency_name','currency_symbol'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Currencies",
								  'addbutton' => 'add currency',
								  'add_form_title' => 'add currency form',
								  'edit_button' => 'edit currency',
								  'edit_form_title' => 'edit currency form',
								  'fieldset_var' => 'currencies',
								  'multilang' => 'false' 
							),
							"languages" => 
							array(
								  'db' => array('id' => 'lang_id'),
								  'header_titles' => array('id','Digits' , 'Name' , 'Shortcuts'),
								  'rows' => array('lang_id','digits','name','shortcuts'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Languages",
								  'addbutton' => 'add language',
								  'add_form_title' => 'add language form',
								  'edit_button' => 'edit language',
								  'edit_form_title' => 'edit language form',
								  'fieldset_var' => 'languages',
								  'multilang' => 'false' 
							),
							"attributes_groups" => 
							array(
								  'db' => array('multilang_id' => 'attr_group_multilang_id' , 'id' => 'group_id'),
								  'header_titles' => array('id','Group Name'),
								  'rows' => array('group_id','group_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Attributes Groups",
								  'addbutton' => 'add group',
								  'add_form_title' => 'add group form',
								  'edit_button' => 'edit group',
								  'edit_form_title' => 'edit group form',
								  'fieldset_var' => 'attr_groups',
								  'multilang' => 'true' 
							),
							"attributes" => 
							array(
								  'db' => array('multilang_id' => 'attr_multilang_id' , 'id' => 'attr_id'),
								  'join' => array('join_table' => 'attributes_groups', 'join_id1' => 'group_id' , 'join_id2' => 'attr_group_id' , 'fields' => array('group_name') , 'group' => array(dbprefix.'_attributes.attr_value')),
								  'header_titles' => array('id','Attribute Name' , 'Group Name'),
								  'rows' => array('attr_id','attr_value' , 'group_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Attributes",
								  'addbutton' => 'add attribute',
								  'add_form_title' => 'add attribute form',
								  'edit_button' => 'edit attribute',
								  'edit_form_title' => 'edit attribute form',
								  'fieldset_var' => 'attributes',
								  'multilang' => 'true' 
							),
							"home_sections" => 
							array(
								  'db' => array('id' => 'home_section_id'),
								  'header_titles' => array('id','Home Tag' , 'Name'),
								  'rows' => array('home_section_id','home_tag' , 'name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Template Sections",
								  'addbutton' => 'add section',
								  'add_form_title' => 'add template section form',
								  'edit_button' => 'edit section',
								  'edit_form_title' => 'edit section form',
								  'fieldset_var' => 'home_sections',
								  'multilang' => 'false' 
							),
							"products" => 
							array(
								  'db' => array('id' => 'product_id'),
								  'join' => array('join_table' => 'products_multilang', 'join_id1' => 'product_id' , 'join_id2' => 'product_id' , 'fields' => array('name' , 'prd_description') , 'group' => array(dbprefix.'_products.product_id')),
								  'header_titles' => array('id' , 'Name' , 'Product Code'),
								  'rows' => array('product_id' ,'name' , 'product_code'),
								  'actions' => array(
								  					array("id" => "edit-product", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-product", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Products",
								  'addbutton' => 'add product',
								  'add_form_title' => 'add product form',
								  'edit_button' => 'edit product',
								  'edit_form_title' => 'edit product form',
								  'fieldset_var' => 'products',
								  'multilang' => 'false' 
							),
							"gallery_types" => 
							array(
								  'db' => array('id' => 'gallery_type_id'),
								  'header_titles' => array('id','Name' , 'Width' , 'Height'),
								  'rows' => array('gallery_type_id','gall_name' , 'gall_width', 'gall_height'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "photogalleries",
								  'addbutton' => 'add photogallery type',
								  'add_form_title' => 'add photogallery section form',
								  'edit_button' => 'edit photogallery type',
								  'edit_form_title' => 'edit photogallery section form',
								  'fieldset_var' => 'gallery_types',
								  'multilang' => 'false' 
							),
							"templates" => 
							array(
								  'db' => array('id' => 'template_id'),
								  'header_titles' => array('id','Name' , 'File'),
								  'rows' => array('template_id','template_name' , 'template_file'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Template Manager",
								  'addbutton' => 'add template',
								  'add_form_title' => 'add template form',
								  'edit_button' => 'edit template',
								  'edit_form_title' => 'edit template form',
								  'fieldset_var' => 'templates',
								  'multilang' => 'false' 
							),	
							"template_view" => 
							array(
								  'db' => array('id' => 'template_view_id'),
								  'header_titles' => array('id','Template Name'),
								  'rows' => array('template_view_id','template_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete"),
								  					array("id" => "extraedit", "icon" => "icon12 wpzoom-settings" , "title" => "template setup")
								  			   ),
								  'main_title' => "Template View Manager",
								  'addbutton' => 'add view template',
								  'add_form_title' => 'add view template form',
								  'edit_button' => 'edit view template',
								  'edit_form_title' => 'edit view template form',
								  'fieldset_var' => 'template_view',
								  'multilang' => 'false' 
							),
							"template_sections" => 
							array(
								  'db' => array('id' => 'template_section_id'),
								  'join' => array('join_table' => 'templates', 'join_id1' => 'template_id' , 'join_id2' => 'template_id' , 'fields' => array('template_name')),
								  'header_titles' => array('id','Section Name' , 'Section File' , 'Template Name'),
								  'rows' => array('template_section_id','section_name' , 'section_file' , 'template_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Section Manager",
								  'addbutton' => 'add template section',
								  'add_form_title' => 'add template section form',
								  'edit_button' => 'edit section template',
								  'edit_form_title' => 'edit template section form',
								  'fieldset_var' => 'template_sections',
								  'multilang' => 'false' 
							),							
							"widgets" => 
							array(
								  'db' => array('id' => 'widget_id'),
								  'header_titles' => array('id','Widget Name'),
								  'rows' => array('widget_id','widget_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Widget Manager",
								  'addbutton' => 'add widget',
								  'add_form_title' => 'add widget form',
								  'edit_button' => 'edit widget',
								  'edit_form_title' => 'edit widget form',
								  'fieldset_var' => 'widget',
								  'multilang' => 'false' 
							),
							"multilanguage_html_content" => 
							array(
								  'db' => array('multilang_id' => 'multilanguage_html_content_id' , 'id' => 'content_id'),
								  'header_titles' => array('id','Name'),
								  'rows' => array('content_id','name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Html Content",
								  'addbutton' => 'add html content',
								  'add_form_title' => 'add html content form',
								  'edit_button' => 'edit html content',
								  'edit_form_title' => 'edit html content form',
								  'fieldset_var' => 'html_content',
								  'multilang' => 'true' 
							),
							"simple_content" => 
							array(
								  'db' => array('multilang_id' => 'cnt_multilang_id' , 'id' => 'cnt_id'),
								  'join' => array('join_table' => 'home_sections', 'join_id1' => 'home_section_id' , 'join_id2' => 'section' , 'fields' => array('name'), 'group' => array(dbprefix.'_simple_content.cnt_id')),
								  'header_titles' => array('id','Title' , 'Description' , 'Section'),
								  'rows' => array('cnt_id','cnt_title' , 'cnt_desc' , 'name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Simple Content",
								  'addbutton' => 'add content',
								  'add_form_title' => 'add content form',
								  'edit_button' => 'edit content',
								  'edit_form_title' => 'edit content form',
								  'fieldset_var' => 'simple_content',
								  'multilang' => 'true' 
							),
							"internet_users" => 
							array(
								  'db' => array('id' => 'user_id'),
								  'header_titles' => array('Firstname' , 'Lastname' , 'email'),
								  'rows' => array('firstname' , 'lastname' , 'username'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Customers",
								  'addbutton' => 'add new customer',
								  'add_form_title' => 'add new customer form',
								  'edit_button' => 'edit customer',
								  'edit_form_title' => 'edit customer form',
								  'fieldset_var' => 'customers',
								  'multilang' => 'false' 
							),
							"shipping_providers" => 
							array(
								  'db' => array('multilang_id' => 'shipping_provider_multilang_id' , 'id' => 'shipping_provider_id'),
								  'header_titles' => array('id','Provider Name'),
								  'rows' => array('shipping_provider_id','shipping_provider_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Shipping Providers",
								  'addbutton' => 'add shipping provider',
								  'add_form_title' => 'add shipping provider form',
								  'edit_button' => 'edit shipping provider',
								  'edit_form_title' => 'edit shipping provider form',
								  'fieldset_var' => 'shipping_providers',
								  'multilang' => 'true' 
							),
							"shipping_packs" => 
							array(
								  'db' => array('multilang_id' => 'shipping_packet_multilang_id' , 'id' => 'shipping_packet_id'),
								  'header_titles' => array('id','Shipping Packet Name' , 'Description'),
								  'rows' => array('shipping_packet_id','shipping_pack_name' , 'shipping_pack_description'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Shipping Packs",
								  'addbutton' => 'add shipping pack',
								  'add_form_title' => 'add shipping pack form',
								  'edit_button' => 'edit shipping pack',
								  'edit_form_title' => 'edit shipping pack form',
								  'fieldset_var' => 'shipping_packs',
								  'multilang' => 'true' 
							),
							"shipping_pack_kg" => 
							array(
								  'db' => array('id' => 'shipping_pack_kg_id'),
								  'join' => array('join_table' => 'shipping_packs', 'join_id1' => 'shipping_packet_id' , 'join_id2' => 'shipping_pack_id' , 'fields' => array('shipping_pack_name') ,'group' => array(dbprefix.'_shipping_pack_kg.shipping_pack_kg_id')),
								  'header_titles' => array('id','Min Weight' , 'Max Weight' , 'Price' , 'Packet Name'),
								  'rows' => array('shipping_pack_kg_id','min_kg' , 'max_kg' , 'shipping_price' , 'shipping_pack_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Shipping packs weight rate",
								  'addbutton' => 'add shipping pack weight rate',
								  'add_form_title' => 'add shipping pack weight rate form',
								  'edit_button' => 'edit shipping pack weight rate',
								  'edit_form_title' => 'edit shipping pack weight rate form',
								  'fieldset_var' => 'shipping_pack_kg',
								  'multilang' => 'false' 
							),
							"shipping_areas" => 
							array(
								  'db' => array('id' => 'shipping_area_id'),
								  'join' => array('join_table' => 'shipping_packs', 'join_id1' => 'shipping_packet_id' , 'join_id2' => 'shipping_pack_id' , 'fields' => array('shipping_pack_name')),
								  'join2' => array('join_table' => 'areas', 'join_id1' => 'area_id' , 'join_id2' => 'area_id' , 'fields' => array('area_name'),'group' => array(dbprefix.'_shipping_areas.shipping_area_id')),
								  'header_titles' => array('id','Area' , 'Packet Name'),
								  'rows' => array('shipping_area_id','area_name' , 'shipping_pack_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Packets & Area relations",
								  'addbutton' => 'add area relation',
								  'add_form_title' => 'add area relation form',
								  'edit_button' => 'edit area relation',
								  'edit_form_title' => 'edit area relation form',
								  'fieldset_var' => 'shipping_areas',
								  'multilang' => 'false' 
							),
							"pay_methods" => 
							array(
								  'db' => array('multilang_id' => 'pay_method_multilang_id_id' , 'id' => 'pay_method_id'),
								  'header_titles' => array('id','Name' , 'Description'),
								  'rows' => array('pay_method_id','pay_name' , 'pay_description'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Payment Methods",
								  'addbutton' => 'add payment method',
								  'add_form_title' => 'add payment method form',
								  'edit_button' => 'edit payment method',
								  'edit_form_title' => 'edit payment method form',
								  'fieldset_var' => 'payment_methods',
								  'multilang' => 'true' 
							),
							"shipping_payment_rel" => 
							array(
								  'db' => array('id' => 'sp_rel_id'),
								  'join' => array('join_table' => 'pay_methods', 'join_id1' => 'pay_method_id' , 'join_id2' => 'pay_method_id' , 'fields' => array('pay_name')),
								  'join2' => array('join_table' => 'shipping_packs', 'join_id1' => 'shipping_packet_id' , 'join_id2' => 'shipping_packet_id' , 'fields' => array('shipping_pack_name'),'group' => array(dbprefix.'_shipping_payment_rel.sp_rel_id')),
								  'header_titles' => array('id','Packet Name' , 'Pay Method'),
								  'rows' => array('sp_rel_id','shipping_pack_name' , 'pay_name'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Packets & Payment relations",
								  'addbutton' => 'add payment relation',
								  'add_form_title' => 'add payment relation form',
								  'edit_button' => 'edit payment relation',
								  'edit_form_title' => 'edit payment relation form',
								  'fieldset_var' => 'shipping_payment_rel',
								  'multilang' => 'false' 
							),
							"orders" => 
							array(
								  'db' => array('id' => 'order_id'),
								  'join' => array('join_table' => 'order_shipping', 'join_id1' => 'order_id' , 'join_id2' => 'order_id' , 'fields' => array('firstname' , 'lastname' , 'email' , 'address' ,'phone')),
								  'header_titles' => array('id','Firstname' , 'Lastname' , 'email' , 'order number' , 'Merchant' , 'Phone'),
								  'rows' => array('order_id','firstname' , 'lastname' , 'email' , 'order_number_id' , 'merchant' , 'phone'),
								  'actions' => array(
								   					array("id" => "edit-order", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit")
								   			   ),
								  'main_title' => "Orders",
								  //'addbutton' => 'add new customer',
								  'add_form_title' => 'add new customer form',
								  'edit_button' => 'edit customer',
								  'edit_form_title' => 'edit customer form',
								  'fieldset_var' => 'customers',
								  'multilang' => 'false' 
							),
							"order_status" => 
							array(
								  'db' => array('id' => 'ord_status_id'),
								  'header_titles' => array('id','Order Status' , 'Email Template'),
								  'rows' => array('ord_status_id','ord_status','ord_email_template'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Order Status",
								  'addbutton' => 'add order status',
								  'add_form_title' => 'add order status form',
								  'edit_button' => 'edit order status',
								  'edit_form_title' => 'edit order status form',
								  'fieldset_var' => 'order_status',
								  'multilang' => 'false' 
							),
							"vouchers" => 
							array(
								  'db' => array('id' => 'v_id'),
								  'header_titles' => array('id','Code','Price' , 'Percent'),
								  'rows' => array('v_id' ,'v_code', 'price' , 'percent'),
								  'actions' => array(
								  					array("id" => "edit-tag", "icon" => "icon12 icomoon-icon-pencil" , "title" => "Edit"),
								  					array("id" => "delete-tag", "icon" => "icon12 icomoon-icon-remove" , "title" => "Delete")
								  			   ),
								  'main_title' => "Vouchers",
								  'addbutton' => 'add voucher',
								  'add_form_title' => 'add voucher form',
								  'edit_button' => 'edit voucher',
								  'edit_form_title' => 'edit voucher form',
								  'fieldset_var' => 'vouchers',
								  'multilang' => 'false' 
							)
	);

?>