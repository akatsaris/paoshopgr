<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class accordeon extends plugins
{
	public $plugin_options;

	public function __construct(){
		$this->plugin_options = parent::readXML(get_class($this));
	}

	public function checkMultilanguage($post){
		parent::checkMultilangPostData($post);
	}

	public function saveData($post){
		parent::savePostData($post);
		//do some other stuff or other procedure
	}

	public function updateData($post , $item_id){
		parent::updatePostData($post,$item_id);
		//do some other stuff or other procedure
	}	
}	