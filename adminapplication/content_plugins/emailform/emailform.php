<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class emailform extends plugins
{
	public $plugin_options;
	public $postData;

	public function __construct($post){
		$this->plugin_options = parent::readXML(get_class($this));
		$this->postData = parent::preparePostData($post);
	}
}	