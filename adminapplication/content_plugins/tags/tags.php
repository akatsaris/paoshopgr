<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class tags extends plugins
{
	public $plugin_options;

	public function __construct(){
		$this->plugin_options = parent::readXML(get_class($this));
	}

	public function checkMultilanguage($post){
		parent::checkMultilangPostData($post);
	}

	public function saveData($post){
		$tags = $post['tagsform'];
		unset($post['tagsform']);

		foreach ($tags as $key => $value) {
			if(is_array($value['tags'])){
				$tags[$key]['tags'] = implode(",", $value['tags']);
			}else{
				$tags[$key][key($value)] = $value[key($value)];
			}
		}

		$post['tagsform'] = $tags;

		parent::savePostData($post);
		//do some other stuff or other procedure
	}

	public function updateData($post , $item_id){
		$tags = $post['tagsform'];
		unset($post['tagsform']);

		foreach ($tags as $key => $value) {
			if(is_array($value['tags'])){
				$tags[$key]['tags'] = implode(",", $value['tags']);
			}else{
				$tags[$key][key($value)] = $value[key($value)];
			}
		}

		$post['tagsform'] = $tags;		
		parent::updatePostData($post,$item_id);
		//do some other stuff or other procedure
	}	
}	