<?php
class Zend_View_Helper_DataTable extends Zend_View_Helper_Abstract
{
    protected $head;
    protected $titles;
    protected $rows;
    protected $footer;
    protected $multilang;
    protected $join;
    protected $join2;
    protected $where;
    public    $jsonrows;

    public function dataTable($main_title , $header_titles , $table , $field_id , $rows , $actions , $multilang , $join , $join2= "" , $where="" )
    {
        $this->multilang = $multilang;
        $this->join = $join;
        $this->join2 = $join2;
        $this->where = $where;
        $this->_setHead($main_title);
        $this->_setHeaderTitles($header_titles, $actions);
        //$this->_setRows($rows , $table , $field_id, $actions);
        $this->_setJsonRows($rows , $table , $field_id, $actions);
        $this->_setFoot();
        return $this->head.$this->titles.$this->rows.$this->footer;
    }

    protected function _setHead($main_title){
        $this->head = '
        <div class="box gradient">
            <div class="title">
                <h4>
                    <span>'.$main_title.'</span>
                    <form action="" class="box-form right">
                        <a href="#" data-toggle="dropdown" class="btn dropdown-toggle">
                            <span class="icon16 icomoon-icon-cog-2"></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="#" id="mass_delete"><span class="icon-trash"></span> Delete</a></li>
                        </ul>
                    </form>
                </h4>
            </div>
            <div class="content noPad clearfix">
                <table cellpadding="0" cellspacing="0" border="0" class="responsive dynamicTable display table table-bordered" width="100%">';

    }

    protected function _setHeaderTitles($hd_titles , $actions){
        $header_titles = "<thead>
                            <tr>";
        
        foreach ($hd_titles as $key => $header_title) {
            $header_titles .= "<th>".Globals::trl($header_title)."</th>";
        }
        
        if(isset($actions)){
            $header_titles .= "<th>".Globals::trl('Actions')."</th>";
            $header_titles .= '<th class="ch" id="masterCh"></th>';
        }

        $header_titles .= "</tr>
                        </thead>";

        $this->titles = $header_titles;                        
    }

    protected function _setRows($rows , $table , $field_id, $actions){
        $data = $this->getTableData($table);
        $rows_body = "<tbody>";
            foreach ($data as $key => $value) {
                $rows_body .= '<tr class="odd">';
                    foreach ($rows as $key => $row) {
                        $rows_body .= "<td>".$value[$row]."</td>";
                    }                
                    if(isset($actions)){
                        $rows_body .= "<td>".$this->_setActionKeys($actions , $value[$field_id])."</td>";
                        $rows_body .= '<td class="chChildren"><div class="checker" id="uniform-undefined"><span><input type="checkbox" class="styled" value="'.$value[$field_id].'" name="checkbox[]" style="opacity: 0;"></span></div></td>';
                    }

                $rows_body .= '</tr>';
            }       
        $rows_body .= "</tbody>";
        $this->rows = $rows_body;
    }

    protected function _setJsonRows($rows , $table , $field_id, $actions){
        $data = $this->getTableData($table);
        $rows_array = array();
            foreach ($data as $dkey => $value) {
                    foreach ($rows as $key => $row) {
                        $rows_array[$dkey][] .= $value[$row];
                    }                
                    if(isset($actions)){
                        $rows_array[$dkey][] = $this->_setActionKeys($actions , $value[$field_id]);
                        $rows_array[$dkey][] = '<td class="chChildren"><div class="checker" id="uniform-undefined"><span><input type="checkbox" class="styled" value="'.$value[$field_id].'" name="checkbox[]" style="opacity: 0;"></span></div></td>';
                    }
            }       

        $json_res = json_encode($rows_array);
        $this->jsonrows = $json_res;
    }    
    
    protected function _setActionKeys($actions , $id){
        $controls = '<div class="controls center">';
        foreach ($actions as $key => $action) {
           $controls .= '<a class="tip '.$action['id'].'" rel="'.$id.'" oldtitle="'.$action['title'].'" title="" style="cursor:pointer;"><span class="'.$action['icon'].'"></span></a>';
        }
        $controls .= '</div>';
        return $controls;
    }

    protected function getTableData($table){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix."_".$table);

        if($this->join){
            $join = $this->join;
            $select->joinLeft(dbprefix.'_'.$join['join_table'] , dbprefix.'_'.$join['join_table'].'.'.$join['join_id1'].' = '.dbprefix.'_'.$table.'.'.$join['join_id2'] , $join['fields']);
        }

       if($this->join2){
            $join2 = $this->join2;
            $select->joinLeft(dbprefix.'_'.$join2['join_table'] , dbprefix.'_'.$join2['join_table'].'.'.$join2['join_id1'].' = '.dbprefix.'_'.$table.'.'.$join2['join_id2'] , $join2['fields']);
        }        

        if(!empty($this->where)){
            $select->where(dbprefix.'_'.$table.'.'.$this->where);
        }

        if($this->multilang == "true"){
            $select->where(dbprefix.'_'.$table.'.la = ?', $_SESSION['language']['def_lang_id']);
        }

        if(isset($this->join['group'])){
            $select->group($join['group']);
        }  

        if(isset($this->join2['group'])){
            $select->group($join2['group']);
        }           

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();
        return $results;
    }

    protected function _setFoot(){
        $this->footer= "</table>
            </div>
        </div>";
    }
}