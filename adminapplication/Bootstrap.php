<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    protected $front;

    protected function _initApplicationSettings()
    {
		error_reporting(E_ALL & ~E_NOTICE);
		date_default_timezone_set('Europe/Athens');
		ini_set('display_errors', true);
		ini_set('log_errors', true);
		ini_set('error_log', '../logs/errors/error.log');
		ini_set("memory_limit","200M");
    }

    protected function _initIncludes()
    {
        require_once "../adminapplication/models/dBug.php";
        require_once "../adminapplication/models/globals.php";
        require_once "../adminapplication/models/yql_class.php";

        //ACL
        require_once "../adminapplication/configs/acl_config.php";
        require_once "../adminapplication/models/acl.php";  
        require_once "../adminapplication/models/plugins.php"; 

        //Queue && Emails
        require_once "../application/models/queue_class.php";
        require_once "../application/models/emailsender_class.php";             
    }

    protected function _initRegisty()
    {
        $configuration = array('ini_status' => APPLICATION_ENV);
        Zend_Registry::set('configuration', $configuration);
    }

    protected function _initDBandSession()
    {
        //database connection
        $config = $this->getOptions();
        $db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);
 
        Zend_Db_Table_Abstract::setDefaultAdapter($db);
        $config = array(
            'name'           => dbprefix.'_sessions',
            'primary'        => 'id',
            'modifiedColumn' => 'modified',
            'dataColumn'     => 'data',
            'lifetimeColumn' => 'lifetime'
        );
         
        //create your Zend_Session_SaveHandler_DbTable and
        //set the save handler for Zend_Session
        Zend_Session::setSaveHandler(new Zend_Session_SaveHandler_DbTable($config));
         
        //start your session!
        Zend_Session::start(); 
        Globals::setPHPLogger('DB connection & Session Start');
    }

    protected function _initLoadPlugins()
    {
    	Zend_Loader::loadClass('plugins_adminaccessPlugin');
        Globals::setPHPLogger('Plugins loaded...');
    }

    protected function _initFront()
    {
    	$this->front = Zend_Controller_Front::getInstance();
    	$this->front->registerPlugin(new plugins_adminaccessPlugin());

	    //Get friendly errors configuration from config.ini
	    $friendlyErrors = Globals::getConfig()->friendlyerrors;

        //front request object instance
        $this->front->setRequest(new Zend_Controller_Request_Http());
        
        if(!$this->front->getRequest()->isXmlHttpRequest()){
            $config = new Zend_Config_Ini('../adminapplication/configs/routes.ini', 'production');
            $router = $this->front->getRouter();
            $router->addConfig($config, 'routes');
        }

	    //Configuration check for friendy errors
	    if (!$friendlyErrors){
            Globals::setPHPLogger('Friendly errors is disabled');
	        $this->front->throwExceptions(true);
	    }else{
            Globals::setPHPLogger('Friendly errors is enabled');
	        $this->front->throwExceptions(false);
	    }
    }

    protected function _initSetLocales()
    {
        Globals::setPHPLogger('Set Locales');
	    Globals::setActiveLang();
    }

}

