<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class DatatablesController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
     /**
     * @var $acl [object]
     */    
    public $acl;   
    /**
     * @var $datatable 
     */    
    public $datatable;
    public $datatable_config;

    public function init(){
        require_once "../adminapplication/configs/datatables_config.php";
        require_once "../adminapplication/models/datatables.php";

        //Get Request
        $request = $this->getRequest();
        $this->datatable = $request->getParam('table');
        $this->datatable_config = $dpg_config_tables;

        //Action
        $this->ctrl_action = $request->action;        

        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        $this->view->prev_location = $this->currentLocation;


        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
          //Disable the view/layout
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
        }  

        //Css and javascript files  
        $this->view->headLink()->appendStylesheet(assetsdir.'js/jqwidgets/styles/jqx.base.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/tables/dataTables/jquery.dataTables.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/forms/select/select2.css');

        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/gettheme.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxcore.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxdata.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxbuttons.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxscrollbar.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxpanel.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxtree.js');   
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js');

        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/multi_clone_tabs.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/dynamicforms.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatableActions.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatable_fields.js');  
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/select/select2.min.js');

        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/dataTables/jquery.dataTables.min.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/responsive-tables/responsive-tables.js');
        $this->view->headScript()->appendFile(assetsdir.'js/datatable.js');         
    }

    public function indexAction(){
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();    
    }

    public function savedataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        
        //params
        $request = $this->getRequest();     
        $dataTableForm = $request->getParam('dataTableForm');
        $table = $request->getParam('table');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'modify')){

            $dttable = new datatable();
            $dttable->save($table , $dataTableForm , $this->datatable_config);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Action complete')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }       
    }

    public function updatedataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        
        //params
        $request = $this->getRequest();     
        $dataTableForm = $request->getParam('dataTableFormEdit');
        $table = $request->getParam('table');
        $item_id = $request->getParam('item_id');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'modify')){

            $dttable = new datatable();
            $dttable->update($table , $dataTableForm , $this->datatable_config , $item_id);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Action complete')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }       
    }    

    public function deleteAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        
        //params
        $request = $this->getRequest();     
        $id = $request->getParam('id');
        $table = $request->getParam('table');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'delete')){

            $dttable = new datatable();
            $dttable->delete($table , $id , $this->datatable_config);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Action complete')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }         
    }

    public function massdeleteAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        
        //params
        $request = $this->getRequest();     
        $ids = $request->getParam('checkbox');
        $table = $request->getParam('table');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'delete')){

            $dttable = new datatable();
            $dttable->massdelete($table , $ids , $this->datatable_config);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Action complete')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }        
    }

    public function getdatatabledataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        
        //params
        $request = $this->getRequest();     
        $id = $request->getParam('id');
        $table = $request->getParam('table');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){
            $dttable = new datatable();
            $data = $dttable->getData($table , $id , $this->datatable_config);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Action complete') , 'data' => $data));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }                 
    }

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

