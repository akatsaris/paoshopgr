<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class OrdersController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
     /**
     * @var $acl [object]
     */    
    public $acl;   
    /**
     * @var $datatable 
     */    
    public $datatable;
    public $datatable_config;     

    public function init(){
        require_once "../adminapplication/configs/datatables_config.php";
        require_once "../adminapplication/models/datatables.php";
        require_once "../adminapplication/models/orders_class.php";
        //Get Request
        $request = $this->getRequest();
        $this->datatable = "orders";
        $this->datatable_config = $dpg_config_tables;
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();
        
        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        }   

        //Css and javascript files  
        $this->view->headLink()->appendStylesheet(assetsdir.'js/jqwidgets/styles/jqx.base.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/tables/dataTables/jquery.dataTables.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/forms/select/select2.css');


        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/gettheme.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxcore.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxdata.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxbuttons.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxscrollbar.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxpanel.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxtree.js');   
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js');

        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/multi_clone_tabs.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/dynamicforms.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatableActions.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatable_fields.js');  

        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/dataTables/jquery.dataTables.min.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/responsive-tables/responsive-tables.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/select/select2.min.js');

        $this->view->headScript()->appendFile(assetsdir.'js/datatable.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/orders/orders.js');
              
    }

    public function indexAction(){
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();  
    }

    public function editAction(){
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){
            $request = $this->getRequest();
            $order_id = $request->getParam('id');

             //ITEMS
            $lang_id = Globals::getLangIDbyShortcut(deflang);
            $db = Zend_Db_Table::getDefaultAdapter();
            $select = $db->select();
            $select->from(dbprefix.'_orders' , array('order_id','order_number_id'));
            
            $select->join(dbprefix.'_order_items' , dbprefix.'_orders.order_id = '.dbprefix.'_order_items.order_id' , array('price','quantity'));
            $select->join(dbprefix.'_products_multilang' , dbprefix.'_order_items.product_id = '.dbprefix.'_products_multilang.product_id' , array('name','prd_description'));
            
            $select->joinLeft(dbprefix.'_attributes' , dbprefix.'_order_items.product_attributes = '.dbprefix.'_attributes.attr_id' , array('attr_value'));
            $select->join(dbprefix.'_products' , dbprefix.'_order_items.product_id = '.dbprefix.'_products.product_id' , array('product_code','accompaying'));
            

            $select->where(dbprefix.'_products_multilang.la = ?', $lang_id);
            $select->where(dbprefix.'_attributes.la = '.$lang_id.' OR '.dbprefix.'_attributes.la IS NULL');
            $select->where(dbprefix.'_orders.order_id = ?', $order_id);

            //Query logger
            Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

            $stmt = $db->query($select);
            $items = $stmt->fetchAll();  
                
            $this->view->items =  $items;   

            //view variables
            $this->view->order_id = $order_id;
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();       
    }

    public function loadvaluesAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'orders' , 'read')){
            $request = $this->getRequest();
            $order_id = $request->getParam('order_id');

            $order_obj = new orders();
            $resdata = $order_obj->getOrder($order_id);
            echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success'));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }          
    }

    public function updateAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'orders' , 'modify')){
            //Post Values
            $request = $this->getRequest();

            $order_basic = $request->getParam('order_basic');     
            $invoice_data = $request->getParam('invoice_data');
            $shipping_data = $request->getParam('shipping_data');
            $order_id = $request->getParam('order_id');

            $shipping_data_db = array_merge($order_basic, $invoice_data);
            $orders_db = $shipping_data;

            $order_obj = new orders();
            $order_obj->updateOrder($order_id , $shipping_data_db , $orders_db);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('product successfuly updated')));
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();         
    }

    public function printorderAction(){
        $request = $this->getRequest();
        $orderID = $request->getParam('orderID');
        
        $this->_helper->layout()->disableLayout();
        $order_obj = new orders();
        $this->view->data = $order_obj->getPrintableOrder($orderID);

    }

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

