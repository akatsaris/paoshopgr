<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class PluginController extends Zend_Controller_Action
{
    /**
    * @var $ctrl_action [it keeps the action name]
    */
    public $ctrl_action;
    /**
    * @var $currentLocation [current url]
    */    
    public $currentLocation;
    /**
    * @var $previousLocation [refferer]
    */    
    public $previousLocation;
    /**
    * @var $acl [object]
    */    
    public $acl;  

    public function init(){
        //Get Request
        $request = $this->getRequest();

        //Action
        $this->ctrl_action = $request->action;        

        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();

        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        //if($this->getRequest()->isXmlHttpRequest()) {
          //Disable the view/layout
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
        //}  
    }

    public function indexAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $plugin = $request->getParam('plugin');
        $plugin_data_id = $request->getParam('plugin_data_id');
        $action = $request->getParam('action');

        $post_data = $_POST;

        require_once "../adminapplication/content_plugins/".$plugin."/".$plugin.".php"; 
        $plugin_obj = new $plugin($post_data);

        echo json_encode(array('form_properties' =>  $plugin_obj->plugin_options, 'success' => 'success'));
    }

    public function saveplugindataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $plugin = $request->getParam('plugin');
        $plugin_data_id = $request->getParam('plugin_data_id');
        $action = $request->getParam('action');

        $post_data = $_POST;
        $plugin_id = $post_data['item_plugin']['plugin_id'];

        require_once "../adminapplication/content_plugins/".$plugin_id."/".$plugin_id.".php"; 
        $plugin_obj = new $plugin_id($post_data);
        $plugin_obj->checkMultilanguage($post_data);
       
        if($plugin_obj->error){
            echo json_encode(array('error_msg' =>  $plugin_obj->error_msg , 'success' => 'failed'));
        }else{
            //save content procedure...
            $plugin_obj->saveData($post_data);
            echo json_encode(array('error_msg' =>  $plugin_obj->error_msg , 'success' => 'true'));
        }
    } 

    public function updateplugindataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $plugin = $request->getParam('plugin');
        $plugin_id = $request->getParam('plugin_id');
        $plugin_data_id = $request->getParam('plugin_data_id');
        $action = $request->getParam('action');
        $menu_item_id = $request->getParam('menu_item_id');

        $post_data = $_POST;

        require_once "../adminapplication/content_plugins/".$plugin_id."/".$plugin_id.".php"; 
        $plugin_obj = new $plugin_id($post_data);
        $plugin_obj->checkMultilanguage($post_data);
       
        if($plugin_obj->error){
            echo json_encode(array('error_msg' =>  $plugin_obj->error_msg , 'success' => 'failed'));
        }else{
            //Update content procedure...
            $plugin_obj->updateData($post_data , $menu_item_id);
            echo json_encode(array('error_msg' =>  $plugin_obj->error_msg , 'success' => 'true'));
        }
    }        

    public function getpluginsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        require_once "../adminapplication/models/content_plugins.php";

        $plugin_obj = new content_plugins();
        $plugin_list = array();

        foreach($plugin_obj->pluginslist['plugin'] as $plugin){
            //show only list of admin plugins
            if($plugin['type'] === "admin"){
                $plugin_list[][$plugin['folder']] = $plugin['title'];
            }
        }
        
        echo json_encode(array('responsedata' =>  $plugin_list, 'success' => 'success'));    
    }

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

