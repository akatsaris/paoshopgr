<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class IndexController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
     /**
     * @var $acl [object]
     */    
    public $acl;  

    public function init(){
        //Get Request
        $request = $this->getRequest();
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();
        
        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        }        
    }

    public function indexAction(){
        $this->runout();
    }

    public function loginAction(){
        if(Globals::isLoggedin()){
            Globals::setRedirect('/admin');
        }

        //Set Layout
        $this->_helper->layout->setLayout('login'); 
        $this->runout();
    }  

    public function noaccessAction(){
        $this->runout();
    }

    public function logoutAction() {
        require_once "../adminapplication/models/authorize_class.php";
        
        //disable layout and view
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);    
        
        $login_obj = new authorize();
        $login_obj->logout();
        Globals::setRedirect('/admin/login');
    }

    public function authenticateAction(){
        require_once "../adminapplication/models/authorize_class.php";

        //Get params
        $request = $this->getRequest();
        $username    = $request->getParam('username');
        $password    = $request->getParam('password');
        $keepforever = $request->getParam('logged');

        //disable layout and view
        $this->_helper->layout()->disableLayout(); 
        $this->_helper->viewRenderer->setNoRender(true);

        $authentication = new authorize();
        $authentication->checkAccess($username , $password , $keepforever);

        if($authentication->login_status){
            Globals::setInstanceMessage($authentication->status_message);
            Globals::setRedirect('/admin');
        }else{
            Globals::setInstanceMessage($authentication->status_message);
            Globals::setRedirect('/admin/login');
        }
    }      

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

