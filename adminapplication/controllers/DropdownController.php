<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */

class DropdownController extends Zend_Controller_Action
{
    /**
    * @var $ctrl_action [it keeps the action name]
    */
    public $ctrl_action;
    /**
    * @var $currentLocation [current url]
    */    
    public $currentLocation;
    /**
    * @var $previousLocation [refferer]
    */    
    public $previousLocation;
    /**
    * @var $acl [object]
    */    
    public $acl;  

    public function init(){
        require_once "../adminapplication/models/dropdown.php";
        //Get Request
        $request = $this->getRequest();

        //Action
        $this->ctrl_action = $request->action;        

        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();

        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }

    public function indexAction(){

    }      

    public function getattributesgroupsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getAttrGroups();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function getattributesbygroupAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getGroupAttributes();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }    

    public function gettagsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getTags();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }  

    public function gethomesectionsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getHomeSections();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }    

    public function getgalleriesAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getGalleriesTypes();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function gettemplatesAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getFileTemplates();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }      

    public function getviewsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getViews();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    } 

    public function getwidgetsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getWidgets();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    } 

    public function getmerchantsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getMerchants();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    } 

    public function getproductviewsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getProductViews();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    } 

    public function getgalleryviewsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getGalleryViews();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function getshippingpacksAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getShippingPacks();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function getshippingareasAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getShippingAreas();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }    

    public function getshippingprovidersAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getShipingProviders();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }   

    public function getpaymethodsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getPayMethods();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function getorderstatusAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getOrderStatus();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }

    public function getemailtemplatesAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  

        $dropdown_obj = new dropdown();
        $results = $dropdown_obj->getEmailTemplates();
        
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success')); 
    }    

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

