<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../adminapplication/models/product_class.php"; 
class ProductController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
     /**
     * @var $acl [object]
     */    
    public $acl; 
    /**
     * @var $datatable 
     */    
    public $datatable;
    public $datatable_config;     

    public function init(){
        require_once "../adminapplication/configs/datatables_config.php";
        require_once "../adminapplication/models/datatables.php";
        //Get Request
        $request = $this->getRequest();
        $this->datatable = "products";
        $this->datatable_config = $dpg_config_tables;
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();
        
        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        }   

        //Css and javascript files  
        $this->view->headLink()->appendStylesheet(assetsdir.'js/jqwidgets/styles/jqx.base.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/tables/dataTables/jquery.dataTables.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/forms/select/select2.css');


        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/gettheme.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxcore.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxdata.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxbuttons.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxscrollbar.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxpanel.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxtree.js');   
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js');

        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/multi_clone_tabs.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/dynamicforms.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatableActions.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/datatable_fields.js');  

        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/dataTables/jquery.dataTables.min.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/responsive-tables/responsive-tables.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/select/select2.min.js');

        $this->view->headScript()->appendFile(assetsdir.'js/datatable.js'); 
              
    }

    public function indexAction(){
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();
    }     

    public function addAction(){
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/product/product.js');
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();
    }

    public function accompayingproductsAction(){
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();        
    }

    public function accaddAction(){
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/product/acc_product.js');
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){

        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();        
    }

    public function saveAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'products' , 'modify')){
            //Post Values
            $request = $this->getRequest();
            $product_basic = $request->getParam('product_basic');
            $product_multilang = $request->getParam('product_multilang');
            $attributes_prod_fields = $request->getParam('attributes_prod_fields');
            $product_attributes = $request->getParam('attribute_sel');
            $photogallery_type = $request->getParam('photogalleries_frm');
            $gallery_photo = $request->getParam('gallery_photo');
            $accompaying_products = $request->getParam('acc_sel');
            $accomp = $request->getParam('accomp');


            if(!empty($accomp)){
                $product_basic['accompaying'] = $accomp;
            }

            //prepare main product data
            $tags = $product_basic['tag_id'];
            unset($product_basic['tag_id']);
            $product = array_merge($product_basic, $attributes_prod_fields);
 
            $product_obj = new product();
            $product_obj->saveProduct($product , $product_multilang , $tags , $product_attributes , $photogallery_type , $gallery_photo, $accompaying_products);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('product successfuly added')));
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();
    }

    public function updateAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'products' , 'modify')){
            //Post Values
            $request = $this->getRequest();
            $product_basic = $request->getParam('product_basic');
            $product_multilang = $request->getParam('product_multilang');
            $attributes_prod_fields = $request->getParam('attributes_prod_fields');
            $product_attributes = $request->getParam('attribute_sel');
            $photogallery_type = $request->getParam('photogalleries_frm');
            $gallery_photo = $request->getParam('gallery_photo');
            $product_id = $request->getParam('product_id');
            $accompaying_products = $request->getParam('acc_sel');

            //prepare main product data
            $tags = $product_basic['tag_id'];
            unset($product_basic['tag_id']);
            $product = array_merge($product_basic, $attributes_prod_fields);

            $product_obj = new product();
            $product_obj->updateProduct($product_id , $product , $product_multilang , $tags , $product_attributes , $photogallery_type , $gallery_photo , $accompaying_products);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('product successfuly updated')));
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();        
    }

    public function editAction(){
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/product/product.js');
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){
            $request = $this->getRequest();
            $product_id = $request->getParam('id');

            //view variables
            $this->view->product_id = $product_id;
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();       
    }

    public function acceditAction(){
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/product/acc_product.js');
        $this->view->datatable = $this->datatable;
        $this->view->datatable_config = $this->datatable_config;
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], $this->datatable , 'read')){
            $request = $this->getRequest();
            $product_id = $request->getParam('id');

            //view variables
            $this->view->product_id = $product_id;
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();       
    }    

    public function loadvaluesAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'products' , 'read')){
            $request = $this->getRequest();
            $product_id = $request->getParam('product_id');

            $product_obj = new product();
            $resdata = $product_obj->getProduct($product_id);
            echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success'));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }          
    }

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

