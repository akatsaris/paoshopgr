<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../adminapplication/models/template_widgets.php"; 
class templatewidgetsController extends Zend_Controller_Action
{
    /**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
     /**
     * @var $acl [object]
     */    
    public $acl; 
  

    public function init(){
        //Get Request
        $request = $this->getRequest();
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();
        
        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        }   

        //Css and javascript files  
        $this->view->headLink()->appendStylesheet(assetsdir.'js/jqwidgets/styles/jqx.base.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'css/template_widgets.css');


        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/gettheme.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxcore.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxdata.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxbuttons.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxscrollbar.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxpanel.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxtree.js');   
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js');

        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/dataTables/jquery.dataTables.min.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/tables/responsive-tables/responsive-tables.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/select/select2.min.js');  

        $this->view->headScript()->appendFile(assetsdir.'js/controllers/template_widgets/temp_widgets.js'); 
    }

    public function indexAction(){
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'templatewidgets' , 'read')){
            $request = $this->getRequest();
            $template_id = $request->getParam('id');

            $widget_obj = new template_widgets();
            $this->view->template_id = $template_id;
            $this->view->template_sections = $widget_obj->getTemplateSections($template_id);
            $this->view->activeWidgets = $widget_obj->getActiveWidgets($template_id);
            $this->view->inactiveWidgets = $widget_obj->getInactiveWidgets($template_id);
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();
    }  

    public function changetemplateAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $section_id = $request->getParam('section_id');
        $widgets_ids = $request->getParam('widgets_ids');
        $template_id = $request->getParam('template_id');

        $widget_obj = new template_widgets();
        $resdata = $widget_obj->saveData($template_id , $section_id , $widgets_ids);

        echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success') , false);          
    }   

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

