<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../adminapplication/models/menu_class.php"; 
class MenusController extends Zend_Controller_Action
{
    /**
    * @var $ctrl_action [it keeps the action name]
    */
    public $ctrl_action;
    /**
    * @var $currentLocation [current url]
    */    
    public $currentLocation;
    /**
    * @var $previousLocation [refferer]
    */    
    public $previousLocation;
    /**
    * @var $acl [object]
    */    
    public $acl;  

    public function init(){
        //Get Request
        $request = $this->getRequest();

        //Action
        $this->ctrl_action = $request->action;        

        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();

        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
          //Disable the view/layout
          $this->_helper->layout()->disableLayout();
          $this->_helper->viewRenderer->setNoRender(TRUE);
        }  

        //Menu Css and javascript files  
        $this->view->headLink()->appendStylesheet(assetsdir.'js/jqwidgets/styles/jqx.base.css');
        $this->view->headLink()->appendStylesheet(assetsdir.'plugins/forms/select/select2.css');

        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/gettheme.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxcore.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxdata.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxbuttons.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxscrollbar.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxpanel.js');
        $this->view->headScript()->appendFile(assetsdir.'js/jqwidgets/jqxtree.js');   
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/smartWizzard/jquery.smartWizard-2.0.min.js');
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/menus/menu.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/multi_clone_tabs.js'); 
        $this->view->headScript()->appendFile(assetsdir.'js/controllers/basic/dynamicforms.js');
        $this->view->headScript()->appendFile(assetsdir.'plugins/forms/select/select2.min.js');
    }

    public function indexAction(){
        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'read')){
            $menu = new menu();
            $this->view->menus = $menu->getMenus();
        }else{
            Globals::setRedirect('/admin/noaccess');
        }

        $this->runout();
    }

    public function getmenubyidAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $menuid = $request->getParam('menuid');

        $menu = new menu();
        $resdata = $menu->getMenuData($menuid);
        echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success'));
    }     
    
    public function addmenuAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'modify')){
            $request = $this->getRequest();
            $menu_name = $request->getParam('menu_name');
            $menu_position_name = $request->getParam('possition_name');

            $menu = new menu();
            $resdata = $menu->addmenu($menu_name , $menu_position_name);
            echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success' , 'msg' => Globals::trl('Menu added')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }
    }  

    public function deletemenuAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'delete')){
            $request = $this->getRequest();
            $menu_id = $request->getParam('menu_id');

            $menu = new menu();
            $menu->deletemenu($menu_id);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Menu deleted')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }  
    }

    public function deleteitemAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'delete')){
            $request = $this->getRequest();
            $item_id = $request->getParam('item_id');

            $menu = new menu();
            $menu->deleteitem($item_id);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Item deleted')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }  
    }    

    public function getmenudataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'modify')){
            $request = $this->getRequest();
            $menu_id = $request->getParam('menu_id');

            $menu = new menu();
            $resdata = $menu->getmenudata_byID($menu_id);
            echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success'));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }  
    }

    public function updatemenuAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'modify')){
            $request = $this->getRequest();
            $menu_id = $request->getParam('menu_id');
            $menu_name = $request->getParam('menu_name');
            $menu_position_name = $request->getParam('possition_name');

            $menu = new menu();
            $menu->updatemenu($menu_id , $menu_name , $menu_position_name);
            echo json_encode(array('success' => 'success' , 'msg' => Globals::trl('Menu updated')));
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }        
    }

    public function getdropdownmenudataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        $request = $this->getRequest();
        $menuid = $request->getParam('menuid');

        $menu = new menu();
        $resdata = $menu->getDropdownMenuData($menuid);

        echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success') , false);        
    }

    public function getmenuplugindataAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');

        if($this->acl->isAllowed($_SESSION['adminpanel']['role'], 'content' , 'modify')){
            $request = $this->getRequest();
            $menuid = $request->getParam('menuid');
            $itemid = $request->getParam('itemid');

            $menu = new menu();
            $resdata = $menu->getMenuPluginData($menuid , $itemid);
            echo json_encode(array('responsedata' =>  $resdata, 'success' => 'success') , false);
        }else{
            echo json_encode(array('success' => 'failed', 'msg' => Globals::trl('No access')));
        }
    }

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

