<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */

class TagcloudController extends Zend_Controller_Action
{
    /**
    * @var $ctrl_action [it keeps the action name]
    */
    public $ctrl_action;
    /**
    * @var $currentLocation [current url]
    */    
    public $currentLocation;
    /**
    * @var $previousLocation [refferer]
    */    
    public $previousLocation;
    /**
    * @var $acl [object]
    */    
    public $acl;  

    public function init(){
        require_once "../adminapplication/models/tagcloud.php";
        //Get Request
        $request = $this->getRequest();

        //Action
        $this->ctrl_action = $request->action;        

        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();

        //Initialize ACL
        $this->acl = new dpgeshop_acl();

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
    }

    public function indexAction(){

    }      

    public function gettagsAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        

        $tagcloud = new tagcloud();
        $results = $tagcloud->getTags();
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));    
    }


    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

