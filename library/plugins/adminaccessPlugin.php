<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class plugins_adminaccessPlugin extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request){
    	$front      = Zend_Controller_Front::getInstance();       
    	$controller = $request->controller;
        $action = $request->action;

        $no_login_allowed_controllers = array('ajax' , 'index' , 'lang' , 'cron' , 'sitemap');
        $no_login_allowed_actions = array('login' , 'authenticate');

        if(!isset($_SESSION['adminpanel']['web_login_status']) || $_SESSION['adminpanel']['web_login_status'] == false){
            $useraccess = false;
        }else{
            $useraccess = true;
        }

        //If no login  check for available actions and set allowed action
        if(!$useraccess){
            if(in_array($controller , $no_login_allowed_controllers) && in_array($action , $no_login_allowed_actions)){
                $action_allowed = true;
            }else{
                $action_allowed = false;
            }
        }

        //if there is no user access and the action is not allowed then redirect to login page
        if(!$useraccess && !$action_allowed){
            Globals::setRedirect('/admin/login');
        }
    }
}
