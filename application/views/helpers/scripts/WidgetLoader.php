<?php
class Zend_View_Helper_WidgetLoader extends Zend_View_Helper_Abstract
{
	public $section;
	public $template_view_id;
	public $widgets;
    public $notices;
    
    public function WidgetLoader($section , $template_view_id , $notices=array())
    {
        $this->notices = $notices;
        $this->section = $section;
        $this->template_view_id = $template_view_id;
        $this->setWidgets();
        $this->setIncludeFiles();
        $this->runPartials(); 
    }

    public function setWidgets(){
    	//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_template_widgets' , array('template_view_id'));
		$select->join(dbprefix.'_template_sections' , dbprefix.'_template_widgets.template_section_id = '.dbprefix.'_template_sections.template_section_id' , array('section_name' , 'section_file' ,'template_section_id'));
		$select->join(dbprefix.'_widgets' , dbprefix.'_template_widgets.widget_id = '.dbprefix.'_widgets.widget_id' , array('widget_name' , 'params' , 'widget' ,'html'));
		$select->where(dbprefix.'_template_widgets.template_view_id = ?', $this->template_view_id);
		$select->where(dbprefix.'_template_sections.section_name = ?', $this->section);
		$select->group(dbprefix.'_template_widgets.template_widget_id');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$this->widgets = $stmt->fetchAll();

        if(!empty($this->notices) && $_SESSION['alert']['type'] != ""){
            $this->widgets[0]['notices'] = $this->notices;
        }
    }

    public function setIncludeFiles(){
    	foreach ($this->widgets as $key => $widget) {
    		require_once "../application/models/widgets/".$widget['widget']."/".$widget['widget']."_widget.php"; 
    	}
    }

    public function runPartials(){
    
		//get page id
		$request = Zend_Controller_Front::getInstance()->getRequest();
		$temp = $request->getParam('temp');
		if($temp == 1){
			//new dBug($this->widgets);
		}
		
    	foreach ($this->widgets as $key => $widget) {
            $string = $this->view->partial("widgets/".$widget['widget']."/".$widget['widget'].".phtml" , $widget);
    			echo $string;
    			if($temp == 1){
				//$trimmed = trim($string);	
				//var_dump($trimmed);
			}
    	}
    }
}
