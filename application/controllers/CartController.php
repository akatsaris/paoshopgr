<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/template_views_model.php";
class CartController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
    /**
     * @var $login status [online/offline]
     */ 
    public $login_status;
 

    public function init(){
        //Get Request
        $request = $this->getRequest();
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        } 

        //login check
        if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
            $this->login_status = "online";
        }else{
            $this->login_status = "offline";
        }  

        $this->view->login_status = $this->login_status;
        $this->view->prev_location = $this->previousLocation;    

        //SEO
        $this->view->headTitle(Globals::getConfig()->sitetitle);
    }

    //homepage Action
    public function indexAction(){
        $this->_helper->layout->setLayout('homepage');  
        $this->view->template_view_id = 1; 
        $this->runout();
    }

    public function connectionAction(){
        if($this->login_status == "online"){
            $this->_redirect('/');
            exit;
        }else{
            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 12; 
            $this->runout();
        }
    }

    public function cartAction(){
        require_once "../application/models/cart_model.php";

        $cart_obj = new cart();
        $this->view->cart_contents = $cart_obj->get_cart_contents();
        $this->view->total_cost    = $cart_obj->total;
        $_SESSION['cart']['totalcost'] = $this->view->total_cost;

        $this->_helper->layout->setLayout('cart');
        $this->view->template_view_id = 6; 
        $this->runout();
    }

    public function checkoutAction(){
        require_once "../application/models/cart_model.php";

        $cart_obj = new cart();
        
        if(!empty($_SESSION['cart']['totalcost'])){
            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 8; 
            $this->runout();
        }else{
            $this->_redirect('/cart');
        }
    }    
 
    public function invoiceAction(){
        require_once "../application/models/cart_model.php";

        if(isset($_SESSION['courier'])){
            $cart_obj = new cart();

            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 8; 
            $this->runout();
        }else{
            $this->_redirect('/cart-step-1');
        } 
    } 

    public function paymentAction(){
        require_once "../application/models/cart_model.php";
        if(isset($_SESSION['courier']) && !empty($_SESSION['cart']['totalcost'])){ 
            $cart_obj = new cart();
            $payment_methods = $cart_obj->getPaymentsTypes($_SESSION['courier']['package_id']);
            $this->view->pay_methods = $payment_methods;

            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 9; 
            $this->runout();
        }else{
            $this->_redirect('/cart-step-1');
        }   
    } 

    public function confirmationAction(){
        require_once "../application/models/cart_model.php";
        require_once "../application/models/shipping_model.php";

        if(isset($_SESSION['courier']) && isset($_SESSION['payment_options']) && !empty($_SESSION['cart']['totalcost'])){ 
            //GET CART INFO
            $cart_obj = new cart();
            $this->view->cart_contents = $cart_obj->get_cart_contents();
            $this->view->total_cost    = $cart_obj->total;
            $this->view->shipping_info = $cart_obj->getShippingInfo();
            
            //GET PAYMENT INFO
            $payment_type = $_SESSION['payment_options']['payment'];
            $merchant_data = $cart_obj->getPaymentMethod($payment_type);
            $merchant = $merchant_data['merchant_plugin'];
            
            //Include model
            require_once "../application/models/merchants/".$merchant."/".$merchant.".php";
            $merchant_class_name =  $merchant."_merchant";
            $merchant_obj = new $merchant_class_name();

            $this->view->payment_data = $merchant_data;


            $region_id    = $this->view->shipping_info['region_id'];
            $country_code = $this->view->shipping_info['country_code'];

            $_SESSION['courier']['country_code'] = $country_code;

            //SHIPPING PROPERTIES
            $shipping_obj = new shipping();
            $package_id = $_SESSION['courier']['package_id'];


            if(!empty($region_id)){
                $this->view->shipp_properties = $shipping_obj->getCourierPackagesByPackageID($region_id , 'region' , $package_id);
            }else{
                $this->view->shipp_properties = $shipping_obj->getCourierPackagesByPackageID($country_code , 'country' , $package_id);
            }


            //Set Data For Merchants
            $cart['cart']    = $this->view->cart_contents;
            $info['info']    = $this->view->shipping_info;
            $shipp['shipp']   = $this->view->shipp_properties;

            $payment['payment'] = $this->view->payment_data;
            $payment_data['payment_data'] = $_SESSION['payment_options'];
            $cart_cost = $this->view->total_cost;
            
            
            if($info['info']['gift'] == 1){
                $gift_price = Globals::getConfig()->gift_price;
            }else{
                $gift_price = 0;
            }


            //DISCOUNTS PROCEDURE
            $discount_obj = new discounts();
            $total_discount = $discount_obj->getDiscount();


            if($this->view->payment_data['cod_extracost_active'] == 1 && $discount_obj->shipping != "free"){
                $cod_price = $this->view->shipp_properties['shipping_pack_cod_val'];
            }else{
                $this->view->shipp_properties['shipping_pack_cod_val'] = 0;
                $cod_price = 0;
            }

            if($discount_obj->shipping == "free"){
                $this->view->shipp_properties['shipping_price'] = 0;
                $cod_price = 0;
            }

            $this->view->discount_amount = $discount_obj->amount;

            $total_cost = ($cart_cost - $total_discount) + $cod_price + $this->view->shipp_properties['shipping_price'] + $gift_price;
            $this->view->cost_including_all = $total_cost;
            $this->view->discount = $discount_obj->discount_str;
            

            $final['final'] = array('cart_cost' => $cart_cost , 'gift_price' => $gift_price , 'total_cost' => $total_cost , 'discount' => $discount_obj->discount_str , 'free_shipping' => $discount_obj->shipping);
            $data_source = array_merge($cart , $info , $shipp , $payment , $final , $payment_data);
            
            $merchant_obj->setData($data_source);
            $this->view->merchant_type = $merchant_obj->type;
            $this->view->merchant_vendor = $merchant;
            $this->view->merchant_vars = $merchant_obj->data;

            if(!isset($_SESSION['order_number'])){
                $this->view->order_num = Globals::getRandomNum();
                $_SESSION['order_number'] = $this->view->order_num;
            }else{
                $this->view->order_num = $_SESSION['order_number'];
            }

            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 10; 
            $this->runout();
        }else{
            $this->_redirect('/cart-step-1');
        }   
    }     


    public function thankyouAction(){
        $request = $this->getRequest();
        if($request->isPost()){
            require_once "../application/models/order_model.php";
            $order_obj = new order();

            $invoice  = $request->getParam('invoice');
            $merchant = $order_obj->getMerchantByOrderID($invoice);

            //Include model
            require_once "../application/models/merchants/".$merchant."/".$merchant.".php";
            $merchant_class_name =  $merchant."_merchant";
            $merchant_obj = new $merchant_class_name();

            $merchant_obj->setToPaid($invoice);
        }

        $this->_helper->layout->setLayout('cart');
        $this->view->template_view_id = 11; 
        $this->runout();  
    }

    public function cancelorderAction(){
        $request = $this->getRequest();

        $this->_helper->layout->setLayout('cart');
        $this->view->template_view_id = 6; 
        $this->runout();          
    }

    public function wishlistAction(){
        require_once "../application/models/wishlist_model.php";
        $request = $this->getRequest();

        if($this->login_status == "offline"){
            $this->_redirect('/');
            exit;
        }else{
            $user_id = $_SESSION['visitor']['user_id']; 

            $wl_obj = new wishlist();
            $this->view->wishlist_data = $wl_obj->getWishList($user_id);

            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 6; 
            $this->runout(); 
        }             
    }    

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

