<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/login_model.php";
require_once "../application/models/template_views_model.php";
class LoginController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;    
    /**
     * @var $login status [online/offline]
     */ 
    public $login_status;
 

    public function init(){
        //Get Request
        $request = $this->getRequest();
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();
        
        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        }  

        //login check
        if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
            $this->login_status = "online";
        }else{
            $this->login_status = "offline";
        }  

        $this->view->login_status = $this->login_status;
        $this->view->prev_location = $this->previousLocation;    
    }

    //login Action
    public function indexAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $request = $this->getRequest();
        $email = $request->getParam('email');
        $password = $request->getParam('password');

        $auth_obj = new authorize();
        $auth_obj->checkAccess($email , $password , '0');

        if($auth_obj->error){
            Globals::setInstanceMessage($auth_obj->status_message);
        }

        $this->_redirect('/cart-step-1');
    } 

    public function logoutAction(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);

        $auth_obj = new authorize();
        $auth_obj->logout();

        $this->_redirect('/');
    }  

    public function registerAction(){
        if($this->login_status == "online"){
            $this->_redirect('/');
            exit;
        }else{
            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 6; 
            $this->runout();
        }
    } 

    public function resetpassAction(){
        if($this->login_status == "online"){
            $this->_redirect('/');
            exit;
        }else{
            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 13; 
            $this->runout();
        }
    } 

    public function myaccountAction(){
        if($this->login_status == "online"){
            $this->_helper->layout->setLayout('cart');
            $this->view->template_view_id = 16; 
        }else{
            $this->_redirect('/');
            exit;
        }
        $this->runout();

    }       

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }
         
}

