<?php
Class CronController extends Zend_Controller_Action
{

    public function init(){
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE); 
        set_time_limit(0);
    }

    public function cronorderconfirmationAction() {
        $queue = new queue('confirmation_queue');
        $emailsender = new EmailSender();
        $emailsender->setQueue($queue->queue);
        $emailsender->sendQueuedEmails(7);
    }

    public function cronorderstatusAction() {
        $queue = new queue('order_status_queue');
        $emailsender = new EmailSender();
        $emailsender->setQueue($queue->queue);
        $emailsender->sendQueuedEmails(7);
    }

    public function cronresetpasswordsAction() {
        $queue = new queue('reset_queue');
        $emailsender = new EmailSender();
        $emailsender->setQueue($queue->queue);
        $emailsender->sendQueuedEmails(7);
    }   

    public function croncontactAction() {
        $queue = new queue('contact_queue');
        $emailsender = new EmailSender();
        $emailsender->setQueue($queue->queue);
        $emailsender->sendQueuedEmails(7);
    }    

}