<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class AjaxController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
 

    public function init(){
        //Get Request
        $request = $this->getRequest();
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Check if is ajax request
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);      
    }

    public function indexAction(){
    	$this->_redirect('/');
        exit;
    }

    public function changecatpropertiesAction(){
    	header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $id = $request->getParam('id');
        $value = $request->getParam('value');

		switch ($id)
		{
		case "orderby":
		  $_SESSION['products_cat_properties']['prd_sorting'] = $value;
		  break;
		case "limit":
		  $_SESSION['products_cat_properties']['prd_per_pages'] = $value;
		  break;
		}

        echo json_encode(array('id' => $id , 'value' => $value , 'success' => 'success')); 
    }

    public function addtocartAction(){
        require_once "../application/models/cart_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  


        $request    = $this->getRequest();
        $product_id = $request->getParam('product_id');   
        $quantity   = $request->getParam('quantity');
        $attributes = $request->getParam('attrs');
        $acc_prd_ids = $request->getParam('prd_ids');

        $cart_obj = new cart();
        $result = $cart_obj->add_to_cart($product_id,$quantity,$attributes,$acc_prd_ids);
        
        if(!$cart_obj->error){
            echo json_encode(array('add_to_cart_result' => $result , 'success' => 'success'));
        }else{
            echo json_encode(array('add_to_cart_result' => $result , 'success' => 'false' , 'msg' => $cart_obj->msg));
        }
         
    }

    public function updatequantityAction(){
        require_once "../application/models/cart_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request    = $this->getRequest();
        $cart_id = $request->getParam('cart_id');   
        $quantity   = $request->getParam('quantity');

        $cart_obj = new cart();
        $result = $cart_obj->update_quantity($cart_id , $quantity);
        
        if(!$cart_obj->error){
            echo json_encode(array('update_cart' => $result , 'success' => 'success'));
        }else{
            echo json_encode(array('update_cart' => $result , 'success' => 'false' , 'msg' => $cart_obj->msg));
        }                
    }

    public function deletefromcartAction(){
        require_once "../application/models/cart_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request    = $this->getRequest();
        $cart_id = $request->getParam('cart_id');   

        $cart_obj = new cart();
        $result = $cart_obj->delete_from_cart($cart_id);
        
        echo json_encode(array('delete_from_cart' => $result , 'success' => 'success'));           
    }

    public function userregisterAction(){
        require_once "../application/models/cart_model.php";
        require_once "../application/models/validation_class.php";
        require_once "../application/models/login_model.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request    = $this->getRequest();
        $firstname  = $request->getParam('firstname');
        $lastname   = $request->getParam('lastname');
        $email      = $request->getParam('email');
        $password   = $request->getParam('password');
        $password2  = $request->getParam('password2');
        
        $day        = $request->getParam('day');
        $month      = $request->getParam('month');
        $year       = $request->getParam('year');
        
        $gender     = $request->getParam('gender');
        
        $member     = $request->getParam('member');
        $newsletter = $request->getParam('newsletter');
        $terms      = $request->getParam('terms');


        $month = sprintf("%02s", $month);

        //set validation object
        $validation = new validation();

        //firstname check
        $validation->stringValidation(Globals::trl('Firstname'), $firstname , 2);

        //lastname check
        $validation->stringValidation(Globals::trl('Lastname'), $lastname , 3);

        //check email format , dublication 
        $validation->checkEmail($email, true);

        //Sex check
        $validation->booleanRequired(Globals::trl('Sex'), $gender);

        //Password check
        $validation->stringValidation(Globals::trl('Password'), $password , 6);
        $validation->checksimilarPassword(Globals::trl('Password') , $password , $password2);

        //Terms
        $validation->booleanRequired(Globals::trl('terms and conditions'), $terms);

        //date of birth check
        $validation->checkValidDate($year.'-'.$month.'-'.$day);



        if(!$validation->error){
            $real_pass = $password;
            $bytes = mb_convert_encoding($password, 'UTF-16LE');
            $salt = Globals::getRandomNum();
            $salt_base64 = base64_decode($salt);
            $password = base64_encode(sha1($salt_base64 . $bytes, true));

            $data = array('firstname' => $firstname,
                          'lastname' => $lastname,
                          'username' => $email,
                          'password' => $password,
                          'salt' => $salt,
                          'sex' => $gender,
                          'birth' => $year.'-'.$month.'-'.$day,
                          'member' => $member,
                          'newsletter' => $newsletter,
                          'terms' => $terms,
                          'role' => "user"
                );

            $cart_obj = new cart();
            $cart_obj->register_user($data);

            $auth_obj = new authorize();
            $auth_obj->checkAccess($email , $real_pass , '0');

            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }    
    }

    public function shippingdataAction(){
        require_once "../application/models/cart_model.php";
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request     = $this->getRequest();
        $firstname   = $request->getParam('firstname');
        $lastname    = $request->getParam('lastname');
        $email    = $request->getParam('email');
        $companyname = $request->getParam('companyname');
        $address     = $request->getParam('address');
        $addressmore = $request->getParam('addressmore');
        $countries   = $request->getParam('countries');
        $regions     = $request->getParam('regions');
        $city        = $request->getParam('city');
        $postal      = $request->getParam('postal');
        $tel         = $request->getParam('tel');
        $courier     = $request->getParam('courier');
        $gift        = $request->getParam('gift');
        $gift_cost   = $request->getParam('gift_cost');

        $shipping_price  = $request->getParam('shipping_price');
        $shipping_weight = $request->getParam('shipping_weight');


        //set validation object
        $validation = new validation();

        //firstname check
        $validation->stringValidation(Globals::trl('Firstname'), $firstname , 2);

        //lastname check
        $validation->stringValidation(Globals::trl('Lastname'), $lastname , 3);

        //check email format , dublication 
        $validation->checkEmail($email, false);

        //address
        $validation->stringValidation(Globals::trl('address'), $address , 5);

        //City
        $validation->stringValidation(Globals::trl('City'), $city , 3);

        //Postal
        $validation->stringValidation(Globals::trl('Postal Code'), $postal , 3);

        //Phone Number
        $validation->stringValidation(Globals::trl('Phone Number'), $tel , 10);

        //Courier
        $validation->required(Globals::trl('Courier'), $courier);

        //Variables
        $user_id = $_SESSION['visitor']['user_id'];
        $sess_id = session_id();

        if(!$validation->error){
            $data = array('firstname' => $firstname,
                          'lastname' => $lastname,
                          'session_id' => $sess_id,
                          'user_id' => $user_id,
                          'gift' => $gift,
                          'gift_wrapping_cost' => $gift_cost,
                          'email' => $email,
                          'address' => $address,
                          'address_more' => $addressmore,
                          'city_name' => $city,
                          'country_code' => $countries,
                          'region_id' => $regions,
                          'postal' => $postal,
                          'company_name' => $companyname,
                          'phone' => $tel,
                          'courier_id' => $courier,
                          'shipping_price' => $shipping_price,
                          'shipping_weight' => $shipping_weight
                );

            $cart_obj = new cart();
            $cart_obj->add_shipping_details($data , $sess_id);

            $_SESSION['courier']['package_id'] = $courier;

            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }  
    } 

    public function invoicedataAction(){
        require_once "../application/models/cart_model.php";
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request     = $this->getRequest();
        $receipttype   = $request->getParam('receipttype');
        $companyname    = $request->getParam('companyname');
        $profession    = $request->getParam('profession');
        $address     = $request->getParam('address');
        $vat = $request->getParam('vat');
        $doy   = $request->getParam('doy');


        //set validation object
        $validation = new validation();

        if($receipttype == 1){

            //Company Name
            $validation->stringValidation(Globals::trl('Company Name'), $companyname , 2);

            //Profession
            $validation->stringValidation(Globals::trl('Profession'), $profession , 3);

            //address
            $validation->stringValidation(Globals::trl('Address'), $address , 5);   

            //vat
            $validation->stringValidation(Globals::trl('VAT ID'), $vat , 5);

            //doy
            $validation->stringValidation(Globals::trl('Tax office'), $doy , 5);        
        }

        $sess_id = session_id();

        if(!$validation->error){
            $data = array('invoice_type' => $receipttype,
                          'invoice_company_name' => $companyname,
                          'session_id' => $sess_id,
                          'invoice_profession' => $profession,
                          'invoice_address' => $address,
                          'vat' => $vat,
                          'doy' => $doy
                );

            $cart_obj = new cart();
            $cart_obj->add_invoice_details($data , $sess_id);

            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }                  
    }

    public function paymentdataAction(){
        require_once "../application/models/cart_model.php";
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request     = $this->getRequest();
        $payment_type   = $request->getParam('payment');

        $cart_obj = new cart();
        $merchant_data = $cart_obj->getPaymentMethod($payment_type);

        $merchant = $merchant_data['merchant_plugin'];

        //Include model
        require_once "../application/models/merchants/".$merchant."/".$merchant.".php";
        $merchant_class_name =  $merchant."_merchant";
        $merchant_obj = new $merchant_class_name();
        $validation_rules = $merchant_obj->view_form_validation;

        if(!empty($validation_rules)){
            $validation = new validation();

            foreach ($validation_rules as $key => $validation_rule) {
                //get request value
                $param_value = $request->getParam($key);
                if(isset($validation_rule['min-digits'])){
                    $min_digits = $validation_rule['min-digits'];
                }else{
                    $min_digits = 100;
                }

                if($validation_rule['required']){
                    switch ($validation_rule['type']) {
                        case 'string':
                            $validation->stringValidation(Globals::trl($validation_rule['trl']), $param_value , $min_digits);
                            break;
                        case 'cardnumber':
                            $validation->checkCreditCardNumber(Globals::trl($validation_rule['trl']), $param_value);
                            //Check for paoshop card
                            $paocard_digits = substr($param_value, 0, 8);
                            if($paocard_digits == "54586513" || $paocard_digits == "54586514"){
                                $_SESSION['discounts']['credit'] = true;
                            }else{
                                $_SESSION['discounts']['credit'] = false;
                            }
                            break;
                    }
                }
            }

        }



        if(!$validation->error){
            $url_params = $request->getParams();
            if(isset($url_params['controller']))
                unset($url_params['controller']);
            if(isset($url_params['action']))
                unset($url_params['action']);
            if (isset($url_params['module']))
                unset($url_params['module']);

            $_SESSION['payment_options'] = $url_params;
            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }         
    }

    public function getregionsAction(){
        require_once "../application/models/shipping_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $country_id = $request->getParam('country_id');

        $shipping_obj = new shipping();
        $results = $shipping_obj->getRegions($country_id);
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));
    }    

    public function getshippingAction(){
        require_once "../application/models/shipping_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $area_id = $request->getParam('area_id');
        $type = $request->getParam('type');

        $shipping_obj = new shipping();
        $results = $shipping_obj->getCourierPackages($area_id , $type);
        echo json_encode(array('responsedata' =>  $results, 'success' => 'success'));        
    }   

    public function changelanguageAction(){
    	header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $lang_id = $request->getParam('value');

        Globals::change_language($lang_id);

        echo json_encode(array('id' => $lang_id , 'success' => 'success')); 
    }
    
    public function addtowishlistAction(){
        require_once "../application/models/wishlist_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request    = $this->getRequest();
        $product_id = $request->getParam('product_id');   
        $user_id = $_SESSION['visitor']['user_id']; 

        if(!empty($product_id) && !empty($user_id)){
            $wl_obj = new wishlist();
            $result = $wl_obj->add_to_wishlist($product_id,$user_id);
            echo json_encode(array('add_to_wishlist_result' => $result , 'success' => 'success')); 
        }else{
            echo json_encode(array('errors' => Globals::trl('You have to be a registered and online member to perform this action') ,'success' => 'false'));
        }
    }

    public function removefromwishlistAction(){
        require_once "../application/models/wishlist_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $wl_id   = $request->getParam('wl_id');   
        $user_id = $_SESSION['visitor']['user_id']; 

        if(!empty($wl_id) && !empty($user_id)){
            $wl_obj = new wishlist();
            $wl_obj->remove_from_wishlist($wl_id,$user_id);
            echo json_encode(array('result' => $result , 'success' => 'success')); 
        }else{
            echo json_encode(array('errors' => Globals::trl('You have to be a registered and online member to perform this action') ,'success' => 'false'));
        }
    }


    public function saveorderAction(){
        require_once "../application/models/order_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $order_num = $request->getParam('order_num');
        $comment = $request->getParam('comment');
        $order_html = $request->getParam('order_html');
        $sess_id = session_id();

        if(isset($_SESSION['payment_options'])){
            $order_obj = new order();
            $order_obj->prepareAndSaveOrder($order_num , $sess_id , $comment , $order_html);

            if(!$order_obj->error){
                echo json_encode(array('order_num' => $order_num , 'success' => 'success'));
            }else{
                echo json_encode(array('order_num' => $order_num , 'success' => 'false' , 'message' => Globals::trl('Payment Failed, Please try other payment type')));
            }
        }else{
            echo json_encode(array('order_num' => $order_num , 'success' => 'false' , 'message' => Globals::trl('Time expired, please retry')));
        } 
    }

    public function removeorderAction(){
        require_once "../application/models/order_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $order_num = $request->getParam('order_num');

        $order_obj = new order();
        $order_obj->removeOrder($order_num);

        echo json_encode(array('order_num' => $order_num , 'success' => 'success'));
    }


    public function checkvoucherAction(){
        require_once "../application/models/vouchers_model.php";
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $voucher = $request->getParam('v_code');

        $voucher_obj = new voucher();
        $results = $voucher_obj->checkVoucherCode($voucher);

        if(!empty($results)){
            $_SESSION['discounts']['voucher'] = $results;
            echo json_encode(array('voucher' => $results , 'success' => 'success' , 'message' => Globals::trl('This voucher is active, you will get the discount at the confirmation step.')));
        }else{
            unset($_SESSION['discounts']['voucher']);
            echo json_encode(array('voucher' => null , 'success' => 'false', 'message' => Globals::trl('This voucher is not active, please try other.')));
        }
    } 

    public function resetpasswordAction(){
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request    = $this->getRequest();
        $email      = $request->getParam('email');


        //set validation object
        $validation = new validation();

        //check email format , dublication 
        $validation->checkEmail($email, false);


        if(!$validation->error){
            $real_pass = Globals::getRandomNum();
            $bytes = mb_convert_encoding($real_pass, 'UTF-16LE');
            $salt = Globals::getRandomNum();
            $salt_base64 = base64_decode($salt);
            $password = base64_encode(sha1($salt_base64 . $bytes, true));

            $queue = new queue('reset_queue');
            @EmailSender::setQueue($queue->queue);
            @EmailSender::sendEmail(array('type' => 'reset_password' , 'email' => $email ,'password' => $real_pass , 'enc_password'=> $password , 'salt' => $salt ,'la_region' => $_SESSION['language']['lang_region']));

            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }    
    }

    public function updateaccountAction(){
        require_once "../application/models/validation_class.php";
        require_once "../application/models/cart_model.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request   = $this->getRequest();
        $firstname = $request->getParam('firstname');
        $lastname  = $request->getParam('lastname');
        $password  = $request->getParam('password');
        $password2 = $request->getParam('password2');



        //set validation object
        $validation = new validation();

        //firstname check
        $validation->stringValidation(Globals::trl('Firstname'), $firstname , 2);

        //lastname check
        $validation->stringValidation(Globals::trl('Lastname'), $lastname , 3);

        //Password check
        $validation->stringValidation(Globals::trl('Password'), $password , 6);
        $validation->checksimilarPassword(Globals::trl('Password') , $password , $password2);


        if(!$validation->error){
            $real_pass = $password;
            $bytes = mb_convert_encoding($password, 'UTF-16LE');
            $salt = Globals::getRandomNum();
            $salt_base64 = base64_decode($salt);
            $password = base64_encode(sha1($salt_base64 . $bytes, true));

            $data = array('firstname' => $firstname,
                          'lastname' => $lastname,
                          'password' => $password,
                          'salt' => $salt
                );

            $cart_obj = new cart();
            $cart_obj->update_user($data);


            echo json_encode(array('success' => 'success'));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }
    }

    public function contactAction(){
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request   = $this->getRequest();
        $firstname = $request->getParam('firstname');
        $lastname  = $request->getParam('lastname');
        $email     = $request->getParam('email');
        $phone     = $request->getParam('phone');
        $comment   = $request->getParam('comment');



        //set validation object
        $validation = new validation();

        //firstname check
        $validation->stringValidation(Globals::trl('Firstname'), $firstname , 2);

        //lastname check
        $validation->stringValidation(Globals::trl('Lastname'), $lastname , 3);

        //check email format , dublication 
        $validation->checkEmail($email, false);

        //Phone
        $validation->stringValidation(Globals::trl('Phone'), $phone , 10);

        //comment
        $validation->stringValidation(Globals::trl('Comment'), $comment , 10);


        if(!$validation->error){

            $queue = new queue('contact_queue');
            @EmailSender::setQueue($queue->queue);
            @EmailSender::sendEmail(array('type' => 'contact' , 'email' => $email ,'firstname' => $firstname , 'lastname'=> $lastname , 'phone' => $phone ,'comment' => $comment));

            echo json_encode(array('success' => 'success' , 'msg'=> Globals::trl('Message Successfuly send')));
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }           

    }

    public function paoticketscheckAction(){
        require_once "../application/models/validation_class.php";

        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json'); 

        $request    = $this->getRequest();
        $firstname  = $request->getParam('firstname');
        $lastname   = $request->getParam('lastname');
        $section    = $request->getParam('section');
        $ticketseat = $request->getParam('ticketseat');
        $ticketrow  = $request->getParam('ticketrow');  

        //set validation object
        $validation = new validation();

        //firstname check
        $validation->stringValidation(Globals::trl('Firstname'), $firstname , 2);

        //lastname check
        $validation->stringValidation(Globals::trl('Lastname'), $lastname , 3);

        //Phone
        $validation->required(Globals::trl('Section'), $section);

        //comment
        $validation->required(Globals::trl('Seat'), $ticketseat);

        //comment
        $validation->required(Globals::trl('Ticket Row'), $ticketrow);
        
        if(!$validation->error){

            $wsdl = 'http://teller.viva.gr/api/client/pao.svc?wsdl';
            $soapClient = new SoapClient($wsdl, array('cache_wsdl' => 0));

            // SOAP call
            $parameters = new stdClass();
            $parameters->request = new stdClass();

            $parameters->request->FirstName = $firstname;
            $parameters->request->LastName = $lastname;
            $parameters->request->Row = $ticketrow;
            $parameters->request->Seat = $ticketseat;
            $parameters->request->Section = $section;

            try
            {
                $result = $soapClient->ValidateSeat($parameters);
                $response = $result->ValidateSeatResult->result;
                $_SESSION['discounts']['ticket'] = $response;
            }
            catch (SoapFault $fault)
            {
                echo json_encode(array('errors' => 'Web Service is down' ,'success' => 'false'));
                exit();
            }

            if($response){
                echo json_encode(array('success' => 'success' , 'msg'=> Globals::trl('Ticket is valid')));
            }else{
                echo json_encode(array('success' => 'success' , 'msg'=> Globals::trl('Ticket is not valid')));
            }    
        }else{
            echo json_encode(array('errors' => $validation->error_messages ,'success' => 'false'));
        }  
    }   


    public function setsearchstringAction(){
        header('Cache-Control: no-cache, must-revalidate');
        header('Content-type: application/json');  
        
        $request = $this->getRequest();
        $search_string = $request->getParam('search');

        if(!empty($search_string)){
            $_SESSION['search']['search_string'] = $search_string;
        }

        echo json_encode(array('search' => $search_string , 'success' => 'success')); 
    }    

}

