<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/template_views_model.php";
class IndexController extends Zend_Controller_Action
{
	/**
     * @var $ctrl_action [it keeps the action name]
     */
    public $ctrl_action;
    /**
     * @var $currentLocation [current url]
     */    
    public $currentLocation;
    /**
     * @var $previousLocation [refferer]
     */    
    public $previousLocation;
    /**
     * @var $login status [online/offline]
     */ 
    public $login_status;
 

    public function init(){
        //Get Request
        $request = $this->getRequest();
        $_SESSION['request'] = $request;
        
        //Action
        $this->ctrl_action = $request->action;        
        
        //Get NoticeMessages to view
        $this->view->messages = Globals::getInstanceMessage();

        //Locations
        $this->currentLocation = $request->getRequestUri();
        $this->previousLocation = Globals::getPreviousLocation();
        
        //Check if is ajax request
        if($this->getRequest()->isXmlHttpRequest()) {
            //Disable the view/layout
            $this->_helper->layout()->disableLayout();
            $this->_helper->viewRenderer->setNoRender(TRUE);
        } 

        //login check
        if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
            $this->login_status = "online";
        }else{
            $this->login_status = "offline";
        }  

        $this->view->login_status  = $this->login_status;   
        $this->view->prev_location = $this->previousLocation;   

    }

    //homepage Action
    public function indexAction(){      
        //SEO
        $this->view->headTitle(Globals::getConfig()->sitetitle);

        $this->_helper->layout->setLayout('homepage');  
        $this->view->template_view_id = 1; 
        $this->runout();
    }

    //Content Action
    public function contentAction(){
        require_once "../application/models/content_model.php";
        $request = $this->getRequest();
        $page_id = $request->getParam('pageId');

        $content_obj = new content($page_id);
        $this->view->content = $content_obj->data;

        $active = $content_obj->data[0]['active'];

        if($active == 1){
            $register_users_only = $content_obj->data[0]['register_users_only'];

            if($this->login_status != "online" && $register_users_only == 1){
                $this->_redirect('/');
                exit;
            }

            //templates and layouts
            $template_view_id = $content_obj->data[0]['template_view_id'];
            $template_view_obj = new template_views($template_view_id , 'content');
            $layout = $template_view_obj->getLayout();
            $temp_view_id = $template_view_obj->getTemplateViewId();

            //set the template and the layout
            $this->_helper->layout->setLayout($layout);  
            $this->view->template_view_id = $temp_view_id; 

            //SEO
            $this->view->headTitle($this->view->content[0]['title']);
            $this->view->headMeta()->appendName('keywords', $this->view->content[0]['google_keywords']);
            $this->view->headMeta()->appendName('description', $this->view->content[0]['google_desc']);

        }else{
            $this->_redirect('/');
            exit; 
        }

        $this->runout();
    }

    //Tags Action
    public function tagsAction(){
        require_once "../application/models/tags_model.php";

        $request = $this->getRequest();
        $page_id = $request->getParam('pageId');
        $curr_page = $request->getParam('curr_page');

        $sort = $_SESSION['products_cat_properties']['prd_sorting'];
        $prd_per_page = $_SESSION['products_cat_properties']['prd_per_pages'];

        $tags_obj = new tags($page_id , $curr_page , $prd_per_page , $sort);
        $this->view->tag_data = $tags_obj->data;
        $this->view->data = $tags_obj->products;
        $this->view->pagination_data = $tags_obj->pagination_data;

        $active = $tags_obj->data[0]['active'];
        if($active){
            $register_users_only = $tags_obj->data[0]['register_users_only'];

            if($this->login_status != "online" && $register_users_only == 1){
                $this->_redirect('/');
                exit;
            }

            //templates and layouts
            $template_view_id = $tags_obj->data[0]['template_view_id'];
            $template_view_obj = new template_views($template_view_id , 'tags');
            $layout = $template_view_obj->getLayout();
            $temp_view_id = $template_view_obj->getTemplateViewId();

            //set the template and the layout
            $this->_helper->layout->setLayout($layout);  
            $this->view->template_view_id = $temp_view_id;

            //SEO
            $this->view->headTitle($this->view->tag_data[0]['title']);
        }else{
            $this->_redirect('/');
            exit; 
        }

        $this->runout();
    }  

    //Product Action
    public function productAction(){
        require_once "../application/models/product_model.php";

        $request = $this->getRequest();
        $page_id = $request->getParam('pageId');

        $product_obj = new product($page_id);
        $this->view->product = $product_obj->data;

        $this->view->attributes = $product_obj->attributes;
        $this->view->gallery    = $product_obj->gallery;
        $this->view->acc_products = $product_obj->acc_products;

        $active = $product_obj->data[0]['active'];

        if($active == 1){
            $register_users_only = $product_obj->data[0]['register_users_only'];

            if($this->login_status != "online" && $register_users_only == 1){
                $this->_redirect('/');
                exit;
            }

            //templates and layouts
            $template_view_id = $product_obj->data[0]['template_view_id'];
            $template_view_obj = new template_views($template_view_id , 'product');
            $layout = $template_view_obj->getLayout();
            $temp_view_id = $template_view_obj->getTemplateViewId();

            //set the template and the layout
            $this->_helper->layout->setLayout($layout);  
            $this->view->template_view_id = $temp_view_id; 

            //SEO
            $this->view->headTitle($this->view->product[0]['name']);
            $this->view->headMeta()->appendName('keywords', $this->view->product[0]['keywords']);
            //$this->view->headMeta()->appendName('description', $this->view->product[0]['google_desc']);
        }else{
            $this->_redirect('/');
            exit; 
        }

        $this->runout();
    }    

    public function linkAction(){
        require_once "../application/models/link_model.php";

        $request = $this->getRequest();
        $page_id = $request->getParam('pageId');

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(TRUE);
        $link_obj = new link($page_id);
        $this->view->data = $link_obj->data;

        $r = Zend_Controller_Action_HelperBroker::getStaticHelper('redirector');
        $r->gotoUrl($this->view->data[0]['link'])->redirectAndExit();
    }  

    public function contactformAction(){

        $request = $this->getRequest();
        $page_id = $request->getParam('pageId');

        $this->view->headTitle(Globals::getConfig()->sitetitle);

        $this->_helper->layout->setLayout('cart');  
        $this->view->template_view_id = 14; 
        $this->runout();
    }     


    public function searchAction(){
        require_once "../application/models/search_class.php";
        $request = $this->getRequest();
        $curr_page = $request->getParam('curr_page');

        $sort = $_SESSION['products_cat_properties']['prd_sorting'];
        $prd_per_page = $_SESSION['products_cat_properties']['prd_per_pages'];

        $search_obj = new search($curr_page , $prd_per_page , $sort);
        $search_string = $_SESSION['search']['search_string'];
        $search_obj->searchString($search_string);

        $this->view->pagination_data = $search_obj->pagination_data;
        $this->view->data = $search_obj->contents;
        $this->view->headTitle(Globals::getConfig()->sitetitle);

        $this->_helper->layout->setLayout('tags');  
        $this->view->template_view_id = 4; 
        $this->runout();
    }  

    public function skroutzAction(){
        $this->_helper->layout()->disableLayout(); 
        header ("Content-Type: application/xml; charset=utf-8"); 
        require_once "../application/models/skroutz.php";
        $request = $this->getRequest();
        
        $skroutz_obj = new skroutz();
        $this->view->products = $skroutz_obj->getProducts();

    }         

    public function runout(){
        //Set LastVisit
        Globals::setLastVisit($this->ctrl_action);
        Globals::resetInstanceMessage();
    }

}

