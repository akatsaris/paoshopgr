<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class search
{

  public $contents;
  public $pagination_data;
  public $totalpages;
  public $items_per_page;
  public $curr_page;

  public function __construct($curr_page , $items_per_page , $order="ASC"){
    $this->order = $order;
    $this->items_per_page = $items_per_page;
    $this->curr_page = $curr_page;
    $this->searchString($search_text);
  }

  public function searchString($search_text){
      //Get db connection
      $db = Zend_Db_Table::getDefaultAdapter();

      $select = $db->select();
      $select->from(dbprefix.'_products_multilang' , array('name' ,'prd_description' , 'main_photo','friendly_url'));
      $select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' ,'end_price'));

      $select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
      $select->where(dbprefix.'_products.active = 1');
      $select->where(dbprefix.'_products.accompaying = 0');

      if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
        $select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
      }else{
        $select->where(dbprefix.'_products.registered_users_only = 0');
      }


      $searchWords = new Zend_Filter_Alnum(array('allowwhitespace' => true));
      $cleanWords = $searchWords->filter($search_text);

      $search_array = explode(' ', $cleanWords);

      $counter = count($search_array);

      foreach ($search_array as $key => $word) {
        $string = pg_escape_string($word);
      
        if(!empty($string)){       
          $parenthesis_open = "(";
          $parenthesis_close = "";
        
          if($key == $counter-1){
            $parenthesis_close = ")";
          }
  
          if($key > 0){
            $select->orWhere(dbprefix.'_products_multilang.name like "%'.$string.'%"');
      
          }else{
            $select->where($parenthesis_open.dbprefix.'_products_multilang.name like "%'.$string.'%"');
          }
          $select->orWhere(dbprefix.'_products_multilang.prd_description like "%'.$string.'%"');
          $select->orWhere(dbprefix.'_products_multilang.prd_description like "%'.$string.'%"');
          $select->orWhere(dbprefix.'_products_multilang.prd_content like "%'.$string.'%"');
          $select->orWhere(dbprefix.'_products_multilang.keywords like "%'.$string.'%"');
          $select->orWhere(dbprefix.'_products_multilang.friendly_url like "%'.$string.'%"');
          $select->orWhere(dbprefix.'_products.product_code like "%'.$string.'%"'.$parenthesis_close);
        }
      }

      $select->order(dbprefix.'_products.'.$this->order);

      //Query logger
      Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
      
      //echo $select->__toString();

      $paginator = Zend_Paginator::factory($select);
      $paginator->setDefaultItemCountPerPage($this->items_per_page);
      $paginator->setCurrentPageNumber($this->curr_page);

      try{
          $results = $paginator->getCurrentItems()->getArrayCopy();
          $this->totalpages = $paginator->getPages()->pageCount;
      }catch (Exception $e) {
          $this->totalpages = 1;
      }

      $this->pagination_data = $paginator->getPages();
      $this->contents = $results;
  }
}  