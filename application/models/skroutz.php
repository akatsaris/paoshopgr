<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class skroutz
{

	public function getProducts(){
		//Get db connection
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products_multilang' , array('la' , 'name' , 'prd_description' , 'main_photo' , 'friendly_url'));
		$select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' ,'product_code','registered_users_only','active','start_price','end_price' ,'weight'));
		$select->join(dbprefix.'_product_tags' , dbprefix.'_products.product_id = '.dbprefix.'_product_tags.product_id' , array('tag_id'));

		$select->join(dbprefix.'_tags' , dbprefix.'_product_tags.tag_id = '.dbprefix.'_tags.tag_id' , array('tag'));
		//$select->where(dbprefix.'_products.product_id in (select '.dbprefix.'_product_tags.product_id  From '.dbprefix.'_product_tags where '.dbprefix.'_product_tags.tag_id in ('.$this->data[0]['tags'].') group by '.dbprefix.'_product_tags.product_id HAVING  count('.dbprefix.'_product_tags.tag_id)='.$tags_counter.')');

		$select->where(dbprefix.'_products.active = 1');
		$select->where(dbprefix.'_products.accompaying = 0');
		$select->where(dbprefix.'_products_multilang.la = ?', 1);
		$select->where(dbprefix.'_tags.la = ?', 1);

		$select->where(dbprefix.'_product_tags.tag_id <> 21');

		//if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
		//	$select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
		//}else{
			$select->where(dbprefix.'_products.registered_users_only = 0');
		//}

		//$select->group(dbprefix.'_product_tags.product_id');
		//$select->order(dbprefix.'_products.'.$this->order);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
		//echo $select->__toString();
		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		return $results;	
	}

}	