<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class cash_on_delivery_merchant
{
	
	public $data;
	public $type = "NOTHING";
	public $view_form_validation = array();

	public function setData($cart_data){
		$this->data = $this->_prepareData($cart_data);
	}

	protected function _prepareData($cart_data){
		return $cart_data;
	}
}
