<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class paypal_merchant
{
	public $data;
	public $type = "REDIRECT";

	public $view_form_validation = array();
	
	public function setData($cart_data){
		$this->data = $this->_prepareData($cart_data);
	}

	protected function _prepareData($cart_data){
		return $cart_data;
	}

	public function setToPaid($order_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$data = array('order_paid' => 1);
		$result = $db->update(dbprefix.'_orders', $data , 'order_number_id = '.$order_id);

		$queue = new queue('confirmation_queue');
        @EmailSender::setQueue($queue->queue);
		@EmailSender::sendEmail(array('type' => 'confirmation' , 'order_id' => $order_id , 'la'=> $_SESSION['language']['def_lang_id'] , 'la_region' => $_SESSION['language']['lang_region']));
	}

}
