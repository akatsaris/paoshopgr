<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/proxypay_class.php";
class eurobank_merchant
{
	public $data;
	public $type = "API";

	public $view_form_validation = array(
		'cardtype' => array('type' => 'string' , 'required' => false),
		'ccnumber' => array('type' => 'cardnumber' , 'required' => true , 'trl' => 'Credit card number'),
		'cccvv' => array('type' => 'string' , 'required' => true, 'min-digits' => 3 , 'trl' => 'CVV'),
		'ccholder' => array('type' => 'string' , 'required' => true, 'min-digits' => 4 , 'trl' => 'Credit card holder'),
		'ccexpiresmonth' => array('type' => 'string' , 'required' => false),
		'ccexpiresyear' => array('type' => 'string' , 'required' => false)
		);

	public function setData($order_data){
		$this->data = $this->_prepareData($order_data);
	}

	protected function _prepareData($order_data){
		//discounts
		$discount_obj = new discounts();
        $discount = $discount_obj->getDiscount();	


		if($order_data['shipping_info']['gift'] == 1){
			$wrap_cost = Globals::getConfig()->gift_price;
		}else{
			$wrap_cost = 0;
		}

		if($order_data['merchant']['cod_extracost_active'] == 1 && $discount_obj->shipping != "free"){
		    $cod_cost = $order_data['shipping']['shipping_pack_cod_val_no_currency'];
		}else{
		    $cod_cost = 0;
		}

		if($discount_obj->shipping != "free"){
			$shipping_price = $order_data['shipping']['shipping_price_no_currency'];
		}else{
			$shipping_price = 0;
		}

		$amount = $shipping_price + $wrap_cost + $cod_cost + ($order_data['totalcost'] - $discount);

		$params = array();
        $params['amount'] = number_format($amount, 2, '.', '');
        $params['creditCardVendor'] = $order_data['cardtype'];
        $params['creditCardOwnerName'] = $order_data['ccholder'];
        $params['creditCardNumber'] = $order_data['ccnumber'];
        $params['creditCardControlCode'] = $order_data['cccvv'];
        $params['InstallmentPeriod'] = $order_data['ccperiod'];

       //credit card expiration date
        $cc_expiration_date = $order_data['ccexpiresmonth'].substr($order_data['ccexpiresyear'], -2);

        $params['creditCardExpireDate'] = $cc_expiration_date;
        $params['sessionID'] = $order_data['order_number']. "-" . date("YmdHis");
        $params['reservationID'] = $order_data['order_number'];
        $params['billingPhone'] = $order_data['shipping_info']['phone'];
        $params['shippingPhone'] = "";
        $params['billingMobile'] = "";
        $params['shippingMobile'] = "";

		return $params;
	}

	public function ApiCall(){
	    $proxypay = new ProxyPay();
        $proxypay->setParams($this->data);
        $proxypay->PreAuth();
        
        if ($proxypay->HasError()) {
        	return false;
        }else{
        	$proxypay->Capture();
        	return true;
        }

	}
}
