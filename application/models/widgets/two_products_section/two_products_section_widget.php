<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class two_products_sections_widget extends widget
{
	public $data;

	public function __construct($id , $limit){
		$this->getSectionContent($id , $limit);
	}

	public function getSectionContent($id , $limit=2){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();


		$select = $db->select();
		$select->from(dbprefix.'_products_multilang' , array('la' , 'name' ,'prd_description' , 'prd_content', 'main_photo','friendly_url'));
		$select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' , 'registered_users_only' ,'product_code' ,'active','start_price','end_price' , 'template_view_id'));
		
		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_products.active = 1');
		$select->where(dbprefix.'_products.home_section_id = ?' , $id);


		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
		}else{
			$select->where(dbprefix.'_products.registered_users_only = 0');
		}

		$select->limit($limit);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}
}