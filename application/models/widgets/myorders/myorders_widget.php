<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class myorders_widget extends widget
{
	public $data;

	public function __construct($user_id, $limit){
		$this->getOrders($user_id , $limit);
	}

	public function getOrders($user_id, $limit){
		$db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_number_id' , 'order_id' , 'order_date' , 'order_status_id' , 'order_price' , 'order_cod' , 'order_html' , 'user_id'));
        $select->joinLeft(dbprefix.'_order_status' , dbprefix.'_orders.order_status_id = '.dbprefix.'_order_status.ord_status_id' , array('ord_status'));

        $select->where(dbprefix.'_orders.user_id = ?', $user_id);
        $select->group(dbprefix.'_orders.order_number_id');
        $select->order(dbprefix.'_orders.order_date DESC');
        


		$select->limit($limit);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		$this->data = $results;
	}
}	