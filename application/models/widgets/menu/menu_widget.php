<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class menu extends widget
{
	public $tag;
	public $menu_data;
	public $temp;

	public $data;
	public $index;

	public function __construct($tag){
		$this->tag = $tag;
		$this->getMenuData();

	}

	public function getMenuData(){

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menus' , array('menu_name'));
		$select->join(dbprefix.'_menu_data' , dbprefix.'_menus.menu_id = '.dbprefix.'_menu_data.menu_id' , array('menu_data_id' , 'parrent_id' , 'type', 'register_users_only'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('la' , 'title' , 'url' , 'active'));
		$select->where(dbprefix.'_menus.menu_position_name = ?', $this->tag);
		$select->where(dbprefix.'_menu_data_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_menu_data_multilang.active = 1');
		
		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$select->where(dbprefix.'_menu_data.register_users_only = 0 OR '.dbprefix.'_menu_data.register_users_only = 1');
		}else{
			$select->where(dbprefix.'_menu_data.register_users_only = 0');
		}

		$select->order('order');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->menu_data = $results;
	}

	public function generatePageTree($datas, $parrent_elem="ul" , $child_elem="li" , $parent=0, $depth=0 ,$title=null, $active_item_id=0){
	    if($depth > 1000) return ''; // Make sure not to have an endless recursion

	  
	    $tree = '<'.$parrent_elem.'>';
	   	if(!empty($title)){
	      	$tree .= $title;
	    }

	    for($i=0, $ni=count($datas); $i < $ni; $i++){

	    	$found_child = $this->_search_child($datas[$i]['menu_data_id'] , $datas);
	    	$child_of_parent_is_active = $this->_search_parent($active_item_id , $datas);
	    	//new dBug($child_of_parent_is_active);
	    	
	    	if($found_child && $child_of_parent_is_active == $datas[$i]['menu_data_id']){
	    		$parent_class = "parent active";
	    	}elseif ($found_child) {
	    		$parent_class = "parent";
	    	}else{
	    		$parent_class = "";
	    	}

	    	if($active_item_id > 0 && $datas[$i]['menu_data_id'] == $active_item_id){
	    		$active_class = " active";
	    	}else{
	    		$active_class = "";
	    	}

	        if($datas[$i]['parrent_id'] == $parent){
	            $tree .= '<'.$child_elem.' id=item_id_'.$datas[$i]['menu_data_id'].' class="'.$parent_class.$active_class.'">';
	            $tree .= '<a href="'."/".$datas[$i]['type']."/".$datas[$i]['menu_data_id']."/".$datas[$i]['url'].'">'.$datas[$i]['title'].'</a>';
	            $tree .= $this->generatePageTree($datas, $parrent_elem , $child_elem , $datas[$i]['menu_data_id'], $depth+1 , 0 , $active_item_id );
	            $tree .= '</'.$child_elem.'>';
	        }
	    }
	    $tree .= '</'.$parrent_elem.'>';
	    $tree = preg_replace('/<'.$parrent_elem.'><\/'.$parrent_elem.'>/', '', $tree);
	    return $tree;
	}

	protected function _search_child($parent_id , $datas){
		foreach ($datas as $key => $value) {
			if($value['parrent_id'] == $parent_id){
				return true;
			}
		}
	}

	protected function _search_parent($active_item_id , $datas){
		foreach ($datas as $key => $value) {
			if($value['menu_data_id'] == $active_item_id){
				return $value['parrent_id'];
			}
		}
	}

}