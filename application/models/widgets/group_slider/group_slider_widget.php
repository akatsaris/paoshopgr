<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class group_slider_widget extends widget
{
	public $data;

	public function __construct($id , $limit){
		$this->getSectionContent($id , $limit);
	}

	public function getSectionContent($id , $limit=5){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();


		$select = $db->select();
		$select->from(dbprefix.'_simple_content');
		$select->join(dbprefix.'_languages' , dbprefix.'_simple_content.la = '.dbprefix.'_languages.lang_id' , array('digits' , 'shortcuts'));
		$select->join(dbprefix.'_home_sections' , dbprefix.'_home_sections.home_section_id = '.dbprefix.'_simple_content.section' , array('home_tag' , 'name'));
		$select->where(dbprefix.'_simple_content.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_simple_content.active = 1');
		$select->where(dbprefix.'_home_sections.home_tag = ?' , $id);

		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$select->where(dbprefix.'_simple_content.register_users_only = 0 OR '.dbprefix.'_simple_content.register_users_only = 1');
		}else{
			$select->where(dbprefix.'_simple_content.register_users_only = 0');
		}

		$select->limit($limit);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}
}