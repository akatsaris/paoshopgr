<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class login_widget extends widget
{
	public $login_status;
	public $email;

	public function __construct(){
		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$this->login_status = "online";
			$this->email = $_SESSION['visitor']['email'];
		}else{
			$this->login_status = "offline";
		}
	}

}