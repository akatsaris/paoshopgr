<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class related_products_widget extends widget
{
	public $data;

	public function __construct($id, $limit){
		$this->getSectionContent($id , $limit);
	}

	public function getSectionContent($id , $limit=5){
		//Get db connection
                $db = Zend_Db_Table::getDefaultAdapter();
                
                //get this product tags
                $tagData = $this->getTagData($id);
                $tags_counter = count($tagData);
                
                foreach($tagData as $key => $val){
                    $tagData_str .= $val['tag_id'].",";
                }
                $tagData_str = rtrim($tagData_str,",");

                $select = $db->select();
                $select->from(dbprefix.'_products_multilang' , array('la' , 'name' , 'prd_description' , 'main_photo' , 'friendly_url'));
                $select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' ,'product_code','registered_users_only','active','start_price','end_price'));
                $select->join(dbprefix.'_product_tags' , dbprefix.'_products.product_id = '.dbprefix.'_product_tags.product_id' , array('tag_id'));

                $select->where(dbprefix.'_products.product_id in (select '.dbprefix.'_product_tags.product_id  From '.dbprefix.'_product_tags where '.dbprefix.'_product_tags.tag_id in ('.$tagData_str.') group by '.dbprefix.'_product_tags.product_id ORDER BY  count('.dbprefix.'_product_tags.tag_id)='.$tags_counter.' DESC)');

                $select->where(dbprefix.'_products.active = 1');
                $select->where(dbprefix.'_products.accompaying = 0');
                $select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);

                if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
                        $select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
                }else{
                        $select->where(dbprefix.'_products.registered_users_only = 0');
                }
                $select->where(dbprefix.'_product_tags.tag_id in ('.$tagData_str.')');
                $select->where(dbprefix.'_products.product_id <> ?',$id);
                

                $select->group(dbprefix.'_product_tags.product_id');
                $select->order('count('.dbprefix.'_product_tags.tag_id) DESC');

		$select->limit($limit);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
        //echo $select->__toString();
		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}
        
        private function getTagData($id){
		//Get db connection
                $db = Zend_Db_Table::getDefaultAdapter();
        
                $select = $db->select();
                $select->from(dbprefix.'_product_tags' , array('tag_id'));
                $select->where(dbprefix.'_product_tags.product_id = '.$id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		return  $results;            
        }
}