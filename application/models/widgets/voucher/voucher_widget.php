<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class voucher_widget extends widget
{
	public function __construct(){
		if(isset($_SESSION['discounts']['voucher'])){
			$this->voucher = $_SESSION['discounts']['voucher'];
		}
	}
}	