<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class paoticket_widget extends widget
{
	public function __construct(){
		if(isset($_SESSION['discounts']['paoticket'])){
			$this->voucher = $_SESSION['discounts']['paoticket'];
		}
	}
}	
