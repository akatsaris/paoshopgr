<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class multilanguage_content_widget extends widget
{
	public $data;

	public function __construct($id){
		$this->getMultilanguageContent($id);
	}

	public function getMultilanguageContent($id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_multilanguage_html_content' , array('name' , 'html_content'));
		$select->where(dbprefix.'_multilanguage_html_content.content_id = ?', $id);
		$select->where(dbprefix.'_multilanguage_html_content.la = ?', $_SESSION['language']['def_lang_id']);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}
}