<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class content
{
	public $data;

	public function __construct($pageid){
		$this->getContent($pageid);
	}

	public function getContent($pageid){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menu_data' , array('menu_data_id' , 'type' ,'template_view_id' , 'register_users_only'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('la' , 'url' ,'active'));
		$select->join(dbprefix.'_content_plugin' , dbprefix.'_menu_data_multilang.menu_data_multilang_id = '.dbprefix.'_content_plugin.menu_data_multilang_id');
		$select->where(dbprefix.'_menu_data_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_menu_data.menu_data_id = ?', $pageid);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}	

}