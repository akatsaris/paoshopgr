<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/cart_model.php";
require_once "../application/models/shipping_model.php";
class order {

	public $order_num;
	public $sess_id;
	public $cart_contents;
	public $total_cost;
	public $shipping_info;
	public $payment_type;
	public $payment_data;
	public $shipp_properties;
	public $paid;
	public $merchant;
	public $comment;
	public $ord_id;
	public $order_html;
	
	public $discount;
	public $discount_type;
	public $free_shipping;

	public $error = false;
	public $error_mesage;

	public function prepareAndSaveOrder($order_num , $sess_id , $comment , $order_html){
		$this->sess_id = $sess_id;
		$this->order_num = $order_num;
		$this->comment = $comment;
		$this->order_html = $order_html;
		if($this->setData()){
			$this->saveOrder();
			$this->saveShipping();
			$this->saveItems();
			$this->resetOrder();
		}		
	}

	public function setData(){
		if(isset($_SESSION['payment_options']['payment'])){
			$cart_obj = new cart();
		    $this->cart_contents = $cart_obj->get_cart_contents();
		    $this->total_cost    = $cart_obj->total;
		    $this->shipping_info = $cart_obj->getShippingInfo();


			$discount_obj = new discounts();
            $this->discount = $discount_obj->getDiscount();	
            $this->discount_type = $discount_obj->discount_type;	

		    if($discount_obj->shipping == "free") {
		    	$this->free_shipping = true;
		    }else{
		    	$this->free_shipping = false;
		    }

		    $this->discount_rule = $discount_obj->priority;                
		    
		    $this->payment_type = $_SESSION['payment_options']['payment'];
		    
		    $merchant_data = $cart_obj->getPaymentMethod($this->payment_type);
		    $merchant = $merchant_data['merchant_plugin'];
		    
		    //Include model
		    require_once "../application/models/merchants/".$merchant."/".$merchant.".php";
		    $merchant_class_name =  $merchant."_merchant";
		    $merchant_obj = new $merchant_class_name();

		    
		    $this->payment_data = $merchant_data;

		    $region_id    = $this->shipping_info['region_id'];
		    $country_code = $this->shipping_info['country_code'];

		    $shipping_obj = new shipping();
		    $package_id = $_SESSION['courier']['package_id'];

		    if(!empty($region_id)){
		        $this->shipp_properties = $shipping_obj->getCourierPackagesByPackageID($region_id , 'region' , $package_id);
		    }else{
		        $this->shipp_properties = $shipping_obj->getCourierPackagesByPackageID($country_code , 'country' , $package_id);
		    }

		    $merchant_params = array_merge(array('shipping' => $this->shipp_properties) , array('merchant' => $merchant_data) , array('shipping_info' => $this->shipping_info) , array('cart' => $this->cart_contents) , $_SESSION['payment_options'] , array('order_number' => $_SESSION['order_number']) , $_SESSION['cart']);
		    $merchant_obj->setData($merchant_params);

		    switch ($merchant_obj->type) {
			    case 'NOTHING':
			        $this->paid = 1;
			        $this->merchant = $merchant;
			        break;
			    case 'REDIRECT':
			        $this->paid = 0;
			        $this->merchant = $merchant;
			        break;
			    case 'API':
			    	$this->merchant = $merchant;
			        //run API
			        if($merchant_obj->ApiCall()){
			        	$this->paid = 1;
			        	return true;
			        }else{
			        	$this->error = true;
			        	$this->error_mesage = "Merchant Payment Failed";
			        	return false;
			    	}
			        break;
			}
			return true;
		}else{
			return false;
		}

	}

	public function saveOrder(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$data = array();
		$data['order_number_id'] = $this->order_num;
		$data['session_id'] = $this->sess_id;
		$data['user_id'] = $this->shipping_info['user_id'];
		
		//Check for free shipping
		if($this->free_shipping == false){
			$data['order_shipping'] = $this->shipp_properties['shipping_price_no_currency'];
		}else{
			$data['order_shipping'] = 0;
		}

		$data['order_shipping_id'] = $this->shipp_properties['shipping_packet_id'];
		
		//Check for free COD and if cost exists
		if($this->payment_data['cod_extracost_active'] == 1 && $this->free_shipping == false){
			$data['order_cod'] = $this->shipp_properties['shipping_pack_cod_val_no_currency'];
		}else{
			$data['order_cod'] = 0;
		}

		$data['order_price'] = $this->total_cost;
		$data['order_paid'] = $this->paid;
		$data['invoice'] = $this->shipping_info['invoice_type'];
		$data['comment'] = $this->comment;
		$data['merchant'] = $this->merchant;
		$data['order_html'] = $this->order_html;
		$data['order_la_region'] = $_SESSION['language']['lang_region'];
		$data['order_discount'] = $this->discount;
		$data['order_discount_type'] = $this->discount_type;
		$data['order_discount_rule'] = $this->discount_rule;

		//wrapping cost
		if($this->shipping_info['gift'] == 1){
			$data['wrap_packaging_cost'] = Globals::getConfig()->gift_price;
		}
		
		if(!empty($separated_attributes)){
			$data['product_attributes'] = $separated_attributes;
		}	
		
		$db->insert(dbprefix.'_orders', $data);
		$this->ord_id = $db->lastInsertId();		
	}

	public function saveShipping(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $data = $this->shipping_info;
        unset($data['session_id']);
		$data['order_id'] = $this->ord_id;

		$db->insert(dbprefix.'_order_shipping', $data);
	}

	public function saveItems(){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$cart = $this->cart_contents;
		foreach ($cart as $key => $cart_row) {
			$data['order_id'] = $this->ord_id;
			$data['product_id'] = $cart_row['product']['product_id'];
			$data['quantity'] = $cart_row['product']['quantity'];
			$data['price'] = $cart_row['product']['end_price'];
			$data['product_attributes'] = $cart_row['product']['product_attributes'];
			$data['accompaying'] = 0;
			$data['parent_product_id'] = 0;
			$data['parent_order_item_id'] = 0;
			
			$db->insert(dbprefix.'_order_items', $data);
			$this->_decreaseQuantity($cart_row['product']['product_id'] , $cart_row['product']['product_attributes'] , $cart_row['product']['quantity']);

			$item_last_id = $db->lastInsertId();

			if(isset($cart_row['acc_products'])){
				foreach ($cart_row['acc_products'] as $key => $cart_row_acc) {
					$data['order_id'] = $this->ord_id;
					$data['product_id'] = $cart_row_acc['product_id'];
					$data['quantity'] = $cart_row_acc['quantity'];
					$data['price'] = $cart_row_acc['end_price'];
					$data['accompaying'] = 1;
					$data['parent_product_id'] = $cart_row['product']['product_id'];
					$data['parent_order_item_id'] = $item_last_id;
					$db->insert(dbprefix.'_order_items', $data);
					$this->_decreaseQuantity($cart_row_acc['product_id'] , "" , $cart_row_acc['quantity']);
				}
			}	
		}
	}

	protected function _decreaseQuantity($product_id , $attributes , $quantity){
		$stock_control = new stock_control();
		$stock_control->updateQuantity($product_id , $attributes , $quantity);
	}

	public function resetOrder(){
		if($this->paid == 1){
			$this->_sendConfirmationEmail($_SESSION['language']['def_lang_id'] , $_SESSION['language']['lang_region'] , $_SESSION['order_number']);
		}

		$cart_obj = new cart();
		$cart_obj->empty_cart();
		
		$this->_makeVoucherUsed();
		unset($_SESSION['discounts']);

		unset($_SESSION['order_number']);
		unset($_SESSION['courier']);
		unset($_SESSION['cart']);
	}

	public function removeOrder($order_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$result = $db->delete(dbprefix.'_orders', "order_number_id = ".$order_id);
		return $result;
	}

	public function getMerchantByOrderID($order_id){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_orders' , array('merchant'));
		$select->where(dbprefix.'_orders.order_number_id = ?', $order_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0]['merchant'];
	}

	protected function _makeVoucherUsed(){
		if(isset($_SESSION['discounts']['voucher']['v_code'])){
			$v_code = $_SESSION['discounts']['voucher']['v_code'];
			$db = Zend_Db_Table::getDefaultAdapter();

			if(!empty($this->discount)){
				$data['used'] = 1;
				$db->update(dbprefix.'_vouchers', $data , 'v_code = "'.$v_code.'"');
			}
		}
	}

	protected function _sendConfirmationEmail($la , $la_region , $order_id){
		$queue = new queue('confirmation_queue');
        @EmailSender::setQueue($queue->queue);
		@EmailSender::sendEmail(array('type' => 'confirmation' , 'order_id' => $order_id , 'la'=> $la , 'la_region' => $la_region));
	}

}
