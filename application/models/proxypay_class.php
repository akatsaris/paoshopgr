<?
class ProxyPay {


    public  $errors;
	public  $params;

    public $errorCode;
    public $errorText;

    // Bank account properties
    private $userName;
    private $password;
    private $url;
    private $http_timeout;

    public function __construct() {
        $this->errors = array();
        $this->userName = Globals::getConfig()->bank->eurobank->login;
        $this->password = Globals::getConfig()->bank->eurobank->password;
        $this->url = Globals::getConfig()->bank->eurobank->postaction;
        $this->http_timeout = 120;
    }

	public function setParams($params){
         $this->params = $params;
	}

    public function PreAuth () {
		$params = $this->params;

        // is credit card vendor supported
        if (!self::isCreditCardVendorSupported($params['creditCardVendor'])) {
           $this->errorCode = 'PP_Vendor';
           $this->errorText = 'Credit card vendor "' . $params['creditCardVendor'] . '" is not supported';
           return '';
        }

        //params
        $amount = isset($params['amount']) ? intval(floatval($params['amount']) * 100) : 0;
        $creditCardOwnerName = isset($params['creditCardOwnerName']) ? $params['creditCardOwnerName'] : "";
        $creditCardNumber = isset($params['creditCardNumber']) ? $params['creditCardNumber'] : "";
        $creditCardControlCode = isset($params['creditCardControlCode']) ? $params['creditCardControlCode'] : "";
        $creditCardExpireDate = isset($params['creditCardExpireDate']) ? $params['creditCardExpireDate'] : "";
        
        $InstallmentPeriod = isset($params['InstallmentPeriod']) ? $params['InstallmentPeriod'] : 0;

        $sessionID = isset($params['sessionID']) ? $params['sessionID'] : "";
        $reservationID = isset($params['reservationID']) ? $params['reservationID'] : "";
        $billingPhone = isset($params['billingPhone']) ? $params['billingPhone'] : "";
        $shippingPhone = isset($params['shippingPhone']) ? $params['shippingPhone'] : "";
        $billingMobile = isset($params['billingMobile']) ? $params['billingMobile'] : "";
        $shippingMobile = isset($params['shippingMobile']) ? $params['shippingMobile'] : "";

        

        // login
        $username = $this->_GetUserName();
        $password = $this->_GetPassword();
        $transaction_id = $this->_GetNextTransactionID();

        $data = '<?xml version="1.0" encoding="UTF-8"?>'.
            '<JProxyPayLink>'.
                '<Message>'.
                    '<Type>PreAuth</Type>'.
                    '<Authentication>'.
                        '<MerchantID>'.$username.'</MerchantID>'.
                        '<Password>'.$password.'</Password>'.
                    '</Authentication>'.
                    '<OrderInfo>'.
                        '<Amount>'.$amount.'</Amount>'.
                        '<MerchantRef>'.$sessionID.'</MerchantRef>'.
                        '<MerchantDesc>'.$reservationID.'</MerchantDesc>'.
                        '<Currency>978</Currency>'.
                        '<CustomerEmail></CustomerEmail>'.
                        '<Var1>Name: '.$this->_EncodeHTML($this->translate_greek_to_latin($creditCardOwnerName)).'</Var1>'.
                        '<Var2>Billing Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingPhone)).'</Var2>'.
                        '<Var3>Shipping Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingPhone)).'</Var3>'.
                        '<Var4>Billing Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingMobile)).'</Var4>'.
                        '<Var5>Shipping Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingMobile)).'</Var5>'.
                        '<Var6>Transaction ID: '.$transaction_id.'</Var6>'.
                        '<Var7></Var7>'.
                        '<Var8></Var8>'.
                        '<Var9></Var9>'.
                    '</OrderInfo>'.
                    '<PaymentInfo>'.
                        '<CCN>'.$creditCardNumber.'</CCN>'.
                        '<Expdate>'.$creditCardExpireDate.'</Expdate>'.
                        '<CVCCVV>'.$creditCardControlCode.'</CVCCVV>'.
                        '<InstallmentOffset>0</InstallmentOffset>'.
                        '<InstallmentPeriod>'.$InstallmentPeriod.'</InstallmentPeriod>'.
                    '</PaymentInfo>'.
                '</Message>'.
            '</JProxyPayLink>';

        $result = $this->_SendRequest($data, $transaction_id);

        return $result;
    }

    public function Capture () {
		$params = $this->params;
        // is credit card vendor supported
        if (!self::isCreditCardVendorSupported($params['creditCardVendor'])) {
           $this->errorCode = 'PP_Vendor';
           $this->errorText = 'Credit card vendor "' . $params['creditCardVendor'] . '" is not supported';
           return '';
        }
        // params
        $amount = isset($params['amount']) ? intval(floatval($params['amount']) * 100) : 0;
        $creditCardOwnerName = isset($params['creditCardOwnerName']) ? $params['creditCardOwnerName'] : "";
        $sessionID = isset($params['sessionID']) ? $params['sessionID'] : "";
        $reservationID = isset($params['reservationID']) ? $params['reservationID'] : "";
        $billingPhone = isset($params['billingPhone']) ? $params['billingPhone'] : "";
        $shippingPhone = isset($params['shippingPhone']) ? $params['shippingPhone'] : "";
        $billingMobile = isset($params['billingMobile']) ? $params['billingMobile'] : "";
        $shippingMobile = isset($params['shippingMobile']) ? $params['shippingMobile'] : "";

        // login
        $username = $this->_GetUserName();
        $password = $this->_GetPassword();
        $transaction_id = $this->_GetNextTransactionID();


        $data = '<?xml version="1.0" encoding="UTF-8"?>'.
            '<JProxyPayLink>'.
                '<Message>'.
                    '<Type>Capture</Type>'.
                    '<Authentication>'.
                        '<MerchantID>'.$username.'</MerchantID>'.
                        '<Password>'.$password.'</Password>'.
                    '</Authentication>'.
                    '<OrderInfo>'.
                        '<Amount>'.$amount.'</Amount>'.
                        '<MerchantRef>'.$sessionID.'</MerchantRef>'.
                        '<MerchantDesc>'.$reservationID.'</MerchantDesc>'.
                        '<Currency>978</Currency>'.
                        '<CustomerEmail></CustomerEmail>'.
                        '<Var1>Name: '.$this->_EncodeHTML($this->translate_greek_to_latin($creditCardOwnerName)).'</Var1>'.
                        '<Var2>Billing Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingPhone)).'</Var2>'.
                        '<Var3>Shipping Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingPhone)).'</Var3>'.
                        '<Var4>Billing Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingMobile)).'</Var4>'.
                        '<Var5>Shipping Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingMobile)).'</Var5>'.
                        '<Var6>Transaction ID: '.$transaction_id.'</Var6>'.
                        '<Var7></Var7>'.
                        '<Var8></Var8>'.
                        '<Var9></Var9>'.
                    '</OrderInfo>'.
                '</Message>'.
            '</JProxyPayLink>';

        $result = $this->_SendRequest($data, $transaction_id);

        return $result;
    }

    public function Cancel () {
        $params = $this->params;
        $amount = isset($params['amount']) ? intval(floatval($params['amount']) * 100) : 0;
        $creditCardOwnerName = isset($params['creditCardOwnerName']) ? $params['creditCardOwnerName'] : "";
        $sessionID = isset($params['sessionID']) ? $params['sessionID'] : "";
        $reservationID = isset($params['reservationID']) ? $params['reservationID'] : "";
        $billingPhone = isset($params['billingPhone']) ? $params['billingPhone'] : "";
        $shippingPhone = isset($params['shippingPhone']) ? $params['shippingPhone'] : "";
        $billingMobile = isset($params['billingMobile']) ? $params['billingMobile'] : "";
        $shippingMobile = isset($params['shippingMobile']) ? $params['shippingMobile'] : "";

        // login
        $username = $this->_GetUserName();
        $password = $this->_GetPassword();
        $transaction_id = $this->_GetNextTransactionID();


        $data = '<?xml version="1.0" encoding="UTF-8"?>'.
            '<JProxyPayLink>'.
                '<Message>'.
                    '<Type>Cancel</Type>'.
                    '<Authentication>'.
                        '<MerchantID>'.$username.'</MerchantID>'.
                        '<Password>'.$password.'</Password>'.
                    '</Authentication>'.
                    '<OrderInfo>'.
                        '<Amount>0</Amount>'.
                        '<MerchantRef>'.$sessionID.'</MerchantRef>'.
                        '<MerchantDesc>'.$reservationID.'</MerchantDesc>'.
                        '<Currency>978</Currency>'.
                        '<CustomerEmail></CustomerEmail>'.
                        '<Var1>Name: '.$this->_EncodeHTML($this->translate_greek_to_latin($creditCardOwnerName)).'</Var1>'.
                        '<Var2>Billing Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingPhone)).'</Var2>'.
                        '<Var3>Shipping Phone: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingPhone)).'</Var3>'.
                        '<Var4>Billing Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($billingMobile)).'</Var4>'.
                        '<Var5>Shipping Mobile: '.$this->_EncodeHTML($this->translate_greek_to_latin($shippingMobile)).'</Var5>'.
                        '<Var6>Transaction ID: '.$transaction_id.'</Var6>'.
                        '<Var7></Var7>'.
                        '<Var8></Var8>'.
                        '<Var9></Var9>'.
                    '</OrderInfo>'.
                '</Message>'.
            '</JProxyPayLink>';

        $result = $this->_SendRequest($data, $transaction_id);

        return $result;
    }

    private function _SendRequest ($data, $transaction_id) {
        $result = "";

        $this->_LogHTTPRequest($data, $transaction_id);

        // arguments
            $arguments = array(
            'APACScommand' => 'NewRequest',
            'Data'         => $data
        );

      $client = new Zend_Http_Client($this->url, array('timeout' => $this->http_timeout));
      $client->setParameterPost($arguments);

     try {
       $response = $client->request('POST');
       $result   = $response->getBody();
      } catch(Exception $e) {

       $this->errorCode = 'PP_HTTP';
       $this->errorText = $e->getMessage();
      }

        $this->_LogHTTPResponse($result, $transaction_id);

        $this->_SetError($result);

        return $result;
    }

    private function _GetUserName () {
        return $this->userName;
    }

    private function _GetPassword () {
        return $this->password;
    }

    /**
     * Gets NextTransactionID.
     */
    // private function _GetNextTransactionID () {
    //     $file = "wwwApplication/config/transaction_id.txt";
    //     if (!file_exists($file)) {
    //         file_put_contents($file, "1");
    //         chmod($file, '0775');
    //     }
    //     $content = file_get_contents($file);
    //     if (($content !== false) && (!empty($content)) && (intval($content) > 0)) {
    //         $content = intval($content) + 1;
    //         $this->file_write($file, $content);
    //     }

    //     // type
    //     $stage = Zend_Registry::get('configuration');
    //     if ($stage['ini_status'] == "production") {
    //         $transaction_id = "styleavenue-live";
    //     } else {
    //         $transaction_id = "styleavenue-dev";
    //     }

    //     // id
    //     $transaction_id .= "-" . sprintf("%010d", !isset($_SESSION['CustomerInstanceID']) ? 0 : intval($_SESSION['CustomerInstanceID']));

    //     // counter
    //     $transaction_id .= "-" . sprintf("%010d", $content);

    //     return $transaction_id;
    // }

    private function _GetNextTransactionID () {
        $unique = Globals::getRandomNum();
        $sess_id = session_id();

        $transaction_id = "paoshop";

        // unique
        $transaction_id .= "-" . sprintf("%010d", $unique);

        return $transaction_id;
    }    

    private function file_write ($sFile, $sContent) {
        $existed = file_exists($sFile);
        if (!$oHandle = fopen($sFile, "w")) { trigger_error("Cannot open file \"" . $sFile . "\"", E_USER_ERROR); }
        if (fwrite($oHandle, $sContent) === false) { trigger_error("Cannot write to file \"" . $sFile . "\"", E_USER_ERROR); }
        fclose($oHandle);
        if (!$existed) {
            chmod($sFile, '0775');
        }
    } //file_write ($sFile, $sContent)

    private function _LogHTTPRequest ($request, $id) {
        $request = preg_replace("#<CCN>([0-9]*)</CCN>#", "<CCN></CCN>", $request);
        $this->create_dir("../logs/" . date("Y.m.d", time()));
        $file = "../logs/" . date("Y.m.d", time()) . "/" . $id  . "_proxypay_request.log";
        $content = "";
        $content .= date("Y.m.d H:i:s", time()) . "\n";
        $content .= $request;
        $this->file_write($file, $content);
    }

    private function _LogHTTPResponse ($response, $id = 0) {
        $this->create_dir("../logs/" . date("Y.m.d", time()));
        $file = "../logs/" . date("Y.m.d", time()) . "/" . $id  . "_proxypay_response.log";
        $content = "";
        $content .= date("Y.m.d H:i:s", time()) . "\n";
        $content .= str_replace("    ", "\t", print_r($response, true));
        $this->file_write($file, $content);
    }

    public function create_dir ($sDir) {
        if (!is_dir($sDir)) {
            mkdir($sDir, 0775);
                trigger_error("Directory \"" . $sDir . "\" cannot be created.", E_USER_ERROR);
        }
    }

    /**
     * Sets the error contained in the result, if applicable
     */
    private function _SetError($result) {
        $ses_id = session_id();
        $databaseDate = date("ymdHis");
        if (!empty($result)) {
            $errorCode = array();
            preg_match("#<ERRORCODE>([0-9]*)</ERRORCODE>#", $result, $errorCode);
            $errorMessage = array();
            preg_match("#<ERRORMESSAGE>(.*)</ERRORMESSAGE>#", $result, $errorMessage);
            if ((count($errorCode) >= 2) && (count($errorMessage) >= 2)) {
                $code = intval(trim($errorCode[1]));
                $message = trim($errorMessage[1]);
                if ($code > 0) {
                    $code = "PP_" . $code;
                    array_push($this->errors, $code);
                }
            }

            if (strpos($result, "Not Found (404)") !== false) {
                //Hotel_Session_Logs::InsertSP("Error (ProxyPay): 404" , $ses_id, $_SESSION['CustomerInstanceID'] , $databaseDate , "[1]" , "");
                array_push($this->errors, "PP_404");
            }

            if (strpos($result, "<?xml") === false) {
                //Hotel_Session_Logs::InsertSP("Error (ProxyPay xml): -0" , $ses_id, $_SESSION['CustomerInstanceID'] , $databaseDate , "[1]" , "");
                array_push($this->errors, "-0");
            }
        } else {
            //Hotel_Session_Logs::InsertSP("Error (ProxyPay): -0" , $ses_id, $_SESSION['CustomerInstanceID'] , $databaseDate , "[1]" , "");
            array_push($this->errors, "-0");
        }
    }

    /**
     * Gets only the first error.
     */
    public function GetError() {
        $error = "";
        if ($this->HasError()) {
            $error = $this->errors[0];
        }
        $this->errors = array();
        return $error;
    }

    /**
     * Gets all errors.
     */
    public function GetErrors() {
        $errors = $this->errors;
        $this->errors = array();
        return $errors;
    }

    public function HasError() {
        return !empty($this->errors);
    }

    private function _EncodeHTML ($text) {
        return htmlspecialchars($text, ENT_COMPAT, "UTF-8");
    }

    private function translate_greek_to_latin($str){
        $greek_mapping = array(
                                'ΟY' => 'U',
                                'Ου' => 'U',
                                'ου' => 'u',
                                'ΕΙ' => 'I',
                                'Ει' => 'I',
                                'ει' => 'i',
                                'ΓΚ' => 'GG',
                                'Γκ' => 'Gg',
                                'γκ' => 'gg',
                                'ΟΙ' => 'I',
                                'Οι' => 'I',
                                'οι' => 'i',
                                'ΜΠ' => 'B',
                                'Μπ' => 'B',
                                'μπ' => 'b',
                                'ΝΤ' => 'D',
                                'Ντ' => 'D',
                                'ντ' => 'd',
                                'ΕΥ' => 'Ev',
                                'Ευ' => 'Ev',
                                'ευ' => 'ev',
                                'ΕΦ' => 'EF',
                                'Εφ' => 'Ef',
                                'εφ' => 'ef',
                                'Θ' => 'Th',
                                'θ' => 'th',
                                'Χ' => 'Ch',
                                'χ' => 'ch',
                                'Ψ' => 'Ps',
                                'ψ' => 'ps',
                                'Α' => 'A',
                                'α' => 'a',
                                'Ά' => 'A',
                                'ά' => 'a',
                                'Β' => 'V',
                                'β' => 'v',
                                'Γ' => 'G',
                                'γ' => 'g',
                                'Δ' => 'D',
                                'δ' => 'd',
                                'Ε' => 'E',
                                'ε' => 'e',
                                'Έ' => 'E',
                                'έ' => 'e',
                                'Ζ' => 'Z',
                                'ζ' => 'z',
                                'Η' => 'I',
                                'η' => 'i',
                                'Ή' => 'I',
                                'ή' => 'i',
                                'Ι' => 'I',
                                'ι' => 'i',
                                'Ί' => 'I',
                                'ί' => 'i',
                                'ϊ' => 'i',
                                'Ϊ' => 'Ι',
                                'Κ' => 'K',
                                'κ' => 'k',
                                'Λ' => 'L',
                                'λ' => 'l',
                                'Μ' => 'M',
                                'μ' => 'm',
                                'Ν' => 'N',
                                'ν' => 'n',
                                'Ξ' => 'X',
                                'ξ' => 'x',
                                'Ο' => 'O',
                                'ο' => 'o',
                                'Ό' => 'O',
                                'ό' => 'o',
                                'Π' => 'P',
                                'π' => 'p',
                                'Ρ' => 'R',
                                'ρ' => 'r',
                                'Σ' => 'S',
                                'σ' => 's',
                                'ς' => 's',
                                'Τ' => 'T',
                                'τ' => 't',
                                'Υ' => 'Y',
                                'υ' => 'y',
                                'Ύ' => 'Y',
                                'ύ' => 'y',
                                'Φ' => 'F',
                                'φ' => 'f',
                                'Ω' => 'O',
                                'ω' => 'o',
                                'Ώ' => 'O',
                                'ώ' => 'o'
                            );

                 $str = strtr($str, $greek_mapping);

                // upper case check
                if (!empty($str)) {
                    $charsTotal = strlen($str);
                    $charsUpper = 0;

                    for ($i = 0; $i < $charsTotal; $i++) {
                        if ($str{$i} != strtolower($str{$i})) {
                            $charsUpper++;
                        }
                    }

                    if (($charsUpper / $charsTotal) > 0.5) {
                        $str = strtoupper($str);
                    }
                }

                return $str;

    }

     public static function isCreditCardVendorSupported($creditCardVendor){
      if (in_array($creditCardVendor, array('DC', 'DM', 'CA', 'VI', 'VE'))) {
       return true;
      }

      return false;
     }

}
?>