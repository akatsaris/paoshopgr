<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class tags
{
	public $data;
	public $contents;
	public $products;
	public $pagination_data;
	public $items_per_page;
	public $curr_page;
	public $order;

	public function __construct($pageid , $curr_page , $items_per_page , $order="ASC"){
		$this->order = $order;
		$this->items_per_page = $items_per_page;
		$this->curr_page = $curr_page;
		$this->getTags($pageid);
		$this->getProducts();
	}

	public function getTags($pageid){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_menu_data' , array('menu_data_id' , 'type' ,'template_view_id', 'register_users_only'));
		$select->join(dbprefix.'_menu_data_multilang' , dbprefix.'_menu_data.menu_data_id = '.dbprefix.'_menu_data_multilang.menu_data_id' , array('la' , 'url','active','title'));
		$select->join(dbprefix.'_tags_plugin' , dbprefix.'_menu_data_multilang.menu_data_multilang_id = '.dbprefix.'_tags_plugin.menu_data_multilang_id');
		$select->where(dbprefix.'_menu_data_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_menu_data.menu_data_id = ?', $pageid);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;		
	}

	public function getContents(){
		$tags = explode(",", $this->data[0]['tags']);

		if(!empty($this->data)){
			//Get db connection
	        $db = Zend_Db_Table::getDefaultAdapter();

			$select = $db->select();
			$select->from(dbprefix.'_simple_content');
			$select->join(dbprefix.'_languages' , dbprefix.'_simple_content.la = '.dbprefix.'_languages.lang_id' , array('shortcuts'));
			foreach ($tags as $key => $tag) {
				$select->orWhere(dbprefix.'_simple_content.tags LIKE ("%'.$tag.'%")');
			}
			
			
			$select->where(dbprefix.'_simple_content.la = ?', $_SESSION['language']['def_lang_id']);
			$select->where(dbprefix.'_simple_content.active = 1');

			if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
				$select->where(dbprefix.'_simple_content.register_users_only = 0 OR '.dbprefix.'_simple_content.register_users_only = 1');
			}else{
				$select->where(dbprefix.'_simple_content.register_users_only = 0');
			}

			//Query logger
			Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

	        $paginator = Zend_Paginator::factory($select);
	        $paginator->setDefaultItemCountPerPage($this->items_per_page);
	        $paginator->setCurrentPageNumber($this->curr_page);

	        try{
	            $results = $paginator->getCurrentItems()->getArrayCopy();
	            $this->totalpages = $paginator->getPages()->pageCount;
	        }catch (Exception $e) {
	            $this->totalpages = 1;
	        }

	        $this->pagination_data = $paginator->getPages();
			$this->contents = $results;
		}else{
			$this->contents = false;
		}
	}

	public function getProducts(){
		if(!empty($this->data)){

			$tags_array = explode(",", $this->data[0]['tags']);
			$tags_counter = count($tags_array);
			
			//Get db connection
	        $db = Zend_Db_Table::getDefaultAdapter();

			$select = $db->select();
			$select->from(dbprefix.'_products_multilang' , array('la' , 'name' , 'prd_description' , 'main_photo' , 'friendly_url'));
			$select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' ,'product_code','registered_users_only','active','start_price','end_price'));
			$select->join(dbprefix.'_product_tags' , dbprefix.'_products.product_id = '.dbprefix.'_product_tags.product_id' , array('tag_id'));
			
			$select->where(dbprefix.'_products.product_id in (select '.dbprefix.'_product_tags.product_id  From '.dbprefix.'_product_tags where '.dbprefix.'_product_tags.tag_id in ('.$this->data[0]['tags'].') group by '.dbprefix.'_product_tags.product_id HAVING  count('.dbprefix.'_product_tags.tag_id)='.$tags_counter.')');

			$select->where(dbprefix.'_products.active = 1');
			$select->where(dbprefix.'_products.accompaying = 0');
			$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);

			if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
				$select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
			}else{
				$select->where(dbprefix.'_products.registered_users_only = 0');
			}

			$select->group(dbprefix.'_product_tags.product_id');
			$select->order(dbprefix.'_products.'.$this->order);
			
			//Query logger
			Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');


	        $paginator = Zend_Paginator::factory($select);
	        $paginator->setDefaultItemCountPerPage($this->items_per_page);
	        $paginator->setCurrentPageNumber($this->curr_page);

	        try{
	            $results = $paginator->getCurrentItems()->getArrayCopy();
	            $this->totalpages = $paginator->getPages()->pageCount;
	        }catch (Exception $e) {
	            $this->totalpages = 1;
	        }

	        $this->pagination_data = $paginator->getPages();			
	        $this->products = $results;
		}else{
			$this->products = false;
		}
	}	

}