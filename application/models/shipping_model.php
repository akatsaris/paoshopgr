<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class shipping {

    public function getRegions($country_code){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_areas' , array('area_id' , 'area_name' , 'area_code'));
        $select->where(dbprefix.'_areas.area_code = ?', $country_code);
        $select->where(dbprefix.'_areas.area_type = 2');

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        foreach ($results as $key => $value) {
            if($_SESSION['language']['def_lang_id'] != 1){
                $area_name = Globals::greek2english($value['area_name']);
            }else{
                $area_name = $value['area_name'];
            }
            $dropdown[][$value['area_id']] = $area_name;
        }
        return $dropdown;
    }

    public function getCartWeight($session_id){
        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_cart' , array('sum_weight' => 'SUM('.dbprefix.'_products.weight * '.dbprefix.'_cart.quantity)'));
        $select->join(dbprefix.'_products' , dbprefix.'_cart.product_id = '.dbprefix.'_products.product_id' , array());
        $select->where(dbprefix.'_cart.session_id = ?', $session_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        return $results[0]['sum_weight'];
    }

    public function getCourierPackages($area_id , $type){

        $sess_id = session_id();
        $weight = $this->getCartWeight($sess_id);
        
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_shipping_pack_kg' , array('shipping_price'));
        
        $select->join(dbprefix.'_shipping_areas' , dbprefix.'_shipping_pack_kg.shipping_pack_id = '.dbprefix.'_shipping_areas.shipping_pack_id' , array());
        $select->join(dbprefix.'_areas' , dbprefix.'_areas.area_id = '.dbprefix.'_shipping_areas.area_id' , array());

        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_shipping_areas.shipping_pack_id',array('shipping_pack_description' , 'shipping_packet_id'));
        $select->join(dbprefix.'_shipping_providers' , dbprefix.'_shipping_providers.shipping_provider_id = '.dbprefix.'_shipping_packs.shipping_provider_id' , array('shipping_provider_name'));

        $select->where(dbprefix.'_shipping_pack_kg.min_kg < ?', $weight);
        $select->where(dbprefix.'_shipping_pack_kg.max_kg >= ?', $weight);
        
        if($type == "region"){
            $select->where(dbprefix.'_shipping_areas.area_id = ?', $area_id);
        }else{
            $select->where(dbprefix.'_areas.area_code = ?', $area_id);
        }

        $select->where(dbprefix.'_shipping_packs.la = ?', $_SESSION['language']['def_lang_id']);
        $select->where(dbprefix.'_shipping_providers.la = ?', $_SESSION['language']['def_lang_id']);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
        
        $stmt = $db->query($select);
        $rate_results = $stmt->fetchAll();


        $select = $db->select();
        $select->from(dbprefix.'_shipping_pack_kg' , array('shipping_price' => 'CEIL(('.$weight.' - '.dbprefix.'_shipping_packs.shipping_pack_maxkg)/'.dbprefix.'_shipping_packs.shipping_pack_extraweight)*'.dbprefix.'_shipping_packs.shipping_pack_extraweight_price + '.dbprefix.'_shipping_packs.shipping_pack_maxweight_price'));
        
        $select->join(dbprefix.'_shipping_areas' , dbprefix.'_shipping_pack_kg.shipping_pack_id = '.dbprefix.'_shipping_areas.shipping_pack_id' , array());
        $select->join(dbprefix.'_areas' , dbprefix.'_areas.area_id = '.dbprefix.'_shipping_areas.area_id' , array());

        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_shipping_areas.shipping_pack_id',array('shipping_pack_description' , 'shipping_packet_id'));
        $select->join(dbprefix.'_shipping_providers' , dbprefix.'_shipping_providers.shipping_provider_id = '.dbprefix.'_shipping_packs.shipping_provider_id' , array('shipping_provider_name'));

        $select->where(dbprefix.'_shipping_packs.shipping_pack_maxkg < ?', $weight);
        
        
        if($type == "region"){
            $select->where(dbprefix.'_shipping_areas.area_id = ?', $area_id);
        }else{
            $select->where(dbprefix.'_areas.area_code = ?', $area_id);
        }

        $select->where(dbprefix.'_shipping_packs.la = ?', $_SESSION['language']['def_lang_id']);
        $select->where(dbprefix.'_shipping_providers.la = ?', $_SESSION['language']['def_lang_id']);
        $select->group(dbprefix.'_shipping_packs.shipping_packet_id');


        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
        
        $stmt = $db->query($select);
        $over_weight_results = $stmt->fetchAll();
        
        $results = array_merge($rate_results , $over_weight_results);

        $fixed_price_array = array();
        foreach ($results as $key => $value) {
            $fixed_price_array[$key]['shipping_pack_description'] = $value['shipping_pack_description'];
            $fixed_price_array[$key]['shipping_packet_id']        = $value['shipping_packet_id'];
            $fixed_price_array[$key]['shipping_price']            = Globals::getthePrice($value['shipping_price']);
            $fixed_price_array[$key]['shipping_provider_name'] = $value['shipping_provider_name'];           
        }

        return $fixed_price_array;           
    } 

    public function getCourierPackagesByPackageID($area_id , $type , $package_id){
        $sess_id = session_id();
        $weight = $this->getCartWeight($sess_id);
        
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_shipping_pack_kg' , array('shipping_price'));
        
        $select->join(dbprefix.'_shipping_areas' , dbprefix.'_shipping_pack_kg.shipping_pack_id = '.dbprefix.'_shipping_areas.shipping_pack_id' , array());
        $select->join(dbprefix.'_areas' , dbprefix.'_areas.area_id = '.dbprefix.'_shipping_areas.area_id' , array());

        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_shipping_areas.shipping_pack_id',array('shipping_pack_description' , 'shipping_packet_id' , 'shipping_pack_cod_val'));
        $select->join(dbprefix.'_shipping_providers' , dbprefix.'_shipping_providers.shipping_provider_id = '.dbprefix.'_shipping_packs.shipping_provider_id' , array('shipping_provider_name'));

        $select->where(dbprefix.'_shipping_pack_kg.min_kg < ?', $weight);
        $select->where(dbprefix.'_shipping_pack_kg.max_kg >= ?', $weight);
        
        if($type == "region"){
            $select->where(dbprefix.'_shipping_areas.area_id = ?', $area_id);
        }else{
            $select->where(dbprefix.'_areas.area_code = ?', $area_id);
        }

        $select->where(dbprefix.'_shipping_packs.la = ?', $_SESSION['language']['def_lang_id']);
        $select->where(dbprefix.'_shipping_providers.la = ?', $_SESSION['language']['def_lang_id']);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        //echo $select->__toString();
        
        $stmt = $db->query($select);
        $rate_results = $stmt->fetchAll();


        $select = $db->select();
        $select->from(dbprefix.'_shipping_pack_kg' , array('shipping_price' => 'CEIL(('.$weight.' - '.dbprefix.'_shipping_packs.shipping_pack_maxkg)/'.dbprefix.'_shipping_packs.shipping_pack_extraweight)*'.dbprefix.'_shipping_packs.shipping_pack_extraweight_price + '.dbprefix.'_shipping_packs.shipping_pack_maxweight_price'));
        
        $select->join(dbprefix.'_shipping_areas' , dbprefix.'_shipping_pack_kg.shipping_pack_id = '.dbprefix.'_shipping_areas.shipping_pack_id' , array());
        $select->join(dbprefix.'_areas' , dbprefix.'_areas.area_id = '.dbprefix.'_shipping_areas.area_id' , array());

        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_shipping_areas.shipping_pack_id',array('shipping_pack_description' , 'shipping_packet_id', 'shipping_pack_cod_val'));
        $select->join(dbprefix.'_shipping_providers' , dbprefix.'_shipping_providers.shipping_provider_id = '.dbprefix.'_shipping_packs.shipping_provider_id' , array('shipping_provider_name'));

        $select->where(dbprefix.'_shipping_packs.shipping_pack_maxkg < ?', $weight);
        
        
        if($type == "region"){
            $select->where(dbprefix.'_shipping_areas.area_id = ?', $area_id);
        }else{
            $select->where(dbprefix.'_areas.area_code = ?', $area_id);
        }

        $select->where(dbprefix.'_shipping_packs.la = ?', $_SESSION['language']['def_lang_id']);
        $select->where(dbprefix.'_shipping_providers.la = ?', $_SESSION['language']['def_lang_id']);
        $select->group(dbprefix.'_shipping_packs.shipping_packet_id');

        //echo $select->__toString();
        
        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
        
        $stmt = $db->query($select);
        $over_weight_results = $stmt->fetchAll();
        
        $results = array_merge($rate_results , $over_weight_results);
        
        $fixed_price_array = array();
        foreach ($results as $key => $value) {
            if($package_id == $value['shipping_packet_id']){
                $fixed_price_array[$key]['shipping_pack_description']         = $value['shipping_pack_description'];
                $fixed_price_array[$key]['shipping_packet_id']                = $value['shipping_packet_id'];
                $fixed_price_array[$key]['shipping_price']                    = Globals::getthePrice($value['shipping_price']);
                $fixed_price_array[$key]['shipping_price_no_currency']        = $value['shipping_price'];
                $fixed_price_array[$key]['shipping_pack_cod_val']             = Globals::getthePrice($value['shipping_pack_cod_val']);
                $fixed_price_array[$key]['shipping_pack_cod_val_no_currency'] = $value['shipping_pack_cod_val'];
                $fixed_price_array[$key]['shipping_provider_name']            = $value['shipping_provider_name'];
                $return_key = $key; 
            }          
        }

        return $fixed_price_array[$return_key];           
    }    

}    