<?php
class queue {

    public $queue = null;

    public function __construct($queue_name){
        $db = Zend_Db_Table::getDefaultAdapter();
        $this->queue = new Zend_Queue('Db', array(
                                      'dbAdapter' => $db,
                                      'options' => array(Zend_Db_Select::FOR_UPDATE => true),
                                      'name' => $queue_name
                                     )
         );
        
    }


}
?>
