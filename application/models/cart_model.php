<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
require_once "../application/models/stock_control_model.php";
class cart
{

	public $total;
	public $error;
	public $msg;

	public function add_to_cart($product_id , $quantity=1 , $attributes=array() , $acc_prd_ids=array()){
		
		$ses_id = session_id();
		
		if(!empty($attributes)){
			asort($attributes);
			$separated_attributes = implode("_", $attributes);
		}else{
			$separated_attributes = "";
		}

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$data = array();
		$data['session_id'] = $ses_id;
		$data['product_id'] = $product_id;
		$data['quantity'] = $quantity;
		
		if(!empty($separated_attributes)){
			$data['product_attributes'] = $separated_attributes;
		}	
		
		$stock = $this->_checkForStock($product_id , $quantity , $separated_attributes , $acc_prd_ids);

		if($stock){
			$db->insert(dbprefix.'_cart', $data);
			$cart_lastinserted_id = $db->lastInsertId();

			if(!empty($acc_prd_ids)){
				foreach ($acc_prd_ids as $key => $acc_prd_id) {
					if($acc_prd_id != 0){
						$acc_data['session_id'] = $ses_id;
						$acc_data['parent_product_id'] = $product_id;
						$acc_data['parent_cart_id'] = $cart_lastinserted_id;
						$acc_data['product_id'] = $acc_prd_id;
						$acc_data['quantity'] = $quantity;
						$db->insert(dbprefix.'_cart', $acc_data);
					}
				}
			}
		}else{
			$this->error = true;
			$this->msg = Globals::trl('The product is temporarily unavailable');
		}	

	}

	protected function _checkForStock($product_id , $quantity , $separated_attributes , $acc_prd_ids){
		$stock_obj = new stock_control();
		$stock_obj->setProductSelections($product_id , $quantity , $separated_attributes , $acc_prd_ids);
		$stock_obj->checkQuantity();
		$this->stock_remaining = $stock_obj->stock_remaining;

		return $stock_obj->is_available;
	}

	public function get_cart_contents(){
        
		$ses_id = session_id();
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products' , array('product_code' , 'end_price' , 'start_price' ,'weight'));
		
		$select->join(dbprefix.'_products_multilang' , dbprefix.'_products.product_id = '.dbprefix.'_products_multilang.product_id' , array('name' , 'prd_description' ,'main_photo'));
		$select->join(dbprefix.'_cart' , dbprefix.'_cart.product_id = '.dbprefix.'_products.product_id',array('session_id' , 'quantity' , 'product_attributes' ,'cart_id' , 'product_id'));
		$select->joinLeft(dbprefix.'_stock_control' , dbprefix.'_stock_control.product_id = '.dbprefix.'_cart.product_id AND '.dbprefix.'_cart.product_attributes = '.dbprefix.'_stock_control.code' , array('code','extra_cost'));

		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_cart.session_id = ?', $ses_id);
		$select->where(dbprefix.'_cart.parent_product_id = 0');


		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$cartrow = array();
		foreach ($results as $key => $cart_row){
			$cartrow[$key]['product'] = $cart_row;

			$attributes = explode("_", $cart_row['product_attributes']);

			foreach ($attributes as $attr){
				$cartrow[$key]['attributes'][] = $this->_getAttributes($attr);
			}

			$acc_product = $this->_getAccProducts($cart_row['cart_id']);
			foreach ($acc_product as $prdkey => $prd) {
				$cartrow[$key]['acc_products'][$prdkey] = $prd;
			}

			$price_with_attributes = ($cartrow[$key]['product']['end_price'] + $cartrow[$key]['product']['extra_cost']) * $cartrow[$key]['product']['quantity'];
			$acc_price = 0;
			if(!empty($cartrow[$key]['acc_products'])){
				foreach ($cartrow[$key]['acc_products'] as $acc_key => $acc_prd_value) {
					$acc_price = $acc_price + ($acc_prd_value['end_price'] * $acc_prd_value['quantity']);
				}
			}else{
				$acc_price = 0;
			}

			$cartrow[$key]['prices']['totalprice'] = $price_with_attributes + $acc_price;
			$cartrow[$key]['prices']['product_price'] = $cartrow[$key]['product']['end_price'] * $cartrow[$key]['product']['quantity'];
			$cartrow[$key]['prices']['acc_price'] = $acc_price;
			$cartrow[$key]['prices']['attr_price'] = $cartrow[$key]['product']['extra_cost'] * $cartrow[$key]['product']['quantity'];

			$this->total = $this->total + $cartrow[$key]['prices']['totalprice'];
		}
		
		return $cartrow;		
	}

	protected function _getAttributes($attr_id){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_attributes' , array('attr_id' , 'attr_value' ,'la'));
		$select->where(dbprefix.'_attributes.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_attributes.attr_id = ?', $attr_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0]['attr_value'];
	}

	protected function _getAccProducts($cart_id){
        $ses_id = session_id();
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products' , array('product_code' , 'end_price' , 'start_price' ,'weight' , 'product_id'));
		
		$select->join(dbprefix.'_products_multilang' , dbprefix.'_products.product_id = '.dbprefix.'_products_multilang.product_id' , array('name' , 'prd_description' ,'main_photo'));
		$select->join(dbprefix.'_cart' , dbprefix.'_cart.product_id = '.dbprefix.'_products.product_id',array('session_id' , 'quantity' , 'cart_id'));
		//$select->joinLeft(dbprefix.'_stock_control' , dbprefix.'_stock_control.product_id = '.dbprefix.'_cart.product_id AND '.dbprefix.'_cart.product_attributes = '.dbprefix.'_stock_control.code' , array('code','extra_cost'));

		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_cart.session_id = ?', $ses_id);
		$select->where(dbprefix.'_cart.parent_cart_id = ?', $cart_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;
	}

	public function update_quantity($cart_id , $quantity){
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_cart' , array('cart_id' , 'product_id' , 'parent_product_id' ,'parent_cart_id' , 'product_attributes' , 'quantity'));
		$select->join(dbprefix.'_products' , dbprefix.'_cart.product_id = '.dbprefix.'_products.product_id',array('product_code' , 'accompaying'));
		$select->where(dbprefix.'_cart.cart_id = ?', $cart_id);
		$select->orWhere(dbprefix.'_cart.parent_cart_id = ?', $cart_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		foreach ($results as $key => $prd) {
			if($prd['accompaying'] == 0){
				$stc_product_id = $prd['product_id'];
				$stc_quantity = $quantity;
				$stc_separated_attributes = $prd['product_attributes'];

			}else{
				$stc_accompaying_ids[] = $prd['product_id'];
			}
		}

		$stock = $this->_checkForStock($stc_product_id , $stc_quantity , $stc_separated_attributes , $stc_accompaying_ids);
		if($stock){
			$data = array('quantity' => $quantity);
			$result = $db->update(dbprefix.'_cart', $data , 'cart_id = '.$cart_id.' OR parent_cart_id = '.$cart_id);
			return $result;
		}else{
			$this->error = true;
			$this->msg = Globals::trl('The Quantity of this product is not available');
		}				
	}

	public function delete_from_cart($cart_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$result = $db->delete(dbprefix.'_cart', "cart_id = ".$cart_id.' OR parent_cart_id = '.$cart_id);
		return $result;
	}

	public function empty_cart(){
		$sess_id = session_id();
		$db = Zend_Db_Table::getDefaultAdapter();
		$result = $db->delete(dbprefix.'_cart', "session_id = '".$sess_id."'");
		return $result;
	}

	public function register_user($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->insert(dbprefix.'_internet_users', $data);
	}

	public function update_user($data){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update(dbprefix.'_internet_users', $data , 'user_id='.$_SESSION['visitor']['user_id']);
	}

	public function add_shipping_details($data , $sess_id){
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_cart_shipping' , array('user_id'));
		$select->where(dbprefix.'_cart_shipping.session_id = ?', $sess_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		if(empty($results)){
			$db->insert(dbprefix.'_cart_shipping', $data);
		}else{
			unset($data['session_id']);
			$db->update(dbprefix.'_cart_shipping', $data , 'session_id = "'.$sess_id.'"');
		}	
	}

	public function add_invoice_details($data , $sess_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$db->update(dbprefix.'_cart_shipping', $data , 'session_id = "'.$sess_id.'"');	
	}	

	public function getPaymentsTypes($shipping_pack_id){
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_shipping_payment_rel' , array());
		
		$select->join(dbprefix.'_pay_methods' , dbprefix.'_shipping_payment_rel.pay_method_id = '.dbprefix.'_pay_methods.pay_method_id' , array('pay_method_id' , 'pay_name' ,'pay_description' , 'pay_notification' , 'merchant_plugin'));
		$select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_payment_rel.shipping_packet_id = '.dbprefix.'_shipping_packs.shipping_packet_id',array('shipping_pack_name' , 'shipping_pack_description'));
		$select->join(dbprefix.'_shipping_providers' , dbprefix.'_shipping_providers.shipping_provider_id = '.dbprefix.'_shipping_packs.shipping_provider_id' , array('shipping_provider_name','la'));

		$select->where(dbprefix.'_pay_methods.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_shipping_packs.shipping_packet_id = ?', $shipping_pack_id);
		$select->group(dbprefix.'_pay_methods.pay_method_id');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;		
	}

	public function getPaymentMethod($pay_method_id){
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_pay_methods' , array('pay_method_id' , 'la' , 'merchant_plugin' ,'pay_name' , 'cod_extracost_active'));
		$select->where(dbprefix.'_pay_methods.pay_method_id = ?', $pay_method_id);
		$select->where(dbprefix.'_pay_methods.la = ?', $_SESSION['language']['def_lang_id']);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0];
	}

	public function getShippingInfo(){
		$sess_id = session_id();
		$db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_cart_shipping' , array('session_id','user_id','gift','gift_wrapping_cost','firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','region_id'));

		//$select->join(dbprefix.'_shipping_packs' , dbprefix.'_cart_shipping.courier_id = '.dbprefix.'_shipping_packs.shipping_packet_id' , array('shipping_packet_id','shipping_pack_name' , 'shipping_pack_cod_val'));
		//$select->join(dbprefix.'_areas' , dbprefix.'_cart_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));

		$select->where(dbprefix.'_cart_shipping.session_id = ?', $sess_id);
		//$select->where(dbprefix.'_shipping_packs.la = ?', $_SESSION['language']['def_lang_id']);
		//$select->group(dbprefix.'_shipping_packs.shipping_packet_id');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
		//echo $select->__toString();
		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0];		
	}

}
