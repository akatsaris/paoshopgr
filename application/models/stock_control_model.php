<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class stock_control {

	public $product_id;
	public $quantity;
	public $separated_attributes;
	public $product_attributes;
	public $acc_prd_ids;
	public $acc_products;
	public $stock_remaining;

	public $is_available;

	public function setProductSelections($product_id , $quantity , $separated_attributes , $acc_prd_ids){
		$this->product_id = $product_id;
		$this->quantity = $quantity;
		$this->separated_attributes = $separated_attributes;
		$this->acc_prd_ids = $acc_prd_ids;

		if(!empty($separated_attributes)){
			$this->product_attributes = true;
		}else{
			$this->product_attributes = false;
		}

		if(!empty($acc_prd_ids)){
			$this->acc_products = true;
		}else{
			$this->acc_products = false;
		}
	}

	public function checkQuantity(){
		if($this->product_attributes){
			//check stock controll
			if($this->checkProductStockControl()){
				if($this->acc_products){
					if($this->checkAccProductsAvailability()){
						$this->is_available = true;
					}else{
						$this->is_available = false;
					}
				}else{
					$this->is_available = true;
				}
			}else{
				$this->is_available = false;
			}
		}else{
			//check product stock
			if($this->checkProductStock()){
				if($this->acc_products){
					if($this->checkAccProductsAvailability()){
						$this->is_available = true;
					}else{
						$this->is_available = false;
					}
				}else{
					$this->is_available = true;
				}
			}else{
				$this->is_available = false;
			}
		}
	}

	public function checkAccProductsAvailability(){
		foreach ($this->acc_prd_ids as $key => $acc_prd_id) {
			if($acc_prd_id != 0){
				if(!$this->checkAccProduct($acc_prd_id)){
					return false;
				}
			}
		}
		return true;
	}

	public function checkAccProduct($acc_prd_id){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products' , array('product_id' ,'quantity'));
		$select->where(dbprefix.'_products.product_id = (?)', $acc_prd_id);
		$select->where(dbprefix.'_products.accompaying = 1');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		if($results[0]['quantity'] >= $this->quantity){
			return true;
		}else{
			$this->stock_remaining = $results[0]['quantity'];
			return false;
		}
	}

	public function checkProductStockControl(){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_stock_control' , array('product_id' , 'code' , 'quantity'));
		$select->where(dbprefix.'_stock_control.product_id = ? ', $this->product_id);
		$select->where(dbprefix.'_stock_control.code = "'.$this->separated_attributes.'"');


		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		if($results[0]['quantity'] >= $this->quantity){
			return true;
		}else{
			$this->stock_remaining = $results[0]['quantity'];
			return false;
		}
	}

	public function checkProductStock(){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products' , array('product_id' ,'quantity'));
		$select->where(dbprefix.'_products.product_id = ? ', $this->product_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		if($results[0]['quantity'] >= $this->quantity){
			return true;
		}else{
			$this->stock_remaining = $results[0]['quantity'];
			return false;
		}
	}

	public function updateQuantity($product_id , $attributes , $quantity){
		$db = Zend_Db_Table::getDefaultAdapter();

		if(!empty($attributes)){
			$select = $db->select();
			$select->from(dbprefix.'_stock_control' , array('product_id' , 'code' , 'quantity'));
			$select->where(dbprefix.'_stock_control.product_id = ? ', $product_id);
			$select->where(dbprefix.'_stock_control.code = "'.$attributes.'"');


			//Query logger
			Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

			$stmt = $db->query($select);
			$results = $stmt->fetchAll();
			
			if(!empty($results)){
				$prd_quantity = $results[0]['quantity'] - $quantity;

				if($prd_quantity < 0){
					$prd_quantity = 0;
				}
			
				$data = array('quantity' => $prd_quantity);
				$db->update(dbprefix.'_stock_control', $data , 'product_id = '.$product_id.' AND code = '.$attributes);
			}
		}else{

			$select = $db->select();
			$select->from(dbprefix.'_products' , array('product_id' ,'quantity'));
			$select->where(dbprefix.'_products.product_id = ? ', $product_id);

			//Query logger
			Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

			$stmt = $db->query($select);
			$results = $stmt->fetchAll();
			
			if(!empty($results)){
				$prd_quantity = $results[0]['quantity'] - $quantity;

				if($prd_quantity < 0){
					$prd_quantity = 0;
				}

				$data = array('quantity' => $prd_quantity);
				$db->update(dbprefix.'_products', $data , 'product_id = '.$product_id);
			}
		}
	}

}
