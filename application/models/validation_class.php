<?php
class validation {

    public  $error_messages = array();
    public  $error;

    public function checkEmail($email , $checkdublicate){
        $emailvalidator = new Zend_Validate_EmailAddress();

        if (!$emailvalidator->isValid($email)) {
            $this->error = true;
            $this->error_messages[] = Globals::trl('Email format is not valid');
            return;
        }

        if($checkdublicate){
            $db = Zend_Db_Table::getDefaultAdapter();

            $select = $db->select();
            $select->from(dbprefix.'_internet_users' , array('rows_counter' => 'count(*)'));
            $select->where('username = ?', $email);

            $query = $select->query();
            $result = $query->fetch();
            
            if($result['rows_counter'] >= 1){
                $this->error = true;
                $this->error_messages[] = Globals::trl('Email already exists');
            }

        }

    }//checkEmail

    public function checksimilarEmail($stringname , $email1 , $email2){
        if($email1 != $email2){
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Email is not similar');
        }
    }//checksimilarEmail

    public function checksimilarPassword($stringname , $pass1 , $pass2){
        if($pass1 != $pass2){
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Password is not the same');
        }
    }//checksimilarEmail

    public function stringValidation($stringname , $string , $length){
        $string_validator = new Zend_Validate_StringLength($length);

        if (!$string_validator->isValid($string)) {
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Minimun chars length is')." (".$length.")";
        }
    }// stringValidation

    public function required($stringname , $string){
        if(empty($string)){
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Is required');
        }
    }//required

    public function booleanRequired($stringname , $string){
        if(($string == 0 || $string == 1) && $string != ""){
            //$this->error = false;
        }else{
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Is required');
        }
    }//required

    public function captchaValidation($code="notsamecode" , $captcha="withcaptcha"){
        if($captcha !== md5($code)){
            $this->error = true;
            $this->error_messages[] = Globals::trl('Wrong Captcha, please retry');
        }
    }

    public function checkValidDate($date){
        $date_validator = new Zend_Validate_Date('YYYY-MM-DD');
        if(!$date_validator->isValid($date)){
            $this->error = true;
            $this->error_messages[] = Globals::trl('Not valid date');
        }
    }

    public function checkCreditCardNumber($stringname , $card_number){
        $validator = new Zend_Validate_CreditCard();
        if (!$validator->isValid($card_number)) {
            $this->error = true;
            $this->error_messages[] = "(".$stringname.") ".Globals::trl('Is not valid');
        }       
    }
}//validation
