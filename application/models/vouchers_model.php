<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class voucher
{


	public function checkVoucherCode($v_code){

        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_vouchers' , array('v_code' , 'user_id' ,'price' , 'percent', 'expiration','used'));
		
		$select->where(dbprefix.'_vouchers.v_code = ?', $v_code);
		$select->where(dbprefix.'_vouchers.used = 0');
		$select->where(dbprefix.'_vouchers.expiration > NOW()');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results[0];		
	}

}	