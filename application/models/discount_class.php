<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class discounts
{
	public $priority;
	public $shipping;
	public $discount;
	public $discount_type;
	public $discount_str;
	public $amount;

	public function __construct(){
		$this->getAmountForDiscount();
		$this->getDiscountPriority();
		$this->getDiscountByPriority();
		$this->checkAmountForShippingDiscount();
	}

	public function getAmountForDiscount(){
		$sess_id = session_id();
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_cart' , array('AmountForDiscount'=> '('.dbprefix.'_products.end_price*'.dbprefix.'_cart.quantity)'));
		$select->join(dbprefix.'_products' , dbprefix.'_cart.product_id = '.dbprefix.'_products.product_id' , array());
		
		$select->where(dbprefix.'_products.accompaying = 0');
		$select->where(dbprefix.'_products.start_price <= 0');
		$select->where(dbprefix.'_cart.session_id = "'.$sess_id.'"');
		
		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$total_amount = 0;
		foreach ($results as $key => $amount) {
			$total_amount = $total_amount + $amount['AmountForDiscount'];
		}

		$this->amount = $total_amount;
		return number_format($total_amount, 2, '.', '');
	}

	public function getDiscountPriority(){
		if(isset($_SESSION['discounts']['loyalty'])){
			$this->priority = "loyalty";
		}

		if(isset($_SESSION['discounts']['ticket']) && $_SESSION['discounts']['ticket'] == true){
			$this->priority = "ticket";
		}

		if(isset($_SESSION['discounts']['credit']) && $_SESSION['discounts']['credit'] == true){
			$this->priority = "credit";
		}

		if(isset($_SESSION['discounts']['voucher'])){
		 	$this->priority = "voucher";
	    }	
	}

	public function checkAmountForShippingDiscount(){
		if($_SESSION['cart']['totalcost'] > 100 && $_SESSION['courier']['country_code'] == "GR"){
			$this->shipping = "free"; //change from free to charge
		}else{
			$this->shipping = "charge";
		}
	}

	public function getDiscountByPriority(){
		switch ($this->priority) {
		    case "voucher":
		        $this->getVoucherDiscount();
		        break;
		    case "loyalty":
		        $this->getLoyaltyDiscount();
		        break;
		    case "credit":
		        $this->getCreditDiscount();
		        break;
		    case "ticket":
		        $this->getTicketDiscount();
		        break;		        		        
		}
	}

	public function getVoucherDiscount(){
		if($_SESSION['discounts']['voucher']['price'] > 0){
			$discount = $_SESSION['discounts']['voucher']['price'];
			$type = "price";
		}else{
			$discount = $_SESSION['discounts']['voucher']['percent'];
			$type = "percent";
		}
		$this->discount_type = $type;
		$this->discount = $discount;
	}

	public function getCreditDiscount(){
		$this->discount_type = "percent";
		$this->discount = 0; //changed to 0 for now
		//$this->discount = 10;
	}

	public function getTicketDiscount(){
		$this->discount_type = "percent";
		$this->discount = 10;
	}

	public function getLoyaltyDiscount(){
		$this->discount_type = "percent";
		$this->discount = 10;
	}	

	public function getDiscount(){
		if($this->discount_type == "price"){
			$total = $this->discount;
		}else{
			$total = $this->amount * ($this->discount / 100);
		}
		$this->discount_str = Globals::getthePrice($total);
		
		if($total > $this->amount){
			$total = $this->amount;
		}
		return number_format($total, 2, '.', '');
	}

}	