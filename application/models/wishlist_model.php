<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class wishlist
{
	public function add_to_wishlist($product_id , $user_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$data = array();
		$data['user_id'] = $user_id;
		$data['product_id'] = $product_id;

		$db->insert(dbprefix.'_wishlist', $data);	

	}

	public function remove_from_wishlist($wl_id , $user_id){
		$db = Zend_Db_Table::getDefaultAdapter();
		$result = $db->delete(dbprefix.'_wishlist', "wl_id = ".$wl_id." AND user_id = ".$user_id);
		return $result;
	}

	public function getWishList($user_id){
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_wishlist' , array('user_id' , 'wl_id'));
		
		$select->join(dbprefix.'_products' , dbprefix.'_wishlist.product_id = '.dbprefix.'_products.product_id' , array('product_code' , 'active' ,'start_price' ,'end_price' , 'product_id'));
		$select->join(dbprefix.'_products_multilang' , dbprefix.'_products.product_id = '.dbprefix.'_products_multilang.product_id',array('name' , 'prd_description' , 'prd_content' , 'main_photo' , 'friendly_url'));
		
		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_wishlist.user_id = ?', $user_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;
	}

}
