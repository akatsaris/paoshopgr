<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class product
{
	public $data;
	public $attributes;
	public $gallery;
	public $acc_products;

	public function __construct($pageid){
		$this->getProduct($pageid);
		$this->getAttributes($pageid);
		$this->getAcc_products($pageid);
		$this->getGallery($pageid);
	}

	public function getProduct($pageid){

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products_multilang' , array('la' , 'name' ,'prd_description' , 'prd_content', 'main_photo','friendly_url' , 'keywords'));
		$select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' , 'registered_users_only' ,'product_code' ,'active','start_price','end_price' , 'template_view_id', 'product_view_template' , 'accompaying'));
		
		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_products.product_id = ?', $pageid);
		$select->where(dbprefix.'_products.active = 1');
		$select->where(dbprefix.'_products.accompaying = 0');

		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
		}else{
			$select->where(dbprefix.'_products.registered_users_only = 0');
		}

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}	

	public function getAttributes($pageid){

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products' , array('product_id'));
		$select->join(dbprefix.'_product_attributes' , dbprefix.'_products.product_id = '.dbprefix.'_product_attributes.product_id' , array('attr_id'));

		$select->join(dbprefix.'_attributes' , dbprefix.'_product_attributes.attr_id = '.dbprefix.'_attributes.attr_id' , array('attr_value' , 'attr_image' , 'attr_multilang_id'));
		
		//HACK
		$select->join(dbprefix.'_stock_control' , dbprefix.'_stock_control.code = '.dbprefix.'_attributes.attr_id AND '.dbprefix.'_stock_control.product_id = '.dbprefix.'_products.product_id' , array('code','quantity'));
		//INNER JOIN dpge_stock_control ON dpge_stock_control.`code` = dpge_attributes.attr_id AND dpge_stock_control.product_id = dpge_products.product_id
		$select->join(dbprefix.'_attributes_groups' , dbprefix.'_attributes.attr_group_id = '.dbprefix.'_attributes_groups.group_id' , array('group_name','group_id','la'));
		
		$select->where(dbprefix.'_attributes_groups.la = ?', $_SESSION['language']['def_lang_id']);
		$select->where(dbprefix.'_attributes.la = ?', $_SESSION['language']['def_lang_id']);

		$select->where(dbprefix.'_products.product_id = ?', $pageid);
		
		//HACK
		$select->where(dbprefix.'_stock_control.quantity > 0');
		//AND dpge_stock_control.quantity > 0
		$select->group(dbprefix.'_attributes.attr_id');
		$select->order(dbprefix.'_attributes_groups.group_id ASC');
		$select->order(dbprefix.'_attributes.attr_multilang_id ASC');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');
		//echo $select->__toString();
		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$group_name = $results[0]['group_name'];
		foreach ($results as $key => $attrs) {
			$groups[$attrs['group_id']][] = $attrs;
		}


		$this->attributes = $groups;		
	}

	public function getAcc_products($pageid){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();	
        
        $select = $db->select();
		$select->from(dbprefix.'_product_accompaying_relation' , array('tags' , 'title'));
		$select->where(dbprefix.'_product_accompaying_relation.product_id = ?', $pageid);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$results_array = array();
		if(!empty($results)){
			foreach ($results as $key => $acc_prd) {
				$results_array[] = array('title'    => $acc_prd['title'],
									   'products' => $this->getAccProductsByTags($acc_prd['tags'])
					);
			}
			$this->acc_products = $results_array;
		}	
	}

	protected function getAccProductsByTags($tags){
		$tags_array = explode(",", $tags);
		$tags_counter = count($tags_array);

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_products_multilang' , array('la' , 'name' , 'prd_description' , 'main_photo' , 'friendly_url'));
		$select->join(dbprefix.'_products' , dbprefix.'_products_multilang.product_id = '.dbprefix.'_products.product_id' , array('product_id' ,'product_code','registered_users_only','active','start_price','end_price' ,'quantity'));
		$select->join(dbprefix.'_product_tags' , dbprefix.'_products.product_id = '.dbprefix.'_product_tags.product_id' , array('tag_id'));
		
		$select->where(dbprefix.'_products.product_id in (select '.dbprefix.'_product_tags.product_id  From '.dbprefix.'_product_tags where '.dbprefix.'_product_tags.tag_id in ('.$tags.') group by '.dbprefix.'_product_tags.product_id HAVING  count('.dbprefix.'_product_tags.tag_id)='.$tags_counter.')');

		$select->where(dbprefix.'_products.active = 1');
		$select->where(dbprefix.'_products.quantity > 0');
		$select->where(dbprefix.'_products.accompaying = 1');
		$select->where(dbprefix.'_products_multilang.la = ?', $_SESSION['language']['def_lang_id']);

		if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
			$select->where(dbprefix.'_products.registered_users_only = 0 OR '.dbprefix.'_products.registered_users_only = 1');
		}else{
			$select->where(dbprefix.'_products.registered_users_only = 0');
		}

		$select->group(dbprefix.'_product_tags.product_id'); 

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();
		return $results;		       
	}

	public function getGallery($pageid){

		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_product_gallery_files' , array('photo_id' , 'photo_path' , 'order'));
		$select->join(dbprefix.'_gallery_types' , dbprefix.'_product_gallery_files.gallery_id = '.dbprefix.'_gallery_types.gallery_type_id' , array('gall_width','gall_height','how_many_items','gallery_template'));
		$select->join(dbprefix.'_products' , dbprefix.'_products.product_id = '.dbprefix.'_product_gallery_files.product_id' , array('product_id'));

		$select->where(dbprefix.'_products.product_id = ?', $pageid);
		$select->order(dbprefix.'_product_gallery_files.order ASC');

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->gallery = $results;		
	}	

}