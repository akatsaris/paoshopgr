<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class authorize {

    public  $status_message;
    public  $login_status;
    public  $error;

    public function __construct(){
        if(isset($_SESSION['visitor']['login_status']) && $_SESSION['visitor']['login_status'] == "online"){
            $this->login_status = "online";
        }else{
            $this->login_status = "offline";
        }
    }

    public function checkAccess($email_login , $password , $keepforever){

        if($email_login == "" || $password == ""){
                $this->status_message = Globals::trl('login credentials missing');
                $this->login_status = false;
                return;
        }

        //Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_internet_users', array('username' , 'firstname' , 'lastname' , 'role' , 'user_id' , 'salt'));
        $select->where('username = (?)', $email_login);
        
        $stmt = $db->query($select);
        $results = $stmt->fetchAll();

        $salt = $results[0]['salt'];
        $password = $this->_passEncrypt($password , $salt);

        //Configure auth adapter
        $authAdapter = new Zend_Auth_Adapter_DbTable($db, dbprefix.'_internet_users', 'username', 'password');
        $authAdapter->setIdentity($email_login)
                    ->setCredential($password);

        // Perform the authentication query, saving the result
        $result = $authAdapter->authenticate();
        $return_msg = $result->getmessages();

         if($return_msg[0] == "Authentication successful.") {
            // User Informations
            $user_info = $authAdapter->getResultRowObject();

            if($keepforever == '1'){
                 Zend_Session::rememberMe();
            }

            // Add values to session
            $userSession = new Zend_Session_Namespace("visitor");
            $userSession->login_status = "online";
            $userSession->firstname = $results[0]['firstname'];
            $userSession->lastname  = $results[0]['lastname'];
            $userSession->username  = $results[0]['username'];
            $userSession->role      = $results[0]['role'];
            $userSession->user_id   = $results[0]['user_id'];


            //Notice Message
            $this->status_message = Globals::trl('login success');

          }else{
          	$userSession = new Zend_Session_Namespace("visitor");
            $userSession->login_status = "offline";
            //Notice Message
            $this->status_message = Globals::trl('login failed');
            $this->error = true;
          }
        return;
    }

	public function logout(){
            //Unset all of the session variables.
            $_SESSION = array();

            // If it's desired to kill the session, also delete the session cookie.
            // Note: This will destroy the session, and not just the session data!
            if (isset($_COOKIE[session_name()])) {
                setcookie(session_name(), '', time()-42000, '/');
            }

            // Finally, destroy the session.
            session_destroy();
            return;
	}

    protected function _passEncrypt($password , $salt){
        $bytes = mb_convert_encoding($password, 'UTF-16LE');
        $salt_base64 = base64_decode($salt);
        return base64_encode(sha1($salt_base64 . $bytes, true));
    }
}
?>