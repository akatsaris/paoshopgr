<?php
class EmailSender {

    private $queue = null;
    
    public function setQueue($queue){
        $this->queue = $queue;
    }

    public function sendEmail($address){
        $this->queue->send(serialize(func_get_args()));
    }

    public function sendQueuedEmails($count){
        $messages = $this->queue->receive(intval($count));
        foreach($messages as $msg) {
            $args = unserialize($msg->body);
            switch ($args[0]['type']) {
                case 'confirmation':
                    $email_options = $this->confirmation_email($args[0]['la'] , $args[0]['la_region'] , $args[0]['order_id']);
                    $this->_senditNow($email_options['email'], Globals::trl_reg($args[0]['la_region'] , 'Order Notification') , $email_options['email_body']);
                    

                    $emailbody = $this->user_order_email($args[0]['la'] , $args[0]['la_region'] , $args[0]['order_id']);
                    $this->_senditNow('orders@paoshop.gr', 'PAOSHOP USER ORDER' , $emailbody);
                    break;
                case 'order_status':
                    $email_options = $this->order_status($args[0]['status_id'] , $args[0]['order_id']);
                    if(!empty($email_options)){
                       $this->_senditNow($email_options['email'], 'Order Information' , $email_options['email_body']);
                    }
                    break;
                case 'reset_password':
                    $email_body = $this->reset_password($args[0]['email'] , $args[0]['password'] , $args[0]['enc_password'] , $args[0]['salt'] , $args[0]['la_region']);
                    $this->_senditNow($args[0]['email'], Globals::trl_reg($args[0]['la_region'] , 'Password Reset') , $email_body);
                    break;
                case 'contact':
                    $email_body = $this->contact($args[0]['email'] , $args[0]['firstname'] , $args[0]['lastname'] , $args[0]['phone'] , $args[0]['comment']);
                    $this->_senditNow('info@paoshop.gr', 'Customer Contact' , $email_body);
                    break;         
            }
            $this->queue->deleteMessage($msg);
        }
    }

    public function contact($email , $firstname , $lastname, $phone , $comment){
        $db = Zend_Db_Table::getDefaultAdapter();

        $data['email']     = $email;
        $data['firstname'] = $firstname;
        $data['lastname']  = $lastname;
        $data['phone']     = $phone;
        $data['comment']   = $comment;

        $mail_body = $this->prepareTemplate('contact_form.phtml' , $data);
        return $mail_body;
    }

    public function reset_password($email , $password , $enc_password, $salt , $la){
        $db = Zend_Db_Table::getDefaultAdapter();

        $data['password'] = $enc_password;
        $data['salt'] = $salt;
        
        $db->update(dbprefix.'_internet_users', $data , 'username = "'.$email.'"');

        $email_data['password'] = $password;
        $email_data['la_region'] = $la;
        $mail_body = $this->prepareTemplate('reset_password.phtml' , $email_data);
        return $mail_body;
    }

    public function order_status($status_id , $order_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('comment' , 'order_la_region'));
        $select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','order_id','phone'));
        $select->join(dbprefix.'_order_status' , dbprefix.'_orders.order_status_id = '.dbprefix.'_order_status.ord_status_id' , array('send_mail','ord_email_template','ord_status',''));
        $select->where(dbprefix.'_orders.order_id = ?', $order_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        $lang_id = Globals::getLangIDbyShortcut($data[0]['order_la_region']);


         //ITEMS
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id'));
        
        $select->join(dbprefix.'_order_items' , dbprefix.'_orders.order_id = '.dbprefix.'_order_items.order_id' , array('price','quantity'));
        $select->join(dbprefix.'_products_multilang' , dbprefix.'_order_items.product_id = '.dbprefix.'_products_multilang.product_id' , array('name','prd_description'));
        
        $select->joinLeft(dbprefix.'_attributes' , dbprefix.'_order_items.product_attributes = '.dbprefix.'_attributes.attr_id' , array('attr_value'));
        $select->join(dbprefix.'_products' , dbprefix.'_order_items.product_id = '.dbprefix.'_products.product_id' , array('product_code','accompaying'));
        

        $select->where(dbprefix.'_products_multilang.la = ?', $lang_id);
        $select->where(dbprefix.'_attributes.la = '.$lang_id.' OR '.dbprefix.'_attributes.la IS NULL');
        $select->where(dbprefix.'_orders.order_id = ?', $order_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $items = $stmt->fetchAll();  
        
        $data[0]['items'] = $items;  
        $data[0]['la_region'] = $data[0]['order_la_region'];

        if($data[0]['send_mail'] == 1){
            $mail_body = $this->prepareTemplate($data[0]['ord_email_template'] , $data);
            
            $email_options['email_body'] = $mail_body;
            $email_options['email'] = $data[0]['email'];
            return $email_options;
        }else{
            return false;
        }
    }    

    public function user_order_email($la , $la_region , $order_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        //multilang product values
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id', 'order_discount' ,'order_date','session_id','user_id','order_cod','order_price','order_voucher_id','order_voucher_value','invoice','order_paid','comment','merchant','wrap_packaging_cost','order_html','order_status_id','order_shipping','order_shipping_id'));
        
        $select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','timestamp'));
        $select->joinLeft(dbprefix.'_areas' , dbprefix.'_order_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));
        $select->joinLeft(dbprefix.'_order_status' , dbprefix.'_order_status.ord_status_id = '.dbprefix.'_orders.order_status_id' , array('ord_status','ord_email_template','send_mail'));
        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_orders.order_shipping_id' , array('shipping_pack_name','shipping_pack_description'));
        $select->join(dbprefix.'_pay_methods' , dbprefix.'_orders.merchant = '.dbprefix.'_pay_methods.merchant_plugin' , array('pay_name','pay_description'));


        $select->where(dbprefix.'_orders.order_number_id = ?', $order_id);
        $select->where(dbprefix.'_shipping_packs.la = ?', $la);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        $data[0]['email_cost'] = ($data[0]['order_price'] + $data[0]['order_cod'] + $data[0]['wrap_packaging_cost'] + $data[0]['order_shipping']) - $data[0]['order_discount'];
        $data[0]['la_region']  = $la_region;

        //ITEMS
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id'));
        
        $select->join(dbprefix.'_order_items' , dbprefix.'_orders.order_id = '.dbprefix.'_order_items.order_id' , array('price','quantity'));
        $select->join(dbprefix.'_products_multilang' , dbprefix.'_order_items.product_id = '.dbprefix.'_products_multilang.product_id' , array('name','prd_description'));
        
        $select->joinLeft(dbprefix.'_attributes' , dbprefix.'_order_items.product_attributes = '.dbprefix.'_attributes.attr_id' , array('attr_value'));
        $select->join(dbprefix.'_products' , dbprefix.'_order_items.product_id = '.dbprefix.'_products.product_id' , array('product_code','accompaying'));
        

        $select->where(dbprefix.'_products_multilang.la = ?', $la);
        $select->where(dbprefix.'_attributes.la = '.$la.' OR '.dbprefix.'_attributes.la IS NULL');
        $select->where(dbprefix.'_orders.order_number_id = ?', $order_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $items = $stmt->fetchAll();        

        $data[0]['items'] = $items;
        $mail_body = $this->prepareTemplate('user_order.phtml' , $data);

        return $mail_body;        
    }

    public function confirmation_email($la , $la_region , $order_id){
        $db = Zend_Db_Table::getDefaultAdapter();

        //multilang product values
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id','order_discount','order_date','session_id','user_id','order_cod','order_price','order_voucher_id','order_voucher_value','invoice','order_paid','comment','merchant','wrap_packaging_cost','order_html','order_status_id','order_shipping','order_shipping_id'));
        
        $select->join(dbprefix.'_order_shipping' , dbprefix.'_orders.order_id = '.dbprefix.'_order_shipping.order_id' , array('firstname','lastname','email','address','address_more','city_name','country_code','postal','doy','vat','company_name','phone','invoice_address','invoice_company_name','invoice_profession','invoice_type','timestamp'));
        $select->joinLeft(dbprefix.'_areas' , dbprefix.'_order_shipping.region_id = '.dbprefix.'_areas.area_id' , array('area_name'));
        $select->joinLeft(dbprefix.'_order_status' , dbprefix.'_order_status.ord_status_id = '.dbprefix.'_orders.order_status_id' , array('ord_status','ord_email_template','send_mail'));
        $select->join(dbprefix.'_shipping_packs' , dbprefix.'_shipping_packs.shipping_packet_id = '.dbprefix.'_orders.order_shipping_id' , array('shipping_pack_name','shipping_pack_description'));
        $select->join(dbprefix.'_pay_methods' , dbprefix.'_orders.merchant = '.dbprefix.'_pay_methods.merchant_plugin' , array('pay_name','pay_description'));


        $select->where(dbprefix.'_orders.order_number_id = ?', $order_id);
        $select->where(dbprefix.'_shipping_packs.la = ?', $la);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $data = $stmt->fetchAll();

        $data[0]['email_cost'] = ($data[0]['order_price'] + $data[0]['order_cod'] + $data[0]['wrap_packaging_cost'] + $data[0]['order_shipping']) - $data[0]['order_discount'];
        $data[0]['la_region']  = $la_region;

        //ITEMS
        $select = $db->select();
        $select->from(dbprefix.'_orders' , array('order_id','order_number_id'));
        
        $select->join(dbprefix.'_order_items' , dbprefix.'_orders.order_id = '.dbprefix.'_order_items.order_id' , array('price','quantity'));
        $select->join(dbprefix.'_products_multilang' , dbprefix.'_order_items.product_id = '.dbprefix.'_products_multilang.product_id' , array('name','prd_description'));
        
        $select->joinLeft(dbprefix.'_attributes' , dbprefix.'_order_items.product_attributes = '.dbprefix.'_attributes.attr_id' , array('attr_value'));
        $select->join(dbprefix.'_products' , dbprefix.'_order_items.product_id = '.dbprefix.'_products.product_id' , array('product_code','accompaying'));
        

        $select->where(dbprefix.'_products_multilang.la = ?', $la);
        $select->where(dbprefix.'_attributes.la = '.$la.' OR '.dbprefix.'_attributes.la IS NULL');
        //$select->orWhere(dbprefix.'_attributes.la IS NULL');
        $select->where(dbprefix.'_orders.order_number_id = ?', $order_id);

        //Query logger
        Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

        $stmt = $db->query($select);
        $items = $stmt->fetchAll();        

        $data[0]['items'] = $items;
        $mail_body = $this->prepareTemplate('order_confirmation.phtml' , $data);

        $email_options['email_body'] = $mail_body;
        $email_options['email'] = $data[0]['email'];
        return $email_options;
    }


    private function prepareTemplate($template_name , $data){
         $view = new Zend_View;
         $view->setScriptPath('../application/views/scripts/email_templates/');
         $view->vars = $data;
         $content = $view->render($template_name);
         return $content;
    }    

    private function _senditNow($emailto , $subject , $mail_body){
        $mail = new Zend_Mail('utf-8');
        $mail->setBodyHtml($mail_body);
        $mail->addTo($emailto);
        $mail->addBcc('paoshop13@gmail.com');
        $mail->setSubject($subject);

        try {
          $mail->send();
          return true;
        } catch (Exception $e) {
          return false;
        }
    }
}
?>
