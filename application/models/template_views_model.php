<?php
/**
 * DGP eshop Platform
 * @copyright DPG
 * @link http://www.dpg.gr
 * @author Angelos Katsaris
 */
class template_views
{
	public $data;
	public $type;

	public function __construct($template_view_id , $type){
		$this->getData($template_view_id);
		$this->type = $type;
	}

	public function getData($template_view_id){
		//Get db connection
        $db = Zend_Db_Table::getDefaultAdapter();

		$select = $db->select();
		$select->from(dbprefix.'_templates' , array('template_id' , 'layout' => 'template_name' ,'template_file'));
		$select->join(dbprefix.'_template_view' , dbprefix.'_templates.template_id = '.dbprefix.'_template_view.template_id');
		$select->where(dbprefix.'_template_view.template_view_id = ?', $template_view_id);

		//Query logger
		Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

		$stmt = $db->query($select);
		$results = $stmt->fetchAll();

		$this->data = $results;
	}

	public function getLayout(){
		if(!empty($this->data)){
			return $this->data[0]['layout'];
		}else{
			return $this->type;
		}
	}	

	public function getTemplateViewId(){
		if(!empty($this->data)){
			return $this->data[0]['template_view_id'];
		}else{
			//Get db connection
	        $db = Zend_Db_Table::getDefaultAdapter();

			$select = $db->select();
			$select->from(dbprefix.'_templates' , array('template_id' , 'layout' => 'template_name' ,'template_file'));
			$select->join(dbprefix.'_template_view' , dbprefix.'_templates.template_id = '.dbprefix.'_template_view.template_id');
			$select->where(dbprefix.'_templates.template_name = ?', $this->type);
			$select->where(dbprefix.'_template_view.default = 1');

			//Query logger
			Globals::setPHPLogger('Query :: '.$select->__toString() , 'DEBUG');

			$stmt = $db->query($select);
			$results = $stmt->fetchAll();

			if(!empty($results)){
				return $results[0]['template_view_id'];
			}else{
				return 0;
			}	
		}
	}	

}