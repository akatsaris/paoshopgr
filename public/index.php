<?php

// Define path to application directory
defined('APPLICATION_PATH')
    || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

// Define application environment
defined('APPLICATION_ENV')
    || define('APPLICATION_ENV', (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'production'));

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

define('appdir' , "application");
define('dbprefix' , "dpge");
define('assetsdir' , "/public/assets/");



/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_PATH . '/configs/application.ini'
);

try{
	$application->bootstrap()->run();
} catch (Exception $exp) {
    $contentType = 'text/html';
    header("Content-Type: $contentType; charset=utf-8");
    echo 'an unexpected error occured.';
    echo '<h2>Unexpected Exception: ' . $exp->getMessage() . '</h2><br /><pre>';
    echo $exp->getTraceAsString();
}