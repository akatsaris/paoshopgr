var eshop = {
    login_status: false,
    prev_location: null,
    next_step: null,
    current_form: null,
    region_label: null,
    notice: null,
    la_id: 0,
    order_submition: 0,
    init: function(properties){
        eshop.login_status = properties.login_status;
        eshop.prev_location =properties.prev_location;    
        eshop.notice =properties.notice;
        eshop.la_id =properties.la_id;      
    },
    change_cat_properties: function(id , value , callback){
        eshop.ajax_req('id='+id+'&value='+value , 'ajax/changecatproperties' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(false);
            }    
        });
    },
    change_language: function(value , callback){
        eshop.ajax_req('value='+value , 'ajax/changelanguage' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(false);
            }    
        });
    },
    add_to_cart: function(product_id , callback){
        var attributes = $("#attributes").serialize();
        var acc_products = $("#acc_products").serialize();
        var quantity   = $("#product-qty").val();

        eshop.ajax_req('product_id='+product_id+'&quantity='+quantity+'&'+attributes+'&'+acc_products , 'ajax/addtocart' , function(res) {  
            if(res.success == 'success'){
                callback(res);
            }else{
                callback(res);
            }    
        });        
    },
    update_cart_quantity: function(cart_id , quantity , callback){
        if(quantity > 0){
            eshop.ajax_req('cart_id='+cart_id+'&quantity='+quantity , 'ajax/updatequantity' , function(res) {  
                callback(res);   
            });  
        }
    },
    delete_from_cart: function(cart_id , callback){
        eshop.ajax_req('cart_id='+cart_id , 'ajax/deletefromcart' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(false);
            }    
        }); 
    },
    user_register: function(formdata , callback){
        eshop.ajax_req(formdata , 'ajax/userregister' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    myaccount_update: function(formdata , callback){
        eshop.ajax_req(formdata , 'ajax/updateaccount' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    search_string: function(searchstring , callback){
        eshop.ajax_req('search='+searchstring , 'ajax/setsearchstring' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    reset_password: function(formdata, callback){
        eshop.ajax_req(formdata , 'ajax/resetpassword' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    contact: function(formdata, callback){
        eshop.ajax_req(formdata , 'ajax/contact' , function(res) {  
            callback(res);
        }); 
    },
    set_shipping_data: function(formdata , callback){
        eshop.ajax_req(formdata , 'ajax/shippingdata' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    set_invoice_data: function(formdata , callback){
        eshop.ajax_req(formdata , 'ajax/invoicedata' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    set_payment_data: function(formdata, callback){
        eshop.ajax_req(formdata , 'ajax/paymentdata' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    remove_order: function(order_num , callback){
        eshop.ajax_req('order_num='+order_num , 'ajax/removeorder' , function(res) {  
            if(res.success == 'success'){
                localStorage.last_order = "";
                localStorage.final_cost = "";
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    save_order: function(order_num , comment , order_html , final_cost , callback){
        if(eshop.order_submition == 0){
            eshop.order_submition = 1;
            eshop.ajax_req('order_num='+order_num+"&comment="+comment+'&order_html='+order_html , 'ajax/saveorder' , function(res) {  
                if(res.success == 'success'){
                    localStorage.last_order = order_num;
                    localStorage.final_cost = final_cost;
                    eshop.order_submition = 0;
                    callback(res);
                }else{
                    eshop.order_submition = 0;
                    callback(res);
                }    
            });
        }else{
            console.log('button inactive');
        } 
    },
    add_to_wishlist: function(product_id , callback){
        eshop.ajax_req('product_id='+product_id , 'ajax/addtowishlist' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        });        
    },
    remove_from_wishlist: function(wlid , callback){
        eshop.ajax_req('wl_id='+wlid , 'ajax/removefromwishlist' , function(res) {  
            if(res.success == 'success'){
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    get_last_order_number: function(){
        return localStorage.last_order;
    },
    get_shipping_packages: function(area_id, type ,callback){
        eshop.ajax_req('area_id='+area_id+'&type='+type , 'ajax/getshipping' , function(res) {  
            if(res.success == 'success'){
                //json each for couriers radio buttons
                //console.log(res);

                var shipping_str = '';

                if(res.responsedata){
                    $.each(res.responsedata, function(key, value){
                        shipping_str += '<fieldset>'+
                                        '<div class="w10">'+
                                            '<input type="radio" name="courier" id="courier" value="'+value.shipping_packet_id+'" /><label for="courier">'+value.shipping_provider_name+' ('+value.shipping_price+')</label>'+
                                        '</div>'+
                                        '</fieldset>';
                     });
                }    
                $("#shipp_packages").html(shipping_str);
                callback(true);
            }else{
                callback(res);
            }    
        }); 
    },
    get_selected_country: function(form){
        if(localStorage.getItem(form) == null){
            return false;
        }else{
            var localstor = localStorage.getItem(form);
            var FormValues = eval('(' + localstor + ')');
            return FormValues[6].value;
        }    
    },
    set_region_dropdown: function(country_path , callback){
        var country_value = $("#"+country_path+" option:selected").val();
        var regions_found = false;
        eshop.ajax_req('country_id='+country_value , 'ajax/getregions' , function(res) {  
            if(res.success == 'success'){
                 var options_str ="";
                 if(res.responsedata){
                     $.each(res.responsedata, function(key, value){
                         $.each(value, function(opt_value, opt_name){
                             options_str += "<option value='"+opt_value+"'>"+opt_name+"</option>";
                             regions_found = true;
                         });
                     });

                    var dropdown_str = "<label>"+eshop.region_label+"</label>"+
                                        "<select id='regions' name='regions' class='normal'>"+
                                         options_str+
                                        "</select>";

                    $("#region_select_box").html(dropdown_str);

                    if(localStorage.getItem(eshop.current_form) != null){
                        //select value from localstorage
                        var localstor = localStorage.getItem(eshop.current_form);
                        var FormValues = eval('(' + localstor + ')');
                        var selected_region_value = FormValues[7].value;
                        $("#regions").find('option[value="'+selected_region_value+'"]').attr("selected",true);
                    }
                }else{
                    $("#region_select_box").html("");
                }
                callback(regions_found);
            }else{
                callback(res);
            }    
        });

    },
    display_errors: function(errors){
        var error_str = "";
        $.each(errors, function(key, value){
            error_str += value+"<br/>";
        });

        $("#error-section-form .info").html(error_str);
        $("#error-section-form").show();
        $('html, body').animate({scrollTop:0}, 'slow');

    },
    display_error: function(error){
        $("#error-section-form .info").html(error);
        $("#error-section-form").show();
        $('html, body').animate({scrollTop:0}, 'slow');
    },
    redirect_to_next_step: function(){
        switch(localStorage.step)
        {
        case "checkout":
          window.location = "/cart-step-2";
          break;
        case "invoice":
            window.location = "/cart-step-3";
          break;
        case "payment":
            window.location = "/cart-step-4";
          break; 
        case "confirmation":
            window.location = "/cart-step-5";
          break;                    
        }
    },
    loadFormValues: function(form){
        eshop.current_form = form;
        if(localStorage.getItem(form) != null){
            values = localStorage[form];
            
            if(values.length > 0){
                var FormValues = eval('(' + values + ')');
                $.each(FormValues, function(key, value){
                    eshop.setValueToField(form , value.name , value.value);  
                });
            }
        }

    },
    StoreFormDataToLocaleStorage: function(form){
        var data = $("#"+form).serializeArray();
        if(form == "payment-form"){
            $.each(data, function(key, value){
                if(value.name == "ccnumber" || value.name == "cccvv"){
                    data[key]['value'] = "";
                }
            });
        }
        localStorage[form] = JSON.stringify(data);
    },
    setValueToField: function(path, field_name , field_value){
        if($("#"+path+" #"+field_name).length > 0){

            if($("#"+path+" #"+field_name).attr('type')){
                var elementType = $("#"+path+" #"+field_name).attr('type').toLowerCase();
            }else{
                var elementType = $("#"+path+" #"+field_name).get(0).tagName.toLowerCase();
            }

            switch(elementType)
            {
            case 'checkbox':
                if(field_value == "1"){
                    $("#"+path+" input#"+field_name).prop('checked', true); 
                }
              break;
            case 'radio':
                if(field_value){
                    $("#"+path+" input#"+field_name).filter('[value='+field_value+']').prop('checked', true); 
                }
              break;
            case 'text':
              $("#"+path+" #"+field_name).val(field_value);  
              break; 
            case 'select':
                $("#"+path+" #"+field_name).find('option[value="'+field_value+'"]').attr("selected",true);
              break;  
            case 'textarea':
                if(field_value){
                    $("#"+path+" #"+field_name).val(field_value); 
                }
              break;                                                      
            }        
        }
    },
    display_notifications: function(){
        if(eshop.notice != ""){
            $("#error-section-form .info").html(eshop.notice);
            $("#error-section-form").show();
        }
    },
    check_voucher: function(v_code , callback){
        eshop.ajax_req('v_code='+v_code , 'ajax/checkvoucher' , function(res) {  
            callback(res);
         }); 
    },
    ajax_req: function(data , action  , callback){
        $('.loading').show();
        $.ajax({
            type: "POST",
            url: '/'+action+'/',
            cache: false,
            data: data,  
            async: true, 
            success: function (result) {
                callback(result);
                $('.loading').hide();
            },
            error: function (request, status, error) {
                callback(error);
            }                                   
        });
    }    
};

$(document).ready(function () { 
        
    $('body').delegate('.add-to-wishlist', 'click', function() { 
        var product_id =  $(this).attr('value');
        eshop.add_to_wishlist(product_id, function(res) {
            console.log(res);
            if(res.success == "false"){
                eshop.display_error(res.errors);
            }else{
                window.location = '/wishlist';
            }
        });
    });   

    $('body').delegate('.wishlist-remove', 'click', function() { 
        var wlid =  $(this).attr('id');
        eshop.remove_from_wishlist(wlid, function(res) {
            window.location = '/wishlist';
        });
    });      
    
    $('body').delegate('.add-to-cart', 'click', function() {   
        var product_id = $("input#product_id").val();
        eshop.add_to_cart(product_id, function(res) {
            if(res.success == "false"){
                $("#stock_notice").html(res.msg).show();
            }else{
                window.location = "/cart";
            }
        });
    });

    $('body').delegate('.cart-action-update', 'click', function() {   
        var cart_id = $(this).attr('id');
        var quantity = $(this).prev("input").val();

        eshop.update_cart_quantity(cart_id , quantity , function(res) {
            if(res.success == "success"){
                window.location = "/cart";
            }else{
                $("#stock_notice_"+cart_id).html(res.msg).show();
            }
        });
    });   

    $('body').delegate('.cart-remove', 'click', function() {   
        var cart_id = $(this).attr('id');

        eshop.delete_from_cart(cart_id , function(res) {
            window.location = "/cart";
        });
    });  

    $('body').delegate('#reg_form', 'click', function(event) { 
        event.preventDefault(); 
        $("#error-section-form").hide();
        var formfields = $("#registration-form").serialize(); 

        eshop.user_register(formfields , function(res) {
            if(res.success == "false"){
                eshop.display_errors(res.errors);
            }else{
                eshop.redirect_to_next_step();
            }
        });        
    });

    $('body').delegate('#myaccount_btn', 'click', function(event) { 
        event.preventDefault(); 
        $("#error-section-form").hide();
        var formfields = $("#myaccount-form").serialize(); 

        eshop.myaccount_update(formfields , function(res) {
            if(res.success == "false"){
                eshop.display_errors(res.errors);
            }else{
                window.location = "/";
            }
        });        
    });


    $('body').delegate('#contact_form_btn', 'click', function(event) { 
        event.preventDefault(); 
        $("#error-section-form").hide();
        var formfields = $("#contact-form").serialize(); 

        eshop.contact(formfields , function(res) {
            console.log(res);
            if(res.success == "false"){
                eshop.display_errors(res.errors);
            }else{
                console.log(res.msg);
                eshop.notice = res.msg;
                eshop.display_notifications();
                //window.location = "/";
            }
        });        
    });

    $('body').delegate('#reset_form', 'click', function(event) { 
        event.preventDefault(); 
        $("#error-section-form").hide();
        var formfields = $("#resetpassword-form").serialize(); 

         eshop.reset_password(formfields , function(res) {
             if(res.success == "false"){
                 eshop.display_errors(res.errors);
             }else{
                 window.location = "/cart-step-1";
             }
         });        
    });           

    $('body').delegate('.checkout-btn', 'click', function() {   
        localStorage.step = "checkout";
        localStorage.stepback = "cart";

        if(eshop.login_status == "online"){
            if (window.location.protocol != "https:"){
                window.location.href = "/cart-step-2";
            }else{
                window.location = "/cart-step-2";
            }
        }else{
            window.location = "/cart-step-1";
        }
    });     

    $('body').delegate('.invoice-step', 'click', function(event) {   
        event.preventDefault(); 
        localStorage.step = "invoice";
        localStorage.stepback = "cart";

        eshop.StoreFormDataToLocaleStorage("checkout-form");
        //shippingdata
        var formfields = $("#checkout-form").serialize();
        eshop.set_shipping_data(formfields , function(res) {
            if(res.success == "false"){
                eshop.display_errors(res.errors);
            }else{
                eshop.redirect_to_next_step();
            }
        });

    }); 

    $('body').delegate('.back-to-cart', 'click', function(event) {   
        event.preventDefault();
        window.location = "/cart";
    });  


     $('body').delegate('.payment-step', 'click', function(event) {   
        event.preventDefault();
        localStorage.step = "payment";
        localStorage.stepback = "invoice";
        
        eshop.StoreFormDataToLocaleStorage("invoice-form");

        var formfields = $("#invoice-form").serialize();
        eshop.set_invoice_data(formfields , function(res) {
            if(res.success == "false"){
                eshop.display_errors(res.errors);
            }else{
                eshop.redirect_to_next_step();
            }
        });
    });    

     
    $('body').delegate('.back-to-payment', 'click', function(event) {   
        event.preventDefault();
        window.location = "/cart-step-4";
    }); 

    $('body').delegate('.back-to-shipping', 'click', function(event) {   
        event.preventDefault();
        window.location = "/cart-step-2";
    }); 

    $('body').delegate('.back-to-invoice', 'click', function(event) {   
        event.preventDefault();
        window.location = "/cart-step-3";
    }); 

    $('body').delegate('.add-to-cart-rel', 'click', function(event) {   
        event.preventDefault();
        var product_link = $(this).attr('rel');
        window.location = product_link;
    }); 

    $('body').delegate('.check-voucher', 'click', function(event) {   
        event.preventDefault();
        var v_code = $("#voucher-section #voucher_code").val();
        eshop.check_voucher(v_code , function(res){
            eshop.notice = res.message;
            $('#voucher-notice').html(res.message).show();
        });
    }); 

    $('body').delegate('.confirmation-step', 'click', function(event) {   
        event.preventDefault();
        localStorage.step = "confirmation";
        localStorage.stepback = "payment";
        
        eshop.StoreFormDataToLocaleStorage("payment-form");
        var payment_type_id = $("#payment-form [name='payment']:checked").attr('class');
        var formfields = $('#'+payment_type_id+' :input').serialize();
        
        eshop.set_payment_data(formfields , function(res) {
             if(res.success == "false"){
                 eshop.display_errors(res.errors);
             }else{
                 eshop.redirect_to_next_step();
             }
        });
    });      

    $('body').delegate('#countries', 'change', function(event) {   
        var value = $("#"+this.id).val();
        eshop.set_region_dropdown("countries" , function(result){ 
            if(result){
                //look shipping by Region
                var area_id = $('#regions :selected').val();
                eshop.get_shipping_packages(area_id, 'region' ,function(res){

                });
            }else{
                //look shipping by country
                var area_id = $('#countries :selected').val();
                eshop.get_shipping_packages(area_id, 'country',function(res){

                });
            }
        });
    });   


    $('body').delegate('#regions', 'change', function(event) {   
        var value = $("#"+this.id).val();
        //look shipping by country
        eshop.get_shipping_packages(value, 'region' ,function(res){

        });
    });   

    $('body').delegate('#search', 'keypress', function(event) {   
        if ( event.which == 13 ) {
             event.preventDefault();
             var value = $("#"+this.id).val();
             eshop.search_string(value , function(res){
                window.location = "/search";
             });
        }
    }); 

});
