/*
// "Keegot Template" for Joomla! 2.5.x - Version: 3.0
// Designed & Developed by Keegot.
// Copyright (c) 2010 - 2012 Keegot. All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.keegot.com
*/

/* http://coding.smashingmagazine.com/2011/10/11/essential-jquery-plugin-patterns/ */

;(function($, window, document, undefined){
	$.fn.panelTabs = function( options ) {  
	
		var settings = $.extend( {
			'blts'		: false,	 // bullets values = 1, 0
			'thumbs'	: false, // thumbs values = 1, 0	 
			'interval'	: 6000, // Interval = 1, 0
			'auto'		: true // Auto tabs = 1, 0
		}, options);
		
		
		this.each(function() {
			
			var timer = null, timing = null;
			var messured = false;
			var uniqueNum = Math.floor( Math.random()*99999 );
			var ID = 'panel' + uniqueNum;
			var s = settings;
			var panel = {};
			panel.wrapper = $(this);
			panel.wrapper.attr('id',ID);
			panel.wrapper.addClass('tabs-panel');
			panel.tabs = $(this).find('.tab');
			panel.nav = $(this).find('ul.nav');
			panel.tabsNum = panel.tabs.length;
			panel.height = 0;
			
			// set visibility
			panel.tabs.not(":eq(0)").addClass('inactive-tab');
			panel.wrapper.find(".tab:first-child").addClass('active-tab');
			
			// prepare markup
			
			// tabs
			panel.navigation = '<ul class="panel-navigation"></ul>';
			$(panel.navigation).insertBefore('.tabs-content');
			panel.navigation_items = panel.tabs.find('.tab-title');
			panel.navigation_items.css('display', 'none');
			
			for(var i=0; i<panel.navigation_items.length; i++ ){
				var activate = (i==0)?' class="active-nav"':'';
				panel.wrapper.find('.panel-navigation').append('<li'+activate+'><a href="#"><span>'+ $(panel.navigation_items[i]).text() +'</span></a></li>');
			}
			
			panel.wrapper.find('.panel-navigation a').click(function(e) {
				e.preventDefault();
				panel.wrapper.find('.panel-navigation li').removeClass('active-nav');
				$(this).parent().addClass('active-nav');
				var d = $(this).parent().index();
				move(d);
			});
			
			// bullets
			panel.blts = '<div class="panel-nav"><ul class="panel-blts"></ul></div>';
			
			if(s.blts===true){
				panel.wrapper.append(panel.blts);
				for(var i=1; i<=panel.tabsNum; i++ ){
					panel.wrapper.find('.panel-blts').append('<li><a href="#"><span>'+ i +'</span></a></li>');
				}
				panel.wrapper.find('.panel-blts li:first-child').addClass('active-blt');
				// bullets click
				panel.wrapper.find('.panel-blts a').click(function(e) {
					e.preventDefault();
					panel.wrapper.find('.panel-blts li').removeClass('active-blt');
					$(this).parent().addClass('active-blt');
					var d = $(this).parent().index();
					move(d);
				});

			}
			
			if(s.thumbs===true && panel.nav.length){
				
				panel.nav.find("li:first-child").addClass('active-thumb');
				
				// tumbs click
				panel.nav.find('a').click(function(e) {
					e.preventDefault();
					panel.nav.find("li").removeClass('active-thumb');
					$(this).parents('.nav-item').addClass('active-thumb');
					var d = $(this).parents('.nav-item').index();
					
					move(d);
					
				});
			}
			
			
			var move = function(dir){
				if (settings.auto === true) {
					clear();
				}
				/*
				if(messured==false){
					console.log(panel.height);
					var height = panel.wrapper.find(".tab:first-child").height();
					panel.wrapper.find(".tab").css('height', height);
					messured = true;
				}
				*/
				
				var activeTab = panel.wrapper.find('.active-tab');
				var activeIndex = panel.wrapper.find('.active-tab').index();
				var act = activeIndex;
				var total = panel.tabsNum-1;
				var target;
				var prevIndex = ((act - 1) >= 0 )?(act-1):(total);
				var nextIndex = ((act + 1) <= total )?(act + 1):0;
				
				panel.tabs.eq(act).removeClass('active-tab').addClass('inactive-tab');
				if(dir=='prev'){
					target = prevIndex;
				}else if(dir=='next'){
					target = nextIndex;
				}else{
					target = dir;
				}
				panel.tabs.eq(target).removeClass('inactive-tab').addClass('active-tab');
				if(s.nav===true && panel.nav.length){
					if(panel.nav.find('li.active-thumb').removeClass('active-thumb')){
						panel.nav.find('li:eq('+target+')').addClass('active-thumb');
					}
				}
				if (settings.auto === true) {
					start();
				}
			}
			
			var start = function(){
				if ( timer ) { clearInterval(timer); }
				var action = function(){
					move('next');
				}
				timer = setInterval( action, settings.interval );
			}
			
			var clear = function(){
				clearInterval(timer);
			}
			
			if (settings.auto === true) {
				start();
			}
			/*
			var clearHeight = function(){
				messured = false;
				panel.wrapper.find(".tab").removeAttr('style');
			}
			
			
			$(window).resize(function() {
			
				if ( timing ) { clearTimeout(timing); }
				timing = setTimeout( clearHeight, 100 );
				
			});
			*/
			
			// placeholder 
			panel.tabs.each(function(i){
				
			});
			
			
		});
	
	};

	$(document).ready(function() {
		$('.tabs-class').panelTabs({
			'blts'	: false,
			'thumbs': false,
			'auto' : false
		});
	});
	

	
	

})(jQuery, window, document);


