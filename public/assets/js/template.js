if ( typeof( jQuery ) != 'undefined' ) {
	(function($){
		$(document).ready(function() {
			if ( !! $('.sticky').offset()) {
				var timer = null;
				var lastWindowTop = $(window).scrollTop();
				var lastSidebarTop = $('.sticky').offset().top;
				var stickyHeight = parseFloat($('.sticky').height());
				var windowBottom = $('.bottom-zone').offset().top - 40;
				
				
				
				function setSidebar() {
					var windowTop = $(window).scrollTop(); 
					var windowHeight = $(window).height();
					var stickyTop = $('.sticky').offset().top;
					
					
					// console.log('monitor = ' + windowHeight + ' scroll-value=' + windowTop + ' sidebar-height=' + stickyHeight + ' bottom-end=' + windowBottom);
					
					
					if( stickyHeight <= windowHeight ){ // bigger window
						if( (windowTop + stickyHeight) <= windowBottom ){
							$('.sticky').css({
								position: 'fixed',
								top: 36
							});
						}else {
							$('.sticky').css({
								position: 'absolute',
								top: ((windowBottom - stickyHeight) + 36)
							});
						}
					}else{ // smaller window
						if(windowTop > lastWindowTop){
							
							if((windowTop + windowHeight) < stickyHeight ){
								$('.sticky').css({
									position: 'absolute',
									top: 36
								});
							}else {
								
								if( (windowTop + windowHeight) > (windowBottom)){
									// console.log(windowTop + ' + ' + stickyHeight + ' >' + windowBottom);
									$('.sticky').css({
										position: 'absolute',
										top:  windowBottom - (stickyHeight + 36)
									});
								}else{
									$('.sticky').css({
										position: 'fixed',
										top: ((windowHeight - stickyHeight)) +10
									});
								}
							}
							
						}else{
							if( (stickyTop - 36)  > windowTop){
							
							
								$('.sticky').css({
									position: 'fixed',
									top: 36
								});
							}
							
						}
							/*if( (windowTop + stickyHeight) > windowBottom  ){
								$('.sticky').css({
									position: 'absolute',
									top: 36
								});
							}else {
								$('.sticky').css({
									position: 'fixed',
									top: (windowBottom - stickyHeight) + 36
								});
							}*/
						
					}
					
					lastWindowTop = windowTop;
					lastSidebarTop = stickyTop;
				}
				
				$(window).scroll(function() {
					
					if ( timer ) { 
						clearTimeout(timer); 
					}
					timer = setTimeout( setSidebar, 5 ); 	
					
				});
			}
				
			
			// position images
			/*
			if ( !! $('.product-thumb').length) {
				
				$('.product-thumb img').each(function(){
					var img_padding = ($(this).parents('.product-thumb').height() - $(this).height()) /2;
					if(img_padding > 0){
						$(this).css('padding-top', img_padding);
					}
				});
			}*/
			
			// Pseudo Select
			$('select.pseudo').each( function(){
				
				// var selectAction = $(this).attr('data-action');
				// $(this).wrap('<span class="btn-group '+ selectAction +'" />');
				// var link = $(this).parents('form').attr('action');
				// var selectName = $(this).attr('name');
				// var ul = document.createElement('ul');
				// var btn = document.createElement('button');
				// $(this).after(ul);
				// $(ul).addClass('closed');
				// var button = '';
				// var options = [];
				// $(this).children('option').each(function(){
				// 	var item = [];
				// 	item.value = $(this).val();
				// 	item.text = $(this).text();
				// 	options.push(item);
				// 	$(ul).addClass('pseudo-select').append('<li><a class="form-sumbit" data-value="'+ item.value +'" href="'+ link +'">'+ item.text +'</a></li>');
				// 	if($(this).prop('selected') == true){
				// 		button = $(this).text();
				// 	}
				// });
				// $(this).hide();
				// $(btn).text(button).addClass('pseudo-btn ').prepend('<span class="arrow">&nbsp;</span>');
				// $(this).before(btn);
	
				
				// $(btn).bind('click', function(b){
				// 	b.preventDefault();
				// 	var el = $(this).parents('.btn-group').find('.pseudo-select');
				// 	if($(el).hasClass('open')){
				// 		el.removeClass('open');
				// 	}else{
				// 		$('.pseudo-select.open').removeClass('open');
				// 		el.addClass('open');
				// 	}
				// });
				
				// $(document).click(function(e){
				//     var target = $(".pseudo-btn").attr('class');
				//     var target_b = $(".pseudo-btn span").attr('class');
				//     var clicked = e.target.className;
				    
				//         if(target != clicked && target_b != clicked) {
				//             $('.pseudo-select.open').removeClass('open');
				//         }
				    
				    
				// });
				
				// $('.submit-on-change .form-sumbit').bind('click', function(e){
				// 	e.preventDefault();
					
				// 	value = $(this).attr('data-value');
				// 	$(this).parents('fieldset').find('option[value="'+ value +'"]').prop('selected', true);
				// 	//$(this).parents('form').submit();
				// });
				
				// $('.change-values .form-sumbit').bind('click', function(e){
				// 	e.preventDefault();
					
				// 	var value = $(this).attr('data-value');
				// 	var text = $(this).text();
				// 	$(this).parents('.btn-group').find('.pseudo-btn').text(text).prepend('<span class="arrow">&nbsp;</span>');
				// 	$(this).parents('fieldset').find('option').attr('selected', false);
				// 	$(this).parents('fieldset').find('option[value="'+ value +'"]').attr('selected', 'selected');
				// });
					
			});
			
			// product photo gallery
			if( !! $('.product-thumbs').length){
				$('.product-thumbs ul li a').bind('click', function(e){
					e.preventDefault();
					var index = $(this).parent().index();
					
						if($('.product-thumb a').removeClass('active')){
							$('.product-thumb').find('a').eq(index).addClass('active');
						}
					
					
				})
			}


			// product photo gallery
			// if( !! $('.product-thumbs').length){
			// 	$('.product-thumbs ul li a').bind('click', function(e){
			// 		e.preventDefault();
			// 		var index = $(this).parent().index();
			// 		var image_src = $(this).find('img').attr('data-big');
			// 		var image_zoom = $(this).find('img').attr('data-zoom-image');
								
			// 		var ez =  $('.unit #zooming img').data('elevateZoom');	  
			// 		ez.swaptheimage(image_src, image_zoom);
				
			// 	})
			// }

			// $(".unit #zooming img").elevateZoom(
			// 	/*{zoomType : "inner"}*/
			// );
			
			// attach fancybox
			$(".product-thumb a").fancybox({
				// add some staff here
				padding: 0,
				helpers : {
			        overlay : {
			            css : {
			                'background' : 'rgba(255, 255, 255, 0.50)'
			            }
			        }
			    }
			});


			
			// tab-class
			if( !! $('.tab-class').length){
				var tabs_container = $('.tab-class');
			}
			
		});
		
	
	})(window.jQuery);
}



/*
if(!Modernizr.input.placeholder) {
    $("input[placeholder]").each(function() {
        var placeholder = $(this).attr("placeholder");

        $(this).val(placeholder).focus(function() {
            if($(this).val() == placeholder) {
                $(this).val("")
            }
        }).blur(function() {
            if($(this).val() == "") {
                $(this).val(placeholder)
            }
        });
    });
}
*/	
