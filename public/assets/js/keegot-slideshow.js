/*
// "Keegot Template" for Joomla! 2.5.x - Version: 3.0
// Designed & Developed by Keegot.
// Copyright (c) 2010 - 2012 Keegot. All rights reserved.
// Released under the GNU/GPL license: http://www.gnu.org/copyleft/gpl.html
// More info at http://www.keegot.com
*/

/* http://coding.smashingmagazine.com/2011/10/11/essential-jquery-plugin-patterns/ */

;(function($, window, document, undefined){
	$.fn.screenSlides = function( options ) {  
	
		var settings = $.extend( {
		'arrows'			: true,	// arrows values = 1, 0
		'blts'				: true,	// bullets values = 1, 0
		'auto'				: true,	// Auto slider = 1, 0
		'interval'			: 6000,	// Interval = 1, 0
		}, options);
		
		this.each(function() {
			
			var timer = null;
			var s = settings;
			var screen = {};
			screen.wrapper = $(this);
			screen.slides = $(this).find('li');
			screen.slidesNum = screen.slides.length;
			screen.arrowLeft = '<a href="#" class="screen-navi screen-prev"><span>&laquo;</span></a>';
			screen.arrowRight = '<a href="#" class="screen-navi screen-next"><span>&raquo;</span></a>';
			
			// prepare markup
			screen.blts = '<div class="screen-nav"><ul class="screen-blts"></ul></div>';
			screen.arrows = screen.arrowLeft + screen.arrowRight;
			
			// set visibility
			screen.slides.not(":eq(0)").addClass('inactive');
			screen.wrapper.find("li:first-child").addClass('active');
			
			if(s.arrows === true){
				screen.wrapper.append(screen.arrows);
				
				// arrows click
				$(this).delegate('a.screen-navi','click', function(e) {
					e.preventDefault();
					var d = 'next';
					if($(this).hasClass('screen-prev')){
						d='prev';
					}
					move(d);
				});
			}
			
			if(s.blts===true){
				screen.wrapper.append(screen.blts);
				for(var i=1; i<=screen.slidesNum; i++ ){
					screen.wrapper.find('.screen-blts').append('<li><a href="#"><span>'+ i +'<span></a></li>');
				}
				
				// arrows click
				screen.wrapper.find('.screen-blts a').click(function(e) {
					e.preventDefault();
					var d = $(this).parent().index();
					move(d);
				});

			}
			
			
			var move = function(dir){
				clear();
				var activeSlide = $(screen.wrapper).find('li.active');
				var activeIndex = $(screen.wrapper).find('li.active').index();
				var act = activeIndex;
				var total = screen.slidesNum-1;
				var target;
				var prevIndex = ((act - 1) >= 0 )?(act-1):(total);
				var nextIndex = ((act + 1) <= total )?(act + 1):0;
				
				screen.slides.eq(act).removeClass('active').addClass('inactive');
				if(dir=='prev'){
					target = prevIndex;
				}else if(dir=='next'){
					target = nextIndex;
				}else{
					target = dir;
				}
				screen.slides.eq(target).removeClass('inactive').addClass('active');
				start();
			}
			
			var start = function(){
				if ( timer ) { clearInterval(timer); }
				var action = function(){
					move('next');
				}
				timer = setInterval( action, settings.interval );
			}
			
			var clear = function(){
				clearInterval(timer);
			}
			
			if (settings.auto === true) {
				start();
			}
			
			
			
			// placeholder 
			screen.slides.each(function(i){
				
			});
			
			
		});
	
	};

	$(document).ready(function() {
		$('.component-area .screen-slides').screenSlides({
			'blts': false
		});
		$('#promo .screen-slides').screenSlides({
			'arrows': false,
			'interval': 10000
		});
	});

})(window.jQuery);


