if ( typeof( jQuery ) != 'undefined' ) {
	(function($){
	
		$(window).load(function () {

            //calculate bottom limit
            var timer;
            var mainOffset = $('.main').offset();
            var mainHeight = $('.main').height();
            var mainBottom = mainOffset.top + mainHeight;
            var widjetHeight = $(".sticky").height();
            var limit = mainBottom - $(".sticky").height();

            //get the moving widjet offset
            var offset = $(".sticky").offset();
            var topPadding = 100; // set the top padding to the broeswe window
            
            function setSidebar() {
            	
            	if ($(window).scrollTop() > offset.top /*+ mainOffset.top - topPadding*/ && $(window).scrollTop() < mainOffset.top + mainHeight - widjetHeight - topPadding) {
                    $(".sticky").css({
                        marginTop: $(window).scrollTop() - offset.top + topPadding
                    });
                }
                else {
                    if ($(window).scrollTop() < mainOffset.top + mainHeight - widjetHeight - topPadding) {
                        $(".sticky").css({
                            marginTop: 0,
                            top:0
                        });
                    }
                    else {
                        $(".sticky").css({
                            top: mainHeight - widjetHeight - topPadding, position: 'fixed'
                        });
                    }
                };
            
            }
            //evemt handler
            $(window).scroll(function () {
            
            	if ( timer ) { 
					clearTimeout(timer); 
				}
				timer = setTimeout( setSidebar, 100 ); 
				
               
            });
            
        }); 			
			
		
			// position images
			/*
			if ( !! $('.product-thumb').length) {
				
				$('.product-thumb img').each(function(){
					var img_padding = ($(this).parents('.product-thumb').height() - $(this).height()) /2;
					if(img_padding > 0){
						$(this).css('padding-top', img_padding);
					}
				});
			}*/
	
	$(document).ready(function() {
			
			// Pseudo Select
			$('fieldset.pseudo select').each( function(){
				
				$(this).wrap('<span class="btn-group" />');
				var link = $(this).parents('form').attr('action');
				var selectName = $(this).attr('name');
				var ul = document.createElement('ul');
				var btn = document.createElement('button');
				$(this).after(ul);
				$(ul).addClass('closed');
				var button = '';
				var options = [];
				$(this).children('option').each(function(){
					var item = [];
					item.value = $(this).val();
					item.text = $(this).text();
					options.push(item);
					$(ul).addClass('pseudo-select').append('<li><a class="form-sumbit" data-value="'+ item.value +'" href="'+ link +'">'+ item.text +'</a></li>');
					if($(this).prop('selected') == true){
						button = $(this).text();
					}
				});
				$(this).hide();
				$(btn).text(button).addClass('pseudo-btn').prepend('<span class="arrow">&nbsp;</span>');
				$(this).before(btn);
	
				
				$(btn).bind('click', function(b){
					b.preventDefault();
					var el = $(this).parents('.pseudo').find('.pseudo-select');
					if($(el).hasClass('open')){
						el.removeClass('open');
					}else{
						$('.pseudo-select.open').removeClass('open');
						el.addClass('open');
					}
				});
				
				$(document).click(function(e){
				    var target = $(".pseudo-btn").attr('class');
				    var target_b = $(".pseudo-btn span").attr('class');
				    var clicked = e.target.className;
				    
				        if(target != clicked && target_b != clicked) {
				            $('.pseudo-select.open').removeClass('open');
				        }
				    
				    
				});
				
				$('.form-sumbit').bind('click', function(e){
					e.preventDefault();
					value = $(this).attr('data-value');
					$(this).parents('fieldset').find('option[value="'+ value +'"]').prop('selected', true);
					$(this).parents('form').submit();
				});
				
				
				
					
			});
	});
		
	
	})(window.jQuery);
}


			
